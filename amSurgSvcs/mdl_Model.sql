--alter procedure amSurgServ.mdl_Model as

declare @ApplicationStartDate date = datefromparts(iif(datepart(mm,getdate())<10,datepart(yyyy,getdate())-2,datepart(yyyy,getdate())-1),10,1);

declare @ApplicationEndDate date = getdate()-1;

-- KEY FIELDS
-- Log ID, Primary Surgeon, Primary Procedure, Room, Facility, Primary Payor, Primary Plan

-- SURGICAL RECORD
-- Case / Log + AN + Events + Durations + HAR; all 1-1 with log ID
--if object_id('amSurgSvcs.SurgicalEncounter', 'U') is not null drop table amSurgSvcs.SurgicalEncounter;

if object_id('amSurgServ.SurgicalEncounter') is not null drop table amSurgServ.SurgicalEncounter;
if object_id('amSurgServ.SurgicalEncounter') is not null drop table amSurgServ.SurgicalEncounter;

with TotalDelay as (
select 
	LogID
	,sum(DelayMinutes) as TotalMinutesDocumentedDelay
	,max(PACUDelayFlag) as PACUDelayFlag
from gt1.ORDelay
group by LogID
)
,TotalCharges as (
select 
	LogID
	,sum(ChargeAmount) as TotalSurgeryCharges
from gt1.HBTransaction
group by LogID
)
select
	se.SurgeryID
	,se.SurgeryDate
	,se.Source as EncounterSource

	--STAFF
	,se.ResponsibleAnesthesiologist 
	,se.PrimarySurgeon 

	--LOCATION
	,f.ORLocation as ORLocation --NEED DOWNSTREAM FIELD FOR ROOM IN SE
	,f.Facility --NEED DOWNSTREAM FIELD FOR ROOM IN SE
	,se.ORRoom as OperatingRoom --NEED DOWNSTREAM FIELD FOR ROOM IN SE

	--PROCEDURES
	,se.ASARating as ASARating
	,se.PrimaryProcedure as PrimaryProcedure
	,se.PrimaryORService as PrimaryORService
	,se.ProcedureLevel as PrimaryORProcedureLevel

	--Patient
	,pt.MRN as PatientMRN
	,pt.PatientID as PatientID
	,pt.Zipcode as PatientZipcode
	,pt.BirthDate as PatientBirthDate
	,se.PatientAge 
	,se.PatientAdultFlag
	,pt.Gender as PatientGender
	,pt.Status as PatientStatus
	,pt.Race as PatientRace

	--Admission
	,se.HospitalEncounterID as AdmissionCSN
	,adm.HospitalAccountID as HospitalAccountID
	,adm.AdmitDate as AdmissionDate
	,adm.DischargeDate
	,adm.LOS as AdmissionLOS
	,ha.DischargeStatus
	--,adm.PatientClass as AdmissionPatientClass
	,ha.AccountClass as AccountPatientClass
	,ha.PrimaryPayor as AccountPrimaryPayor
	,ha.PrimaryPlan as AccountPrimaryPlan
	,ha.FinancialClass as AccountFinancialClass
	,ha.TotalCharges as AccountTotalCharges
	,tc.TotalSurgeryCharges as SurgeryTotalCharges
	,ha.TotalPayments as AccountTotalPayments
	,ha.TotalAdjustments as AccountTotalAdjustments
	,ha.ExpectedPayment as AccountExpectedPayment
	,ha.CurrentBalance as AccountCurrentBalance
	,ha.ReferringProvider
	,ha.AttendingProvider

	--Scheduling
	,se.CaseClass as CaseClass
	,se.SchedPatientClass as ScheduledPatientClass
	,se.FirstScheduledDate
	,se.LastScheduledDate
	,se.ScheduledStartTime
	,se.ScheduledInRoomTime

	--Cancellations
	,se.VoidReason
	,se.CancelDate as CancellationDate 
	,se.CancelReason as CancellatonReason
	
	--AnesthesiaEvents
	,ane.AnesthesiaStartTime
	,ane.AnesthesiaStopTime as AnesthesiaEndTime
	,ane.AirwayPlacedTime
	,ane.AirwayRemovedTime

	--SurgeonEvents
	,ane.SurgeonCalledTime
	,ane.SurgeonArrivedTime
	,se.PrimarySurgeonStartTime
	,se.PrimarySurgeonEndTime
	,se.PrimarySurgeonPrimaryService

	--ProcedureEvents
	,ct.SetUpStartTime
	,ct.SetUpFinishTime as SetUpEndTime
	,ct.PreProcedureStartTime
	,ct.PreProcedureCompleteTime as PreProcedureEndTime
	,ct.ProcedureStartTime
	,ct.ProcedureFinishTime as ProcedureEndTime
	,ct.ProceduralCareCompleteTime as ProceduralCareEndTime

	--PatientMovementEvents
	,ct.PatientInFacilityTime as InFacilityTime
	,ct.PatientInPreopTime as InPreOpTime
	,ct.PreProcedureCompleteTime as OutPreOpTime --note: currently the same as pre proc complete event
	,ane.PatientReadyTime as PatientReadyTime 
	,ct.PatientInRoomTime as InRoomTime
	,ct.PatientOutRoomTime as OutRoomTime
	,ct.PhaseIStartTime as InPhaseITime
	,ct.PhaseIFinishTime as OutPhase1Time
	,ct.PhaseIIStartTime as InPhaseIITime
	,ct.PhaseIIFinishTime as OutPhaseIITime
	,datepart(hour,ct.PatientInRecoveryTime) as RecoveryHourID
	,ct.PatientInRecoveryTime as InRecoveryTime
	,ct.PatientOutRecoveryTime as OutRecoveryTime

	--Duration
	,se.PreOpDuration
	,se.ScheduledToSurgeryDuration
	,se.SetUpOffset as ScheduledSetupDuration 
	,se.CleanUpOffset as ScheduledCleanupDuration
	,se.SurgeonCalledToArrivalDuration as SurgeonCalledtoSurgeonArrivedDuration
	,se.IntraopWaitDuration
	,se.LateInciscionDuration 
	,iif(se.FirstCaseFlag=1 and se.DelayFlag=1, se.DelayedStartDuration,NULL) as CalculatedDelayDuration
	,se.PatientReadyToInRoomDuration as PatientReadyToPatientInRoomDuration
	,se.ORDuration
	,se.AnesthesiaDuration
	,se.SurgeonOutProcedureFinishDuration as PrimarySurgeonEndtoProcedureEndDuration 
	,se.PostOpFlowDuration
	,se.PatientOutRoomtoAnesthesiaStopDuration
	,se.RecoveryPhaseIDuration
	,se.RecoveryPhaseIIDuration
	,se.RecoveryDuration
	--Flags
	,se.DelayFlag
	,se.AvoidableCancellationFlag
	,se.CancelledFlag as CancellationFlag
	,se.SameDayCancelledFlag as CancellationSameDayFlag
	,se.AddOnCaseFlag as AddOnSurgeryFlag
	,se.ElectiveCaseFlag as ElectiveSurgeryFlag
	,se.FirstCaseFlag
	,se.IncludeCaseInTotalFlag as IncludeInTotalSurgeryFlag
	,se.LogPerformedFlag as PerformedSurgeryFlag
	,se.RecoveryCaseFlag as RecoverySurgeryFlag
	,se.AnesthesiaFlag as AnesthesiaSurgeryFlag 
	,td.PACUDelayFlag
	,td.TotalMinutesDocumentedDelay
into amSurgServ.SurgicalEncounter
from gt2.SurgicalEncounter se
left join gt1.HospitalEncounter adm on				--admission
	se.HospitalEncounterID=adm.EncounterID
left join gt1.HospitalAccount ha on					--account
	adm.HospitalAccountID=ha.HospitalAccountID		
left join gt1.Patient pt on							--patient
	ha.PatientID=pt.PatientID
left join gt1.AnesthesiaEvent ane on				--an events
	ane.SurgeryID=se.LogID
left join gt1.ORCaseTracking ct on					--or events
	ct.LogID=se.LogID
left join  gt1.ORFacility f on
	f.RoomID=se.ORRoomID
left join TotalDelay td on 
	td.LogID=se.LogID
left join TotalCharges tc on
	tc.LogID=se.LogID
where se.SurgeryDate >= @ApplicationStartDate and se.SurgeryDate<@ApplicationEndDate
;

-- BLOCK UTILIZATION
-- Base Utilization + Block Release
-- Log ID, Primary Surgeon, Primary Procedure, Room, Facility, Primary Payor, Primary Plan 
if object_id('amSurgServ.BlockUtilization') is not null drop table amSurgServ.BlockUtilization;
if object_id('amSurgServ.BlockUtilization') is not null drop table amSurgServ.BlockUtilization;

with TotalMinutesReleased as (
select 
	ReleasedFromBlockStartDateTime
	,ReleasedFromBlockID 
	,ReleasedFromBlockTypeID
	,sum(ReleasedMinutes) as TotalMinutesReleased
	,max(ReleasedAtDateTime) as LastDateReleased
	,min(ReleaseDaysInAdvance) as DaysInAdvanceLastReleased
from gt1.BlockRelease
group by ReleasedFromBlockStartDateTime,ReleasedFromBlockID ,ReleasedFromBlockTypeID
)
select 
	u.LogID as SurgeryID
	,u.SnapshotDate as SurgeryDate
	,u.RoomID as OperatingRoom
	,f.ORLocation
	,f.Facility
	,u.Service as PrimaryORService
	,u.TurnoverTimeFlag as BlockUtilizationTurnoverTimeFlag
	,u.CorrectTime 
	,u.OverbookTime
	,u.OutsideTime 
	,u.AvailableTime 
	,u.StartTime 
	,u.EndTime
	,u.BlockName as BlockName
	,u.BlockType as BlockType
	,u.[Group] as [Group]
	,totr.TotalMinutesReleased
	,totr.LastDateReleased
	,totr.DaysInAdvanceLastReleased
	,se.PrimarySurgeon
	,se.ResponsibleAnesthesiologist
	--PROCEDURES
	,se.ASARating as ASARating
	,se.PrimaryProcedure as PrimaryProcedure
	,se.ProcedureLevel as PrimaryORProcedureLevel
	--Log flags
	,se.DelayFlag
	,se.AvoidableCancellationFlag
	,se.CancelledFlag as CancellationFlag
	,se.SameDayCancelledFlag as CancellationSameDayFlag
	,se.AddOnCaseFlag as AddOnSurgeryFlag
	,se.ElectiveCaseFlag as ElectiveSurgeryFlag
	,se.FirstCaseFlag
	,se.IncludeCaseInTotalFlag as IncludeInTotalSurgeryFlag
	,se.LogPerformedFlag as PerformedSurgeryFlag
	,se.RecoveryCaseFlag as RecoverySurgeryFlag
into amSurgServ.BlockUtilization
from gt1.BlockUtilization u
left join gt1.ORFacility f on
	f.RoomID=u.RoomID
left join gt2.SurgicalEncounter se on
	se.LogID=u.LogID
left join TotalMinutesReleased totr on
	totr.ReleasedFromBlockStartDateTime=cast(u.SnapshotDate as datetime) + cast(u.StartTime as datetime) and
	totr.ReleasedFromBlockID=u.BlockID and
	totr.ReleasedFromBlockTypeID=u.BlockTypeID
;

-- ROOM UTILIZATION
if object_id('amSurgServ.RoomUtilization') is not null drop table amSurgServ.RoomUtilization;
if object_id('amSurgServ.RoomUtilization') is not null drop table amSurgServ.RoomUtilization;

select 
	ru.LogID as SurgeryID 
	,ru.SnapshotDate as SurgeryDate
	,ru.Room  as OperatingRoom
	,f.ORLocation
	,f.Facility
	,ru.AvailableTime
	,ru.CorrectTime
	,ru.OutsideTime
	,ru.StartTime
	,ru.EndTime
	,se.PrimarySurgeon
	,se.ResponsibleAnesthesiologist
	,se.PrimaryORService
	--Procdures
	,se.ASARating as ASARating
	,se.PrimaryProcedure as PrimaryProcedure
	,se.ProcedureLevel as PrimaryORProcedureLevel
	--Log flags
	,se.DelayFlag
	,se.AvoidableCancellationFlag
	,se.CancelledFlag as CancellationFlag
	,se.SameDayCancelledFlag as CancellationSameDayFlag
	,se.AddOnCaseFlag as AddOnSurgeryFlag
	,se.ElectiveCaseFlag as ElectiveSurgeryFlag
	,se.FirstCaseFlag
	,se.IncludeCaseInTotalFlag as IncludeInTotalSurgeryFlag
	,se.LogPerformedFlag as PerformedSurgeryFlag
	,se.RecoveryCaseFlag as RecoverySurgeryFlag
into amSurgServ.RoomUtilization
from gt1.RoomUtilization ru
left join gt1.ORFacility f on
	f.RoomID=ru.RoomID
left join gt2.SurgicalEncounter se on 
	se.LogID=ru.LogID
where se.SurgeryDate >= @ApplicationStartDate and se.SurgeryDate<@ApplicationEndDate
;

-- CHARGES
if object_id('amSurgServ.AllHBCharge') is not null drop table amSurgServ.AllHBCharge;
if object_id('amSurgServ.AllHBCharge') is not null drop table amSurgServ.AllHBCharge;

select 
	tx.LogID as SurgeryID
	,se.SurgeryDate
	,f.ORLocation
	,f.Facility
	,se.ORRoom as OperatingRoom
	,tx.ProcedureCode
	,tx.ProcedureDescription
	,tx.CPTCode
	,tx.RevenueCodeID as ChargeRevenueCodeID
	,tx.RevenueCode as ChargeRevenueCode
	,tx.ServiceDate as ChargeServiceDate
	,tx.PostDate as ChargePostDate
	,tx.PostLag as ChargePostLag
	,tx.CostCenterCode as ChargeCostCenterCode
	,tx.ChargeAmount
	,tx.Quantity as ChargeQuantity
	,tx.ChargePerUnit
	,tx.DepartmentID as ChargeDepartmentID
	,tx.Department as ChargeDepartment
	,se.PrimarySurgeon
	,se.ResponsibleAnesthesiologist
	,se.PrimaryORService
	--Procdures
	,se.ASARating as ASARating
	,se.PrimaryProcedure as PrimaryProcedure
	,se.ProcedureLevel as PrimaryORProcedureLevel
	--Log flags
	,se.DelayFlag
	,se.AvoidableCancellationFlag
	,se.CancelledFlag as CancellationFlag
	,se.SameDayCancelledFlag as CancellationSameDayFlag
	,se.AddOnCaseFlag as AddOnSurgeryFlag
	,se.ElectiveCaseFlag as ElectiveSurgeryFlag
	,se.FirstCaseFlag
	,se.IncludeCaseInTotalFlag as IncludeInTotalSurgeryFlag
	,se.LogPerformedFlag as PerformedSurgeryFlag
	,se.RecoveryCaseFlag as RecoverySurgeryFlag
	--Supply Details
	,si.ImplantType
	,si.ImplantFlag
	,si.Manufacturer
	,si.SupplyFlag
	,si.Supply
	,si.DerivedUnitCost as CostPerUnit
	--Patient
	,pt.MRN as PatientMRN
	,pt.PatientID as PatientID
	,pt.Zipcode as PatientZipcode
	,pt.BirthDate as PatientBirthDate
	,se.PatientAge 
	,se.PatientAdultFlag
	,pt.Gender as PatientGender
	,pt.Status as PatientStatus
	,pt.Race as PatientRace
	--Admission
	,se.HospitalEncounterID as AdmissionCSN
	,adm.HospitalAccountID as HospitalAccountID
	,ha.PrimaryPayor as AccountPrimaryPayor
	,ha.PrimaryPlan as AccountPrimaryPlan
	,ha.FinancialClass as AccountFinancialClass
	,ha.TotalPayments as AccountTotalPayments
	,ha.TotalAdjustments as AccountTotalAdjustments
	,ha.ExpectedPayment as AccountExpectedPayment
	,ha.CurrentBalance as AccountCurrentBalance
into amSurgServ.AllHBCharge
from gt1.HBTransaction tx
left join gt2.SurgicalEncounter se on 
	se.LogID=tx.LogID
left join gt1.ORLogSupplyAndImplant si on 
	si.LogID=tx.LogID and
	si.SupplyID=tx.SupplyID
left join gt1.HospitalEncounter adm on				--admission
	se.HospitalEncounterID=adm.EncounterID
left join gt1.HospitalAccount ha on					--account
	adm.HospitalAccountID=ha.HospitalAccountID		
left join gt1.ORFacility f on
	f.RoomID=se.ORRoomID
left join gt1.Patient pt on							--patient
	ha.PatientID=pt.PatientID
where se.SurgeryDate >= @ApplicationStartDate and se.SurgeryDate<@ApplicationEndDate
;

-- SUPPLIES
if object_id('amSurgServ.AllSupplyImplant') is not null drop table amSurgServ.AllSupplyImplant;
if object_id('amSurgServ.AllSupplyImplant') is not null drop table amSurgServ.AllSupplyImplant;

select
	si.LogID as SurgeryID
	,f.Facility
	,f.ORLocation
	,se.ORRoom as OperatingRoom
	,se.SurgeryDate
	,si.ChargeableFlag
	,si.ImplantType
	,si.ImplantFlag
	,si.Manufacturer
	,si.SupplyFlag
	,si.Supply
	,si.DerivedUnitCost as SupplyImplantCostPerUnit
	,si.DerivedUnitCostForAvg as SupplyImplantAvgCostPerUnit
	,si.ImplantUnitCost
	,si.SupplyUnitCost
	,si.SupplyOTUnitCost
	,si.PickListSupplyUnitCost
	,si.BalanceOTSupplyUnitCost
	,si.QtyUsed as QuantityUsed
	,si.QtyWasted as QuantityWasted
	,se.PrimarySurgeon
	,se.ResponsibleAnesthesiologist
	,se.PrimaryORService
	--Procdures
	,se.ASARating as ASARating
	,se.PrimaryProcedure as PrimaryProcedure
	,se.ProcedureLevel as PrimaryORProcedureLevel
	--Log flags
	,se.DelayFlag
	,se.AvoidableCancellationFlag
	,se.CancelledFlag as CancellationFlag
	,se.SameDayCancelledFlag as CancellationSameDayFlag
	,se.AddOnCaseFlag as AddOnSurgeryFlag
	,se.ElectiveCaseFlag as ElectiveSurgeryFlag
	,se.FirstCaseFlag
	,se.IncludeCaseInTotalFlag as IncludeInTotalSurgeryFlag
	,se.LogPerformedFlag as PerformedSurgeryFlag
	,se.RecoveryCaseFlag as RecoverySurgeryFlag
	--Admission
	,se.HospitalEncounterID as AdmissionCSN
	,adm.HospitalAccountID as HospitalAccountID
	,ha.PrimaryPayor as AccountPrimaryPayor
	,ha.PrimaryPlan as AccountPrimaryPlan
	,ha.FinancialClass as AccountFinancialClass
	,ha.TotalPayments as AccountTotalPayments
	,ha.TotalAdjustments as AccountTotalAdjustments
	,ha.ExpectedPayment as AccountExpectedPayment
	,ha.CurrentBalance as AccountCurrentBalance
into amSurgServ.AllSupplyImplant
from gt1.ORLogSupplyAndImplant si
left join gt2.SurgicalEncounter se on
	se.LogID=si.LogID
left join gt1.HospitalEncounter adm on				--admission
	se.HospitalEncounterID=adm.EncounterID
left join gt1.HospitalAccount ha on					--account
	adm.HospitalAccountID=ha.HospitalAccountID		
left join gt1.ORFacility f on
	f.RoomID=se.ORRoomID
where se.SurgeryDate >= @ApplicationStartDate and se.SurgeryDate<@ApplicationEndDate
;

-- PAYORS AND PLANS 
if object_id('amSurgServ.AllPayorPlan') is not null drop table amSurgServ.AllPayorPlan;
if object_id('amSurgServ.AllPayorPlan') is not null drop table amSurgServ.AllPayorPlan;

select 
	c.HospitalAccountID
	,c.CoverageID
	,c.LineNumber as CoverageLineNumber
	,c.PayorName as Payor
	,c.PlanName as [Plan]
	,se.LogID as SurgeryID
	,se.SurgeryDate
	,se.ORRoom as OperatingRoom
	,f.ORLocation
	,f.Facility
	,se.PrimaryORService
	,se.PrimarySurgeon
	,adm.AdmitDate as AdmissionDate
	,adm.DischargeDate as DischargeDate
	,adm.LOS as AdmissionLOS
	,ha.AttendingProvider
	,ha.ReferringProvider
	,ha.PrimaryPayor
	,ha.PrimaryPlan
	--Patient
	,pt.MRN as PatientMRN
	,pt.Zipcode as PatientZipcode
	,pt.BirthDate as PatientBirthDate
	,se.PatientAge 
	,se.PatientAdultFlag
	,pt.Gender as PatientGender
	,pt.Status as PatientStatus
	--Procdures
	,se.ASARating as ASARating
	,se.PrimaryProcedure as PrimaryProcedure
	,se.ProcedureLevel as PrimaryORProcedureLevel
	--Log flags
	,se.DelayFlag
	,se.AvoidableCancellationFlag
	,se.CancelledFlag as CancellationFlag
	,se.SameDayCancelledFlag as CancellationSameDayFlag
	,se.AddOnCaseFlag as AddOnSurgeryFlag
	,se.ElectiveCaseFlag as ElectiveSurgeryFlag
	,se.FirstCaseFlag
	,se.IncludeCaseInTotalFlag as IncludeInTotalSurgeryFlag
	,se.LogPerformedFlag as PerformedSurgeryFlag
	,se.RecoveryCaseFlag as RecoverySurgeryFlag
into amSurgServ.AllPayorPlan
from gt1.Coverage c
left join gt1.HospitalAccount ha on					--account
	c.HospitalAccountID=ha.HospitalAccountID	
left join gt1.HospitalEncounter adm on				--admission
	ha.HospitalAccountID=adm.HospitalAccountID
left join gt2.SurgicalEncounter se on
	se.HospitalEncounterID=adm.EncounterID
left join gt1.ORFacility f on
	f.RoomID=se.ORRoomID
left join gt1.Patient pt on							--patient
	ha.PatientID=pt.PatientID
where se.SurgeryDate >= @ApplicationStartDate and se.SurgeryDate<@ApplicationEndDate
;

--PROCEDURES
if object_id('amSurgServ.AllORProcedure') is not null drop table amSurgServ.AllORProcedure;
if object_id('amSurgServ.AllORProcedure') is not null drop table amSurgServ.AllORProcedure;

select
	se.LogID
	,orp.ProcedureName as [Procedure]
	,orp.ProcedureType
	,orp.Line as ProcedureLine
	,orp.Ordinal as ProcedureOrdinal
	,orp.PanelID as PanelID
	,orp.WoundClass 
	,se.SurgeryDate
	,se.ORRoom as OperatingRoom
	,f.ORLocation
	,f.Facility
	,se.PrimaryORService
	,se.PrimarySurgeon
	--Procdures
	,se.PrimaryProcedure as PrimaryProcedure
	,se.ProcedureLevel as PrimaryProcedureLevel
into amSurgServ.AllORProcedure
from gt1.ORAllProcedure orp
left join gt2.SurgicalEncounter se on
	orp.SurgeryID=se.LogID
left join gt1.ORFacility f on
	f.RoomID=se.ORRoomID
where se.SurgeryDate >= @ApplicationStartDate and se.SurgeryDate<@ApplicationEndDate
;

-- STAFF
if object_id('amSurgServ.AllStaff') is not null drop table amSurgServ.AllStaff;
if object_id('amSurgServ.AllStaff') is not null drop table amSurgServ.AllStaff;

select 
	s.LogID
	,s.StaffName as Staff
	,s.StaffStartTime
	,s.StaffEndTime
	,s.StaffIsAnesthesia as AnesthesiaStaffFlag
	,s.StaffMinutesWorked
	,s.StaffType
	,s.Source as StaffSource
	,se.SurgeryDate
	,se.ORRoom as OperatingRoom
	,f.ORLocation
	,f.Facility
	,se.PrimaryORService
	,se.PrimarySurgeon
	,se.PrimaryProcedure
into amSurgServ.AllStaff
from gt1.ORANAllStaff s
left join gt2.SurgicalEncounter se on
	s.LogID=se.LogID
left join gt1.ORFacility f on
	f.RoomID=se.ORRoomID
where s.StaffName is not null and
se.SurgeryDate >= @ApplicationStartDate and se.SurgeryDate<@ApplicationEndDate
--note: Sandor, William shows as AN and Surgeon
;

--DELAY
if object_id('amSurgServ.AllDelay') is not null drop table amSurgServ.AllDelay;
if object_id('amSurgServ.AllDelay') is not null drop table amSurgServ.AllDelay;

select
	se.LogID as SurgeryID 
	,ord.DelayComments
	,ord.DelayMinutes
	,ord.DelayReason
	,ord.DelayType
	,ord.PACUDelayFlag
	,se.SurgeryDate
	,se.ORRoom as OperatingRoom
	,f.ORLocation
	,f.Facility
	,se.PrimaryORService
	,se.PrimarySurgeon
	,se.PrimaryProcedure
	,pt.MRN as PatientMRN
	,pt.Zipcode as PatientZipcode
	,pt.BirthDate as PatientBirthDate
	,se.PatientAge 
	,se.PatientAdultFlag
	,pt.Gender as PatientGender
	,pt.Status as PatientStatus
	--Procdures
	,se.ASARating as ASARating
	,se.ProcedureLevel as PrimaryORProcedureLevel
	--Log flags
	,se.DelayFlag
	,se.AvoidableCancellationFlag
	,se.CancelledFlag as CancellationFlag
	,se.SameDayCancelledFlag as CancellationSameDayFlag
	,se.AddOnCaseFlag as AddOnSurgeryFlag
	,se.ElectiveCaseFlag as ElectiveSurgeryFlag
	,se.FirstCaseFlag
	,se.IncludeCaseInTotalFlag as IncludeInTotalSurgeryFlag
	,se.LogPerformedFlag as PerformedSurgeryFlag
	,se.RecoveryCaseFlag as RecoverySurgeryFlag
into amSurgServ.AllDelay
from gt1.ORDelay ord
left join gt2.SurgicalEncounter se on
	ord.LogID=se.LogID
left join gt1.ORFacility f on
	f.RoomID=se.ORRoomID
left join gt1.HospitalEncounter adm on
	adm.EncounterID=se.HospitalEncounterID
left join gt1.Patient pt on
	pt.PatientID=adm.PatientID
where DelayType<>'A) No Delay'
and se.SurgeryDate >= @ApplicationStartDate and se.SurgeryDate<@ApplicationEndDate
;

--TURNOVER
if object_id('amSurgServ.Turnover') is not null drop table amSurgServ.Turnover;
if object_id('amSurgServ.Turnover') is not null drop table amSurgServ.Turnover;

select 
	t.PreviousLogID as PreviousSurgeryID
	,t.PreviousLogEndTime as PreviousSurgeryEndTime
	,t.LogID as SurgeryID
	,t.Surgeon as Staff
	,t.SurgeonType as StaffType
	,t.LogStartTime as StaffStartTime
	,t.TurnoverTimeMinutes 
	,t.TurnoverType
	,se.SurgeryDate
	,se.ORRoom as OperatingRoom
	,f.ORLocation
	,f.Facility
	,se.PrimaryORService
	,se.PrimarySurgeon
	,se.PrimaryProcedure
	,pt.MRN as PatientMRN
	,pt.Zipcode as PatientZipcode
	,pt.BirthDate as PatientBirthDate
	,se.PatientAge 
	,se.PatientAdultFlag
	,pt.Gender as PatientGender
	,pt.Status as PatientStatus
	--Procdures
	,se.ASARating as ASARating
	,se.ProcedureLevel as PrimaryORProcedureLevel
	--Log flags
	,se.DelayFlag
	,se.AvoidableCancellationFlag
	,se.CancelledFlag as CancellationFlag
	,se.SameDayCancelledFlag as CancellationSameDayFlag
	,se.AddOnCaseFlag as AddOnSurgeryFlag
	,se.ElectiveCaseFlag as ElectiveSurgeryFlag
	,se.FirstCaseFlag
	,se.IncludeCaseInTotalFlag as IncludeInTotalSurgeryFlag
	,se.LogPerformedFlag as PerformedSurgeryFlag
	,se.RecoveryCaseFlag as RecoverySurgeryFlag
into amSurgServ.Turnover
from gt2.ORTurnover t
left join gt2.SurgicalEncounter se on
	t.LogID=se.LogID
left join gt1.ORFacility f on
	f.RoomID=se.ORRoomID
left join gt1.HospitalEncounter adm on
	adm.EncounterID=se.HospitalEncounterID
left join gt1.Patient pt on
	pt.PatientID=adm.PatientID
where se.SurgeryDate >= @ApplicationStartDate and se.SurgeryDate<@ApplicationEndDate
;