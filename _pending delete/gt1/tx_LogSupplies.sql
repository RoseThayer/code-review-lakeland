--if exists (select * from information_schema.tables where TABLE_NAME = 'LogSupplies' and TABLE_SCHEMA = 'gt2')
--drop table gt2.LogSupplies

if object_id('gt1.LogSupplies', 'U') is null drop table gt1.LogSupplies;

create table [gt1].[LogSupplies]
(
    SurgeryID                         numeric(18,0)       NOT NULL
  , LogPanelKey                       varchar(254)            NULL
  , LogPanelProcedureKey              varchar(254)            NULL
  , LogSuppliesProcedureID            numeric(40,0)           NULL
  , LogSuppliesPickListID             numeric(40,0)           NULL
  , LogSuppliesPickListName           varchar(254)            NULL
  , LogSuppliesSupplyID               numeric(18,0)           NULL
  , SupplyID                          int                     NULL-- key to SupplyDetail, LogSupplies
  , LogSuppliesNumberUsed             numeric(40,0)           NULL
  , LogSuppliesNumberWasted           numeric(40,0)           NULL
  , LogSuppliesUnitCharge             numeric(40,0)           NULL
  , LogSuppliesUnitCost               numeric(40,0)           NULL
  , LogSuppliesInventoryLocationName  varchar(254)            NULL
)

insert into [gt1].[LogSupplies]
(
    SurgeryID
  , LogPanelKey
  , LogPanelProcedureKey
  , LogSuppliesProcedureID
  , LogSuppliesPickListID
  , LogSuppliesPickListName
  , LogSuppliesSupplyID
  , SupplyID
  , LogSuppliesNumberUsed
  , LogSuppliesNumberWasted
  , LogSuppliesUnitCharge
  , LogSuppliesUnitCost
  , LogSuppliesInventoryLocationName
)

  select
	  op.CASE_RECORD_ID as SurgeryID
  , concat(op.CASE_RECORD_ID), '|', isnull(op.PANEL_NUMBER,1)) as LogPanelKey
	, concat( op.CASE_RECORD_ID), '|', isnull(op.PANEL_NUMBER,1),'|', op.PROCEDURE_ID) as LogPanelProcedureKey
	, op.PROCEDURE_ID as LogSuppliesProcedureID
	, op.PICK_LIST_ID as LogSuppliesPickListID
	, op.PICK_LIST_NAME as LogSuppliesPickListName
	, opsl.SUPPLY_ID as LogSuppliesSupplyID
	, opsl.SUPPLY_ID as SupplyID
	, opsl.SUPPLIES_USED as LogSuppliesNumberUsed
	, opsl.SUPPLIES_WASTED as LogSuppliesNumberWasted
	, opsl.SUP_UNIT_CHARGE as LogSuppliesUnitCharge
	, opsl.SUP_UNIT_COST as LogSuppliesUnitCost
	, IsNull(cl.LOC_NAME , '*Unspecified') as LogSuppliesInventoryLocationName
  from ge.OR_PKLST op
    where len(op.CASE_RECORD_ID)>0
  left join ge.OR_PKLST_SUP_LIST opsl
    on op.PICK_LIST_ID = opsl.PICK_LIST_ID
      where opsl.SUPPLY_INV_LOC_ID >0
  left join ge.CLARITY_LOC cl
    on op.SUPPLY_INV_LOC_ID = cl.LOC_ID
;
