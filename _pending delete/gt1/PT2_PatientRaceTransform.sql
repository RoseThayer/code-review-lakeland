SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
 
create procedure [gt1].[PT2_PatientRaceTransform]
as

/*
   --> Source Tables<--
		PATIENT 
		ZC_PATIENT_RACE 

	--> Target Tables<--
	    gt1.Patient 

	--> Performance Benchmarks <--
		1843889 rows ~12s
*/

--PatientRace 
if exists 
	(select top 1 * from INFORMATION_SCHEMA.TABLES where TABLE_NAME = N'PatientRace' and TABLE_SCHEMA = N'gt1')
drop table gt1.PatientRace
create table [gt1].[PatientRace]
(
	[PatientID] [varchar](50) NULL,
	[PatientRaceLine] [int] NULL,
	[PatientRaceID] [int] NULL,
	[PatientRace] [varchar](100) NULL
) on [primary]
insert into gt1.PatientRace
(
	[PatientID],
	[PatientRaceLine],
	[PatientRaceID],
	[PatientRace] 
)
select PAT_ID, PatientRaceLine, PatientRaceID, PatientRace
from (
	select 
		pr.PAT_ID
	  , pr.LINE as PatientRaceLine 
	  , pr.PATIENT_RACE_C as PatientRaceID 
	  , zc.[NAME] as PatientRace
	from PA.ge.PATIENT_RACE pr 
	left join PA.ge.ZC_PATIENT_RACE zc
	  on pr.PATIENT_RACE_C = zc.PATIENT_RACE_C
	union
	select  
		pt.PatientID
	  , 1 as PatientRaceLine
	  , -1 as PatientRaceID
	  , '*Unspecified' as PatientRace  
	from PA.ge.PATIENT_RACE pr 
	right join gt1.Patient pt
	  on pr.PAT_ID = pt.PatientID
	where pt.PatientID is null
	) a
	
 
 -- check keys and indexes
exec tools.CheckKeysAndIndexes





GO
