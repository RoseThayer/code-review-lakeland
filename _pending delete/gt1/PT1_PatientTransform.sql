SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
 
 
create procedure [gt1].[PT1_PatientTransform]
as

/*  
   --> Source Tables<--
		PATIENT 
		ZC_SEX
		ZC_ETHNIC_GROUP

	--> Target Tables<--
	    gt1.Patient 

	--> Performance Benchmrk <--
		1896819 rows ~13s
		
*/

-- Patient
if exists 
	(select top 1 * from INFORMATION_SCHEMA.TABLES where TABLE_NAME = N'Patient' and TABLE_SCHEMA = N'gt1')
drop table gt1.Patient
create table [gt1].[Patient]
(
	[PatientID] [varchar](50) NULL,
    --[PatientFirstName] varchar(100) null,  
    --[PatientMiddleName] varchar(100) null,
    --[PatientLastName] varchar(100) null,
	[PatientName] varchar(300) null,
	[PatientBirthDate] [varchar](50) NULL,
	[PatientMRN] [varchar](50) NULL,
	[PatientSex] [varchar](50) NULL,
	[PatientEthnicGroup] [varchar](100) NULL
) on [primary]
insert into gt1.Patient
(
	[PatientID],
--    [PatientFirstName],  
--    [PatientMiddleName],
--    [PatientLastName],
	[PatientName],
	[PatientBirthDate],
	[PatientMRN],
	[PatientSex],
	[PatientEthnicGroup] 
)
select 
    pt.PAT_ID as [PatientID] 
  --, pt.PAT_FIRST_NAME as [PatientFirstName]  -- Not LVHN workflow
  --, pt.PAT_MIDDLE_NAME as [PatientMiddleName] -- Not LVHN workflow
  --, pt.PAT_LAST_NAME as [PatientLastName] -- Not LVHN workflow
  , pt.PAT_NAME as [PatientName]
  , pt.BIRTH_DATE as [PatientBirthDate] 
  , pt.PAT_MRN_ID as [PatientMRN] 
  , zx.[NAME] as [PatientSex] 
  , eg.[NAME] as [PatientEthnicGroup]
from PA.ge.PATIENT pt
left join PA.ge.ZC_SEX zx
  on pt.SEX_C = zx.RCPT_MEM_SEX_C
left join PA.ge.ZC_ETHNIC_GROUP eg
  on pt.ETHNIC_GROUP_C = eg.ETHNIC_GROUP_C
where len(pt.SEX_C) > 0
 

-- check keys and indexes
exec tools.CheckKeysAndIndexes







GO
