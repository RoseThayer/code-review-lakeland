SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
 
 
CREATE procedure [gt1].[MO1_OrderMedmixEncTransform] 
as

/*  --> Source Tables <--
		pa.ge.ORDER_MEDMIXINFO 
		pa.ge.ORDER_MED  
 
	--> Target Table<--
		gt1.OrderMedmixEnc
	
	--> Performance benchmark <--
		224237 rows in ~3 s
*/	

-- get a list of Medication ID values from the variables table
declare @MAR_Medication_ID table (id int identity(1,1), Medication_ID bigint)
insert into @MAR_Medication_ID
select VariableValue from variables.GetTableValues ('@ABX_Medication_ID')
union
select VariableValue from variables.GetTableValues ('@IVFluid_Medication_ID')
union
select VariableValue from variables.GetTableValues ('@Vasopressor_Medication_ID')

if exists (select top 1 * from INFORMATION_SCHEMA.TABLES where TABLE_NAME = N'OrderMedmixEnc' and TABLE_SCHEMA = N'gt1') drop table gt1.OrderMedmixEnc
create table [gt1].[OrderMedmixEnc]
(
	[ORDER_MED_ID] [numeric](18, 0) NOT NULL,
	[PAT_ENC_CSN_ID] [numeric](18, 0) NULL,
	[ORDER_INST] [datetime] NULL,
	[MEDICATION_ID] [numeric] null, 
	[START_DATE] datetime null, 
	[END_DATE] datetime null
)  
insert into [gt1].[OrderMedmixEnc]
(	 [ORDER_MED_ID]
    ,[PAT_ENC_CSN_ID]
    ,[ORDER_INST]
	,[MEDICATION_ID]
	,[START_DATE]
	,[END_DATE]
)
select  
    omd.ORDER_MED_ID 
  , omd.MEDICATION_ID
  , omd.ORDER_INST
  , omd.PAT_ENC_CSN_ID 
  , omd.[START_DATE]
  , omd.[END_DATE]
 from pa.ge.ORDER_MED omd 
 inner join @MAR_Medication_ID vmi
   on omd.MEDICATION_ID = vmi.Medication_ID
union
 select  
    omi.ORDER_MED_ID 
  , omi.MEDICATION_ID
  , null
  , null 
  , null
  , null
 from pa.ge.ORDER_MEDMIXINFO omi 
 inner join @MAR_Medication_ID vmi
   on omi.MEDICATION_ID = vmi.Medication_ID
 where SELECTION > 0 

 -- check keys and indexes
exec tools.CheckKeysAndIndexes







GO
