--if exists (select * from information_schema.tables where TABLE_NAME = 'SuppliesDetail' and TABLE_SCHEMA = 'gt2')
--drop table gt2.SuppliesDetail

--creates temporary table of concatenated External IDs
  select
      osm.ITEM_ID
    , stuff( (select ','+osei.EXTERNAL_ID
               from OR_SPLY_EXT_IDS osei
               where osei.ITEM_ID = osm.ITEM_ID
               order by osei.EXTERNAL_ID
               for xml path(''), TYPE).value('.', 'varchar(max)')
            ,1,1,'')
       as SupplyAllExternalIDs
    from OR_SPLY_MANFACTR osm
      into tSupplyAllExternalIDs
    group by osm.ITEM_ID ;


if object_id('gt1.SuppliesDetail', 'U') is null drop table gt1.SuppliesDetail;

create table [gt1].[SuppliesDetail]
(
    SupplyID numeric(18,0)     NOT NULL
  , SupplyActive varchar(18,0)   NOT NULL
  , SupplyName varchar(254)     NULL
  , SupplyTypeOfItem varchar(254)         NULL
  , SupplyImplantType varchar(254)         NULL
  , SupplyPrimaryExternalID varchar(254)         NULL
  , SupplyManufacturer   varchar(254)         NULL
  , SupplyManufacturerID numeric(18,0) NULL
  , SupplyManufacturer varchar(254) NULL
   , SupplyAllExternalIDs varchar(254)         NULL

)

insert into [gt1].[SuppliesDetail]
(
    SupplyID
  , SupplyActive
  , SupplyName
  , SupplyTypeOfItem
  , SupplyImplantType
  , SupplyPrimaryExternalID
  , SupplyManufacturerID
  , SupplyManufacturer
  , SupplyAllExternalIDs
)

  select
	    os.SUPPLY_ID as SupplyID,
	  , isnull(ACTIVE_YN,'Unknown') as SupplyActive
	  , os.SUPPLY_NAME as SupplyName
    , isnull(zoti.NAME,'*Unspecified') as SupplyTypeOfItem
    , isnull(zoit.NAME,'*Unspecified') as SupplyImplantType
	  , os.PRIMARY_EXT_ID as SupplyPrimaryExternalID
    , isnull(zman.INTERNAL_ID,'*Unspecified') as SupplyManufacturerID
	, isnull(zman.NAME,'*Unspecified') as SupplyManufacturer
    , tsei.SupplyAllExternalIDs
  from OR_SPLY os
    left join OR_SPLY_MANFACTR osm
      on os.SUPPLY_ID = osm.ITEM_ID
    left join ZC_OR_MANUFACTURER zman
      on osm.MANUFACTURER_C = zman.TYPE_OF_ITEM_C
    left join ZC_OR_TYPE_OF_ITEM zoti
      on os.TYPE_OF_ITEM_C = zoti.TYPE_OF_ITEM_C
    left join ZC_OR_IMPLANT_TYPE zoit
      on os.IMPLANT_TYPE_C = zoit.IMPLANT_TYPE_C
    left join OR_SPLY_EXT_IDS osei
      on os.SUPPLY_ID = osei.ITEM_ID
    left join tSupplyAllExternalIDs tsei
      on tsei.ITEM_ID = osm.ITEM_ID
;

drop table tSupplyAllExternalIDs
