ALTER procedure [gt1].[ProfessionalBilling] as

/*
  TODO:
    - validate
    - add logic for matching payments to charges to deal with detail types in a variable
    - add logic for debit adjustments to deal with detail types in a variable
    - add logic for undistributed payments to deal with detail types in a variable
    - 26min
    - limit to company = 30?
    - add bill area name
*/

/*  --> Source Tables <--
		ge.CLARITY_TDL_TRAN
    ge.ARPB_TRANSACTIONS
    ge.ZC_FIN_CLASS / ge.ZC_FINANCIAL_CLASS
    ge.CLARITY_EPM
    ge.CLARITY_EPM_2
    ge.CLARITY_EPP_2
    ge.ZC_PROD_TYPE
    ge.CLARITY_SA
    ge.CLARITY_SER
    ge.CLARITY_LOC
    ge.CLARITY_DEP
    ge.ACCOUNT
    ge.HSP_ACCOUNT

		--> Target Tables <--
		gt1.PBTransactions
    gt1.CMI

		--> Performance Benchmark <--
		gt1.PBTransactions 20MM rows, 7m 40s
    gt1.CMI 1,105,689 rows, ???

*/

if OBJECT_ID('tempdb..#PBDetailType') is not null drop table #PBDetailType;
if OBJECT_ID('tempdb..#CodingStatusComplete') is not null drop table #CodingStatusComplete;
if OBJECT_ID('tempdb..#InpatientClass') is not null drop table #InpatientClass;
if OBJECT_ID('tempdb..#BillAreaToExclude') is not null drop table #BillAreaToExclude;
if OBJECT_ID('gt1.PBTransactions', 'U') is not null drop table gt1.PBTransactions;
if OBJECT_ID('gt1.tPBTransactions', 'U') is not null drop table gt1.tPBTransactions;


/* Configuration variables */
create table #PBDetailType (
    DETAIL_TYPE int
  , Description varchar(80) 
  index cix__PBDetailType__DetailType clustered (DETAIL_TYPE) 
)
insert into #PBDetailType
select VariableValue, 'Adjustment' from variables.GetTableValues('@vcgPBAdjustTypeID')
union all
select VariableValue, 'Payment' from variables.GetTableValues('@vcgPaymentTypeID')
union all
select VariableValue, 'Voided' from variables.GetTableValues('@vcgPBVoidedTypeID')
union all
select VariableValue, 'Undistributed' from variables.GetTableValues('@vcgPBUndistTypeID')
union all
select VariableValue, 'Denial' from variables.GetTableValues('@vcgPBDenialTypeID')
union all
select VariableValue, 'Charge' from variables.GetTableValues('@vcgPBChargeTypeID')
union all
select VariableValue, 'Bad Debt' from variables.GetTableValues('@vcgPBBadDebtTypeID')
;

declare @ApplicationStartDate date = variables.GetSingleValue('@vcgApplicationStartDate');
if @ApplicationStartDate is null
  set @ApplicationStartDate = '2015-01-01' 
;

declare @FiscalYearBegin int = variables.GetSingleValue('@vcgFiscalYearBegin'); -- assumes variable returns an int. May need to update to extract month from date.
--declare @FiscalYearBegin int = datepart(m, variables.GetSingleValue('vcgFiscalYearBegin')); -- proposed date-to-int version
set @FiscalYearBegin = 7; -- hard-coded per Tully, variable deleted from Lehigh spreadsheet


--declare @CodingStatusComplete table
--(
--    CodingStatusCompleteKey int identity(1, 1)
--  , CodingStatusCode   	    int
--);
--insert into @CodingStatusComplete
create table #CodingStatusComplete 
(
  CodingStatusCode   	    int
  index cix__CodingStatusComplete__CodingStatusCode clustered (CodingStatusCode) 
);
insert into #CodingStatusComplete
select
	VariableValue
from variables.GetTableValues('@vcgCodingStatusComplete')
;

--declare @InpatientClass table
--(
--    InpatientClassKey   int identity(1, 1)
--  , AccountBaseClass   	int
--);
--insert into @InpatientClass
create table #InpatientClass
(
  AccountBaseClass   	int
  index cix__InpatientClass__AccountBaseClass clustered (AccountBaseClass) 
);
insert into #InpatientClass
select
	VariableValue
from variables.GetTableValues('@vcgInpatientBaseClassID')
;

--declare @BillAreaToExclude table
--(
--  BILL_AREA_ID int
--);
--insert into @BillAreaToExclude
create table #BillAreaToExclude
(
  BILL_AREA_ID int
  index cix__BillAreaToExclude__BillAreaID clustered (BILL_AREA_ID) 
);
insert into #BillAreaToExclude
select
  VariableValue
from variables.GetTableValues('@vcgBillAreaToExclude')
insert into #BillAreaToExclude 
select 200100102
union select 200100101
union select 200100100
union select 100100102
union select 100100101
union select 100100100
;

-- 16m 33s 
select
    tdl.TX_ID                                                       as TransactionID
  , tdl.MATCH_TRX_ID                                                as MatchTransactionID
  , tdl.MATCH_TX_TYPE                                               as MatchTransactionDetailType
  , tdl.TDL_ID                                                      as TransactionDetailLineID
  , tdl.PROC_ID                                                     as ProcedureID
  , tdl.CPT_CODE                                                    as CPTCode
  , tdl.PAT_ENC_CSN_ID                                              as EncounterCSN
  , tdl.INT_PAT_ID                                                  as PatientID
  , tdl.DETAIL_TYPE                                                 as DetailType
  , convert(date, tdl.POST_DATE)                                    as PostDate
  , convert(date, tdl.ORIG_SERVICE_DATE)                            as ServiceDate
  , tdl.AMOUNT                                                      as Amount
  , tdl.PATIENT_AMOUNT                                              as PatientAmount
  --, iif(tx_type.Description = 'Charge', tdl.AMOUNT , 0)             as ChargeAmount
  --, iif(tx_type.Description = 'Payment', tdl.AMOUNT * -1, 0)        as PaymentAmount
  --, iif(tx_type.Description = 'Denial', tdl.AMOUNT , 0)             as DenialAmount
  --, iif(tx_type.Description = 'Voided', tdl.AMOUNT , 0)             as VoidedAmount
  --, iif(tx_type.Description = 'Adjustment', tdl.AMOUNT , 0)         as AdjustmentAmount
  --, iif(tx_type.Description = 'Undistributed', tdl.AMOUNT , 0)      as UndistributedAmount
  --, iif(tx_type.Description = 'Bad Debt', tdl.AMOUNT , 0)           as BadDebtAmount
  , iif(tdl.DETAIL_TYPE in (1, 10)
      , tdl.AMOUNT
      , 0
    )                                                               as ChargeAmount
  , iif(tdl.DETAIL_TYPE in (2, 5, 11, 20, 22, 32, 33)
      , tdl.AMOUNT * -1
      , 0
    )                                                               as PaymentAmount
  , 0                                                               as DenialAmount
  , 0                                                               as VoidedAmount
  , 0                                                               as AdjustmentAmount
  , 0                                                               as UndistributedAmount
  , 0                                                               as BadDebtAmount
  , tdl.BILL_CLAIM_AMOUNT                                           as BillClaimAmount
  --, iif(tx_type.Description = 'Charge', 1 , 0)                      as ChargeFlag
  --, iif(tx_type.Description = 'Payment', 1 , 0)                     as PaymentFlag
  --, iif(tx_type.Description = 'Denial', 1 , 0)                      as DenialFlag
  --, iif(tx_type.Description = 'Voided', 1 , 0)                      as VoidedFlag
  --, iif(tx_type.Description = 'Adjustment', 1 , 0)                  as AdjustmentFlag
  --, iif(tx_type.Description = 'Undistributed', 1 , 0)               as UndistributedFlag
  --, iif(tx_type.Description = 'Bad Debt', 1 , 0)                    as BadDebtFlag
  , iif(tdl.DETAIL_TYPE in (1, 10)
      , 1
      , 0
    )                                                               as ChargeFlag
  , iif(tdl.DETAIL_TYPE in (2, 5, 11, 20, 22, 32, 33)
      , 1
      , 0
    )                                                               as PaymentFlag
  , 0                                                               as DenialFlag
  , 0                                                               as VoidedFlag
  , 0                                                               as AdjustmentFlag
  , 0                                                               as UndistributedFlag
  , 0                                                               as BadDebtFlag
  , tdl.RVU_WORK
    * tdl.PROCEDURE_QUANTITY                                        as WorkRVU
  , tdl.PROCEDURE_QUANTITY                                          as ProcedureQuantity
  , tdl.ACCOUNT_ID                                                  as GuarantorAccountID
  , acct.ACCOUNT_TYPE_C                                             as GuarantorAccountTypeID
  , tdl.CUR_PAYOR_ID                                                as CurrentPayorID
  , iif(tdl.CUR_PAYOR_ID is null
    , '*Self Pay'
    , iif(epm_curr.PAYOR_ID is null
      , '*Unknown'
      , epm_curr.PAYOR_NAME
      )
    )                                                               as CurrentPayor
  , iif(tdl.CUR_PLAN_ID is null
      , '*Self Pay'
      , iif(currplan.RPT_GRP_ONE is null
        , '*Unknown'
        , currplan.RPT_GRP_ONE
      )
    )                                                               as CurrentPayorAbbr
  , tdl.ORIGINAL_PAYOR_ID                                           as OriginalPayorID
  , iif(tdl.ORIGINAL_PAYOR_ID is null
    , '*Self Pay'
    , iif(epm_orig.PAYOR_ID is null
      , '*Unknown'
      , epm_orig.PAYOR_NAME
      )
    )                                                               as OriginalPayor
  , iif(tdl.ORIGINAL_PLAN_ID is null
      , '*Self Pay'
      , iif(origplan.RPT_GRP_ONE is null
        , '*Unknown'
        , origplan.RPT_GRP_ONE
      )
    )                                                               as OriginalPayorAbbr
  , iif(epm2.PROD_TYPE_C is null
    , '*Unspecified'
    , iif(epmprodtype.PROD_TYPE_C is null
      , '*Unknown'
      , epmprodtype.NAME
      )
    )                                                               as PayorProductType
   , iif(currplan2.PROD_TYPE_C is null
     , '*Unspecified'
     , iif(eppprodtype.PROD_TYPE_C is null
       , '*Unknown'
       , eppprodtype.NAME
       )
     )                                                              as PlanProductType
  , iif(tdl.CUR_FIN_CLASS is null
    , 'Self-Pay'
    , iif(curfinclass.FINANCIAL_CLASS is null
      , '*Unknown'
      , curfinclass.NAME
      )
    )                                                               as CurrentFinancialClass
  , iif(tdl.ORIGINAL_FIN_CLASS is null
    , 'Self-Pay'
    , iif(origfinclass.FINANCIAL_CLASS is null
      , '*Unknown'
      , origfinclass.NAME
      )
    )                                                               as FinancialClass
  , tdl.BILLING_PROVIDER_ID                                         as BillingProviderID
  , iif(tdl.BILLING_PROVIDER_ID is null
    , '*Unspecified'
    , iif(billser.PROV_ID is null
      , '*Unknown'
      , billser.PROV_NAME
      )
    )                                                               as BillingProvider
  , tdl.PERFORMING_PROV_ID                                          as PerformingProviderID
  , iif(tdl.PERFORMING_PROV_ID is null
    , '*Unspecified'
    , iif(perfser.PROV_ID is null
      , '*Unknown'
      , perfser.PROV_NAME
      )
    )                                                               as PerformingProvider
  , tdl.SERV_AREA_ID                                                as ServiceAreaID
  , iif(tdl.SERV_AREA_ID is null
    , '*Unspecified'
    , iif(sa.SERV_AREA_ID is null
      , '*Unknown'
      , sa.SERV_AREA_NAME
      )
    )                                                               as ServiceArea
  , iif(tdl.LOC_ID is null
    , '*Unspecified'
    , iif(loc.LOC_ID is null
      , '*Unknown'
      , loc.LOC_NAME
      )
    )                                                               as RevenueLocation
  , tdl.DEPT_ID                                                     as DepartmentID
  , iif(tdl.DEPT_ID is null
    , '*Unspecified'
    , iif(dep.DEPARTMENT_ID is null
      , '*Unknown'
      , dep.DEPARTMENT_NAME
      )
    )                                                               as Department
  , tdl.HSP_ACCOUNT_ID                                              as HospitalAccountID
  , hspacct.ACCT_BASECLS_HA_C                                       as HARBaseClassID
  , arpbtx.CLAIM_DATE                                               as ClaimDate
  , tdl.BILL_AREA_ID                                                as PBBillArea
  , case 
      when tdl.detail_type = 32
      then chg.BILL_AREA_ID
      else tdl.BILL_AREA_ID
    end                                                             as PBBillAreaFromCharge
  , iif(tdl.BILL_AREA_ID is null
      , '*Unspecified'
      , iif(ba.BILL_AREA_ID is null
        , '*Unknown'
        , ba.RECORD_NAME
      )
    )                                                               as PBBillAreaName 
  , iif(tdl.BILL_AREA_ID is null
      , '*Unspecified'
      , iif(ba.BILL_AREA_ID is null
        , '[' + cast(tdl.BILL_AREA_ID as varchar) + '] *Unknown' -- customer wanted ID before name, so *Unknown will be scattered throughout
        , '[' + cast(tdl.BILL_AREA_ID as varchar) + '] ' + ba.RECORD_NAME
      )
    )                                                               as PBBillAreaNameWithID
  , case
      when left(ba.BILL_AREA_ID, 2) = '30'
        then 'LVPG'
      when left(ba.BILL_AREA_ID, 2) = '61'
        then 'LVPG-S'
      when left(ba.BILL_AREA_ID, 2) = '71'
        then 'LVPG-P'
      else IsNull( barptgrp11.NAME, '*Unknown' )
    end as HospitalLocationName
  , case
      when left(ba.BILL_AREA_ID, 2) = '30'
        then 'LVPG'
      when left(ba.BILL_AREA_ID, 2) = '61'
        then 'LVPG-S'
      when left(ba.BILL_AREA_ID, 2) = '71'
        then 'LVPG-P'
      else IsNull( ec.Name, '*Unknown' )
    end as HospitalEntity
  , case
      when left(ba.BILL_AREA_ID, 2) = '30'
        then 'LVPG [30]'
      when left(ba.BILL_AREA_ID, 2) = '61'
        then 'LVPG-S [61]'
      when left(ba.BILL_AREA_ID, 2) = '71'
        then 'LVPG-P [71]'
      when barptgrp11.NAME is null
        then '*Unknown'
      else barptgrp11.NAME + ' [' + cast(barptgrp11.RPT_GRP_11_BIL_C as varchar) + ']'
    end as HospitalLocationNameWithID
into gt1.tPBTransactions
from ge.CLARITY_TDL_TRAN tdl
--inner join #PBDetailType tx_type
--  on tx_type.DETAIL_TYPE = tdl.DETAIL_TYPE
--    and tx_type.DETAIL_TYPE in (1, 10) -- change logic so this is just detail type 1, or is it ok to have charge/void details?
--left join #BillAreaToExclude bill_area
--  on bill_area.BILL_AREA_ID = tdl.BILL_AREA_ID
left join ge.clarity_tdl_tran chg
  on chg.tx_id = tdl.MATCH_TRX_ID
    and chg.DETAIL_TYPE = tdl.MATCH_TX_TYPE
    and tdl.detail_type = 32
left join ge.ACCOUNT acct
  on acct.ACCOUNT_ID = tdl.ACCOUNT_ID
left join ge.CLARITY_EPM epm_curr
  on epm_curr.PAYOR_ID = tdl.CUR_PAYOR_ID
left join ge.CLARITY_EPM epm_orig
  on epm_orig.PAYOR_ID = tdl.ORIGINAL_PAYOR_ID
left join ge.CLARITY_EPM_2 epm2
  on epm2.PAYOR_ID = tdl.CUR_PAYOR_ID
left join ge.ZC_PROD_TYPE epmprodtype
  on epmprodtype.PROD_TYPE_C = epm2.PROD_TYPE_C
left join ge.CLARITY_EPP currplan
  on currplan.BENEFIT_PLAN_ID = tdl.CUR_PLAN_ID
left join ge.CLARITY_EPP origplan
  on origplan.BENEFIT_PLAN_ID = tdl.ORIGINAL_PLAN_ID
left join ge.CLARITY_EPP_2 currplan2
   on currplan2.BENEFIT_PLAN_ID = tdl.CUR_PLAN_ID
left join ge.ZC_PROD_TYPE eppprodtype
   on eppprodtype.PROD_TYPE_C = currplan2.PROD_TYPE_C
left join ge.ZC_FINANCIAL_CLASS curfinclass
  on curfinclass.FINANCIAL_CLASS = tdl.CUR_FIN_CLASS
left join ge.ZC_FINANCIAL_CLASS origfinclass
  on origfinclass.FINANCIAL_CLASS = tdl.ORIGINAL_FIN_CLASS
left join ge.CLARITY_SER billser
  on billser.PROV_ID = tdl.BILLING_PROVIDER_ID
left join ge.CLARITY_SER perfser
  on perfser.PROV_ID = tdl.PERFORMING_PROV_ID
left join ge.CLARITY_SA sa
  on sa.SERV_AREA_ID = tdl.SERV_AREA_ID
left join ge.CLARITY_LOC loc
  on loc.LOC_ID = tdl.LOC_ID
left join ge.CLARITY_DEP dep
  on dep.DEPARTMENT_ID = tdl.DEPT_ID
left join ge.HSP_ACCOUNT hspacct
  on hspacct.HSP_ACCOUNT_ID = tdl.HSP_ACCOUNT_ID
left join ge.ARPB_TRANSACTIONS arpbtx
  on arpbtx.TX_ID = tdl.TX_ID
left join ge.PATIENT_3 pat3
  on pat3.PAT_ID = tdl.INT_PAT_ID
left join ge.BILL_AREA ba
  on ba.BILL_AREA_ID = tdl.BILL_AREA_ID
left join ge.ZC_RPT_GRP_11_BIL barptgrp11
  on barptgrp11.RPT_GRP_11_BIL_C = ba.RPT_GRP_ELEVEN_C
left join variables.EntityCrosswalk ec
  on ec.RPT_GRP_11_BIL_C = ba.RPT_GRP_ELEVEN_C
where
  tdl.POST_DATE >= @ApplicationStartDate
    and tdl.ORIG_SERVICE_DATE >= @ApplicationStartDate
    and tdl.DETAIL_TYPE in (1, 10, 2, 5, 11, 20, 22, 32, 33)
    and (pat3.IS_TEST_PAT_YN is null or pat3.IS_TEST_PAT_YN = 'N')
;

-- 40 seconds
insert into gt1.tPBTransactions
select
    tdl.TransactionID
  , tdl.MatchTransactionID
  , tdl.MatchTransactionDetailType
  , tdl.TransactionDetailLineID
  , tdl.ProcedureID
  , tdl.CPTCode
  , tdl.EncounterCSN
  , tdl.PatientID
  , 2                                                               as DetailType
  , tdl.PostDate
  , tdl.ServiceDate
  , tdl.Amount * -1                                                 as Amount
  , tdl.PatientAmount
  , tdl.ChargeAmount
  , tdl.PaymentAmount
  , tdl.DenialAmount
  , tdl.VoidedAmount
  , tdl.AdjustmentAmount
  , tdl.UndistributedAmount
  , tdl.BadDebtAmount
  , tdl.BillClaimAmount
  , tdl.ChargeFlag
  , tdl.PaymentFlag
  , tdl.DenialFlag
  , tdl.VoidedFlag
  , tdl.AdjustmentFlag
  , tdl.UndistributedFlag
  , tdl.BadDebtFlag
  , tdl.WorkRVU
  , tdl.ProcedureQuantity
  , tdl.GuarantorAccountID
  , tdl.GuarantorAccountTypeID
  , tdl.CurrentPayorID
  , tdl.CurrentPayor
  , tdl.CurrentPayorAbbr
  , tdl.OriginalPayorID
  , tdl.OriginalPayor
  , tdl.OriginalPayorAbbr 
  , tdl.PayorProductType
  , tdl.PlanProductType
  , tdl.CurrentFinancialClass
  , tdl.FinancialClass
  , tdl.BillingProviderID
  , tdl.BillingProvider
  , tdl.PerformingProviderID
  , tdl.PerformingProvider
  , tdl.ServiceAreaID
  , tdl.ServiceArea
  , tdl.RevenueLocation
  , tdl.DepartmentID
  , tdl.Department
  , tdl.HospitalAccountID
  , tdl.HARBaseClassID
  , tdl.ClaimDate
  , tdl.PBBillArea
  , tdl.PBBillAreaFromCharge
  , tdl.PBBillAreaName
  , tdl.PBBillAreaNameWithID
  , tdl.HospitalLocationName
  , tdl.HospitalEntity
  , tdl.HospitalLocationNameWithID
from gt1.tPBTransactions tdl
where
  tdl.DetailType = 32
    and tdl.PBBillAreaFromCharge in (30262, 30263, 100100100, 100100101, 100100102, 200100100, 200100101, 200100102)      
;

-- 32 seconds
create nonclustered index ncix__tPBTransactions__DetailTypeBillArea on gt1.tPBTransactions (DetailType, PBBillArea);

-- 1 minute
select *
into gt1.PBTransactions
from gt1.tPBTransactions pb
where
  not (pb.DetailType in (1,10,20) and pb.PBBillArea in (30262, 30263, 100100100, 100100101, 100100102, 200100100, 200100101, 200100102))
  and not (pb.DetailType in (32) and pb.PBBillAreaFromCharge in (30262, 30263, 100100100, 100100101, 100100102, 200100100, 200100101, 200100102))

drop table gt1.tPBTransactions;

-- create indexes
-- 45 seconds 
create nonclustered index ncix__PBTransactions__PatientID on gt1.PBTransactions (PatientID);
-- 32 seconds
create nonclustered index ncix__PBTransactions__DetailType on gt1.PBTransactions (DetailType);
-- 29 seconds
create nonclustered index ncix__PBTransactions__PostDate on gt1.PBTransactions (PostDate);


-- gt1.CMI
if OBJECT_ID('gt1.CMI', 'U') is not null drop table gt1.CMI;

with drgweight (DRG_ID, DRG_WEIGHT_RATING) as (
  select
      DRG_ID
    , avg(DRG_WEIGHT_RATING) as DRG_WEIGHT_RATING
  from ge.DRG_REIMBURSEMENT
  where
    DRG_WEIGHT_RATING is not null
  group by 
    DRG_ID
) 
select
    mdrg.HSP_ACCOUNT_ID                     as HospitalAccountID
  , hspacct.DISCH_DEPT_ID                   as DischargeDepartmentID
  , convert(date, hspacct.DISCH_DATE_TIME)  as DischargeDate
  , 'MS DRG'                                as DRGType
  , mdrg.DRG_ID_TYPE_ID                     as DRGIDTypeID
  , datepart(
      yy
    , dateadd(
        m
      , 12 - @FiscalYearBegin + 1
      , hspacct.DISCH_DATE_TIME
      )
    )                                       as DischargeFiscalYear
  , hspacct.SERV_AREA_ID                    as ServiceAreaID
  , hspacct.PRIMARY_PAYOR_ID                as HARPayorID
  , iif(hspacct.PRIMARY_PAYOR_ID is null
    , '*Unspecified'
    , iif(epm.PAYOR_ID is null
      , '*Unknown'
      , epm.PAYOR_NAME
      )
    )                                       as HARPayor
  , drgweight.DRG_WEIGHT_RATING                  as DRGWeight
into gt1.CMI
from ge.HSP_ACCT_MULT_DRGS mdrg
--inner join tools.EncounterFilter as ef
--  on ef.HSP_ACCOUNT_ID = mdrg.HSP_ACCOUNT_ID
left join ge.HSP_ACCOUNT hspacct
  on hspacct.HSP_ACCOUNT_ID = mdrg.HSP_ACCOUNT_ID
--left join ge.DRG_REIMBURSEMENT drgweight
--  on drgweight.DRG_ID = mdrg.DRG_ID
left join drgweight  
  on drgweight.DRG_ID = mdrg.DRG_ID 
left join ge.CLARITY_EPM epm
  on epm.PAYOR_ID = hspacct.PRIMARY_PAYOR_ID
--left join @CodingStatusComplete csc
--  on csc.CodingStatusCode = hspacct.CODING_STATUS_C
--left join @InpatientClass ic
--  on ic.AccountBaseClass = hspacct.ACCT_BASECLS_HA_C
left join #CodingStatusComplete csc
  on csc.CodingStatusCode = hspacct.CODING_STATUS_C
left join #InpatientClass ic
  on ic.AccountBaseClass = hspacct.ACCT_BASECLS_HA_C
where
  hspacct.DISCH_DATE_TIME >= @ApplicationStartDate
    and csc.CodingStatusCode is not null
    and ic.AccountBaseClass is not null
;

-- create indexes
create nonclustered index ncix__CMI__HospitalAccountID on gt1.CMI (HospitalAccountID);

-- check keys and indexes
exec tools.CheckKeysAndIndexes;
GO
