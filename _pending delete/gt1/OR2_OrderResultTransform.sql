SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
 
 
CREATE PROCEDURE [gt1].[OR2_OrderResultTransform] 
AS 

/*  --> Source Tables <--
		OrderResultDetail

	--> Target Table <--
		gt1.OrderResult 

	--> Performance Benchmark <--
		3009007 rows ~46s

*/	 
BEGIN

SET NOCOUNT ON;
 
if exists (select * from information_schema.tables where TABLE_NAME = 'OrderResult' and TABLE_SCHEMA = 'gt1')
   drop table gt1.OrderResult
create table [gt1].[OrderResult]
(
	[EncounterCSN] [varchar](15) NULL,
	[ResultTime] [datetime] NULL,
	[OrderTime] [datetime] NULL,
	[SpecimenTakenTime] [datetime] NULL, 
	[MinWBC] [varchar](15) NULL,
	[maxWBC] [varchar](15) NULL,
	[MinBands] [varchar](15) NULL,
	[MaxBands] [varchar](15) NULL,
	[MinBilirubin] [varchar](15) NULL,
	[MaxBilirubin] [varchar](15) NULL,
	[MinPlateletCount] [int] NULL,
	[MaxPlateletCount] [int] NULL,
	[MinLactate] [varchar](15) NULL,
	[MaxLactate] [varchar](15) NULL,
	[MinCreatinine] [varchar](15) NULL,
	[MaxCreatinine] [varchar](15) NULL,
	[MinPTINR] [varchar](15) NULL,
	[MaxPTINR] [varchar](15) NULL,
	[MinPTT] [varchar](15) NULL,
	[MaxPTT] [varchar](15) NULL,
	MaxPO2Arterial varchar(15) null,
	[SepsisOrdersetUsedFlag] [tinyint] NULL,
	[OrdersetName] [varchar](500) NULL
)  
insert into [gt1].[OrderResult]
(	 
	 [EncounterCSN]
    ,[ResultTime]
    ,[OrderTime]
	,[SpecimenTakenTime] 
    ,[MinWBC]
    ,[maxWBC]
    ,[MinBands]
    ,[MaxBands]
    ,[MinBilirubin]
    ,[MaxBilirubin]
    ,[MinPlateletCount]
    ,[MaxPlateletCount]
    ,[MinLactate]
    ,[MaxLactate]
    ,[MinCreatinine]
    ,[MaxCreatinine]
    ,[MinPTINR]
    ,[MaxPTINR]
    ,[MinPTT]
    ,[MaxPTT]
	,[MaxPO2Arterial]
    ,[SepsisOrdersetUsedFlag]
    ,[OrdersetName]
)
 select
   EncounterCSN
  ,ResultTime
  ,OrderTime
  ,SpecimenTakenTime 
  ,min(Wbc) as MinWBC
  ,max(Wbc) as maxWBC
  ,min(Bands) as MinBands
  ,max(Bands) as MaxBands
  ,min(Bilirubin) as MinBilirubin
  ,max(Bilirubin) as MaxBilirubin
  ,min(PlateletCount) as MinPlateletCount
  ,max(PlateletCount) as MaxPlateletCount
  ,min(Lactate) as MinLactate
  ,max(Lactate) as MaxLactate
  ,min(Creatinine) as MinCreatinine
  ,max(Creatinine) as MaxCreatinine
  ,min(PtInr) as MinPTINR
  ,max(PtInr) as MaxPTINR
  ,min(Ptt) as MinPTT
  ,max(Ptt) as MaxPTT
  --,min(PO2Arterial) as MinPO2Arterial -- Not LVHN workflow
  ,max(PO2Arterial) as MaxPO2Arterial  
  ,max(SepsisOrdersetUsedFlag) as SepsisOrdersetUsedFlag
  --,max(SepsisIPOrdersetUsedFlag) as SepsisIPOrdersetUsedFlag -- Not LVHN workflow
  --,max(SepsisEDOrdersetUsedFlag) as SepsisEDOrdersetUsedFlag -- Not LVHN workflow
  ,max(OrdersetName) as OrdersetName 
from gt1.OrderResultDetail
group by EncounterCSN, ResultTime, OrderTime, SpecimenTakenTime

 
END

-- check keys and indexes
exec tools.CheckKeysAndIndexes

 




GO
