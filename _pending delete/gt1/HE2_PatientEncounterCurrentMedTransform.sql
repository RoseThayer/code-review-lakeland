SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [gt1].[HE2_PatientEncounterCurrentMedTransform]
 as

/*  --> Source Tables <--
		pa.ge.PAT_ENC_CURR_MEDS
		pa.ge.CLARITY_MEDICATION 

	--> Target Table <--
		gt1.PatientEncounterCurrentMed

	--> Performance Benchmark <--
		8624852 rows in ~1m
*/	

declare @vCoumadinMedID int = (select variables.GetSingleValue('@vCoumadinMedID'))
declare @vHeparinMedID int = (select variables.GetSingleValue('@vHeparinMedID'))

if OBJECT_ID('gt1.PatientEncounterCurrentMed', 'U') is not null drop table gt1.PatientEncounterCurrentMed;
 create table [gt1].[PatientEncounterCurrentMed]
 (
	[PAT_ENC_CSN_ID] [numeric](18, 0) NOT NULL,
	[MedicationIsCoumadin] [int] NOT NULL,
	[MedicationIsHeparin] [int] NOT NULL
) 
insert into [gt1].[PatientEncounterCurrentMed]
(
	[PAT_ENC_CSN_ID]
   ,[MedicationIsCoumadin]
   ,[MedicationIsHeparin]
)
select 
	enc.PAT_ENC_CSN_ID  
  ,	iif(med.PHARM_CLASS_C = @vCoumadinMedID, 1, 0) as MedicationIsCoumadin
  , iif(med.PHARM_SUBCLASS_C = @vHeparinMedID, 1, 0) as MedicationIsHeparin 
from pa.ge.PAT_ENC_CURR_MEDS enc
inner join pa.ge.PAT_ENC_HSP pat -- limited to 2015+
   on enc.PAT_ENC_CSN_ID = pat.PAT_ENC_CSN_ID
left join pa.ge.CLARITY_MEDICATION med
  on enc.MEDICATION_ID = med.MEDICATION_ID
where med.MEDICATION_ID is not null
 
create index ncix__PatientEncounterCurrentMed__PAT_ENC_CSN_ID on gt1.PatientEncounterCurrentMed (PAT_ENC_CSN_ID) include (MedicationIsCoumadin, MedicationIsHeparin)
 
 -- check keys and indexes
exec tools.CheckKeysAndIndexes







GO
