USE [paval]
GO

/****** Object:  StoredProcedure [gt1].[PayorData]    Script Date: 9/1/2017 1:43:02 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

ALTER procedure [gt1].[PayorData] as

/*
  TODO:
    - replace ZC_FIN_CLASS with ZC_FINANCIAL_CLASS (ZC_FIN_CLASS will be deprecated soon)
    - uncomment exec tools.CheckKeysAndIndexes at end of script
    - validate
*/

/*  --> Source Tables <--
		ge.CLARITY_EPM
		ge.CLARITY_EPP
		ge.ZC_FIN_CLASS

    --> Target Tables <--
		gt1.Payor
		gt1.BenefitPlan

		--> Performance Benchmark <--
		gt1.Payor: 215 rows, < 1s
		gt1.BenefitPlan: 489 rows, < 1s
 
*/

-- if table <schema.tablename> exists, drop it
if OBJECT_ID('gt1.Payor', 'U') is not null drop table gt1.Payor;
if OBJECT_ID('gt1.BenefitPlan', 'U') is not null drop table gt1.BenefitPlan;

create table gt1.Payor
(
		PayorID							numeric(18, 0)
	, PayorName						varchar(80)
	, FinancialClassID		varchar(66)
	, FinancialClass			varchar(254)	-- coming from ZC_FIN_CLASS but should be ZC_FINANCIAL_CLASS, soon-to-be deprecated
	, ProductType					varchar(66)
	, PayorShortName			varchar(254)
);

create table gt1.BenefitPlan
(
		BenefitPlanID				numeric(18, 0)
	, BenefitPlanName			varchar(100)
	, ProductType					varchar(254)
	--, ReportGroup1				varchar(80)
	--, ReportGroup2				varchar(80)
	--, ReportGroup3				varchar(80)
	--, ReportGroup4				varchar(80)
	--, ReportGroup5				varchar(80)
	, PayorID							numeric(18, 0)
);

insert into gt1.Payor
(
		PayorID
	, PayorName
	, FinancialClassID
	, FinancialClass
	, ProductType
	, PayorShortName
)
select
      epm.PAYOR_ID					as PayorID
	, epm.PAYOR_NAME				as PayorName
	, epm.FINANCIAL_CLASS		as FinancialClassID
	, iif(epm.FINANCIAL_CLASS is null
		, '*Unspecified'
		, iif(zcfc.FINANCIAL_CLASS is null
			, '*Unknown'
			, zcfc.NAME
			)
		)									as FinancialClass
	, epm.PRODUCT_TYPE			as ProductType
	, iif(epm.SHORT_NAME is null
		, epm.PAYOR_NAME
		, epm.SHORT_NAME
		)									as PayorShortName
from ge.CLARITY_EPM epm
left join ge.ZC_FINANCIAL_CLASS zcfc
	on zcfc.FINANCIAL_CLASS = epm.FINANCIAL_CLASS
;

insert into gt1.BenefitPlan
(
		BenefitPlanID
	, BenefitPlanName
	, ProductType
	--, ReportGroup1
	--, ReportGroup2
	--, ReportGroup3
	--, ReportGroup4
	--, ReportGroup5
	, PayorID
)
select
		epp.BENEFIT_PLAN_ID			as BenefitPlanID
	, epp.BENEFIT_PLAN_NAME		as BenefitPlanName
	, epp.PRODUCT_TYPE				as ProductType
	--, null										as ReportGroup1
	--, null										as ReportGroup2
	--, null										as ReportGroup3
	--, null										as ReportGroup4
	--, null										as ReportGroup5
	, epp.PAYOR_ID						as PayorID
from ge.CLARITY_EPP epp
;

create nonclustered index ncix__Payor__PayorID on gt1.Payor (PayorID);

create nonclustered index ncix__BenefitPlan__BenefitPlanID on gt1.BenefitPlan (BenefitPlanID);

-- check keys and indexes
-- exec tools.CheckKeysAndIndexes
GO

