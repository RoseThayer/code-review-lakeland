if object_id('gt1.ORPanelAllTime', 'U') is null drop table gt1.ORPanelAllTime;

create table gt1.ORPanelAllTime (
	LogID [numeric] (18,0) NOT NULL
	,PanelNumber [numeric] (18,0) NULL
	,PanelStartTime [datetime] NULL
	,PanelEndTime [datetime] NULL
)
insert into gt1.ORPanelAllTime
(
	LogID
	,PanelNumber
	,PanelStartTime
	,PanelEndTime
)
select
	cast(LOG_ID as numeric) as LogID
	,PANEL_NUMBER as PanelNumber
	,min(PANEL_START_TIME) as PanelStartTime
	,min(PANEL_END_TIME) as PanelEndTime
from ge.OR_LOG_PANEL_TIMES
group by LogID, PANEL_NUMBER
;
