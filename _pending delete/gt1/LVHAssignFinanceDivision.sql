alter procedure gt1.LVHAssignFinanceDivision
as

/*
  3m 8s

  All logic copied directly from Lehigh's procedures:
    - LV_FINANCE_DB_LVH_ACUTE_DETAILS_SP.sql
    - LV_FINANCE_DB_LVHM_ACUTE_DETAILS_SP.sql
*/

if OBJECT_ID('gt1.LVHFinanceDivisionByHAR', 'U') is not null drop table gt1.LVHFinanceDivisionByHAR;

create table gt1.LVHFinanceDivisionByHAR
(
    HSP_ACCOUNT_ID numeric(18, 0)
  , FINANCE_DIVISION varchar(254)
);

insert into gt1.LVHFinanceDivisionByHAR
(
    HSP_ACCOUNT_ID
  , FINANCE_DIVISION
)
select
    har.HSP_ACCOUNT_ID
  , case
        when loc.RPT_GRP_SIX = 1 -- LVH
            then case
                     when har.ACCT_BASECLS_HA_C = 1
                         and har.ACCT_CLASS_HA_C in (117)
                         then 'TSU'
                     when har.ACCT_BASECLS_HA_C = 1
                         and har.ACCT_CLASS_HA_C in (133)
                         or grp18.RPT_GRP_EIGHTEEN_C = 65 --'Physical Medicine-Rehabilitation'
                         then 'IRF'
                     when har.ACCT_BASECLS_HA_C = 1
                         and (
                                 ped_ip_surgery.Peds_IP_Sur_AMT > 0
                                 and isnull(PEDS_picu_ip.PICU_IP_AMT, 0) = 0
                                 and isnull(PEDS_picu_ip.PICU_IP_AMT, 0) = 0
                             )
                         and grp17.RPT_GRP_SEVNTEEN_C = 12
                         and grp18.RPT_GRP_EIGHTEEN_C in
                         (
                             63
                           , 95
                         ) --17 'Department of Surgery'    18 'Pediatric Surgical Specialties','95 Pediatric Surgical Specialties/Otolaryngology-Head & Neck Surgery ' 
                         then 'Peds - IP Surgery'
                     when har.ACCT_BASECLS_HA_C = 1
                         and pednicip.NICU_IP_AMT > 0
                         and har.ACCT_CLASS_HA_C <> 107
                         then 'Peds - Nicu Ip'
                     when har.ACCT_BASECLS_HA_C = 1
                         and PEDS_picu_ip.PICU_IP_AMT > 0
                         then 'Peds - PICU IP'
                     when har.ACCT_BASECLS_HA_C = 1
                         and har.ACCT_CLASS_HA_C = 107
                         then '0.New Born'
                     when har.ACCT_BASECLS_HA_C = 1
                         and har.ACCT_CLASS_HA_C = 101
                         and pat.BIRTH_DATE = har.ADM_DATE_TIME
                         then '0.New Born 2'
                     when datediff(minute, pat.BIRTH_DATE, har.ADM_DATE_TIME) between 1 and 1200
                         and har.ACCT_BASECLS_HA_C = 1
                         and har.ACCT_CLASS_HA_C = 101
                         and pat.BIRTH_DATE < har.ADM_DATE_TIME
                         then '0.New Born 3'
                     when datediff(minute, pat.BIRTH_DATE, har.ADM_DATE_TIME) between -120 and -1
                         and har.ACCT_BASECLS_HA_C = 1
                         and har.ACCT_CLASS_HA_C = 101
                         then '0.New Born 3'
                     when har.ACCT_BASECLS_HA_C = 1 /*and FAM_MED.FammedAMT  >  0 */
                         and har.ACCT_CLASS_HA_C not in
                         (
                             107
                           , 129
                           , 131
                         )
                         and grp17.RPT_GRP_SEVNTEEN_C = 4 --'DEPARTMENT OF FAMILY MEDICINE' 
                         then 'Family Medicine IP'
                     when har.ACCT_BASECLS_HA_C = 1
                         and gen_data.genAMT > 0
                         and har.ACCT_CLASS_HA_C not in
                         (
                             107
                           , 129
                         )
                         and grp18.RPT_GRP_EIGHTEEN_C = 84 --'Cardiology' 
                         then 'Medicine - Cardio IP'
                     when har.ACCT_BASECLS_HA_C = 1
                         and gen_data.genAMT > 0
                         and har.ACCT_CLASS_HA_C not in
                         (
                             107
                           , 117
                           , 129
                         )
                         and grp18.RPT_GRP_EIGHTEEN_C = 25 --'Gastroenterology' 
                         then 'Medicine - Gastro IP'
                     when har.ACCT_BASECLS_HA_C = 1
                         and gen_data.genAMT > 0
                         and har.ACCT_CLASS_HA_C not in
                         (
                             107
                           , 117
                           , 128
                           , 129
                         )
                         and grp18.RPT_GRP_EIGHTEEN_C = 39 --'Hematology-Medical Oncology'
                         then 'Medicine - Hem IP'
                     when (
                              har.ACCT_BASECLS_HA_C = 1
                              and datediff(month, pat.BIRTH_DATE, har.ADM_DATE_TIME) >= 1
                              and gen_data.genAMT > 0
                              and har.ACCT_CLASS_HA_C in (101)
                              and grp18.RPT_GRP_EIGHTEEN_C in
                         (
                             29
                           , 62
                           , 14
                           , 30
                           , 94
                         )
                              --('General Pediatrics','Pediatric Subspecialties','Critical Care Medicine','General Pediatrics/Critical Care Medicine','Pediatric Emergency Medicine') 
                              and grp17.RPT_GRP_SEVNTEEN_C = (8) --('Department of Pediatrics') )-- added Critical Care Medicine on 2015-09-15 Paul b
                              or grp18_99.RPT_GRP_EIGHTEEN_C in
                         (
                             29
                           , 62
                           , 14
                           , 30
                           , 94
                         )
                              --('General Pediatrics','Pediatric Subspecialties','Critical Care Medicine','General Pediatrics/Critical Care Medicine','Pediatric Emergency Medicine') 
                              and grp17_99.RPT_GRP_SEVNTEEN_C = (8)
                          ) --('Department of Pediatrics') )-- added Critical Care Medicine on 2015-09-15 Paul b
                         or (
                                datediff(month, pat.BIRTH_DATE, har.ADM_DATE_TIME) >= 1
                                and har.ACCT_BASECLS_HA_C = 1
                                and isnull(gen_data.genAMT, 0) > 0
                                and har.ACCT_CLASS_HA_C in (101)
                                and grp18.RPT_GRP_EIGHTEEN_C in
                         (
                             29
                           , 62
                           , 29
                           , 94
                         )
                                --('General Pediatrics','Pediatric Subspecialties','General Pediatrics','Pediatric Emergency Medicine') 
                                and grp17.RPT_GRP_SEVNTEEN_C = (8)
                            ) --('Department of Pediatrics') )
                         or (
                                datediff(day, pat.BIRTH_DATE, har.ADM_DATE_TIME) >= 1
                                and har.ACCT_BASECLS_HA_C = 1
                                and isnull(gen_data.genAMT, 0) > 0
                                and har.ACCT_CLASS_HA_C in (101)
                                and grp18.RPT_GRP_EIGHTEEN_C in
                         (
                             29
                           , 62
                           , 29
                           , 94
                         )
                                --('General Pediatrics','Pediatric Subspecialties','General Pediatrics','Pediatric Emergency Medicine') 
                                and grp17.RPT_GRP_SEVNTEEN_C = (8)
                            ) --('Department of Pediatrics'))
                         then 'Medicine - Peds Floor IP'
                     when har.ACCT_BASECLS_HA_C = 1
                         and gen_data.genAMT > 0
                         and har.ACCT_CLASS_HA_C not in
                         (
                             107
                           , 129
                         )
                         and grp18.RPT_GRP_EIGHTEEN_C = 40 --'Infectious Diseases'
                         then 'Medicine - Infectious IP'
                     when har.ACCT_BASECLS_HA_C = 1
                         and gen_data.genAMT > 0
                         and har.ACCT_CLASS_HA_C not in
                         (
                             107
                           , 129
                         )
                         and grp18.RPT_GRP_EIGHTEEN_C = 43 --'Nephrology'
                         then 'Medicine - Neph IP'
                     when har.ACCT_BASECLS_HA_C = 1
                         and gen_data.genAMT > 0
                         and har.ACCT_CLASS_HA_C not in
                         (
                             107
                           , 129
                         )
                         and grp18.RPT_GRP_EIGHTEEN_C = 46 --'Neurology'
                         then 'Medicine - Neuro IP'
                     when har.ACCT_BASECLS_HA_C = 1
                         and gen_data.genAMT > 0
                         and har.ACCT_CLASS_HA_C not in
                         (
                             107
                           , 129
                         )
                         and grp18.RPT_GRP_EIGHTEEN_C in
                         (
                             73
                           , 74
                           , 15
                         ) --('Pulmonary','Pulmonary/Critical Care Medicine', 'Critical Care Medicine/Pulmonary')
                         then 'Medicine - Pulm IP'
                     when har.ACCT_BASECLS_HA_C = 1
                         and gen_data.genAMT > 0
                         and har.ACCT_CLASS_HA_C not in
                         (
                             107
                           , 129
                         )
                         and grp18.RPT_GRP_EIGHTEEN_C in (9) --('CARDIOTHORACIC SURGERY')
                         then 'Surgery Cardio-Thorac IP'
                     when har.ACCT_BASECLS_HA_C = 1
                         and gen_data.genAMT > 0
                         and har.ACCT_CLASS_HA_C not in
                         (
                             107
                           , 129
                         )
                         and grp18_99.RPT_GRP_EIGHTEEN_C in (9) --('CARDIOTHORACIC SURGERY')
                         then 'Surgery Cardio-Thorac IP'
                     when har.ACCT_BASECLS_HA_C = 1
                         and gen_data.genAMT > 0
                         and har.ACCT_CLASS_HA_C not in
                         (
                             107
                           , 129
                         )
                         and grp18.RPT_GRP_EIGHTEEN_C in (11) --('Colon and Rectal Surgery')
                         then 'Surgery Colon and Rectal Surgery'
                     when har.ACCT_BASECLS_HA_C = 1
                         and gen_data.genAMT > 0
                         and har.ACCT_CLASS_HA_C not in
                         (
                             107
                           , 129
                         )
                         and grp18.RPT_GRP_EIGHTEEN_C = 31 --('General Surgery')
                         then 'Surgery General'
                     when har.ACCT_BASECLS_HA_C = 1
                         and gen_data.genAMT > 0
                         and har.ACCT_CLASS_HA_C not in
                         (
                             107
                           , 129
                         )
                         and grp18.RPT_GRP_EIGHTEEN_C in
                         (
                             5
                           , 32
                           , 79
                           , 80
                           , 96
                         )
                         then 'Surgery Trauma'
                     when har.ACCT_BASECLS_HA_C = 1
                         and gen_data.genAMT > 0
                         and har.ACCT_CLASS_HA_C not in
                         (
                             107
                           , 129
                         )
                         and grp18_99.RPT_GRP_EIGHTEEN_C in
                         (
                             5
                           , 32
                           , 79
                           , 80
                           , 96
                         )
                         then 'Surgery Trauma'
                     when har.ACCT_BASECLS_HA_C = 1
                         and gen_data.genAMT > 0
                         and har.ACCT_CLASS_HA_C not in
                         (
                             107
                           , 129
                         )
                         and grp18.RPT_GRP_EIGHTEEN_C in
                         (
                             44
                           , 45
                           , 77
                           , 93
                         ) --('Neurological Surgery','Neurological Surgery/Spine Surgery','Spine Surgery/Neurological Surgery','Neurosurgery')  
                         then 'Surgery Neuro IP'
                     when har.ACCT_BASECLS_HA_C = 1
                         and gen_data.genAMT > 0
                         and har.ACCT_CLASS_HA_C not in
                         (
                             107
                           , 129
                         )
                         and grp18.RPT_GRP_EIGHTEEN_C in (52) --('Oral and Maxillofacial Surgery')
                         then 'Surgery Oral IP'
                     when har.ACCT_BASECLS_HA_C = 1
                         and gen_data.genAMT > 0
                         and har.ACCT_CLASS_HA_C not in
                         (
                             107
                           , 129
                         )
                         and grp18.RPT_GRP_EIGHTEEN_C in
                         (
                             38
                           , 54
                           , 55
                           , 56
                           , 78
                         )
                         --('Hand Surgery/Orthopedic Surgery','Orthopedic Surgery','Orthopedic Surgery/Hand Surgery','Orthopedic Surgery/Spine Surgery','Spine Surgery/Orthopedic Surgery')
                         then 'Surgery Ortho IP'
                     when har.ACCT_BASECLS_HA_C = 1
                         and gen_data.genAMT > 0
                         and har.ACCT_CLASS_HA_C not in
                         (
                             107
                           , 129
                         )
                         and grp18.RPT_GRP_EIGHTEEN_C in (57) --('Otolaryngology-Head & Neck Surgery')
                         then 'Surgery Otal IP'
                     when har.ACCT_BASECLS_HA_C = 1
                         and gen_data.genAMT > 0
                         and har.ACCT_CLASS_HA_C not in
                         (
                             107
                           , 129
                         )
                         and grp18.RPT_GRP_EIGHTEEN_C in
                         (
                             6
                           , 66
                           , 67
                           , 68
                         )
                         --('Burn/Trauma-Surgical Critical Care/Plastic Surgery','Plastic Surgery','Plastic Surgery/Hand Surgery','Plastic Surgery/Hand Surgery/Burn')
                         then 'Surgery Plastic IP'
                     when har.ACCT_BASECLS_HA_C = 1
                         and gen_data.genAMT > 0
                         and har.ACCT_CLASS_HA_C not in
                         (
                             107
                           , 129
                         )
                         and grp18.RPT_GRP_EIGHTEEN_C in (69) --('Podiatric Surgery')
                         then 'Surgery Podiatric IP'
                     when
                     (
                         har.ACCT_BASECLS_HA_C = 1
                         and gen_data.genAMT > 0
                         and har.ACCT_CLASS_HA_C not in
                         (
                             107
                           , 129
                         )
                         and grp17.RPT_GRP_SEVNTEEN_C = 12
                         and grp18.RPT_GRP_EIGHTEEN_C in (82) --17 'Department of Surgery' 18 ('Urology')
                         or har.ACCT_BASECLS_HA_C = 1
                         and gen_data.genAMT > 0
                         and har.ACCT_CLASS_HA_C not in
                         (
                             107
                           , 129
                         )
                         and grp17_99.RPT_GRP_SEVNTEEN_C = 12
                         and grp18_99.RPT_GRP_EIGHTEEN_C in (82)
                     ) --17 'Department of Surgery' 18 ('Urology')
                         then 'Surgery Urology IP'
                     when har.ACCT_BASECLS_HA_C = 1
                         and gen_data.genAMT > 0
                         and har.ACCT_CLASS_HA_C not in
                         (
                             107
                           , 129
                         )
                         and grp17.RPT_GRP_SEVNTEEN_C = 12
                         and grp18.RPT_GRP_EIGHTEEN_C in (83) --17 'Department of Surgery' 18 ('Vascular and Endovascular Surgery')
                         then 'Surgery Vascular IP'
                     when har.ACCT_BASECLS_HA_C = 1
                         and gen_data.genAMT > 0
                         and har.ACCT_CLASS_HA_C not in
                         (
                             107
                           , 129
                         )
                         and grp17.RPT_GRP_SEVNTEEN_C = 12 --'Department of Surgery' 
                         and grp18.RPT_GRP_EIGHTEEN_C not in
                         (
                             9
                           , 11
                           , 32
                           , 38
                           , 45
                           , 52
                           , 54
                           , 55
                           , 56
                           , 57
                           , 63
                           , 69
                           , 78
                           , 82
                           , 83
                         )
                         --      ('CARDIOTHORACIC SURGERY','Colon and Rectal Surgery','General Surgery/Trauma-Surgical Critical Care','Hand Surgery/Orthopedic Surgery','Neurological Surgery/Spine Surgery','Oral and Maxillofacial Surgery','Orthopedic Surgery'
                         --      ,'Orthopedic Surgery/Hand Surgery','Orthopedic Surgery/Spine Surgery','Otolaryngology-Head & Neck Surgery','Pediatric Surgical Specialties','Podiatric Surgery','Spine Surgery/Orthopedic Surgery','Urology','Vascular and Endovascular Surgery')
                         then 'Surgery All Other IP'
                     when har.ACCT_BASECLS_HA_C = 1
                         and gen_data.genAMT > 0
                         and har.ACCT_CLASS_HA_C not in
                         (
                             107
                           , 129
                         )
                         and grp17.RPT_GRP_SEVNTEEN_C = 9 --'Department of Psychiatry'
                         then 'Psych IP'
                     when har.ACCT_BASECLS_HA_C = 1
                         and har.ACCT_CLASS_HA_C not in
                         (
                             107
                           , 129
                         )
                         and (
                                 grp17.RPT_GRP_SEVNTEEN_C = 6
                                 or grp17_99.RPT_GRP_SEVNTEEN_C = 6
                             ) --'Department of Obstetrics & Gynecology' or 'Department of Obstetrics & Gynecology'
                         and drg.DRG_NUMBER in
                         (
                             'CMS370'
                           , 'CMS371'
                           , 'CMS372'
                           , 'CMS373'
                           , 'CMS374'
                           , 'CMS375'
                           , 'MS765'
                           , 'MS766'
                           , 'MS767'
                           , 'MS768'
                           , 'MS774'
                           , 'MS775'
                           , 'APR540'
                           , 'APR541'
                           , 'APR542'
                           , 'APR560'
                         )
                         then 'OBGYN - Deliveries IP'
                     when har.ACCT_BASECLS_HA_C = 1
                         and har.ACCT_CLASS_HA_C not in
                         (
                             107
                           , 129
                         )
                         and grp17.RPT_GRP_SEVNTEEN_C = 4 --'Department of Family Medicine' 
                         and drg.DRG_NUMBER in
                         (
                             'CMS370'
                           , 'CMS371'
                           , 'CMS372'
                           , 'CMS373'
                           , 'CMS374'
                           , 'CMS375'
                           , 'MS765'
                           , 'MS766'
                           , 'MS767'
                           , 'MS768'
                           , 'MS774'
                           , 'MS775'
                           , 'APR540'
                           , 'APR541'
                           , 'APR542'
                           , 'APR560'
                         )
                         then 'OBGYN - Deliveries - Family Practice IP'
                     when del.OB_DELIVERY_DATE is not null
                         then 'OBGYN - Unassign Deliveries IP' --Added per Janine Barynak required to increase Delivery totals
                     when har.ACCT_BASECLS_HA_C = 1
                         and har.ACCT_CLASS_HA_C not in
                         (
                             107
                           , 129
                         )
                         and grp17.RPT_GRP_SEVNTEEN_C = 6 --'Department of Obstetrics & Gynecology'
                         and drg.DRG_NUMBER in
                         (
                             'CMS376'
                           , 'CMS377'
                           , 'CMS378'
                           , 'CMS379'
                           , 'CMS382'
                           , 'CMS383'
                           , 'CMS384'
                           , 'MS769'
                           , 'MS776'
                           , 'MS777'
                           , 'MS778'
                           , 'MS780'
                           , 'MS781'
                           , 'MS782'
                           , 'APR544'
                           , 'APR545'
                           , 'APR546'
                           , 'APR561'
                           , 'APR563'
                           , 'APR564'
                           , 'APR565'
                           , 'APR566'
                         )
                         then 'OBGYN - Obstetrics IP'
                     when (
                              har.ACCT_BASECLS_HA_C = 1
                              and har.ACCT_CLASS_HA_C not in
                         (
                             107
                           , 129
                         )
                              and grp17.RPT_GRP_SEVNTEEN_C = 6
                          ) --'Department of Obstetrics & Gynecology')
                         or (
                                har.ACCT_BASECLS_HA_C = 1
                                and har.ACCT_CLASS_HA_C not in
                         (
                             107
                           , 129
                         )
                                and (grp18.RPT_GRP_EIGHTEEN_C in
                         (
                             41
                           , 35
                         )
                                    )
                                or grp17_99.RPT_GRP_SEVNTEEN_C = 6
                                or grp18_99.RPT_GRP_EIGHTEEN_C = 50
                            ) --17 'Department of Obstetrics & Gynecology' 18 'Obstetrics/Maternal-Fetal Medicine')
                         and (
                                 drg.DRG_NUMBER in
                         (
                             'MS734'
                           , 'MS735'
                           , 'MS736'
                           , 'MS737'
                           , 'MS738'
                           , 'MS739'
                           , 'MS740'
                           , 'MS741'
                           , 'MS742'
                           , 'MS743'
                           , 'MS744'
                           , 'MS745'
                           , 'MS746'
                           , 'MS747'
                           , 'MS781' --DD PER DIAS 20151113
                           , 'MS748'
                           , 'MS749'
                           , 'MS750'
                           , 'MS754'
                           , 'MS755'
                           , 'MS756'
                           , 'MS757'
                           , 'MS758'
                           , 'MS759'
                           , 'MS760'
                           , 'MS761'
                           , 'APR510'
                           , 'APR511'
                           , 'APR512'
                           , 'APR513'
                           , 'APR514'
                           , 'APR517'
                           , 'APR518'
                           , 'APR519'
                           , 'APR530'
                           , 'APR531'
                           , 'APR532'
                         )
                                 or drg.DRG_NUMBER between 'CMS353' and 'CMS369'
                             )
                         then 'OBGYN - Gynecology IP'          --MFM or Gynecology
                     when har.ACCT_BASECLS_HA_C = 1
                         and har.ACCT_CLASS_HA_C not in
                         (
                             107
                           , 129
                         )
                         and grp17.RPT_GRP_SEVNTEEN_C = 6 --'Department of Obstetrics & Gynecology' 
                         and (
                                 drg.DRG_NUMBER not in
                         (
                             'CMS370'
                           , 'CMS371'
                           , 'CMS372'
                           , 'CMS373'
                           , 'CMS374'
                           , 'CMS375'
                           , 'CMS376'
                           , 'CMS377'
                           , 'CMS378'
                           , 'CMS379'
                           , 'CMS382'
                           , 'CMS383'
                           , 'CMS384'
                           , 'MS765'
                           , 'MS766'
                           , 'MS767'
                           , 'MS768'
                           , 'MS774'
                           , 'MS775'
                           , 'MS769'
                           , 'MS776'
                           , 'MS777'
                           , 'MS778'
                           , 'MS780'
                           , 'MS781'
                           , 'MS782'
                           , 'MS734'
                           , 'MS735'
                           , 'MS736'
                           , 'MS737'
                           , 'MS738'
                           , 'MS739'
                           , 'MS740'
                           , 'MS741'
                           , 'MS742'
                           , 'MS743'
                           , 'MS744'
                           , 'MS745'
                           , 'MS746'
                           , 'MS747'
                           , 'MS748'
                           , 'MS749'
                           , 'MS750'
                           , 'MS754'
                           , 'MS755'
                           , 'MS756'
                           , 'MS757'
                           , 'MS758'
                           , 'MS759'
                           , 'MS760'
                           , 'MS761'
                           , 'APR540'
                           , 'APR541'
                           , 'APR542'
                           , 'APR560'
                           , 'APR544'
                           , 'APR545'
                           , 'APR546'
                           , 'APR561'
                           , 'APR563'
                           , 'APR564'
                           , 'APR565'
                           , 'APR566'
                           , 'APR510'
                           , 'APR511'
                           , 'APR512'
                           , 'APR513'
                           , 'APR514'
                           , 'APR517'
                           , 'APR518'
                           , 'APR519'
                           , 'APR530'
                           , 'APR531'
                           , 'APR532'
                         )
                                 or drg.DRG_NUMBER between 'CMS353' and 'CMS369'
                             )
                         then 'L OBGYN - All Other IP'
                     when har.ACCT_BASECLS_HA_C = 1
                         and gen_data.genAMT > 0
                         and har.ACCT_CLASS_HA_C not in
                         (
                             117
                           , 129
                         )
                         and har.FINAL_DRG_ID is not null
                         and (
                                 grp17.RPT_GRP_SEVNTEEN_C not in
                         (
                             4
                           , 9
                           , 5
                           , 8
                           , 12
                           , 6
                           , 3
                         )       -- 'Department of Family Medicine','Department of Psychiatry','Department of Medicine','Department of Pediatrics','Department of Surgery','Department of Obstetrics & Gynecology','Department of Emergency Medicine','Emergency and Hospital Medicine') 
                                 and har.FINAL_DRG_ID is not null
                             )
                         then 'Misc IP'
                     when har.ACCT_BASECLS_HA_C = 1
                         and gen_data.genAMT > 0
                         and har.ACCT_CLASS_HA_C not in (129)
                         and grp17.RPT_GRP_SEVNTEEN_C = 6
                         and har.FINAL_DRG_ID is null --'Department of Obstetrics & Gynecology' 
                         then 'L OBGYN - Not Yet Asssigned IP'
                     when
                     (
                         har.ACCT_BASECLS_HA_C = 1
                         and gen_data.genAMT > 0
                         and har.ACCT_CLASS_HA_C not in
                         (
                             107
                           , 117
                           , 129
                         )
                         and grp18.RPT_GRP_EIGHTEEN_C in
                         (
                             27
                           , 28
                           , 92
                         ) --('General Internal Medicine','General Internal Medicine/Geriatrics', 'General Internal Medicine/Hospital Medicine') 
                         or (
                                grp17.RPT_GRP_SEVNTEEN_C in
                         (
                             3
                           , 5
                         )      --('Department of Emergency Medicine','Department of Medicine') 
                                and grp18.RPT_GRP_EIGHTEEN_C not in (65)
                            ) --('Physical Medicine-Rehabilitation')) 
                         or isnull(grp18.name, 'Department of Medicine') in ('Department of Medicine')
                         and (grp17.RPT_GRP_SEVNTEEN_C in (5)) --('Department of Medicine'))
                         and har.ACCT_BASECLS_HA_C = 1
                         and gen_data.genAMT > 0
                         or grp17.RPT_GRP_SEVNTEEN_C in (3)
                         and har.ACCT_BASECLS_HA_C = 1
                         and gen_data.genAMT > 0
                         and har.ACCT_CLASS_HA_C not in
                         (
                             129
                           , 128
                           , 131
                         ) --('Department of Emergency Medicine')
                         or grp17_99.RPT_GRP_SEVNTEEN_C in (3)
                         and har.ACCT_BASECLS_HA_C = 1
                         and gen_data.genAMT > 0
                         and har.ACCT_CLASS_HA_C not in
                         (
                             129
                           , 128
                           , 131
                         ) --('Emergency and Hospital Medicine')
                         or har.ACCT_BASECLS_HA_C = 1
                         and gen_data.genAMT > 0
                         and har.ACCT_CLASS_HA_C not in
                         (
                             107
                           , 117
                           , 129
                         )
                         and grp18.RPT_GRP_EIGHTEEN_C in
                         (
                             27
                           , 92
                         ) --('General Internal Medicine','General Internal Medicine/Hospital Medicine') 
                         or har.ACCT_BASECLS_HA_C = 1
                         and gen_data.genAMT > 0
                         and grp17_99.RPT_GRP_SEVNTEEN_C = 5
                         and grp18_99.RPT_GRP_EIGHTEEN_C in
                         (
                             27
                           , 92
                         )
                     ) -- 17 'Department of Medicine'  18 ('General Internal Medicine'))
                         then 'Medicine - General IP'
                     when har.ACCT_BASECLS_HA_C = 1
                         and gen_data.genAMT > 0
                         and har.ACCT_CLASS_HA_C not in
                         (
                             107
                           , 117
                           , 129
                         )
                         and (
                                 grp17.RPT_GRP_SEVNTEEN_C = 10
                                 or grp18.RPT_GRP_EIGHTEEN_C in
                         (
                             27
                           , 33
                         )
                             ) --17 'Department of Radiation Oncology' 18 ('General Internal Medicine','Geriatrics/General Internal Medicine','General Internal Medicine/Hospital Medicine'))
                         then 'Medicine - All Other IP'
                     else
                         null
                 end
        when loc.RPT_GRP_SIX = 2 -- LVHM
            then case
                     when har.ACCT_BASECLS_HA_C = 1
                         and har.ACCT_CLASS_HA_C in (133)
                         or grp18.RPT_GRP_EIGHTEEN_C = 65 --'Physical Medicine-Rehabilitation' 
                         then 'M IRF'                                     ----newly added 6/21/17 for M Tower opening - CJG
                     when har.ACCT_BASECLS_HA_C = 1
                         and (
                                 ped_ip_surgery.Peds_IP_Sur_AMT > 0
                                 and isnull(PEDS_picu_ip.PICU_IP_AMT, 0) = 0
                                 and isnull(PEDS_picu_ip.PICU_IP_AMT, 0) = 0
                             )
                         and grp17.RPT_GRP_SEVNTEEN_C = 12
                         and grp18.RPT_GRP_EIGHTEEN_C in
                         (
                             63
                           , 95
                         ) --17 'Department of Surgery'    18 'Pediatric Surgical Specialties','95 Pediatric Surgical Specialties/Otolaryngology-Head & Neck Surgery ' 
                         then 'M Peds - IP Surgery'
                     when har.ACCT_BASECLS_HA_C = 1
                         and pednicip.NICU_IP_AMT > 0
                         and har.ACCT_CLASS_HA_C <> 107                         
                         then 'M Peds - Nicu Ip'
                     when har.ACCT_BASECLS_HA_C = 1
                         and har.ACCT_CLASS_HA_C = 107
                         then '0.M New Born'
                     when har.ACCT_BASECLS_HA_C = 1
                         and har.ACCT_CLASS_HA_C = 101
                         and pat.BIRTH_DATE = har.ADM_DATE_TIME
                         then '0.M New Born 2'
                     when datediff(minute, pat.BIRTH_DATE, har.ADM_DATE_TIME) between 1 and 1200
                         and har.ACCT_BASECLS_HA_C = 1
                         and har.ACCT_CLASS_HA_C = 101
                         and pat.BIRTH_DATE < har.ADM_DATE_TIME
                         then '0.M New Born 3'
                     when datediff(minute, pat.BIRTH_DATE, har.ADM_DATE_TIME) between -120 and -1
                         and har.ACCT_BASECLS_HA_C = 1
                         and har.ACCT_CLASS_HA_C = 101
                         then '0.M New Born 3'
                     when grp18.RPT_GRP_EIGHTEEN_C = 75
                         then 'M Medicine - Endo  IP'
                     when grp17.RPT_GRP_SEVNTEEN_C = 4
                         and ser.PROV_ID <> 'J1971'
                         then 'M FAMILY Medicine IP'
                     when grp17.RPT_GRP_SEVNTEEN_C = 4
                         and ser.PROV_ID = 'J1971'
                         then 'M Family Medicine Classified as Medince IP'
                     when har.ACCT_BASECLS_HA_C = 1
                         and grp18.RPT_GRP_EIGHTEEN_C = 3
                         then 'M MEDICINE ALLERPY IP'
                     when grp18.RPT_GRP_EIGHTEEN_C = 51
                         then 'M Surgery - Opht IP'
                     when har.ACCT_BASECLS_HA_C = 1
                         and pednicip.NICU_IP_AMT > 0
                         and har.ACCT_CLASS_HA_C <> 107
                         then 'M Peds - Nicu Ip'
                     when har.ACCT_BASECLS_HA_C = 1
                         and PEDS_picu_ip.PICU_IP_AMT > 0
                         then 'M Peds - PICU IP'
                     when har.ACCT_BASECLS_HA_C = 1
                         and FAM_MED.FammedAMT > 0
                         and har.ACCT_CLASS_HA_C <> 129
                         and grp17.RPT_GRP_SEVNTEEN_C = 4 --'DEPARTMENT OF FAMILY MEDICINE' 
                         then 'M Family Medicine IP'
                     when har.ACCT_BASECLS_HA_C = 1
                         and gen_data.genAMT > 0
                         and har.ACCT_CLASS_HA_C not in
                         (
                             107
                           , 129
                         )
                         and grp18.RPT_GRP_EIGHTEEN_C = 84
                         or ser.PROV_ID = 'S7587' --'Cardiology' 
                         then 'M Medicine - Cardio IP'
                     when har.ACCT_BASECLS_HA_C = 1
                         and gen_data.genAMT > 0
                         and har.ACCT_CLASS_HA_C not in
                         (
                             107
                           , 117
                           , 129
                         )
                         and grp18.RPT_GRP_EIGHTEEN_C = 25 --'Gastroenterology' 
                         then 'M Medicine - Gastro IP'
                     when har.ACCT_BASECLS_HA_C = 1
                         and gen_data.genAMT > 0
                         and har.ACCT_CLASS_HA_C not in
                         (
                             107
                           , 117
                           , 129
                         )
                         and grp18.RPT_GRP_EIGHTEEN_C in
                         (
                             27
                           , 92
                         )
                         or ( --('General Internal Medicine','General Internal Medicine/Hospital Medicine') 
                                grp18.RPT_GRP_EIGHTEEN_C is null
                                and grp17.RPT_GRP_SEVNTEEN_C = 5
                            ) --'Department of Medicine') 
                         or (
                                grp18.RPT_GRP_EIGHTEEN_C in
                         (
                             27
                           , 92
                         )      /*('General Internal Medicine','General Internal Medicine/Hospital Medicine') */
                                and grp17.RPT_GRP_SEVNTEEN_C = 5
                                and gen_data.genAMT > 0
                            ) --17 'Department of Medicine'
                         or grp17.RPT_GRP_SEVNTEEN_C = 3
                         and har.ACCT_BASECLS_HA_C = 1
                         and gen_data.genAMT > 0
                         and har.ACCT_CLASS_HA_C not in
                         (
                             107
                           , 117
                           , 129
                         ) --17 'Department of Emergency Medicine' 
                         or grp17.RPT_GRP_SEVNTEEN_C = 3
                         and har.ACCT_BASECLS_HA_C = 1
                         and gen_data.genAMT > 0
                         and har.ACCT_CLASS_HA_C not in
                         (
                             107
                           , 117
                           , 129
                         ) --17 'Department of Emergency Medicine' 
                         or ser.PROV_ID in
                         (
                             '19477'
                           , 'G5383'
                         )
                         or grp17.RPT_GRP_SEVNTEEN_C = 5 /*Department of Medicine*/
                         and grp18.RPT_GRP_EIGHTEEN_C = 85 --Hospital Medicine
                         then 'M Medicine - General IP'
                     when har.ACCT_BASECLS_HA_C = 1
                         and gen_data.genAMT > 0
                         and har.ACCT_CLASS_HA_C not in
                         (
                             107
                           , 117
                           , 128
                           , 129
                         )
                         and grp18.RPT_GRP_EIGHTEEN_C = 39 --'Hematology-Medical Oncology'
                         then 'M Medicine - Hem IP'
                     when har.ACCT_BASECLS_HA_C = 1
                         and gen_data.genAMT > 0
                         and har.ACCT_CLASS_HA_C not in
                         (
                             107
                           , 129
                         )
                         and grp18.RPT_GRP_EIGHTEEN_C = 40 --'Infectious Diseases'
                         then 'Medicine - Infectious IP'
                     when har.ACCT_BASECLS_HA_C = 1
                         and gen_data.genAMT > 0
                         and har.ACCT_CLASS_HA_C not in
                         (
                             107
                           , 129
                         )
                         and grp18.RPT_GRP_EIGHTEEN_C = 43 --'Nephrology'
                         then 'M Medicine - Neph IP'
                     when har.ACCT_BASECLS_HA_C = 1
                         and gen_data.genAMT > 0
                         and har.ACCT_CLASS_HA_C not in
                         (
                             107
                           , 129
                         )
                         and grp18.RPT_GRP_EIGHTEEN_C = 46 --'Neurology'
                         then 'M Medicine - Neuro IP'
                     when har.ACCT_BASECLS_HA_C = 1
                         and gen_data.genAMT > 0
                         and har.ACCT_CLASS_HA_C not in
                         (
                             107
                           , 129
                         )
                         and grp18.RPT_GRP_EIGHTEEN_C in
                         (
                             73
                           , 74
                           , 15
                         ) --('Pulmonary','Pulmonary/Critical Care Medicine', 'Critical Care Medicine/Pulmonary')
                         then 'M Medicine - Pulm IP'
                     when har.ACCT_BASECLS_HA_C = 1
                         and gen_data.genAMT > 0
                         and har.ACCT_CLASS_HA_C not in
                         (
                             107
                           , 117
                           , 129
                         )
                         and grp18.RPT_GRP_EIGHTEEN_C in
                         (
                             27
                           , 28
                           , 33
                         ) --('General Internal Medicine','General Internal Medicine/Geriatrics','Geriatrics/General Internal Medicine')
                         then 'M Medicine - All Other IP'
                     when har.ACCT_BASECLS_HA_C = 1
                         and gen_data.genAMT > 0
                         and har.ACCT_CLASS_HA_C not in
                         (
                             107
                           , 129
                         )
                         and grp18.RPT_GRP_EIGHTEEN_C in (9) --('CARDIOTHORACIC SURGERY')
                         then 'M Surgery Cardio-Thorac IP'
                     when har.ACCT_BASECLS_HA_C = 1
                         and gen_data.genAMT > 0
                         and har.ACCT_CLASS_HA_C not in
                         (
                             107
                           , 129
                         )
                         and grp18.RPT_GRP_EIGHTEEN_C in (11)
                         or har.ATTENDING_PROV_ID = 'P5632' --('Colon and Rectal Surgery') 
                         then 'M Surgery Colon and Rectal Surgery'
                     when har.ACCT_BASECLS_HA_C = 1
                         and gen_data.genAMT > 0
                         and har.ACCT_CLASS_HA_C not in
                         (
                             107
                           , 129
                         )
                         and grp18.RPT_GRP_EIGHTEEN_C in
                         (
                             32
                           , 31
                           , 69
                         ) --('General Surgery/Trauma-Surgical Critical Care','General Surgery','Podiatric Surgery')
                         then 'M Surgery General IP'
                     when har.ACCT_BASECLS_HA_C = 1
                         and gen_data.genAMT > 0
                         and har.ACCT_CLASS_HA_C not in
                         (
                             107
                           , 129
                         )
                         and grp18.RPT_GRP_EIGHTEEN_C in
                         (
                             44
                           , 45
                           , 46
                           , 77
                           , 93
                         ) --('Neurological Surgery','Neurological Surgery/Spine Surgery','Neurology','Spine Surgery/Neurological Surgery','Neurosurgery')  
                         then 'M Surgery Neuro IP'
                     when har.ACCT_BASECLS_HA_C = 1
                         and gen_data.genAMT > 0
                         and har.ACCT_CLASS_HA_C not in
                         (
                             107
                           , 129
                         )
                         and grp18.RPT_GRP_EIGHTEEN_C in (52) --('Oral and Maxillofacial Surgery')  
                         then 'M Surgery Oral IP'
                     when har.ACCT_BASECLS_HA_C = 1
                         and gen_data.genAMT > 0
                         and har.ACCT_CLASS_HA_C not in
                         (
                             107
                           , 129
                         )
                         and grp18.RPT_GRP_EIGHTEEN_C in
                         (
                             38
                           , 54
                           , 55
                           , 56
                           , 78
                         ) --('Hand Surgery/Orthopedic Surgery','Orthopedic Surgery','Orthopedic Surgery/Hand Surgery','Orthopedic Surgery/Spine Surgery','Spine Surgery/Orthopedic Surgery')  
                         then 'M Surgery Ortho IP'
                     when har.ACCT_BASECLS_HA_C = 1
                         and gen_data.genAMT > 0
                         and har.ACCT_CLASS_HA_C not in
                         (
                             107
                           , 129
                         )
                         and grp18.RPT_GRP_EIGHTEEN_C in (57) --('Otolaryngology-Head & Neck Surgery')  
                         then 'M Surgery Otal IP'
                     when har.ACCT_BASECLS_HA_C = 1
                         and gen_data.genAMT > 0
                         and har.ACCT_CLASS_HA_C not in
                         (
                             107
                           , 129
                         )
                         and grp18.RPT_GRP_EIGHTEEN_C in
                         (
                             6
                           , 66
                           , 67
                           , 68
                         ) --('Burn/Trauma-Surgical Critical Care/Plastic Surgery','Plastic Surgery','Plastic Surgery/Hand Surgery','Plastic Surgery/Hand Surgery/Burn')  
                         then 'M Surgery Plastic IP'
                     when har.ACCT_BASECLS_HA_C = 1
                         and gen_data.genAMT > 0
                         and har.ACCT_CLASS_HA_C not in
                         (
                             107
                           , 129
                         )
                         and grp17.RPT_GRP_SEVNTEEN_C = 12
                         and grp18.RPT_GRP_EIGHTEEN_C in (82) --17 'Department of Surgery' 18 ('Urology')  
                         then 'M Surgery Urology IP'
                     when har.ACCT_BASECLS_HA_C = 1
                         and gen_data.genAMT > 0
                         and har.ACCT_CLASS_HA_C not in
                         (
                             107
                           , 129
                         )
                         and grp17.RPT_GRP_SEVNTEEN_C = 12
                         and grp18.RPT_GRP_EIGHTEEN_C in (83) --17 'Department of Surgery' 18 ('Vascular and Endovascular Surgery')
                         then 'M Surgery Vascular IP'
                     when har.ACCT_BASECLS_HA_C = 1
                         and gen_data.genAMT > 0
                         and har.ACCT_CLASS_HA_C not in
                         (
                             107
                           , 129
                         )
                         and grp17.RPT_GRP_SEVNTEEN_C = 12
                         and grp18.RPT_GRP_EIGHTEEN_C not in
                         (
                             9
                           , 11
                           , 27
                           , 32
                           , 38
                           , 45
                           , 51
                           , 52
                           , 54
                           , 55
                           , 56
                           , 57
                           , 69
                           , 78
                           , 82
                           , 83
                         )
                         --17 'Department of Surgery' 18 ('CARDIOTHORACIC SURGERY','Colon and Rectal Surgery','General Surgery','General Surgery/Trauma-Surgical Critical Care','Hand Surgery/Orthopedic Surgery','Neurological Surgery/Spine Surgery','Ophthalmology','Oral and Maxillofacial Surgery','Orthopedic Surgery'
                         --,'Orthopedic Surgery/Hand Surgery','Orthopedic Surgery/Spine Surgery','Otolaryngology-Head & Neck Surgery','Podiatric Surgery','Spine Surgery/Orthopedic Surgery','Urology','Vascular and Endovascular Surgery')
                         then 'M Surgery All Other IP'
                     when har.ACCT_BASECLS_HA_C = 1
                         and gen_data.genAMT > 0
                         and har.ACCT_CLASS_HA_C not in
                         (
                             107
                           , 129
                         )
                         and grp17.RPT_GRP_SEVNTEEN_C = 9 --'Department of Psychiatry'
                         then 'M Psych IP'
                     when har.ACCT_BASECLS_HA_C = 1
                         and har.ACCT_CLASS_HA_C not in
                         (
                             107
                           , 129
                         )
                         and (
                                 grp17.RPT_GRP_SEVNTEEN_C = 6
                                 or grp17_99.RPT_GRP_SEVNTEEN_C = 6
                             ) --'Department of Obstetrics & Gynecology' or 'Department of Obstetrics & Gynecology'
                         and drg.DRG_NUMBER in
                         (
                             'CMS370'
                           , 'CMS371'
                           , 'CMS372'
                           , 'CMS373'
                           , 'CMS374'
                           , 'CMS375'
                           , 'MS765'
                           , 'MS766'
                           , 'MS767'
                           , 'MS768'
                           , 'MS774'
                           , 'MS775'
                           , 'APR540'
                           , 'APR541'
                           , 'APR542'
                           , 'APR560'
                         )
                         then 'M OBGYN - Deliveries IP'                   ----newly added 6/21/17 for M Tower opening - CJG
                     when har.ACCT_BASECLS_HA_C = 1
                         and har.ACCT_CLASS_HA_C not in
                         (
                             107
                           , 129
                         )
                         and grp17.RPT_GRP_SEVNTEEN_C = 4 --'Department of Family Medicine' 
                         and drg.DRG_NUMBER in
                         (
                             'CMS370'
                           , 'CMS371'
                           , 'CMS372'
                           , 'CMS373'
                           , 'CMS374'
                           , 'CMS375'
                           , 'MS765'
                           , 'MS766'
                           , 'MS767'
                           , 'MS768'
                           , 'MS774'
                           , 'MS775'
                           , 'APR540'
                           , 'APR541'
                           , 'APR542'
                           , 'APR560'
                         )
                         then 'M OBGYN - Deliveries - Family Practice IP' ----newly added 6/21/17 for M Tower opening - CJG
                     when har.ACCT_BASECLS_HA_C = 1
                         and obg.delAMT > 0
                         and har.ACCT_CLASS_HA_C not in
                         (
                             107
                           , 129
                         )
                         and grp17.RPT_GRP_SEVNTEEN_C = 6 --'Department of Obstetrics & Gynecology'  
                         then 'M OBGYN IP'
                     when har.ACCT_BASECLS_HA_C = 1
                         and obsAMT > 0
                         and har.ACCT_CLASS_HA_C not in
                         (
                             107
                           , 129
                         )
                         and grp17.RPT_GRP_SEVNTEEN_C = 6 --'Department of Obstetrics & Gynecology'
                         then 'M OBGYN IP'
                     when har.ACCT_BASECLS_HA_C = 1
                         and gynecology.gynAMT > 0
                         and har.ACCT_CLASS_HA_C not in
                         (
                             107
                           , 129
                         )
                         and grp17.RPT_GRP_SEVNTEEN_C = 6 --'Department of Obstetrics & Gynecology'
                         then 'M OBGYN IP'
                     when har.ACCT_BASECLS_HA_C = 1
                         and obgynother.gynotherAMT > 0
                         and har.ACCT_CLASS_HA_C not in
                         (
                             107
                           , 129
                         )
                         and grp17.RPT_GRP_SEVNTEEN_C = 6 --'Department of Obstetrics & Gynecology'  
                         then 'M OBGYN - All Other IP'
                     when har.ACCT_BASECLS_HA_C = 1
                         and gen_data.genAMT > 0
                         and har.ACCT_CLASS_HA_C not in
                         (
                             117
                           , 129
                         )
                         and har.FINAL_DRG_ID is not null
                         and (
                                 grp17.RPT_GRP_SEVNTEEN_C not in
                         (
                             4
                           , 9
                           , 5
                         )       --('Department of Family Medicine','Department of Psychiatry','Department of Medicine') 
                                 or grp18.RPT_GRP_EIGHTEEN_C not in
                         (
                             29
                           , 31
                           , 36
                           , 41
                           , 49
                           , 50
                         )       --('General Pediatrics','General Surgery', 'Gynecology/Obstetrics','Maternal-Fetal Medicine/Obstetrics','Obstetrics/Gynecology','Obstetrics/Maternal-Fetal Medicine') 
                                 and har.FINAL_DRG_ID is not null
                             )
                         then 'M Misc IP'
                     when har.ACCT_BASECLS_HA_C = 1
                         and gen_data.genAMT > 0
                         and har.ACCT_CLASS_HA_C not in (129)
                         and grp17.RPT_GRP_SEVNTEEN_C = 6
                         and har.FINAL_DRG_ID is null --'Department of Obstetrics & Gynecology'
                         then 'M OBGYN - Not Yet Asssigned IP'
                     else
                         null
                 end
        else
            '*Unknown'
    end as FINANCE_DIVISION
from ge.HSP_ACCOUNT            har
left join ge.CLARITY_LOC       loc
    on loc.LOC_ID = har.LOC_ID
left join ge.CLARITY_DRG       drg
    on drg.DRG_ID = har.FINAL_DRG_ID
left join ge.OB_HSB_DELIVERY   del
    on har.PRIM_ENC_CSN_ID = del.DELIVERY_DATE_CSN
        and del.OB_DELIVERY_DATE is not null
left join ge.PATIENT           pat
    on pat.PAT_ID = har.PAT_ID
left join ge.PATIENT_3         pat3
    on pat3.PAT_ID = har.PAT_ID
left join ge.CLARITY_SER       ser
    on ser.PROV_ID = har.ATTENDING_PROV_ID
left join ge.ZC_SER_RPT_GRP_17 grp17_99
    on grp17_99.RPT_GRP_SEVNTEEN_C = ser.RPT_GRP_SEVNTEEN_C -- department
left join ge.ZC_SER_RPT_GRP_18 grp18_99
    on grp18_99.RPT_GRP_EIGHTEEN_C = ser.RPT_GRP_EIGHTEEN_C -- division
left join
(
    select
        har.HSP_ACCOUNT_ID
      , ser.PROV_ID
      , grp17_2.NAME as vwDept
      , grp17.name   as actdept
      , case
            when grp17.name is null
                then grp17_2.NAME
            else
                grp17.name
        end          as name
      , case
            when ser1.RPT_GRP_SEVNTEEN_C is null
                then ser.RPT_GRP_SEVNTEEN_C
            else
                ser1.RPT_GRP_SEVNTEEN_C
        end          as RPT_GRP_SEVNTEEN_C
    from ge.HSP_ACCOUNT            har
    left join ge.HSP_ATND_PROV     atnd
        on har.PRIM_ENC_CSN_ID = atnd.PAT_ENC_CSN_ID
            and atnd.LINE = 1
    left join ge.CLARITY_SER       ser
        on case
               when har.ATTENDING_PROV_ID is not null
                   then har.ATTENDING_PROV_ID
               else
                   atnd.PROV_ID
           end = ser.PROV_ID
    left join ge.CLARITY_SER       ser1
        on har.ATTENDING_PROV_ID = ser1.PROV_ID
    left join ge.ZC_SER_RPT_GRP_17 grp17
        on grp17.RPT_GRP_SEVNTEEN_C = ser1.RPT_GRP_SEVNTEEN_C
    left join ge.ZC_SER_RPT_GRP_17 grp17_2
        on grp17_2.RPT_GRP_SEVNTEEN_C = ser.RPT_GRP_SEVNTEEN_C
    where
        grp17_2.NAME is not null
)                              grp17
    on grp17.HSP_ACCOUNT_ID = har.HSP_ACCOUNT_ID
left join
(
    select
        har.HSP_ACCOUNT_ID
      , ser.PROV_ID
      , grp18_2.NAME as vwdiv
      , grp18.name   as actdiv
      , case
            when grp18.name is null
                then grp18_2.NAME
            else
                grp18.name
        end          as name
      , case
            when ser1.RPT_GRP_EIGHTEEN_C is null
                then ser.RPT_GRP_EIGHTEEN_C
            else
                ser1.RPT_GRP_EIGHTEEN_C
        end          as RPT_GRP_EIGHTEEN_C
    from ge.HSP_ACCOUNT            har
    left join ge.HSP_ATND_PROV     atnd
        on har.PRIM_ENC_CSN_ID = atnd.PAT_ENC_CSN_ID
            and atnd.LINE = 1
    left join ge.CLARITY_SER       ser1
        on har.ATTENDING_PROV_ID = ser1.PROV_ID
    left join ge.ZC_SER_RPT_GRP_18 grp18
        on grp18.RPT_GRP_EIGHTEEN_C = ser1.RPT_GRP_EIGHTEEN_C
    left join ge.CLARITY_SER       ser
        on case
               when har.ATTENDING_PROV_ID is not null
                   then har.ATTENDING_PROV_ID
               else
                   atnd.PROV_ID
           end = ser.PROV_ID
    left join ge.ZC_SER_RPT_GRP_18 grp18_2
        on grp18_2.RPT_GRP_EIGHTEEN_C = ser.RPT_GRP_EIGHTEEN_C
)                              grp18
    on grp18.HSP_ACCOUNT_ID = har.HSP_ACCOUNT_ID
--ped ip Surgery
left join
(
    select
        htx.HSP_ACCOUNT_ID
      --, 'Peds- IP Surgery ' as [Peds- IP Surgery]
      , sum(htx.TX_AMOUNT) as Peds_IP_Sur_AMT
    from ge.HSP_TRANSACTIONS htx
    inner join ge.HSP_ACCOUNT har
      on har.HSP_ACCOUNT_ID = htx.HSP_ACCOUNT_ID
    inner join ge.CLARITY_LOC loc
      on loc.LOC_ID = har.LOC_ID
    left join ge.CLARITY_EAP eap
        on htx.PROC_ID = eap.PROC_ID
    where
        eap.PROC_CODE not in
            (
                '110000039'
              , '110000025'
              , '110000024'
              , '110000039'
              , '110000022'
            )
        and (
          (
            loc.RPT_GRP_SIX = 1 -- LVH
              and htx.DEPARTMENT not in
                  (
                      '10001034'
                    , '100002023'
                    , '100002058'
                  )
          )
          or (
            loc.RPT_GRP_SIX = 2 -- LVHM
              and htx.DEPARTMENT not in
                  (
                      '10001034'
                    , '100002023'
                    , '100002058'
                    , '10003091'
                  )
          )
        )
    group by
        htx.HSP_ACCOUNT_ID
)                              ped_ip_surgery
    on ped_ip_surgery.HSP_ACCOUNT_ID = har.HSP_ACCOUNT_ID
--NICU IP
left join
(
    select
        htx.HSP_ACCOUNT_ID
      --, 'Peds- NICU IP '   as [Peds- NICU IP]
      , sum(htx.TX_AMOUNT) as NICU_IP_AMT
    from ge.HSP_TRANSACTIONS htx
    inner join ge.HSP_ACCOUNT har
      on har.HSP_ACCOUNT_ID = htx.HSP_ACCOUNT_ID
    inner join ge.CLARITY_LOC loc
      on loc.LOC_ID = har.LOC_ID
    left join ge.CLARITY_EAP eap
        on htx.PROC_ID = eap.PROC_ID
    where
        eap.PROC_CODE not in ('110000039')
        and (
          (
            loc.RPT_GRP_SIX = 1
              and htx.DEPARTMENT in ('10001034')
          )
          or (
            loc.RPT_GRP_SIX = 2
              and htx.DEPARTMENT in ('10003091')
          )
        )
    group by
        htx.HSP_ACCOUNT_ID
)                              pednicip
    on pednicip.HSP_ACCOUNT_ID = har.HSP_ACCOUNT_ID
--Peds PicU IP
left join
(
    select
        htx.HSP_ACCOUNT_ID
      --, 'Peds- PICU IP ' as [Peds- PICU IP]
      , sum(htx.TX_AMOUNT) as PICU_IP_AMT
    from ge.HSP_TRANSACTIONS htx
    left join ge.CLARITY_EAP eap
        on htx.PROC_ID = eap.PROC_ID
    where
        eap.PROC_CODE in ('110000039')
        and htx.TX_TYPE_HA_C = 1
    group by
        htx.HSP_ACCOUNT_ID
    having
        sum(htx.TX_AMOUNT) > 0
)                              PEDS_picu_ip
    on PEDS_picu_ip.HSP_ACCOUNT_ID = har.HSP_ACCOUNT_ID
--general charges exclude dept peds tsus nicu picu and pasu
left join
(
    select
        htx.HSP_ACCOUNT_ID
      --'Medicine - Cardio '           AS  [Medicine - Cardio]   
      , sum(htx.TX_AMOUNT) as genAMT
    from ge.HSP_TRANSACTIONS htx
    --left join ge.CLARITY_EAP eap
    --  on htx.PROC_ID = eap.PROC_ID
    inner join ge.HSP_ACCOUNT har
      on har.HSP_ACCOUNT_ID = htx.HSP_ACCOUNT_ID
    inner join ge.CLARITY_LOC loc
      on loc.LOC_ID = har.LOC_ID
    where
        htx.TX_TYPE_HA_C = 1
          and (
            (
              loc.RPT_GRP_SIX = 1
            )
            or (
              loc.RPT_GRP_SIX = 2 -- LVHM              
				        and htx.DEPARTMENT not in (
                      10001067
                    , 10001065
                    , 10002023
                    , 10002058
                    , 10001034
                    , 10001044
                    , 10001066
                    , 10003091
                  ) 
            )
          )
    group by
        htx.HSP_ACCOUNT_ID
)                              gen_data
    on gen_data.HSP_ACCOUNT_ID = har.HSP_ACCOUNT_ID
--FAM_MED
left join
(
    select
        htx.HSP_ACCOUNT_ID
      --, har.FINAL_DRG_ID
      --, har.BILL_DRG_IDTYPE_ID
      --, 'Family Medicine IP ' [Family Medicine IP]
      , sum(htx.TX_AMOUNT) FammedAMT
    from ge.HSP_TRANSACTIONS htx
    --left join ge.CLARITY_EAP eap
    --  on htx.PROC_ID = eap.PROC_ID
    left join ge.HSP_ACCOUNT har
        on har.HSP_ACCOUNT_ID = htx.HSP_ACCOUNT_ID
    where
        htx.DEPARTMENT not in
            (
                10001067
              , 10001065
              , 10002023
              , 10002058
              , 10001034
              , 10001044
              , 10001066
              , 10003091
            ) --added 10003091 on 6/22/17
        --and htx.TX_POST_DATE between @admit and @discharge 
        and htx.TX_TYPE_HA_C = 1
        and (
                (
                    har.BILL_DRG_IDTYPE_ID not in
                        (
                            54
                          , 55
                        )
                    and har.FINAL_DRG_ID not between 370 and 375
                )
                or (
                       har.BILL_DRG_IDTYPE_ID not in
                           (
                               56
                             , 57
                             , 49
                             , 83
                             , 102
                             , 115
                             , 41
                             , 189
                           )
                       and har.FINAL_DRG_ID not in
                               (
                                   765
                                 , 766
                                 , 767
                                 , 768
                                 , 774
                                 , 775
                               )
                   )
                or (
                       har.BILL_DRG_IDTYPE_ID not in
                           (
                               159
                             , 196
                           )
                       and har.FINAL_DRG_ID not in
                               (
                                   540
                                 , 541
                                 , 542
                                 , 560
                               )
                   )
            )
    group by
        htx.HSP_ACCOUNT_ID
    --, har.FINAL_DRG_ID
    --, har.BILL_DRG_IDTYPE_ID
    having
        sum(htx.TX_AMOUNT) > 0
)                              FAM_MED
    on FAM_MED.HSP_ACCOUNT_ID = har.HSP_ACCOUNT_ID
--OBGYN del IP
left join
(
    select
        htx.HSP_ACCOUNT_ID
      --, har.FINAL_DRG_ID
      --, har.BILL_DRG_IDTYPE_ID
      --, drg.DRG_NAME
      --, 'used for deliver IP ' [OBGYN del IP]
      , sum(htx.TX_AMOUNT) delAMT
    from ge.HSP_TRANSACTIONS htx
    --left join ge.CLARITY_EAP eap
    --  on htx.PROC_ID = eap.PROC_ID
    left join ge.HSP_ACCOUNT har
        on har.HSP_ACCOUNT_ID = htx.HSP_ACCOUNT_ID
    --left join ge.CLARITY_DRG drg
    --  on drg.DRG_ID = har.FINAL_DRG_ID
    where
        htx.DEPARTMENT not in
            (
                10001067
              , 10001065
              , 10002023
              , 10002058
              , 10001034
              , 10001044
              , 10001066
              , 10003091
            ) --added 10003091 on 6/22/17
        --and har.ADM_DATE_TIME >= @admit
        --and har.ADM_DATE_TIME < @discharge
        and htx.TX_TYPE_HA_C = 1
        and (har.BILL_DRG_IDTYPE_ID in
                 (
                     54
                   , 55
                   , 159
                   , 196
                   , 56
                   , 57
                   , 49
                   , 83
                   , 102
                   , 115
                   , 41
                   , 189
                 )
            ) 
        and har.FINAL_DRG_ID in
                (
                    5044
                  , 5045
                  , 5046
                  , 5047
                  , 5050
                  , 5051
                  , 540
                  , 541
                  , 542
                  , 560
                  , 3746
                  , 3747
                  , 3748
                  , 3749
                  , 3750
                  , 3751
                ) 
    group by
        htx.HSP_ACCOUNT_ID
    --, har.FINAL_DRG_ID
    --, har.BILL_DRG_IDTYPE_ID
    --, drg.DRG_NAME
    having
        sum(htx.TX_AMOUNT) > 0
)                              obg
    on obg.HSP_ACCOUNT_ID = har.HSP_ACCOUNT_ID
--gynecology
left join
(
    select
        htx.HSP_ACCOUNT_ID
      --, har.FINAL_DRG_ID
      --, har.BILL_DRG_IDTYPE_ID
      --, drg.DRG_NAME
      --, drg.DRG_NUMBER
      --, 'ob gynecology'  [OBGYN gynecology IP]
      , sum(htx.TX_AMOUNT) gynAMT
    from ge.HSP_TRANSACTIONS htx
    --left join ge.CLARITY_EAP eap
    --  on htx.PROC_ID = eap.PROC_ID
    left join ge.HSP_ACCOUNT har
        on har.HSP_ACCOUNT_ID = htx.HSP_ACCOUNT_ID
    --left join ge.CLARITY_DRG drg
    --  on drg.DRG_ID = har.FINAL_DRG_ID
    where
        htx.DEPARTMENT not in
            (
                10001067
              , 10001065
              , 10002023
              , 10002058
              , 10001034
              , 10001044
              , 10001066
              , 10003091
            ) --added 10003091 on 6/22/17 
        --and har.ADM_DATE_TIME >= @admit
        --and har.ADM_DATE_TIME < @discharge
        and htx.TX_TYPE_HA_C = 1
        and (har.BILL_DRG_IDTYPE_ID in
                 (
                     54
                   , 55
                   , 159
                   , 196
                   , 56
                   , 57
                   , 49
                   , 83
                   , 102
                   , 115
                   , 41
                   , 189
                 ) 
            )
        and har.FINAL_DRG_ID in
                (
                    3729
                  , 3730
                  , 3731
                  , 3732
                  , 3733
                  , 3734
                  , 3735
                  , 3736
                  , 3737
                  , 3738
                  , 3739
                  , 3740
                  , 3741
                  , 3742
                  , 3743
                  , 3744
                  , 3745
                  , 5019 
                  , 5020
                  , 5021
                  , 5022
                  , 5023
                  , 5024
                  , 5025
                  , 5026
                  , 5027
                  , 5028
                  , 5029
                  , 5030
                  , 5031
                  , 5032
                  , 5033
                  , 5034
                  , 5035
                  , 5036
                  , 5037
                  , 5038
                  , 5039
                  , 5040
                  , 5041
                  , 5042
                  , 5043
                  , 510
                  , 511
                  , 512
                  , 513
                  , 514
                  , 517
                  , 518
                  , 519
                  , 530
                  , 532
                )
    group by
        htx.HSP_ACCOUNT_ID
    --, har.FINAL_DRG_ID
    --, har.BILL_DRG_IDTYPE_ID
    --, drg.DRG_NAME
    --, drg.DRG_NUMBER
    having
        sum(htx.TX_AMOUNT) > 0
)                              gynecology
    on gynecology.HSP_ACCOUNT_ID = har.HSP_ACCOUNT_ID
--obgyn- other 
left join
(
    select
        htx.HSP_ACCOUNT_ID
      --, har.FINAL_DRG_ID
      --, har.BILL_DRG_IDTYPE_ID
      --, drg.DRG_NAME
      --, drg.DRG_NUMBER
      --, 'ob gynecology'  [OBGYN gynecology IP]
      , sum(htx.TX_AMOUNT) gynotherAMT
    from ge.HSP_TRANSACTIONS htx
    --left join ge.CLARITY_EAP eap
    --  on htx.PROC_ID = eap.PROC_ID
    left join ge.HSP_ACCOUNT har
        on har.HSP_ACCOUNT_ID = htx.HSP_ACCOUNT_ID
    --left join ge.CLARITY_DRG drg
    --  on drg.DRG_ID = har.FINAL_DRG_ID
    where
        htx.DEPARTMENT not in
            (
                10001067
              , 10001065
              , 10002023
              , 10002058
              , 10001034
              , 10001044
              , 10001066
              , 10003091
            ) --added 10003091 on 6/22/17
        --and har.ADM_DATE_TIME >= @admit
        --and har.ADM_DATE_TIME < @discharge
        and htx.TX_TYPE_HA_C = 1
        and (har.BILL_DRG_IDTYPE_ID not in
                 (
                     54
                   , 55
                   , 159
                   , 196
                   , 56
                   , 57
                   , 49
                   , 83
                   , 102
                   , 115
                   , 41
                   , 189
                 )
            )
        or har.FINAL_DRG_ID not in
               (
                   3729
                 , 3730
                 , 3731
                 , 3732
                 , 3733
                 , 3734
                 , 3735
                 , 3736
                 , 3737
                 , 3738
                 , 3739
                 , 3740
                 , 3741
                 , 3742
                 , 3743
                 , 3744
                 , 3745
                 , 5019
                 , 5020
                 , 5021
                 , 5022
                 , 5023 
                 , 5024
                 , 5025
                 , 5026
                 , 5027
                 , 5028
                 , 5029
                 , 5030
                 , 5031
                 , 5032
                 , 5033
                 , 5034
                 , 5035
                 , 5036
                 , 5037
                 , 5038
                 , 5039
                 , 5040
                 , 5041
                 , 5042
                 , 5043
                 , 510 
                 , 511
                 , 512
                 , 513
                 , 514
                 , 517
                 , 518
                 , 519
                 , 530
                 , 532
                 , 5044
                 , 5045
                 , 5046
                 , 5047
                 , 5050
                 , 5051
                 , 540
                 , 541
                 , 542
                 , 560 
                 , 3746
                 , 3747
                 , 3748
                 , 3749
                 , 3750
                 , 3751
                 , 544
                 , 545
                 , 546
                 , 563
                 , 564
                 , 565
                 , 566
                 , 3752
                 , 3753
                 , 3754
                 , 3755
                 , 3758
                 , 3759
                 , 3760
                 , 5048
                 , 5052
                 , 5053
                 , 5054
                 , 5056
                 , 5057
                 , 5082
               )
    group by
        htx.HSP_ACCOUNT_ID
    --, har.FINAL_DRG_ID
    --, har.BILL_DRG_IDTYPE_ID
    --, drg.DRG_NAME
    --, drg.DRG_NUMBER
    having
        sum(htx.TX_AMOUNT) > 0
)                              obgynother
    on obgynother.HSP_ACCOUNT_ID = har.HSP_ACCOUNT_ID
--obstetrics ip
left join
(
    select
        htx.HSP_ACCOUNT_ID
      --, har.FINAL_DRG_ID
      --, har.BILL_DRG_IDTYPE_ID
      --, drg.DRG_NAME
      --, 'obstetrics ip ' [obstetrics ip]
      , sum(htx.TX_AMOUNT) obsAMT
    from ge.HSP_TRANSACTIONS htx
    --left join ge.CLARITY_EAP eap
    --  on htx.PROC_ID = eap.PROC_ID
    left join ge.HSP_ACCOUNT har
        on har.HSP_ACCOUNT_ID = htx.HSP_ACCOUNT_ID
    --left join ge.CLARITY_DRG drg
    --  on drg.DRG_ID = har.FINAL_DRG_ID
    where
        htx.DEPARTMENT not in
            (
                10001067
              , 10001065
              , 10002023
              , 10002058
              , 10001034
              , 10001044
              , 10001066
              , 10003091
            ) --added 10003091 on 6/22/17 
        --and har.ADM_DATE_TIME >= @admit
        --and har.ADM_DATE_TIME < @discharge
        and htx.TX_TYPE_HA_C = 1
        and (har.BILL_DRG_IDTYPE_ID in
                 (
                     54
                   , 55
                   , 159
                   , 196
                   , 56
                   , 57
                   , 49
                   , 83
                   , 102
                   , 115
                   , 41
                   , 189
                 )
            )
        and har.FINAL_DRG_ID in
                (
                    544
                  , 545
                  , 546
                  , 563
                  , 564
                  , 565
                  , 566
                  , 3752
                  , 3753
                  , 3754
                  , 3755
                  , 3758
                  , 3759
                  , 3760
                  , 5048
                  , 5052
                  , 5053
                  , 5054
                  , 5056
                  , 5057
                  , 5082
                ) --5000 series msdrg, 500 series apr 3000 series CMS
    group by
        htx.HSP_ACCOUNT_ID
    --, har.FINAL_DRG_ID
    --, har.BILL_DRG_IDTYPE_ID
    --, drg.DRG_NAME
    having
        sum(htx.TX_AMOUNT) > 0
)                              obstetrics
    on obstetrics.HSP_ACCOUNT_ID = har.HSP_ACCOUNT_ID;

create nonclustered index ncix__LVHFinanceDivisionByHAR__HSP_ACCOUNT_ID on gt1.LVHFinanceDivisionByHAR (HSP_ACCOUNT_ID);

GO