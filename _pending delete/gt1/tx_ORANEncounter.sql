if object_id('gt1.ORANEncounter', 'U') is null drop table gt1.ORANEncounter;

--Variables
declare @ORScheduleActionID numeric (18,0) = variables.GetSingleValue('@vcgORScheduleActionID');
declare @PostedActionID numeric (18,0) = variables.GetSingleValue('@vcgPostedActionID');

declare @NotPerformedLogStatusID table ( STATUS_C tinyint );
insert into @NotPerformedLogStatusID select * from variables.GetTableValues('@vcgNotPerformedLogStatusID');

--Load Base Records
with
ORAN as (
  --Logs
  select distinct
  	log.LOG_ID as SurgeryID
    ,trim(date(log.SURGERY_DATE)) as SurgeryDate
    ,log.PAT_ID as PatientID
  	,log.ROOM_ID as ORRoomID
  	,log.LOC_ID as ORLocationID
  	,log.SERVICE_C as PrimaryORServiceID
    ,surg.SurgeonID as PrimarySurgeonID
    ,zserv.NAME as PrimaryORService
  from ge.OR_LOG log
  left join gt1.ORLogAllSurgeon surg on
    surg.LogID=LOG_ID
    and StaffIsPrimarySurgeon=1
  left join ge.ZC_OR_SERVICE zserv on
    zserv.SERVICE_C=SERVICE_C

union --I want to add ONLY the new surgeryIDs - how do I do that?
  -- Cases
  select distinct
  	case.OR_CASE_ID as SurgeryID
    ,trim(date(case.SURGERY_DATE)) as SurgeryDate
    ,case.PAT_ID as PatientID
  	,case.ROOM_ID as ORRoomID
  	,case.LOC_ID as ORLocationID
  	,case.SERVICE_C as PrimaryORServiceID
    ,surg.SurgeonID as PrimarySurgeonID
    ,zserv.NAME as PrimaryORService
  from ge.OR_CASE case
  left join gt1.ORCaseAllSurgeon surg on
    surg.LogID=LOG_ID
    and StaffIsPrimarySurgeon=1
  left join ge.ZC_OR_SERVICE zserv on
    zserv.SERVICE_C=SERVICE_C
  where OR_CASE_ID not in ( select LOG_ID from ge.OR_LOG )

union --I want to add ONLY the new surgeryIDs - how do I do that?
  -- AN
  select distinct
    SurgeryID as SurgeryID
    ,Date as SurgeryDate
    ,PatientID as PatientID
  	,'Unknown' as ORRoomID
    ,'Unknown' as ORLocationID
    ,'Unknown' as PrimarySurgeonID
    ,'' as PrimaryORServiceID
    ,'Anesthesia' as PrimaryORService
  from gt1.AnesthesiaEncounter
  where SurgeryID not in ( select LOG_ID from ge.OR_LOG )

),
--needed to track surgeon's primary service
SurgeonService as (
  select
    PROV_ID
    ,LOC_ID
    ,firstvalue(SERVICE_C) over (order by LINE asc) as PrimaryServiceID
  from $(vQVDGlobalExtract)OR_SER_SURG_SRVC.qvd (qvd)
  group by PROV_ID,LOC_ID
),
--needed to track date of posting
PostedLine as (
  select
  	LogID
    ,min(Line) as FirstPostedLine
  from gt1.ORAuditLog
  group by LogID
  where ActionID=@vcgPostedActionID
),
--needed to track date of scheduling
ScheduledLine as (
  select
  	CaseID
   ,max(Line) as LastScheduledLine
  from gt1.ORAuditCase
  group by CaseID
  where ActionID=@ORScheduleActionID
),
ResponsibleAN as (
  select
  	SurgeryID
  	,ANRespProviderID
  from gt1.AnesthesiaEncounter

  union --only records that don't yet exist should be added

  select distinct
  	LogID
  	,firstvalue(StaffID) over (order by StaffStartTime,StaffRecordID asc) as ANRespProviderID
  from gt1.ORAllStaffInfo
  where StaffIsAnesthesia=1 and LogID not in (select SurgeryID from ge.ANLogsAppointments)
  group by LogID
),
Procedures as (
  select
    SurgeryID
    ,ProcedureName
  from gt1.ORLogAllProcedure
  where PrimaryProcedureFlag=1

  union --only records that don't yet exist should be added

  select
    CaseID as SurgeryID
    ,ProcedureName
  from gt1.ORCaseAllProcedure
  where PrimaryProcedureFlag=1
  and CaseID not in (select SurgeryID from gt1.ORLogAllProcedure)
)

create table gt1.ORANEncounter (
  SurgeryID [numeric] (18,0) NULL
  ,SurgeryDate [date] NULL
  ,PatientID [numeric] (18,0) NULL
  ,ORRoomID [numeric] (18,0) NULL
  ,ORLocationID [numeric] (18,0) NULL
  ,PrimarySurgeonID [numeric] (18,0) NULL
  ,PrimaryProviderServiceID [numeric] (18,0) NULL
  ,PrimaryProviderService [varchar](254) NULL
  ,PatientDateOfBirth [date] NULL
  ,PatientAgeYears [numeric] (18,0) NULL
  ,HospitalEncounterID [numeric] (18,0) NULL
  ,SurgicalEncounterID [numeric] (18,0) NULL
  ,Anesthesia52PatientEncounterID [numeric] (18,0) NULL
  ,AnesthesiaStartTime [datetime] NULL
  ,AnesthesiaStopTime [datetime] NULL
  ,AnesthesiaResponsibleProviderID [numeric] (18,0) NULL
  ,AnesthesiaResponsibleProvider [varchar](254) NULL
  ,PrimaryProcedureName [varchar](254) NULL
  ,LogStatusID [numeric] (18,0) NULL
  ,LogStatus [varchar](254) NULL
  ,VoidReasonID [numeric] (18,0) NULL
  ,VoidReason [varchar](254) NULL
  ,InpatientDataID [numeric] (18,0) NULL
  ,ASARatingID [numeric] (18,0) NULL
  ,ASARating [varchar](254) NULL
  ,ScheduledStartTime [datetime] NULL
  ,ScheduledEndTime [numeric] (18,0) NULL
  ,ProcedureNotPerformedID [numeric] (18,0) NULL
  ,ProcedureNotPerformed [varchar](254) NULL
  ,Emergent? [varchar](254) NULL
  ,ProcedureLevelID [numeric] (18,0) NULL
  ,ProcedureLevel [varchar](254) NULL
  ,CaseTrackingPatientInPreopTime [datetime] NULL
  ,CaseTrackingPreProcedureStartTime [datetime] NULL
  ,CaseTrackingPreProcedureCompleteTime [datetime] NULL
  ,CaseTrackingPatientInRoomTime [datetime] NULL
  ,CaseTrackingPatientOutRoomTime [datetime] NULL
  ,CaseTrackingPatientInRecoveryTime [datetime] NULL
  ,CaseTrackingPatientOutRecoveryTime [datetime] NULL
  ,CaseTrackingPhaseIStartTime [datetime] NULL
  ,CaseTrackingPhaseIFinishTime [datetime] NULL
  ,CaseTrackingProcedureStartTime [datetime] NULL
  ,CaseTrackingProcedureFinishTime [datetime] NULL
  ,CaseTrackingScheduledIncisionStartTime [datetime] NULL
  ,CaseTrackingPatientInFacilityTime [datetime] NULL
  ,CaseTrackingAnesthesiaStartTime [datetime] NULL
  ,CaseTrackingAnesthesiaFinishTime [datetime] NULL
  ,CaseTrackingPhaseIIStartTime [datetime] NULL
  ,CaseTrackingPhaseIIFinishTime [datetime] NULL
  ,CaseTrackingProceduralCareCompleteTime [datetime] NULL
  ,PrimarySurgeonStartTime [datetime] NULL
  ,PrimarySurgeonEndTime [datetime] NULL
  ,PostedDateTime [datetime] NULL
  ,LastScheduledDateTime [datetime] NULL
  ,ScheduledLength [numeric] (18,0) NULL
  ,PreopInRoomWaitDuration [numeric] (18,0) NULL
  ,OverallPreopDuration [numeric] (18,0) NULL
  ,ScheduledActualIncisionDuration [numeric] (18,0) NULL
  ,IntraOpWaitDuration [numeric] (18,0) NULL
  ,ProcedureDuration [numeric] (18,0) NULL
  ,PreopDuration [numeric] (18,0) NULL
  ,PostopInRoomWaitDuration [numeric] (18,0) NULL
  ,ORDuration [numeric] (18,0) NULL
  ,RecoveryDuration [numeric] (18,0) NULL
  ,PhaseIRecoveryDuration [numeric] (18,0) NULL
  ,PhaseIIRecoveryDuration [numeric] (18,0) NULL
  ,SurgeonOutProcedureFinishDuration [numeric] (18,0) NULL
  ,SurgeonDuration [numeric] (18,0) NULL
  ,AnesthesiaChartingDuration [numeric] (18,0) NULL
  ,AnesthesiaDuration [numeric] (18,0) NULL
  ,AnesthesiaStartIncisionDuration [numeric] (18,0) NULL
  ,DaysToPostDuration [numeric] (18,0) NULL
  ,DaysOpenDuration [numeric] (18,0) NULL
  ,ScheduledToSurgeryDuration [numeric] (18,0) NULL
  ,CalculatedDelayDuration [numeric] (18,0) NULL
  ,CaseClassID [numeric] (18,0) NULL
  ,CaseClass varchar](254) NULL
  ,SetupMinutes [numeric] (18,0) NULL
  ,ScheduledPatientClassID [numeric] (18,0) NULL
  ,ScheduledPatientClass varchar](254) NULL
  ,CaseCancelReasonID [numeric] (18,0) NULL
  ,CaseCancelReason varchar](254) NULL
  ,CaseCancelComments varchar](254) NULL
  ,CaseCancelDate [date] NULL
  ,CaseCancelUserID [numeric] (18,0) NULL
  ,CaseCancelUser varchar](254) NULL
  ,SchedulingStatusID [numeric] (18,0) NULL
  ,SchedulingStatus varchar](254) NULL
  ,AddOnCaseCheckBox? varchar](254) NULL
  ,VoidReasonID [numeric] (18,0) NULL
  ,VoidReason varchar](254) NULL
	,ActualRoomID [numeric] (18,0) NULL
	,ActualRoom varchar](254) NULL
	,ScheduledPatientInTime [datetime] NULL
	,ScheduledPatientOutTime [datetime] NULL
	,CleanupStartTime [datetime] NULL
	,CleanupCompleteTime [datetime] NULL
	,SetupStartTime [datetime] NULL
	,SetupCompleteTime [datetime] NULL
	,ScheduledCleanupDuration [numeric] (18,0) NULL
  ,DelayID [numeric] (18,0) NULL
  ,DocumentedDelayMinutes [numeric] (18,0) NULL
  ,EndedDuringHour [numeric] (18,0) NULL
  ,BeganDuringHour [numeric] (18,0) NULL
  ,ScheduledProcedureStartTime varchar](254) NULL
  ,CasePerformed [numeric] (18,0) NULL
)
insert into gt1.ORANEncounter
(
  SurgeryID
  ,SurgeryDate
  ,PatientID
  ,ORRoomID
  ,ORLocationID
  ,PrimarySurgeonID
  ,PrimaryProviderServiceID
  ,PrimaryProviderService
  ,PatientDateOfBirth
  ,PatientAgeYears
  ,HospitalEncounterID
  ,SurgicalEncounterID
  ,Anesthesia52PatientEncounterID
  ,AnesthesiaStartTime
  ,AnesthesiaStopTime
  ,AnesthesiaResponsibleProviderID
  ,AnesthesiaResponsibleProvider
  ,PrimaryProcedureName
  ,LogStatusID
  ,LogStatus
  ,VoidReasonID
  ,VoidReason
  ,InpatientDataID
  ,ASARatingID
  ,ASARating
  ,ScheduledStartTime
  ,ScheduledEndTime
  ,ProcedureNotPerformedID
  ,ProcedureNotPerformed
  ,Emergent?
  ,ProcedureLevelID
  ,ProcedureLevel
  ,CaseTrackingPatientInPreopTime
  ,CaseTrackingPreProcedureStartTime
  ,CaseTrackingPreProcedureCompleteTime
  ,CaseTrackingPatientInRoomTime
  ,CaseTrackingPatientOutRoomTime
  ,CaseTrackingPatientInRecoveryTime
  ,CaseTrackingPatientOutRecoveryTime
  ,CaseTrackingPhaseIStartTime
  ,CaseTrackingPhaseIFinishTime
  ,CaseTrackingProcedureStartTime
  ,CaseTrackingProcedureFinishTime
  ,CaseTrackingScheduledIncisionStartTime
  ,CaseTrackingPatientInFacilityTime
  ,CaseTrackingAnesthesiaStartTime
  ,CaseTrackingAnesthesiaFinishTime
  ,CaseTrackingPhaseIIStartTime
  ,CaseTrackingPhaseIIFinishTime
  ,CaseTrackingProceduralCareCompleteTime
  ,PrimarySurgeonStartTime
  ,PrimarySurgeonEndTime
  ,PostedDateTime
  ,LastScheduledDateTime
  ,ScheduledLength
  ,PreopInRoomWaitDuration
  ,OverallPreopDuration
  ,ScheduledActualIncisionDuration
  ,IntraOpWaitDuration
  ,ProcedureDuration
  ,PreopDuration
  ,PostopInRoomWaitDuration
  ,ORDuration
  ,RecoveryDuration
  ,PhaseIRecoveryDuration
  ,PhaseIIRecoveryDuration
  ,SurgeonOutProcedureFinishDuration
  ,SurgeonDuration
  ,AnesthesiaChartingDuration
  ,AnesthesiaDuration
  ,AnesthesiaStartIncisionDuration
  ,DaysToPostDuration
  ,DaysOpenDuration
  ,ScheduledToSurgeryDuration
  ,CalculatedDelayDuration
	,CaseClassID 
	,CaseClass 
  ,SetupMinutes
  ,ScheduledPatientClassID
  ,ScheduledPatientClass
  ,CaseCancelReasonID
  ,CaseCancelReason
  ,CaseCancelComments
  ,CaseCancelDate
  ,CaseCancelUserID
  ,CaseCancelUser
  ,SchedulingStatusID
  ,SchedulingStatus
  ,AddOnCaseCheckBox?
  ,VoidReasonID
  ,VoidReason
	,ActualRoomID 
	,ActualRoom 
	,ScheduledPatientInTime
	,ScheduledPatientOutTime 
	,CleanupStartTime
	,CleanupCompleteTime
	,SetupStartTime 
	,SetupCompleteTime 
	,ScheduledCleanupDuration
  ,DelayID
  ,DocumentedDelayMinutes
  ,EndedDuringHour
  ,BeganDuringHour
  ,ScheduledProcedureStartTime
  ,CasePerformed
)
select distinct
--base encounter fields
  base.SurgeryID
  ,base.SurgeryDate
  ,base.PatientID
  ,base.ORRoomID
  ,base.ORLocationID
  ,base.PrimarySurgeonID
--surgeon fields
  ,base.PrimaryServiceID as PrimaryProviderServiceID
  ,zserv2.NAME as PrimaryProviderService
--patient
  ,pat.BIRTH_DATE as PatientDateOfBirth
  ,select round(months_between(SurgeryDate,pat.BIRTH_DATE,'DD-MON-YY')))/12 as PatientAgeYears
  ,csn.OR_LINK_CSN as HospitalEncounterID --used for typical hospital encounter linking
  ,csn2.PAT_ENC_CSN_ID as SurgicalEncounterID
--an data fields
  ,an.Anesthesia52PatientEncounterID
  ,an.AnesthesiaStartTime
  ,an.AnesthesiaStopTime
  ,anresp.ANRespProviderID as AnesthesiaResponsibleProviderID
  ,anser.PROV_NAME as AnesthesiaResponsibleProvider
  ,procs.ProcedureName as PrimaryProcedureName
--log fields
  ,log.STATUS_C as LogStatusID
  ,zsched.NAME as LogStatus
  ,log.VOID_REASON_C as VoidReasonID
  ,zvoid.NAME as VoidReason
  ,log.INPATIENT_DATA_ID as InpatientDataID
  ,log.ASA_RATING_C as ASARatingID
  ,zasa.NAME as ASARating
  ,log.SCHED_START_TIME as ScheduledStartTime
  ,log.SCHED_START_TIME+(log.TOTAL_TIME_NEEDED/1440)) as ScheduledEndTime
  ,log.PROC_NOT_PERF_C as ProcedureNotPerformedID
  ,zperf.NAME as ProcedureNotPerformed
  ,log.EMERG_STATUS_YN as Emergent?
  ,log.PROC_LEVEL_C as ProcedureLevelID
  ,zlevel.NAME as ProcedureLevel
--case tracking fields
  ,ct.CaseTrackingPatientInPreopTime
  ,ct.CaseTrackingPreProcedureStartTime
  ,ct.CaseTrackingPreProcedureCompleteTime
  ,ct.CaseTrackingPatientInRoomTime
  ,ct.CaseTrackingPatientOutRoomTime
  ,ct.CaseTrackingPatientInRecoveryTime
  ,ct.CaseTrackingPatientOutRecoveryTime
  ,ct.CaseTrackingPhaseIStartTime
  ,ct.CaseTrackingPhaseIFinishTime
  ,ct.CaseTrackingProcedureStartTime
  ,ct.CaseTrackingProcedureFinishTime
  ,ct.CaseTrackingScheduledIncisionStartTime
  ,ct.CaseTrackingPatientInFacilityTime
  ,ct.CaseTrackingAnesthesiaStartTime
  ,ct.CaseTrackingAnesthesiaFinishTime
  ,ct.CaseTrackingPhaseIIStartTime
  ,ct.CaseTrackingPhaseIIFinishTime
  ,ct.CaseTrackingProceduralCareCompleteTime
  ,surg.StartTime as PrimarySurgeonStartTime
  ,surg.EndTime as PrimarySurgeonEndTime
  ,post2.DateTime as PostedDateTime
  ,sched2.DateTime as LastScheduledDateTime
--case tracking durations
  ,DateDiff( minute, ct.ScheduledStartTime, ct.ScheduledEndTime ) as ScheduledLength
  ,DateDiff( minute, ct.CaseTrackingPreProcedureCompleteTime, ct.CaseTrackingPatientInRoomTime ) as PreopInRoomWaitDuration
  ,DateDiff( minute, ct.CaseTrackingPatientInPreopTime, ct.CaseTrackingPatientInRoomTime ) as OverallPreopDuration
  ,DateDiff( minute, ct.ScheduledProcedureStartTime, ct.CaseTrackingProcedureStartTime ) as ScheduledActualIncisionDuration
  ,DateDiff( minute, ct.CaseTrackingPatientInRoomTime, ct.CaseTrackingProcedureStartTime ) as IntraOpWaitDuration
  ,DateDiff( minute, ct.CaseTrackingProcedureStartTime, ct.CaseTrackingProcedureFinishTime ) as ProcedureDuration
  ,DateDiff( minute, ct.CaseTrackingPatientInPreopTime, ct.CaseTrackingPreProcedureCompleteTime ) as PreopDuration
  ,DateDiff( minute, ct.CaseTrackingProcedureFinishTime, ct.CaseTrackingPatientOutRoomTime ) as PostopInRoomWaitDuration
  ,DateDiff( minute, ct.CaseTrackingPatientInRoomTime, ct.CaseTrackingPatientOutRoomTime ) as ORDuration
  ,DateDiff( minute, ct.CaseTrackingPatientInRecoveryTime, ct.CaseTrackingPatientOutRecoveryTime ) as RecoveryDuration
  ,DateDiff( minute, ct.CaseTrackingPhaseIStartTime, ct.CaseTrackingPhaseIFinishTime ) as PhaseIRecoveryDuration
  ,DateDiff( minute, ct.CaseTrackingPhaseIIStartTime, ct.CaseTrackingPhaseIIFinishTime ) as PhaseIIRecoveryDuration
  ,DateDiff( minute, surg.PrimarySurgeonStartTime, ct.CaseTrackingProcedureFinishTime ) as SurgeonOutProcedureFinishDuration
  ,DateDiff( minute, surg.PrimarySurgeonStartTime, surg.PrimarySurgeonEndTime ) as SurgeonDuration
  ,DateDiff( minute, ct.CaseTrackingPatientOutRoomTime, an.AnesthesiaStopTime ) as AnesthesiaChartingDuration
  ,DateDiff( minute, an.AnesthesiaStartTime, an.AnesthesiaStopTime ) as AnesthesiaDuration
  ,DateDiff( minute, ct.CaseTrackingPatientInRoomTime, an.AnesthesiaStopTime ) as AnesthesiaStartIncisionDuration
  ,iff(len(post2.DateTime)>0, DateDiff( days, SurgeryDate, post2.DateTime )) as DaysToPostDuration
  ,iff(len(post2.DateTime)=0, DateDiff( days, SurgeryDate, today )) as DaysOpenDuration
  ,DateDiff( minute, sched2.DateTime, SurgeryDate ) as ScheduledToSurgeryDuration
  ,DateDiff( minute, log.SCHED_START_TIME, ct.CaseTrackingPatientInRoomTime) as CalculatedDelayDuration
--case specific information
  ,case.CASE_CLASS_C as CaseClassID
  ,zcclass.NAME as CaseClass
  ,case.SETUP_OFFSET as SetupMinutes
  ,case.PAT_CLASS_C as ScheduledPatientClassID
  ,case.zclass.NAME as ScheduledPatientClass
  ,case.CANCEL_REASON_C as CaseCancelReasonID
  ,zcancel.NAME as CaseCancelReason
  ,case.CANCEL_COMMENTS as CaseCancelComments
  ,date(case.CANCEL_DATE) as CaseCancelDate
  ,case.CANCEL_USER_ID as CaseCancelUserID
  ,emp.NAME as CaseCancelUser
  ,case.SCHED_STATUS_C as SchedulingStatusID
  ,zsched.NAME as SchedulingStatus
  ,case.ADD_ON_CASE_YN as AddOnCaseCheckBox? --Add Y/N mapping
  ,case.VOID_REASON_C as VoidReasonID
  ,zvoid.NAME as VoidReason
--view specific information 
	,vsurg.ACTUAL_ROOM_ID as ActualRoomID 
	,cs.PROV_NAME as ActualRoom 
	,vsurg.SCHED_SURGERY_DATETIME as ScheduledPatientInTime
	,vsurg.SCHED_PAT_OUT_ROOM_DATETIME as ScheduledPatientOutTime 
	,vsurg.CLEANUP_START_DATETIME as CleanupStartTime
	,vsurg.CLEANUP_COMPLETE_DATETIME as CleanupCompleteTime
	,vsurg.PREP_START_DATETIME as SetupStartTime 
	,vsurg.PREP_COMPLETE_DATETIME as SetupCompleteTime 
	,vsurg.SCHED_CLEANUP_DUR as ScheduledCleanupDuration
--facility structure
  -- 	applymap('LOC_NAME_MAP',LocationID) as LocationName,
  --applymap('LOC_SA_MAP',LocationID) as ServiceAreaID
--delay
  ,delay.DELAY_ID as DelayID
  ,delaylnlg.DELAY_LENGTH as DocumentedDelayMinutes
--time flags
  ,datepart(hour,,datepart(hour, ct.CaseTrackingPatientOutRoomTime) as EndedDuringHour
  ,datepart(hour, ct.CaseTrackingPatientInRoomTime) as BeganDuringHour
  ,dateadd(mi, case.SetupMinutes, case.ScheduledStartTime) as ScheduledProcedureStartTime
  ,iif(perfID.STATUS_C is null and log.PROC_NOT_PERF_C is null, 1, 0) as CasePerformed
--misc flags
  --if(count(ProcedurePanel)>1 or count(ProcedureName)>1, dual('N',0), dual('Y',1)) as SingleProcedureSurgery?
  --if(wildmatch(weekday(SurgeryDate),'S*'), dual('Y',1), dual('N',0)) as WeekendSurgery //for checking compliance
  --if(ScheduledActualIncisionDuration < $(vcgORDelayTimeMinLimit), dual('Y',1), dual('N',0)) as CompliantCaseStartTime
into gt1.ORANEncounter
from ORAN base
left join ge.V_SURGERY vsurg on 
	vsurg.LOG_ID=base.SurgeryID
left join ge.OR_LOG log on --log
  log.LOG_ID=base.SurgeryID
left join gt1.ORCaseTracking ct on -- case tracking
  ct.LogID=base.SurgeryID
left join gt1.AnesthesiaEncounter an on
  an.LogID=base.SurgeryID
left join ResponsibleAN respan on -- responsible AN
  respan.SurgeryID=base.SurgeryID
left join ge.CLARITY_SER anser on -- an name
  anser.PROV_ID=respan.ANRespProviderID
left join ge.OR_CASE case on -- case
  case.OR_CASE_ID=base.SurgeryID
left join Procedures procs on --primary procedures
  procs.SurgeryID=base.SurgeryID
left join ge.ZC_PAT_CLASS zclass on --pt class
  zclass.ADT_PAT_CLASS_C=case.PAT_CLASS_C
left join ge.CLARITY_EMP emp on --employee name
  emp.USER_ID=case.CANCEL_USER_ID
left join ge.ZC_PROC_NOT_PERF zperf on --proc not performed
  zperf.PROC_NOT_PERF_C=log.PROC_NOT_PERF_C
left join ge.ZC_OR_ASA_RATING zasa on --asa
  zasa.ASA_RATING_C=log.ASA_RATING_C
left join ge.ZC_OR_STATUS zstatus on --log status
  zstatus.STATUS_C=log.STATUS_C
left join ge.ZC_PROC_LEVEL zlevel on --proc level
  zlevel.PROC_LEVEL_C=log.PROC_LEVEL_C
left join ge.ZC_OR_VOID_REASON zvoid on --void
  zvoid.VOID_REASON_C=log.VOID_REASON_C
left join ge.ZC_OR_SCHED_STATUS zsched on --scheduling status
  zsched.SCHED_STATUS_C=case.SCHED_STATUS_C
left join ge.ZC_OR_CANCEL_RSN zcancel on --cancel reason
  zcancel.CANCEL_REASON_C=case.CANCEL_REASON_C
left join ge.ZC_OR_VOID_REASON zvoid on --void reason
  zvoid.VOID_REASON_C=log.VOID_REASON_C
left join gt1.ORLogAllSurgeon surg on --primary surgeon
  surg.LogID=base.LOG_ID
  and StaffIsPrimarySurgeon=1
left join SurgeonService surgserv on --primary surgeon service ID
  surgserv.PROV_ID=base.PrimarySurgeonID
  surgserv.LOC_ID=base.ORLocationID
left join ge.ZC_OR_SERVICE zserv2 on --primary surgeon service name
  zserv2.SERVICE_C=surgserv.PrimaryServiceID
left join ge.PATIENT pat on --patient
  pat.PAT_ID=base.PatientID
left join ge.PAT_OR_ADM_LINK csn on --encounter csn
  csn.LOG_ID=base.SurgeryID
left join ge.PAT_OR_ADM_LINK csn2 on --surgical csn
  csn2.LOG_ID=base.SurgeryID
left join PostedLine post on --posted line
  post.LogID=base.SurgeryID
left join gt1.ORAuditLog post2 on --posted line 2
  post2.LogID=post.LogID
  and post2.Line=post.FirstPostedLine
left join ScheduledLine sched on --sched line
  sched.CaseID=base.SurgeryID
left join gt1.ORAuditCase sched2 on --sched line 2
  sched2.CaseID=sched.LogID
  and sched2.Line=sched.LastScheduledLine
left join ge.OR_LOG_LN_DELAY delay on --delay
  delay.LOG_ID=base.SurgeryID
left join ge.OR_LNLG_DELAY delaylnlg on
  delaylnlg.RECORD_ID=delay.DELAY_ID--delay length
left join @NotPerformedLogStatusID perfID on --log perf variable
  perfID.STATUS_C=log.STATUS_C
left join ZC_OR_CASE_CLASS zcclass on 
	zcclass.CASE_CLASS_C=case.CASE_CLASS_C
left join ge.CLARITY_SER cs on 
	vsurg.ACTUAL_ROOM_ID = cs.PROV_ID
;

--ADDITIONAL FEATURES
--first case range
--cancellations
--adjustments to schedule
--documentation flags?
