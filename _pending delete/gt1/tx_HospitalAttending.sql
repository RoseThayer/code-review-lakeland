<<<<<<< HEAD:gt1/HospitalAttending.sql
create procedure [gt1].[tx_HospitalAttending] as

/*  
    --> Source Tables <--
		ge.HSP_ATND_PROV

    --> Target Tables <--
		gt1.HospitalAttending

		--> Performance Benchmark <--
	  gt1.HospitalAttending: X rows, hh:mm:ss runtime
		
    --> To-Do <--

    --> Change Log <--

    Change Date   Change Owner                  Change Description
    =========================================================================================================================
    YYYY-MM-DD    chris.perry                   description of change

*/


/* drop and recreate destination transform table */
if object_id('gt1.[HospitalAttending]', 'U') is null
  drop table gt1.[HospitalAttending];

create table [gt1].[HospitalAttending]
  (
    EncounterCSN numeric(18, 0)
  , EDAttendingProviderYN char
  , HospitalAttendingLine int
  , AttendingProviderID varchar(18)
  , AttendingFromDate datetime
  , AttendingToDate datetime
  , AttendingAssignedToPatientLength int
  )

/* configuration variables */

/* populate transform table */
insert into [gt1].[HospitalAttending]
  (
    EncounterCSN
  , EDAttendingProviderYN
  , HospitalAttendingLine
  , AttendingProviderID
  , AttendingFromDate
  , AttendingToDate
  , AttendingAssignedToPatientLength
  )
select
  PAT_ENC_CSN_ID                                     as EncounterCSN
, ED_ATTEND_YN                                       as EDAttendingProviderYN
, LINE                                               as HospitalAttendingLine
, PROV_ID                                            as AttendingProviderID
, ATTEND_FROM_DATE                                   as AttendingFromDate
, ATTEND_TO_DATE                                     as AttendingToDate
, datediff(minute, ATTEND_FROM_DATE, ATTEND_TO_DATE) as AttendingAssignedToPatientLength
from ge.HSP_ATND_PROV;

/* create any needed indexes */

/* quit */
return 0;

go

=======
create procedure [gt1].[HospitalAttendingSp] as

if object_id('gt1.[HospitalAttending]', 'U') is null
  drop table gt1.[HospitalAttending];

create table [gt1].[HospitalAttending]
  (
    EncounterCSN numeric(18, 0)
  , EDAttendingProviderYN char
  , HospitalAttendingLine int
  , AttendingProviderID varchar(18)
  , AttendingFromDate datetime
  , AttendingToDate datetime
  , AttendingAssignedToPatientLength int
  )

insert into [gt1].[HospitalAttending]
  (
    EncounterCSN
  , EDAttendingProviderYN
  , HospitalAttendingLine
  , AttendingProviderID
  , AttendingFromDate
  , AttendingToDate
  , AttendingAssignedToPatientLength
  )
select
  PAT_ENC_CSN_ID                                     as EncounterCSN
, ED_ATTEND_YN                                       as EDAttendingProviderYN
, LINE                                               as HospitalAttendingLine
, PROV_ID                                            as AttendingProviderID
, ATTEND_FROM_DATE                                   as AttendingFromDate
, ATTEND_TO_DATE                                     as AttendingToDate
, datediff(minute, ATTEND_FROM_DATE, ATTEND_TO_DATE) as AttendingAssignedToPatientLength
from ge.HSP_ATND_PROV;

>>>>>>> 11383c829f5244c6a6c16013aef7e450b6819de7:gt1/tx_HospitalAttending.sql
