SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
 
CREATE procedure [gt1].[MO2_OrderMedTransform] 
as

/*  --> Source Tables <--
	pa.ge.ORDER_MEDMIXINFO 
	pa.ge.ORDER_MED  
 
	--> Target Table <--
	gt1.OrderMed 

	--> Performance benchmark <--
		5594229 rows in ~50s
*/	

--> get variable values
declare @vCoumadinMedID int = (select variables.GetSingleValue('@vCoumadinMedID'))
declare @vHeparinMedID int = (select variables.GetSingleValue('@vHeparinMedID'))

declare @ABX_Medication_ID table (id int identity(1,1), Medication_ID bigint)
insert into @ABX_Medication_ID
select VariableValue from variables.GetTableValues ('@ABX_Medication_ID')

declare @IVFluid_Medication_ID table (id int identity(1,1), Medication_ID bigint)
insert into @IVFluid_Medication_ID
select VariableValue from variables.GetTableValues ('@IVFluid_Medication_ID')

declare @Vasopressor_Medication_ID table (id int identity(1,1), Medication_ID bigint)
insert into @Vasopressor_Medication_ID
select VariableValue from variables.GetTableValues ('@Vasopressor_Medication_ID')
	
--ORDER_MED:
if exists (select top 1 * from INFORMATION_SCHEMA.TABLES where TABLE_NAME = N'OrderMed' and TABLE_SCHEMA = N'gt1') drop table gt1.OrderMed 
create table [gt1].[OrderMed]
(
	[ORDER_MED_ID] [numeric](18, 0) NOT NULL,
	[MEDICATION_ID] [numeric](18, 0) NULL,
	[MedicationOrderTime] [datetime] NULL,
	[PAT_ENC_CSN_ID] [numeric](18, 0) NULL,
	[START_DATE] [datetime] NULL,
	[END_DATE] [datetime] NULL,
	[MedicationName] [varchar](255) NOT NULL,
	[MedicationGroup] [varchar](255) NULL,	
	[AbxFlag] int NOT NULL,
	[IVFluidFlag] int NOT NULL,
	[VasopressorFlag] int NOT NULL, 
	[CoumadinFlag] int NOT NULL,
	[HeparinFlag] int NOT NULL
)
insert into [gt1].[OrderMed]
 (	 [ORDER_MED_ID]
    ,[MEDICATION_ID]
    ,[PAT_ENC_CSN_ID]
    ,[MedicationOrderTime] 
	,[START_DATE]
    ,[END_DATE]
	,[MedicationName]
    ,[MedicationGroup]
    ,[AbxFlag]
    ,[IVFluidFlag]
    ,[VasopressorFlag]
	,[CoumadinFlag]
	,[HeparinFlag]
)
select 
  omd.ORDER_MED_ID,
  omd.MEDICATION_ID,
  omd.PAT_ENC_CSN_ID,
  omd.ORDER_INST as MedicationOrderTime,
  omd.[START_DATE],
  omd.[END_DATE], 
  isnull(cmc.[NAME],'*Unknown') as MedicationName,
  substring(isnull(cmc.[NAME],'*Unknown'),1, charindex(' ',isnull(cmc.[NAME],'*Unknown'),1))  as MedicationGroup,
  iif(abx.Medication_ID is not null, 1, 0) as AbxFlag,
  iif(ivf.Medication_ID is not null, 1, 0)  as IVFluidFlag,
  iif(vsp.Medication_ID is not null, 1, 0)  as VasopressorFlag, 
  iif(cmc.PHARM_CLASS_C = @vCoumadinMedID, 1, 0) as CoumadinFlag, 
  iif(cmc.PHARM_SUBCLASS_C = @vHeparinMedID, 1, 0) as HeparinFlag
from pa.ge.ORDER_MED omd with (nolock)
inner join gt2.HospitalEncounter he with (nolock)
  on omd.PAT_ENC_CSN_ID = he.PAT_ENC_CSN_ID
left join pa.ge.CLARITY_MEDICATION cmc with (nolock)
  on omd.MEDICATION_ID = cmc.MEDICATION_ID
left join @ABX_Medication_ID abx  
  on omd.MEDICATION_ID = abx.Medication_ID
left join @IVFluid_Medication_ID ivf  
  on omd.MEDICATION_ID = ivf.Medication_ID
left join @Vasopressor_Medication_ID vsp  
  on omd.MEDICATION_ID = vsp.Medication_ID


create index ncix__OrderMed__ORDER_MED_ID on [gt1].[OrderMed]
(
	[ORDER_MED_ID] ASC
)
include 
(
 	[MEDICATION_ID],
	[MedicationOrderTime],
	[PAT_ENC_CSN_ID],
	[MedicationName],
	[MedicationGroup],
	[AbxFlag],
	[IVFluidFlag],
	[VasopressorFlag]
) 
 
create index ncix__OrderMed__AbxFlag on gt1.OrderMed(AbxFlag) include (ORDER_MED_ID, MedicationOrderTime, PAT_ENC_CSN_ID, MedicationName, MedicationGroup)

-- check keys and indexes
exec tools.CheckKeysAndIndexes










GO
