alter procedure [gt1].[FacilityStructure] as

/*
  TODO:    
    - @vcgADTNonAdmitDepartmentIDs is still null
    - validate
*/

/*  --> Source Tables <--
		ge.CLARITY_DEP
		ge.CLARITY_LOC
		ge.CLARITY_SA
		ge.CLARITY_BED
		ge.CLARITY_ROM
		ge.CLARITY_SER
		ge.OR_LOC
		ge.OR_SER_SURG_SRVC

    --> Target Tables <--
		gt1.DepartmentInfo
		gt1.ORFacility

		--> Performance Benchmark ~1s <--
		gt1.DepartmentInfo: 1628 rows 
		gt1.ORFacility: 8507 rows  

*/

/* per selah b. and erin y. not creating this as it was sourced from custom Gunderson table and not used in Executive Scorecard app
create table gt1.CensusBeds
(
    DepartmentAbbreviation	nvarchar				-- update datatype
  , Overflow								tinyint					-- update datatype
  , RoomBed									numeric(18, 0)	-- update datatype
);
*/

/* configuration variables */

declare @ADTNonAdmitDepartmentIDs table
(
    ADTNonAdmitDeptKey		int identity(1, 1)
  , DepartmentId					numeric(18, 0)
)
insert into @ADTNonAdmitDepartmentIDs
select
	VariableValue
from variables.GetTableValues('@vcgADTNonAdmitDepartmentIDs')
where 
  VariableValue <> 'NULL'
;

/* used for CensusBeds which we are not creating at this time
declare @BedExcludeCensus table
(
    BedExcludeCensusKey		int identity(1, 1)
  , RoomBed								nvarchar				-- update datatype
)
insert into @BedExcludeCensus
select
	VariableValue
from variables.GetTableValues('@vcgBedExcludeCensusMatchable')
;
*/

/* used for CensusBeds which we are not creating at this time
declare @UnitExcludeCensus table
(
    UnitExcludeCensusKey	int identity(1, 1)
  , NurseStation					nvarchar				-- update datatype
)
insert into @UnitExcludeCensus
select
	VariableValue
from variables.GetTableValues('@vcgUnitExcludeCensusMatchable')
;
*/

if OBJECT_ID('gt1.DepartmentInfo', 'U') is not null drop table gt1.DepartmentInfo;
create table [gt1].[DepartmentInfo](
	[DepartmentID] [numeric](18, 0) NOT NULL,
	[ADTAdmitDepartment] [int] NOT NULL,
	[DepartmentName] [varchar](254) NULL,
	[DepartmentAbbreviation] [varchar](20) NULL,
	[DepartmentSpecialty] [varchar](50) NULL,
	[LocationID] [numeric](18, 0) NULL,
	[LocationName] [varchar](80) NULL,
	[HospitalID] [numeric](18, 0) NULL,
	[HospitalName] [varchar](80) NULL,
	[ServiceAreaID] [numeric](18, 0) NULL,
	[ServiceAreaName] [varchar](80) NULL,
	[NumberCensusBeds] [int] NULL
)
insert into [gt1].[DepartmentInfo]
           ([DepartmentID]
           ,[ADTAdmitDepartment]
           ,[DepartmentName]
           ,[DepartmentAbbreviation]
           ,[DepartmentSpecialty]
           ,[LocationID]
           ,[LocationName]
           ,[HospitalID]
           ,[HospitalName]
           ,[ServiceAreaID]
           ,[ServiceAreaName]
           ,[NumberCensusBeds])
 select
    dep.DEPARTMENT_ID				as DepartmentID
  , iif(ADTNonAdmitDepts.departmentID is null
		, 1
		, 0
		)												as ADTAdmitDepartment
  , dep.DEPARTMENT_NAME			as DepartmentName
  , dep.DEPT_ABBREVIATION		as DepartmentAbbreviation
  , dep.SPECIALTY						as DepartmentSpecialty
  , dep.REV_LOC_ID					as LocationID
  , iif(dep.REV_LOC_ID is null
		, '*Unspecified'
		, iif(loc.LOC_ID is null
			, '*Unknown'
			, loc.LOC_NAME
			)
		)												as LocationName
  , iif(loc.HOSP_PARENT_LOC_ID > 0
		, loc.HOSP_PARENT_LOC_ID
		, null
		)												as HospitalID
  , iif(loc.HOSP_PARENT_LOC_ID > 0
		, iif(locHosp.LOC_ID is not null
			, locHosp.LOC_NAME
			, null
			)
		, null
		)												as HospitalName
  , dep.SERV_AREA_ID				as ServiceAreaID
  , iif(dep.SERV_AREA_ID is null
		, '*Unspecified'
		, iif(sa.SERV_AREA_ID is null
			, '*Unknown'
			, sa.SERV_AREA_NAME
		)
	)													as ServiceAreaName
  , null                    as NumberCensusBeds
from ge.CLARITY_DEP dep
left join @ADTNonAdmitDepartmentIDs ADTNonAdmitDepts
  on ADTNonAdmitDepts.departmentId = dep.DEPARTMENT_ID
left join ge.CLARITY_LOC loc
  on loc.LOC_ID = dep.REV_LOC_ID
left join ge.CLARITY_LOC locHosp
  on locHosp.LOC_ID = loc.HOSP_PARENT_LOC_ID
    and loc.HOSP_PARENT_LOC_ID > 0
left join ge.CLARITY_SA sa
  on sa.SERV_AREA_ID = dep.SERV_AREA_ID
;

/* not creating CensusBeds at this time
insert into gt1.CensusBeds
(
    DepartmentAbbreviation
  , Overflow
  , RoomBed
)
select distinct
    rbm.NURSE_STATION as DepartmentAbbreviation
  , iif(rbm.OVERFLOW_IND = 'Y', 1, 0) as Overflow
  , rbm.ROOM_BED as RoomBed
from ge.TBL_ROOM_BED_MASTER rbm
left join @BedExcludeCensus bec
  on bec.roomBed = rbm.ROOM_BED -- this used WildMatch, are they partial strings?
left join @UnitExcludeCensus uec
  on uec.nurseStation = rbm.NURSE_STATION
where
  bec.id is null -- not listed in bed exclusion criteria
    and uec.id is null -- not listed in unit exclusion criteria
	and not (rbm.NURSE_STATION = 'NURS' and rbm.ROOM_BED like '%B01') -- this seems very customer specific
	and not (rbm.NURSE_STATION = '50BS' and rbm.ROOM_BED like '%B')
	and not (rbm.NURSE_STATION = 'LD' and rbm.ROOM_BED like '%B')
	and rbm.OVERFLOW_IND <> 'Y'
	and rbm.ACTIVE = 'Y'

merge
into gt1.DepartmentInfo as di
using (
	select
	    DepartmentAbbreviation
	  , count(distinct RoomBed) as NumberCensusBeds
	from gt1.CensusBeds
	group by
	  DepartmentAbbreviation
) cb
  on di.DepartmentAbbreviation = cb.DepartmentAbbreviation
when matched then
  update
    set di.NumberCensusBeds = cb.NumberCensusBeds
;
*/

/* add NumberCensusBeds to DepartmentInfo */
merge
into gt1.DepartmentInfo as di
using (
  select
	    count(bed.BED_ID) as NumberCensusBeds
		, room.DEPARTMENT_ID as DepartmentID
	from ge.CLARITY_BED bed
  inner join (
    select
        bed.BED_ID
      , max(bed.BED_CONT_DATE_REAL) as BED_CONT_DATE_REAL
    from ge.CLARITY_BED bed
    group by
      bed.BED_ID
  ) curbed
    on curbed.BED_ID = bed.BED_ID
      and curbed.BED_CONT_DATE_REAL = bed.BED_CONT_DATE_REAL
	inner join ge.CLARITY_ROM room
	  on room.ROOM_ID = bed.ROOM_ID
	inner join (
    select
        room.ROOM_ID
      , max(room.ROM_CONT_DATE_REAL)  as ROM_CONT_DATE_REAL
    from ge.CLARITY_ROM room
    group by
      room.ROOM_ID
  ) curroom
    on curroom.ROOM_ID = room.ROOM_ID
      and curroom.ROM_CONT_DATE_REAL = room.ROM_CONT_DATE_REAL
  where
	  bed.CENSUS_INCLUSN_YN = 'Y'
      and room.DEPARTMENT_ID is not null
  group by
	  room.DEPARTMENT_ID
) cb
  on di.DepartmentID = cb.DepartmentID
when matched then
  update
	  set di.NumberCensusBeds = cb.NumberCensusBeds
;


if OBJECT_ID('gt1.ORFacility', 'U') is not null drop table gt1.ORFacility;
create table [gt1].[ORFacility](
	[ORLocationID] [numeric](18, 0) NOT NULL,
	[ORLocationName] [varchar](80) NULL,
	[ORDepartmentID] [numeric](18, 0) NULL,
	[ORRoomID] [varchar](18) NULL,
	[ORRoomName] [varchar](200) NULL
)  
insert into [gt1].[ORFacility]
           ([ORLocationID]
           ,[ORLocationName]
           ,[ORDepartmentID]
           ,[ORRoomID]
           ,[ORRoomName])
 select
		orl.LOC_ID							as ORLocationID
	, iif(orl.LOC_ID is null
		, '*Unspecified'
		, iif(loc.LOC_ID is null
			, '*Unknown'
			, loc.LOC_NAME
			)
		)												as ORLocationName
	, orl.OR_DEPARTMENT_ID		as ORDepartmentID
	, orsss.PROV_ID						as ORRoomID
	, ser.PROV_NAME						as ORRoomName
from ge.OR_LOC orl
left join ge.CLARITY_LOC loc
  on loc.LOC_ID = orl.LOC_ID
left join ge.OR_SER_SURG_SRVC orsss
	on orsss.LOC_ID = orl.LOC_ID
left join ge.CLARITY_SER ser
  on ser.PROV_ID = orsss.PROV_ID
;

create nonclustered index ncix__DepartmentInfo__DepartmentID on gt1.DepartmentInfo (DepartmentID);

create nonclustered index ncix__ORFacility__ORLocationID on gt1.ORFacility (ORLocationID);

-- check keys and indexes
exec tools.CheckKeysAndIndexes

GO

