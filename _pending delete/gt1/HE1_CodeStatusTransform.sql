SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
 

 
CREATE procedure [gt1].[HE1_CodeStatusTransform]
as
 
/*
-->	Source tables used <--
	dbo.Numbers
    PA.ge.OCS_CODE_STATUS 
	PA.ge.ZC_CODE_STATUS 

--> Target table created <--
	gt2.CodeStatus

--> Performance benchmark <--
	108373 rows ~1 second

*/	

-- get variable values
declare @CodeStatus_CD_STATUS_C table (id int identity(1,1), CD_STATUS_C int)
insert into @CodeStatus_CD_STATUS_C
select VariableValue from variables.GetTableValues ('@CodeStatus_CD_STATUS_C')

-- get the distinct list of patients and their max DNR value
if exists (select * from INFORMATION_SCHEMA.TABLES where TABLE_NAME = 'CodeStatus' and TABLE_SCHEMA = 'gt1') drop table gt1.CodeStatus
create table [gt1].[CodeStatus]
(
	[PAT_ENC_CSN_ID] [numeric](18, 0) NULL,
	[DNRStatus] [int] NULL,
	[CodeStatusList] [nvarchar](max) NULL
)  

;with CodeName as
(select  
    ocs2.PATIENT_CSN as PAT_ENC_CSN_ID 
  , max(case when csc.CD_STATUS_C is not null then 1 end) as [DNRStatus] 
  , zcc.NAME 
  from PA.ge.OCS_CODE_STATUS ocs2 
left join PA.ge.ZC_CODE_STATUS zcc
  on ocs2.CODE_STATUS_C = zcc.CD_STATUS_C 
left join @CodeStatus_CD_STATUS_C csc
  on zcc.CD_STATUS_C = csc.id 
group by ocs2.PATIENT_CSN, zcc.NAME
)

insert into [gt1].[CodeStatus]
 (	 [PAT_ENC_CSN_ID]
    ,[DNRStatus]
    ,[CodeStatusList]
)
select  
    cn2.PAT_ENC_CSN_ID 
  , max(cn2.DNRStatus) as DNRStatus  
  , (select stuff((select  ',' + cast(cn1.NAME as varchar(50))
		from CodeName cn1
		where cn1.PAT_ENC_CSN_ID = cn2.PAT_ENC_CSN_ID
		for xml path('') ), 1, 1, '' )) as CodeStatusList
from CodeName cn2 
group by cn2.PAT_ENC_CSN_ID 
 
create index ncix__CodeStatus__PAT_ENC_CSN_ID on gt1.CodeStatus (PAT_ENC_CSN_ID) include (DNRStatus, CodeStatusList)

-- check keys and indexes
exec tools.CheckKeysAndIndexes







GO
