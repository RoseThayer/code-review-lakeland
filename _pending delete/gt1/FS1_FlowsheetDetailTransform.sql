SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
 
 
CREATE  PROCEDURE [gt1].[FS1_FlowsheetDetailTransform] 
AS 

/*
	--> Source Tables used: <--
	    pa.ge.IP_FLWSHT_MEAS 
		pa.ge.IP_FLWSHT_REC
		pa.ge.PAT_ENC_HSP 

	--> Target tables created: <--
		padev.gt1.FlowsheetDetail

	--> Performance benchmark: <--
		27953790 rows in 4:31
*/
  
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON 

declare @vSystolicBPFlowsheetID numeric = (select cast(variables.GetSingleValue('@vSystolicBPFlowsheetID') as numeric))

declare @vDiastolicBPFlowsheetID table (id int identity(1,1), FLO_MEAS_ID numeric)
insert into @vDiastolicBPFlowsheetID
select cast(variables.GetSingleValue('@vDiastolicBPFlowsheetID') as numeric)
 
Declare @vTemperatureFlowsheetID table (id int identity(1,1), FLO_MEAS_ID numeric)
insert into @vTemperatureFlowsheetID
select cast(variables.GetSingleValue('@vTemperatureFlowsheetID') as numeric)
 
Declare @vPulseFlowsheetID table (id int identity(1,1), FLO_MEAS_ID numeric)
insert into @vPulseFlowsheetID
select cast(variables.GetSingleValue('@vPulseFlowsheetID') as numeric) 

Declare @vRespirationRateFlowsheetID table (id int identity(1,1), FLO_MEAS_ID numeric)
insert into @vRespirationRateFlowsheetID
select cast(variables.GetSingleValue('@vRespirationRateFlowsheetID') as numeric) 
  
/*
--Not in LVHN workflow
Declare @vFIO2FlowsheetIDs table (id int identity(1,1), FLO_MEAS_ID numeric)
insert into @vFIO2FlowsheetIDs
select FLO_MEAS_ID from @vSystolicBPFlowsheetIDs
*/

Declare @vScvO2FlowsheetID table (id int identity(1,1), FLO_MEAS_ID numeric)
insert into @vScvO2FlowsheetID
select cast(variables.GetSingleValue('@vScvO2FlowsheetID') as numeric) 

/*
--Not in LVHN workflow
Declare @vPRBCFlowsheetIDs table (id int identity(1,1), FLO_MEAS_ID numeric)
insert into @vPRBCFlowsheetIDs
select FLO_MEAS_ID from @vSystolicBPFlowsheetIDs
*/

Declare @vCVPFlowsheetID table (id int identity(1,1), FLO_MEAS_ID numeric)
insert into @vCVPFlowsheetID
select * from variables.GetTableValues('@vCVPFlowsheetID')

Declare @vMAPFlowsheetID table (id int identity(1,1), FLO_MEAS_ID numeric)
insert into @vMAPFlowsheetID
select * from variables.GetTableValues('@vMAPFlowsheetID')

Declare @vUrineFlowsheetID table (id int identity(1,1), FLO_MEAS_ID numeric)
insert into @vUrineFlowsheetID
select cast(variables.GetSingleValue('@vUrineFlowsheetID') as numeric) 

Declare @vWeightFlowsheetID table (id int identity(1,1), FLO_MEAS_ID numeric)
insert into @vWeightFlowsheetID
select cast(variables.GetSingleValue('@vWeightFlowsheetID') as numeric) 

/*
--Not in LVHN workflow
Declare @vPassiveLegRaiseFlowsheetIDs table (id int identity(1,1), FLO_MEAS_ID numeric)
insert into @vPassiveLegRaiseFlowsheetIDs
select FLO_MEAS_ID from @vSystolicBPFlowsheetIDs
*/

/*
--Not in LVHN workflow
Declare @vcgSepsisRiskScoreFlowsheetID table (id int identity(1,1), FLO_MEAS_ID numeric)
insert into @vcgSepsisRiskScoreFlowsheetID
select FLO_MEAS_ID from @vSystolicBPFlowsheetIDs

Declare @vcgMDSuspectsSepsisFlowsheetID table (id int identity(1,1), FLO_MEAS_ID numeric)
insert into @vcgMDSuspectsSepsisFlowsheetID
select FLO_MEAS_ID from @vSystolicBPFlowsheetIDs

Declare @vcgEDArrivalMethodRowID table (id int identity(1,1), FLO_MEAS_ID numeric)
insert into @vcgEDArrivalMethodRowID
select FLO_MEAS_ID from @vSystolicBPFlowsheetIDs

Declare @vcgEDAcuityLevelRowID table (id int identity(1,1), FLO_MEAS_ID numeric)
insert into @vcgEDAcuityLevelRowID
select FLO_MEAS_ID from @vSystolicBPFlowsheetIDs

Declare @vcgEDDFlowsheetID table (id int identity(1,1), FLO_MEAS_ID numeric)
insert into @vcgEDDFlowsheetID
select FLO_MEAS_ID from @vSystolicBPFlowsheetIDs

Declare @vcgADDFlowsheetID table (id int identity(1,1), FLO_MEAS_ID numeric)
insert into @vcgADDFlowsheetID
select FLO_MEAS_ID from @vSystolicBPFlowsheetIDs
*/

--FlowsheetDetails
if OBJECT_ID('gt1.FlowsheetDetail', 'U') is not null drop table gt1.FlowsheetDetail;
create table [gt1].[FlowsheetDetail]
(
	[EncounterCSN] [numeric](18, 0) NOT NULL,
	[FLO_MEAS_ID] [varchar](18) NULL,
	[FlowsheetRecordedTime] [datetime] NULL,
	[FlowsheetValue] [varchar](2500) NULL,
	[SystolicBloodPressure] [int] NULL,
	[TemperatureF] [float] NULL,
	[TemperatureC] [float] NULL,
	[Pulse] [float] NULL,
	[RespirationRate] [float] NULL,
	[ScvO2] float null,
	[CVP] [float] NULL,
	[MAP] [float] NULL,
	[Urine] [float] NULL,
	[Weight] [float] NULL,
	[WeightKg] float NULL
)  
insert into [gt1].[FlowsheetDetail]
(	 [EncounterCSN]
    ,[FLO_MEAS_ID]
    ,[FlowsheetRecordedTime]
    ,[FlowsheetValue]
    ,[SystolicBloodPressure]
    ,[TemperatureF]
    ,[TemperatureC]
    ,[Pulse]
    ,[RespirationRate]
	,[ScvO2]
    ,[CVP]
    ,[MAP]
    ,[Urine]
    ,[Weight]
    ,[WeightKg]
) 
 select  
   PAT_ENC_HSP.PAT_ENC_CSN_ID as EncounterCSN
  ,IP_FLWSHT_MEAS.FLO_MEAS_ID  
  ,IP_FLWSHT_MEAS.RECORDED_TIME as FlowsheetRecordedTime
  ,IP_FLWSHT_MEAS.MEAS_VALUE as FlowsheetValue
  ,IIF(IP_FLWSHT_MEAS.FLO_MEAS_ID = @vSystolicBPFlowsheetID, cast(substring(cast(MEAS_VALUE as varchar), 1, charindex('/',Meas_Value,1)-1) as numeric),NULL) as SystolicBloodPressure
  --,IIF(IP_FLWSHT_MEAS.FLO_MEAS_ID IN  (select FLO_MEAS_ID from @vDiastolicBPFlowsheetIDs),SUBSTRING(IP_FLWSHT_MEAS.MEAS_VALUE,CHARINDEX(IP_FLWSHT_MEAS.MEAS_VALUE,'/') + 1,LEN(IP_FLWSHT_MEAS.MEAS_VALUE) - CHARINDEX(IP_FLWSHT_MEAS.MEAS_VALUE,'/')),NULL) as DiastolicBloodPressure
  ,IIF(IP_FLWSHT_MEAS.FLO_MEAS_ID IN  (select FLO_MEAS_ID from @vTemperatureFlowsheetID),cast(IP_FLWSHT_MEAS.MEAS_VALUE as float),NULL) as TemperatureF
  ,IIF(IP_FLWSHT_MEAS.FLO_MEAS_ID IN  (select FLO_MEAS_ID from @vTemperatureFlowsheetID),(cast(IP_FLWSHT_MEAS.MEAS_VALUE as float) - 32)*5/9,NULL) as TemperatureC
  ,IIF(IP_FLWSHT_MEAS.FLO_MEAS_ID IN  (select FLO_MEAS_ID from @vPulseFlowsheetID),cast(IP_FLWSHT_MEAS.MEAS_VALUE as float),NULL) as Pulse
  ,IIF(IP_FLWSHT_MEAS.FLO_MEAS_ID IN  (select FLO_MEAS_ID from @vRespirationRateFlowsheetID),cast(IP_FLWSHT_MEAS.MEAS_VALUE as float),NULL) as RespirationRate
  --,IIF(IP_FLWSHT_MEAS.FLO_MEAS_ID IN  (select FLO_MEAS_ID from @vFIO2FlowsheetIDs),Cast(IP_FLWSHT_MEAS.MEAS_VALUE as float),NULL) as FIO2
  ,IIF(IP_FLWSHT_MEAS.FLO_MEAS_ID IN  (select FLO_MEAS_ID from @vScvO2FlowsheetID),Cast(IP_FLWSHT_MEAS.MEAS_VALUE as float),NULL) as ScvO2
 -- ,IIF(IP_FLWSHT_MEAS.FLO_MEAS_ID IN  (select FLO_MEAS_ID from @vPRBCFlowsheetIDs),Cast(IP_FLWSHT_MEAS.MEAS_VALUE as float),NULL) as PRBC
  ,IIF(IP_FLWSHT_MEAS.FLO_MEAS_ID IN  (select FLO_MEAS_ID from @vCVPFlowsheetID),Cast(IP_FLWSHT_MEAS.MEAS_VALUE as float),NULL) as CVP
  ,IIF(IP_FLWSHT_MEAS.FLO_MEAS_ID IN  (select FLO_MEAS_ID from @vMAPFlowsheetID),Cast(IP_FLWSHT_MEAS.MEAS_VALUE as float),NULL) as MAP
  ,IIF(IP_FLWSHT_MEAS.FLO_MEAS_ID IN  (select FLO_MEAS_ID from @vUrineFlowsheetID),Cast(IP_FLWSHT_MEAS.MEAS_VALUE as float),NULL) as Urine
  ,IIF(IP_FLWSHT_MEAS.FLO_MEAS_ID IN  (select FLO_MEAS_ID from @vWeightFlowsheetID),Cast(IP_FLWSHT_MEAS.MEAS_VALUE as float),NULL) as Weight
  ,IIF(IP_FLWSHT_MEAS.FLO_MEAS_ID IN  (select FLO_MEAS_ID from @vWeightFlowsheetID), cast(IP_FLWSHT_MEAS.MEAS_VALUE as float)/35274.0,NULL) as WeightKg
  --,IIF(IP_FLWSHT_MEAS.FLO_MEAS_ID IN  (select FLO_MEAS_ID from @vPassiveLegRaiseFlowsheetIDs),IP_FLWSHT_MEAS.MEAS_VALUE,NULL) as PassiveLegRaise
  --,IIF(IP_FLWSHT_MEAS.FLO_MEAS_ID IN  (select FLO_MEAS_ID from @vcgSepsisRiskScoreFlowsheetID),Cast(IP_FLWSHT_MEAS.MEAS_VALUE as float),NULL) as SepsisRiskScore
  --,IIF(IP_FLWSHT_MEAS.FLO_MEAS_ID IN  (select FLO_MEAS_ID from @vcgMDSuspectsSepsisFlowsheetID), IP_FLWSHT_MEAS.MEAS_VALUE,NULL) as MDSuspectsSepsis
  --,IIF(IP_FLWSHT_MEAS.FLO_MEAS_ID IN  (select FLO_MEAS_ID from @vcgEDArrivalMethodRowID), 1,0) as EDArrivalMethodFlag
  --,IIF(IP_FLWSHT_MEAS.FLO_MEAS_ID IN  (select FLO_MEAS_ID from @vcgEDAcuityLevelRowID), 1,0) as EDAcuityFlag
  --,IIF(IP_FLWSHT_MEAS.FLO_MEAS_ID IN  (select FLO_MEAS_ID from @vcgEDDFlowsheetID), 1,0) as ExpectedDischargeDateFlag
  --,IIF(IP_FLWSHT_MEAS.FLO_MEAS_ID IN  (select FLO_MEAS_ID from @vcgADDFlowsheetID), 1,0) as AnticipatedDischargeDateFlag
  --,IIF(IP_FLWSHT_MEAS.FLO_MEAS_ID IN  (select FLO_MEAS_ID from @vcgSepsisRiskScoreFlowsheetID), 1,0) as SepsisRiskScoreFlag
  --,IIF(IP_FLWSHT_MEAS.FLO_MEAS_ID IN  (select FLO_MEAS_ID from @vcgMDSuspectsSepsisFlowsheetID), 1,0) as MDSuspectsSepsisFlag
from pa.ge.IP_FLWSHT_MEAS as IP_FLWSHT_MEAS
inner join pa.ge.IP_FLWSHT_REC as IP_FLWSHT_REC ON IP_FLWSHT_REC.FSD_ID = IP_FLWSHT_MEAS.FSD_ID
inner join pa.ge.PAT_ENC_HSP as PAT_ENC_HSP on IP_FLWSHT_REC.INPATIENT_DATA_ID=PAT_ENC_HSP.INPATIENT_DATA_ID

END  

---- indexes
create index ncix__FlowsheetDetail__EncounterCSN on gt1.FlowsheetDetail (EncounterCSN)
       include (FlowsheetRecordedTime, SystolicBloodPressure, TemperatureF, TemperatureC, Pulse,  RespirationRate, ScvO2, CVP, MAP, Urine, WeightKg)

-- check keys and indexes
exec tools.CheckKeysAndIndexes






GO
