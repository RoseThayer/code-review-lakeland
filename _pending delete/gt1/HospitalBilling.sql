alter procedure gt1.HospitalBilling as

/*
  TODO:    
    - performance
    - add checks for missing indexes?
    - should we be replacing null IDs (e.g. non-Epic data Billing Provider ID) with placeholders?
    - extract HSD_BASE_CLASS_MAP and uncomment fields and joins related to AccountBaseClass
    - look at null replacements for payor names when the financial class is not self-pay. Orig source (QV) set to 'SELF-PAY' but that doesn't seem to make sense
    - TxPostLag can be negative (there are rows in HSP_TRANSACTIONS where TX_POST_DATE < SERVICE_DATE, for some reason). How to address?
    - How to set TxPostLagGroup when TxPostLag is negative (if negative is allowed)    
    - validate
*/


/*  --> Source Tables <--
    ge.HSP_TRANSACTIONS

    --> Target Tables <--
		gt1.HBTransactions
    gt1.HBInactiveTransactions

		--> Performance Benchmark <--
    total run time ~ 5m
    gt1.HBTransactions : 59MM rows, 2h 52m <-- needs improvement
    gt1.HBInactiveTransactions : 14,986,200 rows, ???

*/

-- if table <schema.tablename> exists, drop it
if OBJECT_ID('gt1.HBInactiveTransactions', 'U') is not null drop table gt1.HBInactiveTransactions;
if OBJECT_ID('gt1.HBTransactions', 'U') is not null drop table gt1.HBTransactions;
if OBJECT_ID('tempdb..#tHBTransactions') is not null drop table #tHBTransactions;
if OBJECT_ID('tempdb..#tAdmitDep') is not null drop table #tAdmitDep;
if OBJECT_ID('tempdb..#HospitalPaymentType') is not null drop table #HospitalPaymentType;
if OBJECT_ID('tempdb..#HospitalChargeType') is not null drop table #HospitalChargeType;

--update statistics ge.HSP_TRANSACTIONS with fullscan
--update statistics ge.HSP_ACCOUNT with fullscan
--update statistics ge.CLARITY_DEP with fullscan
--update statistics ge.CLARITY_EPP with fullscan
--update statistics ge.CLARITY_SER with fullscan
--update statistics ge.CL_COST_CNTR with fullscan
--update statistics ge.CLARITY_LOC with fullscan
--update statistics ge.CLARITY_SA with fullscan
--update statistics ge.CL_UB_REV_CODE with fullscan
--update statistics ge.CLARITY_EAP with fullscan
--update statistics ge.HSP_BUCKET with fullscan
--update statistics ge.CLARITY_ADT with fullscan

/* Configuration variables */
declare @Unknown varchar(8) = '*Unknown';
declare @Unspecified varchar(12) = '*Unspecified';
declare @NotApplicable varchar(15) = '*Not Applicable';
declare @SelfPay varchar(8) = 'Self-pay';

declare @ApplicationStartDate date = variables.GetSingleValue('@vcgApplicationStartDate');
if @ApplicationStartDate is null
  set @ApplicationStartDate = '2015-01-01' 
;

create table #HospitalPaymentType (
  TransactionType		        int
  index ncix__HospitalPaymentType__TransactionType nonclustered (TransactionType)
)
;
insert into #HospitalPaymentType
select
	VariableValue
from variables.GetTableValues('@vcgHospitalPayments')
;

create table #HospitalChargeType (
  TransactionType		        int
  index ncix__HospitalChargeType__TransactionType nonclustered (TransactionType)
)
;
insert into #HospitalChargeType
select
	VariableValue
from variables.GetTableValues('@vcgHospitalCharges')
;

create table #HospitalTxType (
  TransactionType           int
  index ncix__HospitalTxType__TransactionType nonclustered (TransactionType)
)
;
insert into #HospitalTxType
select
	VariableValue
from variables.GetTableValues('@vcgHospitalCharges')
union all
select
	VariableValue
from variables.GetTableValues('@vcgHospitalPayments')
;

declare @BillingStatusExclusion table
(
    BillingStatusExclusionKey	int identity(1, 1)
  , AccountStatus 		        int
);
insert into @BillingStatusExclusion
select
	VariableValue
from variables.GetTableValues('@vcgBillingStatusToExclude')
;

declare @vcgLateChargeGroupBckt1 int = 7;
declare @vcgLateChargeGroupBckt1Name varchar(5) = '<7';
declare @vcgLateChargeGroupBckt2 int = 14;
declare @vcgLateChargeGroupBckt2Name varchar(5) = '7-14';
declare @vcgLateChargeGroupBckt3 int = 30;
declare @vcgLateChargeGroupBckt3Name varchar(5) = '15-30';
declare @vcgLastChargeOldestGroup varchar(3) = '30+';

-- create tHBTransactions
select 
    htx.TX_ID                                     as TxID
  , htx.HSP_ACCOUNT_ID                            as HospitalAccount
  , hspacct.PRIM_ENC_CSN_ID                       as tPrimaryEncCSNID
  , htx.ACCT_CLASS_HA_C                           as AccountClassID
  , iif(htx.ACCT_CLASS_HA_C is null
    , @Unspecified
    , iif(acctcls.ACCT_CLASS_HA_C is null
      , @Unknown
      , acctcls.NAME
      )
    )                                             as AccountClass
  , iif(htx.ACCT_CLASS_HA_C is null
  , @Unspecified
  , iif(acctbcls.ACCT_BASECLS_HA_C is null
     , @Unknown
     , acctbcls.NAME
     )
   )                                              as AccountBaseClass
  , hspacct.DISCH_DEPT_ID                         as DischargeDepartmentID
  , iif(htx.TX_ID >= 9990000000000000 -- this will need to be updated if change logic for identifying non-Epic data
      , 'Schuylkill'
      , iif(hspacct.DISCH_DEPT_ID is null
        , @Unspecified
        , iif(depdisch.DEPARTMENT_ID is null
          , @Unknown
          , depdisch.DEPARTMENT_NAME
        )
      )
    )                                             as DischargeDepartment
  , iif(htx.TX_ID >= 9990000000000000 -- this will need to be updated if change logic for identifying non-Epic data
      , 'Schuylkill'
        , iif(locdisch.RPT_GRP_SIX is null
          , @Unspecified
          , iif(dischhosp.RPT_GRP_SIX is null
            , @Unknown
            , dischhosp.NAME
          )
        )
    )                                             as DischargeHospital
  , htx.BILLING_PROV_ID                           as BillingProviderID
  , iif(htx.TX_ID >= 9990000000000000 -- this will need to be updated if change logic for identifying non-Epic data
    , 'Schuylkill Billing Provider'
    , iif(htx.BILLING_PROV_ID is null
      , @Unspecified
      , iif(ser.PROV_ID is null
        , @Unknown
        , ser.PROV_NAME
        )
      )
    )                                             as BillingProvider
  , htx.BUCKET_ID                                 as BucketID
  , htx.CPT_CODE                                  as CPTCode
  , htx.COST_CNTR_ID                              as CostCenterID
  , iif(htx.COST_CNTR_ID is null
    , @Unspecified
    , iif(ccc.COST_CNTR_ID is null
      , @Unknown
      , ccc.COST_CENTER_NAME
      )
    )                                             as CostCenter
  , iif(htx.COST_CNTR_ID is null
    , @Unspecified
    , iif(ccc.COST_CNTR_ID is null
      , @Unknown
      , ccc.COST_CENTER_CODE
      )
    )                                             as CostCenterCode
  , htx.DEPARTMENT                                as DepartmentID
  , iif(htx.DEPARTMENT is null
    , @Unspecified
    , iif(dep.DEPARTMENT_ID is null
      , @Unknown
      , dep.DEPARTMENT_NAME
      )
    )                                             as Department
  , iif(htx.IS_LATE_CHARGE_YN = 'Y'
    , 1
    , 0
    )                                             as IsLateChargeInd
  , iif(htx.IS_LATE_CHARGE_YN <> 'Y'
    , 'N'
    , htx.IS_LATE_CHARGE_YN
    )                                             as LateChargeFlag  
  , htx.PAT_ENC_CSN_ID                            as CSN  
  , hspacct.PRIMARY_PAYOR_ID                      as PayorID
  , iif(hspacct.PRIMARY_PLAN_ID is null
      , @SelfPay
      , iif(primpyr.PAYOR_ID is null
          , @Unknown
          , primpyr.PAYOR_NAME
        )
    )                                             as Payor
  , iif(hspacct.PRIMARY_PLAN_ID is null
      , @SelfPay
      , iif(hspacctepp.RPT_GRP_ONE is null
          , @Unknown
          , hspacctepp.RPT_GRP_ONE
        )
    )                                             as PayorAbbr
  , htx.PROC_ID                                   as ProcedureID
  , iif(htx.PROC_ID is null
    , @Unspecified
    , iif(eap.PROC_ID is null
      , @Unknown
      , eap.PROC_CODE
      )
    )                                             as ProcedureCode
  , iif(htx.PROC_ID is null
    , @Unspecified
    , iif(eap.PROC_ID is null
      , @Unknown
      , eap.PROC_NAME
      )
    )                                             as ProcedureDescription
  , htx.QUANTITY                                  as Quantity
  , htx.UB_REV_CODE_ID                            as RevCodeID
  , iif(htx.UB_REV_CODE_ID is null
    , @Unspecified
    , iif(revcode.UB_REV_CODE_ID is null
      , @Unknown
      , revcode.REVENUE_CODE
      )
    )                                             as RevenueCode
  /* Lehigh prefers to see location come from the HAR */
  --, htx.REVENUE_LOC_ID                            as LocationID
  --, iif(htx.REVENUE_LOC_ID is null
  --  , @Unspecified
  --  , iif(loc.LOC_ID is null
  --    , @Unknown
  --    , loc.LOC_NAME
  --    )
  --  )                                             as Location
  , hspacct.LOC_ID                                as LocationID
  , iif(hspacct.LOC_ID is null
    , @Unspecified
    , iif(loc.LOC_ID is null
      , @Unknown
      , loc.LOC_NAME
      )
    )                                             as Location  
  , htx.SERV_AREA_ID                              as ServiceAreaID
  , iif(htx.SERV_AREA_ID is null
    , @Unspecified
    , iif(sa.SERV_AREA_ID is null
      , @Unknown
      , sa.SERV_AREA_NAME
      )
    )                                             as ServiceArea
  , convert(date, htx.SERVICE_DATE)               as ServiceDate -- why convert?
  , htx.TX_AMOUNT                                 as Amount
  , iif(vchgtyp.TransactionType is not null
    , TX_AMOUNT
    , null
    )                                             as HBChargeAmount
  , iif(vpmttyp.TransactionType is not null
    , TX_AMOUNT * -1
    , null
    )                                             as HBPaymentAmount
  , convert(date, htx.TX_POST_DATE)               as PostDate -- why convert?
  , datediff(d
    , htx.SERVICE_DATE
    , htx.TX_POST_DATE
    )                                             as TxPostLag -- this can be negative, what to do?
  , case 
     when datediff(d, htx.SERVICE_DATE, htx.TX_POST_DATE) < @vcgLateChargeGroupBckt1 then @vcgLateChargeGroupBckt1Name
     when datediff(d, htx.SERVICE_DATE, htx.TX_POST_DATE) < @vcgLateChargeGroupBckt2 then @vcgLateChargeGroupBckt2Name
     when datediff(d, htx.SERVICE_DATE, htx.TX_POST_DATE) < @vcgLateChargeGroupBckt3 then @vcgLateChargeGroupBckt3Name
     else @vcgLastChargeOldestGroup
    end as TxPostLagGroup
  , htx.TX_SOURCE_HA_C                            as TxSourceID
  , iif(htx.TX_SOURCE_HA_C is null
    , @Unspecified
    , iif(txsrc.TX_SOURCE_HA_C is null            
      , @Unknown
      , txsrc.NAME
      )
    )                                             as TxSource
  , htx.TX_TYPE_HA_C                              as TxTypeID
  , iif(htx.TX_TYPE_HA_C is null
    , @Unspecified
    , iif(txtyp.TX_TYPE_HA_C is null
      , @Unknown
      , txtyp.NAME
      )
    )                                             as TxType
  , htx.INVOICE_NUM                               as InvoiceNumber
  , iif(htx.FIN_CLASS_C = '4'
    , '0'
    , iif(vchgtyp.TransactionType is not null
      , iif(htx.PRIMARY_PLAN_ID is null
        , '0'
        , htx.PRIMARY_PLAN_ID
        )
      , iif(bckt.BENEFIT_PLAN_ID is null
        , '0'
        , bckt.BENEFIT_PLAN_ID
        )
      )
    )                                             as BenefitPlanID
  , iif(htx.FIN_CLASS_C = '4'
    , @SelfPay
    , iif(vchgtyp.TransactionType is not null
      , iif(htx.PRIMARY_PLAN_ID is null
        , @SelfPay -- does SELF PAY make sense here or should it be *Unspecified?
        , iif(hspacctepp.BENEFIT_PLAN_ID is null
          , @Unknown
          , hspacctepp.BENEFIT_PLAN_NAME
          )
        )
      , iif(bckt.BENEFIT_PLAN_ID is null
        , @SelfPay -- does SELF PAY make sense here or should it be *Unspecified?
        , iif(bcktepp.BENEFIT_PLAN_ID is null
          , @Unknown
          , bcktepp.BENEFIT_PLAN_NAME
          )
        )
      )
    )                                             as BenefitPlan
  , iif(htx.FIN_CLASS_C = '4'
    , @SelfPay
    , iif(vchgtyp.TransactionType is not null
      , iif(htx.PRIMARY_PLAN_ID is null
        , @SelfPay -- does SELF PAY make sense here or should it be *Unspecified?
        , iif(hspacctepp.BENEFIT_PLAN_ID is null
          , @Unknown
          , iif(hspacctepp.PRODUCT_TYPE is null
            , @Unspecified
            , hspacctepp.PRODUCT_TYPE
            )
          )
        )
      , iif(bckt.BENEFIT_PLAN_ID is null
        , @SelfPay -- does SELF PAY make sense here or should it be *Unspecified?
        , iif(bcktepp.BENEFIT_PLAN_ID is null
          , @Unknown
          , iif(bcktepp.PRODUCT_TYPE is null
            , @Unspecified
            , bcktepp.PRODUCT_TYPE
            )
          )
        )
      )
    )                                             as PlanProductType    
  , 0                                             as InactiveTxInd
  , iif(htx.GL_CREDIT_NUM is null
    , '-1'
    , htx.GL_CREDIT_NUM
    )                                             as GLCreditNum
  , iif(htx.LATE_CRCTN_ORIG_ID is not null
    , 1
    , 0
    )                                             as LateCorrectionFlag
  , primpyr.FINANCIAL_CLASS                       as FinancialClassId
  , iif(hspacct.PRIMARY_PAYOR_ID is null
      , @SelfPay
      , iif(primpyr.FINANCIAL_CLASS is null
          , @Unspecified
          , iif(zfc.FINANCIAL_CLASS is null
              , @Unknown
              , zfc.NAME
            )
        )
    )                                             as FinancialClass
  , hspacct.ACCT_BILLSTS_HA_C                     as BillingStatusID
  , coalesce(pat3.IS_TEST_PAT_YN, 'N')            as TestPatientFlag
into #tHBTransactions 
from ge.HSP_TRANSACTIONS htx
inner join #HospitalTxType txtype
  on txtype.TransactionType = htx.TX_TYPE_HA_C
left join ge.ZC_ACCT_CLASS_HA acctcls
  on acctcls.ACCT_CLASS_HA_C = htx.ACCT_CLASS_HA_C
left join ge.ZC_TX_SOURCE_HA txsrc
  on txsrc.TX_SOURCE_HA_C = htx.TX_SOURCE_HA_C
left join ge.ZC_TX_TYPE_HA txtyp
  on txtyp.TX_TYPE_HA_C = htx.TX_TYPE_HA_C
left join ge.HSP_ACCOUNT hspacct
  on hspacct.HSP_ACCOUNT_ID = htx.HSP_ACCOUNT_ID
left join ge.ZC_ACCT_BASECLS_HA acctbcls
  on acctbcls.ACCT_BASECLS_HA_C = hspacct.ACCT_BASECLS_HA_C
left join ge.CLARITY_EPM primpyr
  on primpyr.PAYOR_ID = hspacct.PRIMARY_PAYOR_ID
left join ge.ZC_FINANCIAL_CLASS as zfc
  --on hspacct.ACCT_FIN_CLASS_C = zfc.FINANCIAL_CLASS  
  on primpyr.FINANCIAL_CLASS = zfc.FINANCIAL_CLASS
left join ge.CLARITY_DEP depdisch
  on depdisch.DEPARTMENT_ID = hspacct.DISCH_DEPT_ID
left join ge.CLARITY_LOC as locdisch
  on locdisch.LOC_ID = depdisch.REV_LOC_ID
left join ge.ZC_DEP_RPT_GRP_6 as dischhosp
  on dischhosp.RPT_GRP_SIX = locdisch.RPT_GRP_SIX
left join ge.CLARITY_EPP hspacctepp
  on hspacctepp.BENEFIT_PLAN_ID = hspacct.PRIMARY_PLAN_ID
left join ge.CLARITY_SER ser
  on ser.PROV_ID = htx.BILLING_PROV_ID
left join ge.CL_COST_CNTR ccc
  on ccc.COST_CNTR_ID = htx.COST_CNTR_ID
left join ge.CLARITY_DEP dep
  on dep.DEPARTMENT_ID = htx.DEPARTMENT
--left join ge.CLARITY_LOC loc
--  on loc.LOC_ID = htx.REVENUE_LOC_ID
left join ge.CLARITY_LOC loc
  on loc.LOC_ID = hspacct.LOC_ID
left join ge.CLARITY_SA sa
  on sa.SERV_AREA_ID = htx.SERV_AREA_ID
left join ge.CL_UB_REV_CODE revcode
  on revcode.UB_REV_CODE_ID = htx.UB_REV_CODE_ID
left join ge.CLARITY_EAP eap
  on eap.PROC_ID = htx.PROC_ID
left join #HospitalPaymentType vpmttyp
  on vpmttyp.TransactionType = htx.TX_TYPE_HA_C
left join #HospitalChargeType vchgtyp
  on vchgtyp.TransactionType = htx.TX_TYPE_HA_C
left join ge.HSP_BUCKET bckt
  on bckt.BUCKET_ID = htx.BUCKET_ID
left join ge.CLARITY_EPP bcktepp
  on bcktepp.BENEFIT_PLAN_ID = bckt.BENEFIT_PLAN_ID
left join ge.PATIENT_3 pat3
  on pat3.PAT_ID = hspacct.PAT_ID
--left join ge.CLARITY_EPM htxpyr
--  on htxpyr.PAYOR_ID = htx.PAYOR_ID
where
  htx.SERVICE_DATE >= @ApplicationStartDate
    and htx.TX_POST_DATE >= @ApplicationStartDate
option (optimize for(@ApplicationStartDate = '2015-02-01'))
;

-- get the latest admission department for each ADT PAT_ENC_CSN_ID
select 
    admitdep.PAT_ENC_CSN_ID
  , admitdep.DEPARTMENT_ID            as AdmitDepartmentID
  , iif(depadm.DEPARTMENT_ID is null
      , @Unknown
      , depadm.DEPARTMENT_NAME
    )                                 as AdmitDepartment
  , iif(locadm.RPT_GRP_SIX is null
      , @Unspecified
      , iif(admithosp.RPT_GRP_SIX is null
          , @Unknown
          , admithosp.NAME
        )
    )                                 as AdmitHospital
into #tAdmitDep
from ge.CLARITY_ADT admitdep 
left join ge.clarity_adt admitdep_next 
  on admitdep.PAT_ENC_CSN_ID = admitdep_next.PAT_ENC_CSN_ID
	and admitdep.EVENT_TYPE_C = admitdep_next.EVENT_TYPE_C
    and admitdep.EVENT_ID < admitdep_next.EVENT_ID 
left join ge.CLARITY_DEP depadm
  on depadm.DEPARTMENT_ID = admitdep.DEPARTMENT_ID
left join ge.CLARITY_LOC as locadm
  on locadm.LOC_ID = depadm.REV_LOC_ID
left join ge.ZC_DEP_RPT_GRP_6 as admithosp
  on admithosp.RPT_GRP_SIX = locadm.RPT_GRP_SIX
where
  admitdep.EVENT_TYPE_C = 1 -- admit
    and admitdep_next.event_id is null -- get latest admission record from ADT
;

-- add the admission department info
select
    htx.*
  , t.AdmitDepartmentID
  , iif(htx.TxID >= 9990000000000000
      , 'Schuylkill'
      , iif(t.AdmitDepartment is null
        , @Unspecified
        , t.AdmitDepartment
      )
    )                                 as AdmitDepartment
  , iif(htx.TxID >= 9990000000000000
      , 'Schuylkill'
      , iif(t.AdmitDepartment is null
        , @Unspecified
        , t.AdmitHospital
      )
    )                                 as AdmitHospital
into gt1.HBTransactions
from #tHBTransactions htx
left join #tAdmitDep t
  on t.PAT_ENC_CSN_ID = htx.tPrimaryEncCSNID 
where
 htx.TestPatientFlag = 'N'
;

create nonclustered index ncix__HBTransactions__TxID on gt1.HBTransactions (TxID);

-- update the InactiveTxInd flag
update gt1.HBTransactions
  set InactiveTxInd = 
    iif(inactvtx.TX_ID is not null
      , 1
      , 0
      )  
from gt1.HBTransactions htx
inner join ge.F_ARHB_INACTIVE_TX inactvtx
  on inactvtx.TX_ID = htx.TxID
;

-- clean up
if OBJECT_ID('gt1.tHBTransactions') is not null drop table gt1.tHBTransactions;
if OBJECT_ID('tempdb..#HospitalPaymentType') is not null drop table #HospitalPaymentType;
if OBJECT_ID('tempdb..#HospitalChargeType') is not null drop table #HospitalChargeType;
if OBJECT_ID('tempdb..#tAdmitDep') is not null drop table #tAdmitDep;

alter table gt1.HBTransactions drop column tPrimaryEncCSNID;

create nonclustered index ncix__HBTransactions__CSN on gt1.HBTransactions (CSN);

-- create gt1.HBInactiveTransactions
select 
    inactvtx.TX_ID                          as TxID
  , inactvtx.IS_REVERSAL                    as IsReversal
  , convert(date, inactvtx.INACTIVE_DATE)   as InactiveDate
into gt1.HBInactiveTransactions
from ge.F_ARHB_INACTIVE_TX inactvtx
inner join ge.HSP_TRANSACTIONS htx
  on inactvtx.TX_ID = htx.TX_ID
;

create nonclustered index ncix__HBInactiveTransactions_TxID on gt1.HBInactiveTransactions (TxID);

create nonclustered index ncix__HBTransactions__TxTypeIDEtAl_1 on gt1.HBTransactions (TxTypeID ASC)
include ( 	
    TxID
	, InactiveTxInd
	, HospitalAccount
)

create nonclustered index ncix__HBTransactions__TxTypeIDEtAl_2 on gt1.HBTransactions (TxTypeID ASC)
include ( 	
    TxID
	, Quantity
	, ServiceDate
	, HospitalAccount
	, DepartmentID
	, ServiceArea
)

create nonclustered index ncix__HBTransactions__TxTypeIdEtAl_3 on gt1.HBTransactions (TxTypeID ASC)
include ( 	
    TxID
	, InactiveTxInd
	, HospitalAccount
)

create nonclustered index ncix__HBTransactions__HospitalAccount on gt1.HBTransactions (HospitalAccount ASC)

create nonclustered index ncix__HBTransactions__InactiveTxID on gt1.HBTransactions (InactiveTxInd ASC)

create nonclustered index ncix__HBTransactions__Quantity on gt1.HBTransactions (Quantity ASC)

create nonclustered index ncix__HBTransactions__RevenueCode on gt1.HBTransactions (RevenueCode ASC)

create nonclustered index ncix__HBTransactions__ServiceDate on gt1.HBTransactions (ServiceDate ASC)

create nonclustered index ncix__HBTransactions__TxTypeID on gt1.HBTransactions (TxTypeID ASC)

-- check keys and indexes
exec tools.CheckKeysAndIndexes