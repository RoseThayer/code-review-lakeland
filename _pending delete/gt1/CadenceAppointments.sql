--alter procedure gt1.CadenceAppointments as

/*  --> Source Tables <--
    ge.F_SCHED_APPT
    ge.CLARITY_SER
    ge.CLARITY_DEP
    ge.CLARITY_PRC
    ge.CLARITY_EPM
    ge.COVERAGE
    ge.ACCESS_PROV

    --> Target Tables <--
    gt1.Appointments
    gt1.ProviderAccessibility

    --> Performance Benchmark <--
    using a min date of 1/1/2017 (1:14 total)
    gt1.Appointments: 3771633 row 
    gt1.ProviderAccessibility: 5787216 rows 

*/

/* configuration variables */

declare @UnknownLabel varchar(8) = '*Unknown';
declare @UnspecifiedLabel varchar(12) = '*Unspecified';
declare @NotApplicableLabel varchar(15) = '*Not Applicable';
declare @SelfPayLabel varchar(8) = 'Self-pay';

declare @AppointmentStatus table ( APPT_STATUS_C int, Description varchar(80) );

insert into @AppointmentStatus select variables.GetSingleValue('@vcgNoShowAppointments'), 'No Show';
insert into @AppointmentStatus select variables.GetSingleValue('@vcgCompletedAppointments'), 'Completed';

declare @StartDate date = variables.GetSingleValue('@AmbulatoryStartDate');
declare @Today date = convert(date, getdate());
declare @Yesterday date = DateAdd( Day, -1, @Today );

declare @NonResourceProviderType table ( PROVIDER_TYPE_C varchar(66))
;

insert into @NonResourceProviderType
select VariableValue
from variables.GetTableValues('@vcgNonResourceProviderTypes')
;

-- if table <schema.tablename> exists, drop it
if object_id('gt1.Appointments', 'U') is not null
  drop table gt1.Appointments
  ;

create table [gt1].[Appointments](
	[VisitID] [numeric](18, 0) NOT NULL,
	[VisitDate] [date] NULL,
	[VisitDateTime] [datetime] NULL,
	[VisitStatusId] [int] NULL,
	[VisitDepartmentID] [numeric](18, 0) NULL,
	[VisitDepartment] [varchar](254) NULL,
	[VisitProviderID] [varchar](18) NULL,
	[VisitProvider] [varchar](200) NULL,
	[VisitProviderType] [varchar](66) NULL,
	[VisitType] [varchar](200) NULL,
	[VisitPayorID] [numeric](18, 0) NULL,
	[VisitPayor] [varchar](80) NULL,
	[VisitPayorAbbr] [varchar](80) NULL,
	[VisitPayorFinancialClass] [varchar](254) NULL,
	[VisitMadeDate] [date] NULL,
	[ScheduleLag] [int] NULL,
	[PatientID] [varchar](18) NULL,
	[VisitStatus] [varchar](80) NULL,
	[HospitalLocationID] [varchar](66) NULL,
	[HospitalLocation] [varchar](254) NULL,
	[HospitalEntity] [varchar](254) NULL
)  
insert into [gt1].[Appointments]
           ([VisitID]
           ,[VisitDate]
           ,[VisitDateTime]
           ,[VisitStatusId]
           ,[VisitDepartmentID]
           ,[VisitDepartment]
           ,[VisitProviderID]
           ,[VisitProvider]
           ,[VisitProviderType]
           ,[VisitType]
           ,[VisitPayorID]
           ,[VisitPayor]
           ,[VisitPayorAbbr]
           ,[VisitPayorFinancialClass]
           ,[VisitMadeDate]
           ,[ScheduleLag]
           ,[PatientID]
           ,[VisitStatus]
           ,[HospitalLocationID]
           ,[HospitalLocation]
           ,[HospitalEntity])
 
select
    fsa.PAT_ENC_CSN_ID                                  as VisitID
  , Convert(date, fsa.APPT_DTTM)                        as VisitDate
  , fsa.APPT_DTTM                                       as VisitDateTime
  , fsa.APPT_STATUS_C                                   as VisitStatusId
  , fsa.DEPARTMENT_ID                                   as VisitDepartmentID
  , Iif(fsa.DEPARTMENT_ID is null
      , @UnspecifiedLabel
      , Iif(dep.DEPARTMENT_ID is null
          , @UnknownLabel
          , dep.DEPARTMENT_NAME
        )
    )                                                   as VisitDepartment
  , fsa.PROV_ID                                         as VisitProviderID
  , Iif(fsa.PROV_ID is null
      , @UnspecifiedLabel
      , Iif(ser.PROV_ID is null
          , @UnknownLabel
          , ser.PROV_NAME
        )
    )                                                   as VisitProvider
  , Iif(fsa.PROV_ID is null
      , @UnspecifiedLabel
      , Iif(ser.PROV_ID is null
          , @UnknownLabel
          , ser.PROV_TYPE
        )
    )                                                   as VisitProviderType
  , Iif(fsa.PRC_ID is null
      , @UnspecifiedLabel
      , Iif(prc.PRC_ID is null
          , @UnknownLabel
          , prc.PRC_NAME
        )
    )                                                   as VisitType
  , Iif(fsa.COVERAGE_ID is null
      , null
      , Iif(cvg.PAYOR_ID is null
          , null
          , cvg.PAYOR_ID
        )
    )                                                   as VisitPayorID
  , Iif(fsa.COVERAGE_ID is null
      , @SelfPayLabel
      , iif(cvg.PAYOR_ID is null
          , @UnspecifiedLabel
          , iif(epm.PAYOR_ID is null
              , @UnknownLabel
              , epm.PAYOR_NAME
            )
        )
    )                                                   as VisitPayor
  , iif(fsa.COVERAGE_ID is null
      , @SelfPayLabel
      , iif(cvg.PLAN_ID is null
          , @UnspecifiedLabel
          , iif(epp.RPT_GRP_ONE is null
              , @UnknownLabel
              , epp.RPT_GRP_ONE
            )
        )
    )                                                   as VisitPayorAbbr
  , iif(fsa.COVERAGE_ID is null
      , @SelfPayLabel
      , iif(epm.FINANCIAL_CLASS is null
          , @UnspecifiedLabel
          , iif(zfc.FINANCIAL_CLASS is null
              , @UnknownLabel
              , zfc.NAME
            )
        )
    )                                                   as VisitPayorFinancialClass
  , cast(fsa.APPT_MADE_DTTM as date)                    as VisitMadeDate
  , datediff(day, fsa.APPT_MADE_DTTM, fsa.APPT_DTTM)    as ScheduleLag
  , fsa.PAT_ID                                          as PatientID
  , apptsts.Description                                 as VisitStatus
  , dep.RPT_GRP_NINE                                    as HospitalLocationID
  , IsNull(zdrg.Name, @UnknownLabel)                    as HospitalLocation
  , IsNull(ec.Name, @UnknownLabel)                      as HospitalEntity 
from ge.F_SCHED_APPT fsa
inner join @AppointmentStatus as apptsts
  on fsa.APPT_STATUS_C = apptsts.APPT_STATUS_C
left join ge.PATIENT_3 pthree
  on fsa.PAT_ID = pthree.PAT_ID
left join ge.CLARITY_DEP dep
  on dep.DEPARTMENT_ID = fsa.DEPARTMENT_ID
inner join ge.ZC_DEP_RPT_GRP_9 zdrg
  on dep.RPT_GRP_NINE = zdrg.RPT_GRP_NINE
left join variables.EntityCrosswalk ec
  on dep.RPT_GRP_NINE = ec.RPT_GRP_NINE
inner join ge.CLARITY_SER ser
  on ser.PROV_ID = fsa.PROV_ID
left join @NonResourceProviderType nrpt
  on nrpt.PROVIDER_TYPE_C = ser.PROVIDER_TYPE_C
left join ge.CLARITY_PRC prc
  on prc.PRC_ID = fsa.PRC_ID
left join ge.COVERAGE cvg
  on cvg.COVERAGE_ID = fsa.COVERAGE_ID
left join ge.CLARITY_EPM epm
  on epm.PAYOR_ID = cvg.PAYOR_ID
left join ge.CLARITY_EPP epp
  on epp.BENEFIT_PLAN_ID = cvg.PLAN_ID
left join ge.ZC_FINANCIAL_CLASS zfc
  on zfc.FINANCIAL_CLASS = epm.FINANCIAL_CLASS
where cast( fsa.APPT_DTTM as date ) between @StartDate and @Yesterday
and nrpt.PROVIDER_TYPE_C is null
and zdrg.NAME like '%LVPG%' -- Filter out HODs
and ( pthree.IS_TEST_PAT_YN is null or pthree.IS_TEST_PAT_YN = 'N' )
;

insert into @NonResourceProviderType
select VariableValue
from variables.GetTableValues('@vcgNonResourceProviderTypes')
;

if object_id('gt1.ProviderAccessibility', 'U') is not null
  drop table gt1.ProviderAccessibility
  ;

create table [gt1].[ProviderAccessibility](
	[ProviderID] [varchar](18) NOT NULL,
	[ProviderName] [varchar](200) NULL,
	[ProviderType] [varchar](66) NULL,
	[DepartmentID] [numeric](18, 0) NOT NULL,
	[Department] [varchar](254) NULL,
	[SearchDate] [date] NULL,
	[SlotDate] [date] NULL,
	[SlotTime] [time](7) NULL,
	[SlotHour] [int] NULL,
	[DaysWait] [int] NULL,
	[SlotLength] [int] NULL,
	[HospitalLocationID] [varchar](66) NULL,
	[HospitalLocation] [varchar](254) NULL,
	[HospitalEntity] [varchar](254) NULL
) 
insert into [gt1].[ProviderAccessibility]
           ([ProviderID]
           ,[ProviderName]
           ,[ProviderType]
           ,[DepartmentID]
           ,[Department]
           ,[SearchDate]
           ,[SlotDate]
           ,[SlotTime]
           ,[SlotHour]
           ,[DaysWait]
           ,[SlotLength]
           ,[HospitalLocationID]
           ,[HospitalLocation]
           ,[HospitalEntity])
select
  acp.PROV_ID                                           as ProviderID
, ser.PROV_NAME                                         as ProviderName
, ser.PROV_TYPE                                         as ProviderType
, acp.DEPARTMENT_ID                                     as DepartmentID
, dep.DEPARTMENT_NAME                                   as Department
, convert(date, acp.SEARCH_DATE)                        as SearchDate
, convert(date, acp.SLOT_TIME)                          as SlotDate
, convert(time, acp.SLOT_TIME)                          as SlotTime
, datepart(hour, convert(time, acp.SLOT_TIME))          as SlotHour
, acp.DAYS_WAIT                                         as DaysWait
, acp.SLOT_LENGTH                                       as SlotLength
, dep.RPT_GRP_NINE                                      as HospitalLocationID
, IsNull(zdrg.Name, @UnknownLabel)                      as HospitalLocation
, IsNull(ec.Name, @UnknownLabel)                        as HospitalEntity 
from ge.ACCESS_PROV acp
  inner join ge.CLARITY_SER ser
    on ser.PROV_ID = acp.PROV_ID
  left join @NonResourceProviderType nrpt
    on nrpt.PROVIDER_TYPE_C = ser.PROVIDER_TYPE_C
  Left join ge.CLARITY_DEP dep
    on acp.DEPARTMENT_ID = dep.DEPARTMENT_ID
  left join ge.ZC_DEP_RPT_GRP_9 zdrg
    on dep.RPT_GRP_NINE = zdrg.RPT_GRP_NINE
  left join variables.EntityCrosswalk ec
    on dep.RPT_GRP_NINE = ec.RPT_GRP_NINE
where acp.SEARCH_DATE >= @StartDate
  and acp.SEARCH_DATE <= GetDate()
  and nrpt.PROVIDER_TYPE_C is null
;

create nonclustered index ncix__ProviderAccessibility__ProviderID
on gt1.ProviderAccessibility ( ProviderID )
;

-- check keys and indexes
exec tools.CheckKeysAndIndexes

 
--create nonclustered index ncix__F_SCHED_APPT__COVERAGE_ID on ge.F_SCHED_APPT (COVERAGE_ID);
--create nonclustered index ncix__COVERAGE__COVERAGE_ID on ge.COVERAGE (COVERAGE_ID);
--create nonclustered index ncix__COVERAGE__PAYOR_ID on ge.COVERAGE (PAYOR_ID);
--create nonclustered index ncix__CLARITY_EPM__PAYOR_ID on ge.CLARITY_EPM (PAYOR_ID);
