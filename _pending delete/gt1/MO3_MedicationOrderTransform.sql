SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
 

CREATE procedure [gt1].[MO3_MedicationOrderTransform] 
as

/*  --> Source Tables <--
	    pa.ge.ORDER_SMARTSET
	    pa.ge.CL_PRL_SS  
 
	--> Target Table <--
	    gt1.MedicationOrder 

	--> Performance Benchmark <--
		1557795 rows ~11s

*/		

--declare variables
 
declare @vSepsisOrderSet table (id int identity(1,1), PROTOCOL_ID numeric)  
insert into @vSepsisOrderSet
select PROTOCOL_ID from pa.ge.CL_PRL_SS where PROTOCOL_NAME like '%sep%'

declare @vSepsisIPOrderSet table (id int identity(1,1), PROTOCOL_ID numeric)
insert into @vSepsisIPOrderSet
select PROTOCOL_ID from pa.ge.CL_PRL_SS where PROTOCOL_NAME like '%sep%'

declare @vSepsisEDOrderSet table (id int identity(1,1), PROTOCOL_ID numeric)
insert into @vSepsisEDOrderSet
select PROTOCOL_ID from pa.ge.CL_PRL_SS where PROTOCOL_NAME like '%sep%'


--MedicationOrders
if exists (select top 1 * from INFORMATION_SCHEMA.TABLES where TABLE_NAME = N'MedicationOrder' and TABLE_SCHEMA = N'gt1') drop table gt1.MedicationOrder
create table [gt1].[MedicationOrder]
(
	[ORDER_MED_ID] [numeric](18, 0) NOT NULL,
	[SS_PRL_ID] [numeric](18, 0) NULL,
	[SepsisOrdersetUsedFlag] [varchar](1) NOT NULL,
	[SepsisIPOrdersetUsedFlag] [varchar](1) NOT NULL,
	[SepsisEDOrdersetUsedFlag] [varchar](1) NOT NULL,
	[OrdersetName] [varchar](200) NOT NULL
)  
insert into [gt1].[MedicationOrder]
(
	 [ORDER_MED_ID]
    ,[SS_PRL_ID]
    ,[SepsisOrdersetUsedFlag]
    ,[SepsisIPOrdersetUsedFlag]
    ,[SepsisEDOrdersetUsedFlag]
    ,[OrdersetName]
)
select  
   oss.ORDER_ID as ORDER_MED_ID 
  ,oss.SS_PRL_ID
  ,iif(sos.PROTOCOL_ID is not null, 1,0) as SepsisOrdersetUsedFlag
  ,iif(sip.PROTOCOL_ID is not null, 1,0) as SepsisIPOrdersetUsedFlag
  ,iif(sed.PROTOCOL_ID is not null, 1,0) as SepsisEDOrdersetUsedFlag
  ,isnull(cps.PROTOCOL_NAME, '*Unknown') as OrdersetName
from pa.ge.ORDER_SMARTSET oss
inner join gt1.OrderMed omd
  on oss.ORDER_ID = omd.ORDER_MED_ID
left join @vSepsisOrderSet sos
  on oss.SS_PRL_ID = sos.PROTOCOL_ID  
left join @vSepsisIPOrderSet sip
  on oss.SS_PRL_ID = sos.PROTOCOL_ID  
left join @vSepsisEDOrderSet sed
  on oss.SS_PRL_ID = sos.PROTOCOL_ID 
left join pa.ge.CL_PRL_SS cps
  on oss.SS_PRL_ID = cps.PROTOCOL_ID  

  create index ncix__MedicationOrder__ORDER_MED_ID on gt1.MedicationOrder(ORDER_MED_ID) include (SepsisOrdersetUsedFlag, OrdersetName)
 
 -- check keys and indexes
exec tools.CheckKeysAndIndexes







GO
