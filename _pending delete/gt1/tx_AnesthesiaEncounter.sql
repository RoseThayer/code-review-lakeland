if object_id('gt1.AnesthesiaEncounter', 'U') is null drop table gt1.AnesthesiaEncounter;

with LatestEpisode as (
	select
		AN_LOG_ID
		max(AN_EPISODE_ID) as MaxEpisodeID
	from ge.F_AN_RECORD_SUMMARY
)

create table gt1.AnesthesiaEncounter (
	AnesthesiaResponsibleProviderID [numeric](18, 0) NULL
	,SurgeryDate [date] NULL
	,AnesthesiaStartTime [datetime] NULL
	,AnesthesiaStopTime [datetime] NULL
	,Anesthesia52PatientEncounterID [numeric] (18,0) NULL
	,ANProcedureName [varchar](254)
	,EpisodeID [numeric] (18,0) NULL
	,PatientID [numeric] (18,0) NULL
	,LogID [numeric] (18,0) NOT NULL --expecting data here, required for linking
)
insert into gt1.AnesthesiaEncounter
(
	AnesthesiaResponsibleProviderID -- field order in insert must match select
	,SurgeryDate
	,AnesthesiaStartTime
	,AnesthesiaStopTime
	,Anesthesia52PatientEncounterID
	,ANProcedureName
	,EpisodeID
	,PatientID
	,LogID
)
select
	an.AN_RESP_PROV_ID as AnesthesiaResponsibleProviderID -- field order in create must match insert
	,convert(an.AN_DATE,date) as SurgeryDate
	,an.AN_START_DATETIME as AnesthesiaStartTime
	,an.AN_STOP_DATETIME as AnesthesiaStopTime
	,an.AN_52_ENC_CSN_ID as Anesthesia52PatientEncounterID
	,an.AN_PROC_NAME as ANProcedureName
	,an.AN_EPISODE_ID as EpisodeID
	,an.AN_PAT_ID as PatientID
	--AN_53_ENC_CSN_ID,
	--AN_INPATIENT_DATA_ID,
	,an.AN_LOG_ID as LogID
	--AN_PREOP_NOTE_ID,
	--AN_BLOCK_NOTE_ID
from ge.F_AN_RECORD_SUMMARY an
inner join LatestEpisode ep on
	on ep.MaxEpisodeID=an.AN_EPISODE_ID --keep only the most recent episode
;
