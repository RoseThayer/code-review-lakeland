SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
 
 
CREATE procedure [gt1].[ED0_EDEventTransform]
as 
/*
	--> Source Tables used: <--
	    pa.ge.ED_IEV_EVENT_INFO
		pa.ge.ED_IEV_PAT_INFO 

	--> Target tables created: <--
		padev.gt1.EDEvent

	--> Performance benchmark: <--
		559527 rows in ~3s
*/
 
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

--Set variables
declare @vcgEDArrivalEventID as VARCHAR (50) = (select variables.GetSingleValue ('@vcgEDArrivalEventID')) 
     , @EDTriageStartedEventID as varchar(50) = (select variables.GetSingleValue ('@EDTriageStartedEventID'));

if exists 
	(select top 1 * from INFORMATION_SCHEMA.TABLES where TABLE_NAME = N'EDEvent' and TABLE_SCHEMA = N'gt1')
drop table gt1.EDEvent
create table [gt1].[EDEvent]
(
	[EventID] numeric(18,0) NOT NULL,
	[EventType] [varchar](18) NULL,
	[EncounterCSN] [numeric](18, 0) NULL,
	[Line] [numeric](18, 0)  NOT NULL,
	[EventTime] [datetime] NULL,
	[EventDepartmentID] [numeric](18, 0) NULL,
	[EDArrival] [datetime] NULL,
	[EDTriageStarted] [datetime] NULL
) 
insert into [gt1].[EDEvent]
(	 [EventID]
    ,[EventType]
    ,[EncounterCSN]
    ,[Line]
    ,[EventTime]
    ,[EventDepartmentID]
    ,[EDArrival]
	,[EDTriageStarted]
)
select 
	 info.EVENT_ID as EventID
	,info.EVENT_TYPE as EventType
	,pat.PAT_ENC_CSN_ID as EncounterCSN
	,info.LINE as Line
	,info.EVENT_TIME as EventTime
	,info.EVENT_DEPT_ID as EventDepartmentID
	,iif(info.EVENT_TYPE = @vcgEDArrivalEventID, info.EVENT_TIME, null) as EDArrival
	,iif(info.EVENT_TYPE = @EDTriageStartedEventID, info.EVENT_TIME, null) as EDTriageStarted
from pa.ge.ED_IEV_EVENT_INFO info
left join pa.ge.ED_IEV_PAT_INFO pat
  on info.EVENT_ID = pat.EVENT_ID
end
 
-- check keys and indexes
exec tools.CheckKeysAndIndexes


 




GO
