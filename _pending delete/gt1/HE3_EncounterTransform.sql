SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
 
 CREATE procedure [gt1].[HE3_EncounterTransform] as 

declare @start datetime = getdate()

/*
-->	Source tables used <--
    PA.ge.PAT_ENC_HSP
	PA.ge.HSP_ACCOUNT
	gt1.CodeStatus
	gt1.PatientEncounterCurrentMed
	gt1.OrderResult
 
--> Target table created <--
	gt1.Encounter

--> Performance benchmark <--
    1517122 rows in 1:30
*/	 
-- Encounters
if exists (select * from information_schema.tables where TABLE_NAME = 'Encounter' and TABLE_SCHEMA = 'gt1')
   drop table gt1.Encounter
create table [gt1].Encounter
(
	[PAT_ENC_CSN_ID] [numeric](18, 0) NOT NULL,
	[PAT_ID] [varchar](18) NULL,
	[DEPARTMENT_ID] [numeric](18, 0) NULL,
	[HOSP_ADMSN_TIME] [datetime] NULL,
	[PAT_ENC_DATE_REAL] [numeric] (18, 0) NULL,
	[ADT_PATIENT_STAT_C] [int] NULL,
	[HSP_ACCOUNT_ID] [numeric](18, 0) NULL,
	[INPATIENT_DATA_ID] [varchar](18) NULL,
	[DISCH_DISP_C] [varchar](66) NULL,
	[HOSP_DISCH_TIME] [datetime] NULL,
	[ED_DISPOSITION_C] [varchar](66) NULL,
	[ED_DEPARTURE_TIME] [datetime] NULL,
	[ED_DISP_TIME] [datetime] NULL,
	[ED_EPISODE_ID] [numeric](18, 0) NULL,
	[ADMISSION_PROV_ID] [varchar](18) NULL,
	[DISCHARGE_PROV_ID] [varchar](18) NULL,
	[INP_ADM_DATE] [datetime] NULL,
	[INP_ADM_EVENT_ID] [numeric](18, 0) NULL,
	[EMER_ADM_EVENT_ID] [numeric](18, 0) NULL,
	[EMER_ADM_DATE] [datetime] NULL,
	[OP_ADM_DATE] datetime null, 
	[ADMIT_SOURCE_C] [varchar](66) NULL,
	[LEVEL_OF_CARE_C] [varchar](66) NULL,
	[ADT_PAT_CLASS_C] [varchar](66) NULL,
	[HOSP_SERV_C] [varchar](66) NULL,
	[ADT_ARRIVAL_TIME] [datetime] NULL, 
	[ADMISSION_SOURCE_C] varchar(100) null, 
	[ACCT_BASECLS_HA_C] varchar(100) null, 
	[ACCT_FIN_CLASS_C] varchar(100) null, 
	[HasDNRStatus] varchar(1) null, 
	[CodeStatuses] varchar(500) null, 
	--LengthOfStayNowIfNull varchar(50) null,
	CoumadinFlag int null, 
	HeparinFlag int null
)   
 
insert into gt1.Encounter (PAT_ENC_CSN_ID, HasDNRStatus, CoumadinFlag, HeparinFlag)
select  
	peh.PAT_ENC_CSN_ID, 
	iif(max(cs.[DNRStatus]) = 1, 1, 0) as [HasDNRStatus],
	iif(max(pem.MedicationIsCoumadin) = 1, 1, 0) as CoumadinFlag,   
	iif(max(pem.MedicationIsHeparin) = 1, 1, 0) as HeparinFlag  
from PA.ge.PAT_ENC_HSP peh with (nolock)
left join gt1.CodeStatus cs
  on peh.PAT_ENC_CSN_ID = cs.PAT_ENC_CSN_ID
left join gt1.PatientEncounterCurrentMed pem  with (nolock)
  on peh.PAT_ENC_CSN_ID = pem.PAT_ENC_CSN_ID
group by peh.PAT_ENC_CSN_ID  

alter table gt1.Encounter add primary key (PAT_ENC_CSN_ID)   

update gt1.Encounter
   set [PAT_ID] = peh.[PAT_ID]
      ,[DEPARTMENT_ID] = peh.[DEPARTMENT_ID]
      ,[HOSP_ADMSN_TIME] = peh.[HOSP_ADMSN_TIME]
	  ,[PAT_ENC_DATE_REAL] = cast(peh.[PAT_ENC_DATE_REAL] as numeric(18,0))
      ,[ADT_PATIENT_STAT_C] = peh.[ADT_PATIENT_STAT_C]
      ,[HSP_ACCOUNT_ID] = peh.[HSP_ACCOUNT_ID]
      ,[INPATIENT_DATA_ID] = peh.[INPATIENT_DATA_ID]
      ,[DISCH_DISP_C] = peh.[DISCH_DISP_C]
      ,[HOSP_DISCH_TIME] = peh.[HOSP_DISCH_TIME]
      ,[ED_DISPOSITION_C] = peh.[ED_DISPOSITION_C]
      ,[ED_DEPARTURE_TIME] = peh.[ED_DEPARTURE_TIME]
      ,[ED_DISP_TIME] = peh.[ED_DISP_TIME]
      ,[ED_EPISODE_ID] = peh.[ED_EPISODE_ID]
      ,[ADMISSION_PROV_ID] = peh.[ADMISSION_PROV_ID]
      ,[DISCHARGE_PROV_ID] = peh.[DISCHARGE_PROV_ID]
      ,[INP_ADM_DATE] = peh.[INP_ADM_DATE]
      ,[INP_ADM_EVENT_ID] = peh.[INP_ADM_EVENT_ID]
      ,[EMER_ADM_EVENT_ID] = peh.[EMER_ADM_EVENT_ID]
      ,[EMER_ADM_DATE] = peh.[EMER_ADM_DATE]
	  ,[OP_ADM_DATE] = peh.[OP_ADM_DATE]
      ,[ADMIT_SOURCE_C] = peh.[ADMIT_SOURCE_C]
      ,[LEVEL_OF_CARE_C] = peh.[LEVEL_OF_CARE_C]
      ,[ADT_PAT_CLASS_C] = peh.[ADT_PAT_CLASS_C]
      ,[HOSP_SERV_C] = peh.[HOSP_SERV_C]
      ,[ADT_ARRIVAL_TIME] = peh.[ADT_ARRIVAL_TIME]
      ,ADMISSION_SOURCE_C = ha.ADMISSION_SOURCE_C  
  	  ,ACCT_BASECLS_HA_C = ha.ACCT_BASECLS_HA_C   
  	  ,ACCT_FIN_CLASS_C = ha.ACCT_FIN_CLASS_C  
      ,CodeStatuses = cs.CodeStatusList  	
	  --,LengthOfStayNowIfNull = isnull(convert(varchar, peh.HOSP_DISCH_TIME, 120), cast(datediff(hour, peh.HOSP_ADMSN_TIME, getdate()) as varchar(50))) --> RT ::: is the default unit of time hours?  SQL's datediff function requires explicit declaration of units.  Also, it looks like this is supposed to return a date if DOD is not null and an int if it is?  How to use a date for a lookup in LOS Group?
	  ,CoumadinFlag = case when pem.MedicationIsCoumadin = 1 or CoumadinFlag = 1 then 1 else 0 end 
	  ,HeparinFlag = case when pem.MedicationIsHeparin = 1 or HeparinFlag = 1 then 1 else 0 end   	  	
from gt1.Encounter enc 
inner join PA.ge.PAT_ENC_HSP peh with (nolock)
  on enc.PAT_ENC_CSN_ID = peh.PAT_ENC_CSN_ID
left join PA.ge.HSP_ACCOUNT ha with (nolock)
  on peh.HSP_ACCOUNT_ID = ha.HSP_ACCOUNT_ID
left join gt1.CodeStatus cs with (nolock)
  on peh.PAT_ENC_CSN_ID = cs.PAT_ENC_CSN_ID 
left join gt1.PatientEncounterCurrentMed pem  with (nolock)
  on enc.PAT_ENC_CSN_ID = pem.PAT_ENC_CSN_ID
left join gt1.OrderResult ord  with (nolock)
  on enc.PAT_ENC_CSN_ID = ord.EncounterCSN
 
--print datediff(second, @start, getdate())
--print '--------- table complete - adding indices ---------'
--set @start = getdate()

create index ncix__Encounter__HSP_ACCOUNT_ID on gt1.Encounter(HSP_ACCOUNT_ID)
create index ncix__Encounter__DEPARTMENT_ID on gt1.Encounter(DEPARTMENT_ID) 
create index ncix__Encounter__ADT_PATIENT_STAT_C on gt1.Encounter(ADT_PATIENT_STAT_C)
create index ncix__Encounter__DISCH_DISP_C on gt1.Encounter(DISCH_DISP_C)
create index ncix__Encounter__ADMISSION_SOURCE_C on gt1.Encounter(ADMISSION_SOURCE_C)
create index ncix__Encounter__ACCT_BASECLS_HA_C on gt1.Encounter(ACCT_BASECLS_HA_C)
create index ncix__Encounter__ADT_PAT_CLASS_C on gt1.Encounter(ADT_PAT_CLASS_C)
create index ncix__Encounter__ED_DISPOSITION_C on gt1.Encounter(ED_DISPOSITION_C)
create index ncix__Encounter__ADMISSION_PROV_ID on gt1.Encounter(ADMISSION_PROV_ID)
create index ncix__Encounter__ACCT_FIN_CLASS_C on gt1.Encounter(ACCT_FIN_CLASS_C)
create index ncix__Encounter__INP_ADM_EVENT_ID on gt1.Encounter(INP_ADM_EVENT_ID)
create index ncix__Encounter__EMER_ADM_EVENT_ID on gt1.Encounter(EMER_ADM_EVENT_ID)
 
--print datediff(second, @start, getdate())
--print '--------- indexing complete ---------'
  
  -- check keys and indexes
exec tools.CheckKeysAndIndexes

  


 










GO
