SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

 
 
CREATE procedure [gt1].[ADT0_ADTEncTransform]
as
 
/*
-->	Source tables used <--
    pa.ge.CLARITY_ADT 
 
--> Target table created <--
	gt1.ADTEnc

--> Performance benchmark <--
	371468 rows in < 1 second
*/	
  
--> Updated variables
declare  @vcgADTEventSubTypeCancelled int = (select cast(variables.GetSingleValue('@vcgADTEventSubTypeCancelled') as int))
       , @vcgADTEventTypeAdmit int = (select cast(variables.GetSingleValue('@vcgADTEventTypeAdmit') as int))    
       , @vcgADTEventTypeTransferIn int = (select cast(variables.GetSingleValue('@vcgADTEventTypeTransferIn') as int))
	   , @vcgADTEventTypePatClassChange int = (select cast(variables.GetSingleValue('@vcgADTEventTypePatClassChange') as int))

-- ADT
if exists (select * from INFORMATION_SCHEMA.TABLES where TABLE_NAME = 'ADTEnc' and TABLE_SCHEMA = 'gt1') drop table gt1.ADTEnc
create table [gt1].[ADTEnc]
(
	[PAT_ENC_CSN_ID] [numeric](18, 0) NULL,
	[MinEffectiveTime] [datetime] NULL
)   
insert into gt1.ADTEnc
select 
    PAT_ENC_CSN_ID 
  , min(EFFECTIVE_TIME) as MinEffectiveTime
from pa.ge.CLARITY_ADT ca
where --_BASE_CLASS_C = @vcgADTBaseClassObservation ::: intentionally omitted; Lehigh doesn't seem to use
     EVENT_SUBTYPE_C <> @vcgADTEventSubTypeCancelled
 and (EVENT_TYPE_C = @vcgADTEventTypeAdmit or EVENT_TYPE_C = @vcgADTEventTypeTransferIn or EVENT_TYPE_C = @vcgADTEventTypePatClassChange)
group by PAT_ENC_CSN_ID
  
 -- check indexing
 exec tools.CheckKeysAndIndexes







GO
