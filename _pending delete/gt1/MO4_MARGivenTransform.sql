SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

 
 CREATE procedure [gt1].[MO4_MARGivenTransform] 
 as
 
/*  --> Source Tables <--
    pa.ge.MAR_ADMIN_INFO
    gt1.MedicationOrders
    gt1.OrderMed

  --> Target Table <--
    gt1.MARGiven

  --> Performance Benchmark <--
    484476 rows ~17s
*/

-- declare variables
declare @MARStoppedAction int = (select VariableValue from variables.MasterGlobalReference where VariableName = '@MARStoppedAction')

declare @MAR_Actions_Given table (id int identity(1,1), MAR_ACTION_C numeric)
insert into @MAR_Actions_Given
select VariableValue from variables.GetTableValues ('@MAR_Result_C')

 MARGiven
if exists (select top 1 * from INFORMATION_SCHEMA.TABLES where TABLE_NAME = N'MARGiven' and TABLE_SCHEMA = N'gt1') drop table gt1.MARGiven
create table [gt1].[MARGiven]
(
  [ORDER_MED_ID] [numeric](18, 0) NOT NULL,
  [PAT_ENC_CSN_ID] [numeric](18, 0) NULL,
  [MEDICATION_ID] [numeric](18, 0) NULL,
  [MedicationName] [varchar](255) NOT NULL,
  [MedicationGroup] [varchar](255) NULL,
  [MedicationOrderTime] [datetime] NULL,
  [AbxTakenFlag] [int] NOT NULL,
  [IVFluidTakenFlag] [int] NOT NULL,
  [VasopressorTakenFlag] [int] NOT NULL,
  [MedicationTakenTime] [datetime] NULL,
  [TAKEN_TIME] [datetime] NULL,
  [MAR_ACTION_C] [int] NULL,
  [SepsisOrdersetUsedFlag] [varchar](1) NOT NULL,
  [OrdersetName] [varchar](200) NULL
, [MedicationStoppedTime] [datetime] NULL,
)
insert into [gt1].[MARGiven]
(
   [ORDER_MED_ID]
  ,[PAT_ENC_CSN_ID]
  ,[MEDICATION_ID]
  ,[MedicationName]
  ,[MedicationGroup]
  ,[MedicationOrderTime]
  ,[AbxTakenFlag]
  ,[IVFluidTakenFlag]
  ,[VasopressorTakenFlag]
  ,[MedicationTakenTime]
  ,[TAKEN_TIME]
  ,[MAR_ACTION_C]
  ,[SepsisOrdersetUsedFlag]
  ,[OrdersetName]
  ,[MedicationStoppedTime]
)
select
   om.ORDER_MED_ID
 , om.PAT_ENC_CSN_ID
 , om.MEDICATION_ID
 , om.MedicationName
 , om.MedicationGroup
 , om.MedicationOrderTime
 , om.AbxFlag as AbxTakenFlag
 , om.IVFluidFlag as IVFluidTakenFlag
 , om.VasopressorFlag as VasopressorTakenFlag
 , mar.TAKEN_TIME as MedicationTakenTime
 , mar.TAKEN_TIME
 , mar.MAR_ACTION_C
 , isnull(mo.SepsisOrdersetUsedFlag,0) as SepsisOrdersetUsedFlag
 , isnull(mo.OrdersetName,'*Unknown') as OrdersetName
 , mar_stopped.TAKEN_TIME as MedicationStoppedTime
 from gt1.OrderMed om
 inner join pa.ge.MAR_ADMIN_INFO mar
   on om.ORDER_MED_ID = mar.ORDER_MED_ID
 left join gt1.MedicationOrder mo
   on om.ORDER_MED_ID = mo.ORDER_MED_ID
 inner join @MAR_Actions_Given mag
   on mar.MAR_ACTION_C = mag.MAR_ACTION_C
 left join pa.ge.MAR_ADMIN_INFO mar_stopped
   on om.ORDER_MED_ID = mar_stopped.ORDER_MED_ID
   and mar_stopped.MAR_ACTION_C = @MARStoppedAction 
 where om.AbxFlag = 1 or IVFluidFlag = 1 or om.VasopressorFlag = 1

-- check keys and indexes
exec tools.CheckKeysAndIndexes








GO
