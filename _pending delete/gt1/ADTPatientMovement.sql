 ALTER procedure [gt1].[AdtPatientMovement] as


/*
    --> Source Tables <--
		ge.CLARITY_ADT

	--> Target Tables <--
		gt1.tClarityAdtInterpretation
		gt1.tADTIntervals
		gt1.ADTInterval 

	--> Performance Benchmark <--
		3873651 rows ~2:45
*/

-- new views:  CENSUS_INFO 
-- to do: cleanup; inefficient but let's wait until after validation to delete the seemingly unnecessary code
--		  variables review and update

declare @InpatientPatientClass table (id int identity(1,1), PatientClass int)
insert into @InpatientPatientClass
-- temp for validation
--select 101
select VariableValue from variables.GetTableValues('@vcgPtClassInpatient')

declare @OutpatientPatientClass table (id int identity(1,1), PatientClass int)
insert into @OutpatientPatientClass
--select 104 union select 130
select VariableValue from variables.GetTableValues('@vcgPtClassObservation') where VariableValue is not null
union 
select VariableValue from variables.GetTableValues('@vcgPtClassOutpatient') where VariableValue is not null

declare @UpgradeDowngradeEvents table (id int identity(1,1), EventType int)
insert into @UpgradeDowngradeEvents
select 1 union select 2 union select 3 union select 4 union select 5

/****************************set variables******************************/

--@EventType table variable for use with where EVENT_TYPE_C
declare @EventType table
(
    EVENT_TYPE_C int
  , EVENT_DESCRIPTION varchar(66)
);

insert into
    @EventType(   EVENT_TYPE_C
                , EVENT_DESCRIPTION
              )
values
  (1, 'Admission')
, (2, 'Discharge')
, (3, 'Transfer In')
, (4, 'Transfer Out')
, (5, 'Patient Update')
, (6, 'Census')
--, (7, 'Hospital Outpatient')
--, (8, 'Leave of Absence Out')
--, (9, 'Leave of Absence Return');

----@EventSubType table variable for use with where EVENT_SUBTYPE_C
--declare @EventSubType table
--(
--    EVENT_SUBTYPE_C int
--  , EVENT_DESCRIPTION varchar(66)
--);

--insert into
--    @EventSubType(   EVENT_SUBTYPE_C
--                   , EVENT_DESCRIPTION
--                 )
--values
--  (1, 'Original')
--, (2, 'Cancelled')
--, (3, 'Update');


----@OBDelEpisType table variable for use with where OB_DEL_EPIS_TYPE_C
--declare @OBDelEpisType table
--(
--    OB_DEL_EPIS_TYPE_C int
--  , TYPE_DESCRIPTION varchar(66)
--);

--insert into
--    @OBDelEpisType(   OB_DEL_EPIS_TYPE_C
--                    , TYPE_DESCRIPTION
--                  )
--values
--  (1, 'Obstetrics')
--, (2, 'Acute Care' )
--, (3, 'Anticoagulation')
--, (4, 'Transplant')
--, (5, 'Oncology - Chemotherapy')
--, (6, 'Oncology - Supportive Care')
--, (7, 'Oncology - Follow-up')
--, (8, 'Home Health')
--, (9, 'Hospice')
--, (10, 'Obstetrics - Delivery')
--, (11, 'Blood and Marrow Transplant')
--, (12, 'Traveler');

----@BaseClass table variable for use with where FROM_BASE_CLASS_C and TO_BASE_CLASS_C
--declare @BaseClass table (
--    BASE_CLASS_C int
--  , CLASS_DESCRIPTION varchar(66)
--);

--insert into @BaseClass (
--    BASE_CLASS_C
--  , CLASS_DESCRIPTION
--)
--values 
--    (0, 'Direct')
--  , (1, 'Inpatient')
--  , (2, 'Outpatient')
--  , (3, 'Emergency')
--  , (4, 'Observation')
--; 

declare @MinDate datetime = (select min(EFFECTIVE_TIME)from ge.CLARITY_ADT);
--print @MinDate;

declare @EndTime datetime = getdate();

declare @CensusStartDate date = variables.GetSingleValue('@CensusStartDate');

declare @ServiceAreas numeric = (select variables.GetSingleValue('@ServiceAreas'));


/************************EVENT DATA**************************/

----------------------------------------------------------------------------------
print('-----------gt1.tUpgradeDowngrades--------------');

if object_id('gt1.tUpgradeDowngrades', 'U') is not null
    drop table gt1.tUpgradeDowngrades;

select
	PAT_ENC_CSN_ID
  , EVENT_ID
  , case when ca.PAT_ENC_CSN_ID = lag(ca.PAT_ENC_CSN_ID) over (partition by ca.PAT_ENC_CSN_ID order by ca.EFFECTIVE_TIME, ca.EVENT_ID) 
		    and lag(ca.PAT_CLASS_C) over (partition by ca.PAT_ENC_CSN_ID order by ca.EFFECTIVE_TIME) in (select PatientClass from @OutpatientPatientClass) -- 104, 130
		    and ca.PAT_CLASS_C in (select PatientClass from @InpatientPatientClass) -- 101
			then 1 else 0 
	end as EVENT_IS_UPGRADE -- was outpatient, now inpatient ::: this is an UPGRADE to whatever condition put the patient in the hospital, not an upgrade of the patient's status
, case when ca.PAT_ENC_CSN_ID = lag(ca.PAT_ENC_CSN_ID) over (partition by ca.PAT_ENC_CSN_ID order by ca.EFFECTIVE_TIME, ca.EVENT_ID) 
		    and lag(ca.PAT_CLASS_C) over (partition by ca.PAT_ENC_CSN_ID order by ca.EFFECTIVE_TIME) in (select PatientClass from @InpatientPatientClass) --101
		    and ca.PAT_CLASS_C in (select PatientClass from @OutpatientPatientClass) -- 104, 130
			then 1 else 0 
	end as EVENT_IS_DOWNGRADE -- was inpatient, now outpatient ::: this is an DOWNGRADE of whatever condition put the patient in the hospital, not a downgrade of the patient's status
into gt1.tUpgradeDowngrades
from ge.CLARITY_ADT as ca
inner join @UpgradeDowngradeEvents et
   on ca.EVENT_TYPE_C = et.EventType
 where ca.EVENT_SUBTYPE_C <> 2	 
   and ca.PAT_ENC_CSN_ID is not null
   and ca.EFFECTIVE_TIME >= @CensusStartDate  

--create index ncix__tUpgradeDowngrade__EVENT_ID on gt1.tUpgradeDowngrade (EVENT_ID) include (EVENT_IS_UPGRADE, EVENT_IS_DOWNGRADE)
 
--------------------------------------------------------------------------------
print('-----------gt1.tClarityAdtInterpretation--------------');

if object_id('gt1.tClarityAdtInterpretation', 'U') is not null
    drop table gt1.tClarityAdtInterpretation;

create table gt1.[tClarityAdtInterpretation]
(
    [EVENT_ID] [numeric](18, 0) not null
  , [PAT_ENC_CSN_ID] [numeric](18, 0) null
  , [PAT_CLASS_C] [varchar](66) null
  , [PAT_SERVICE_C] [varchar](66) null
  , [EVENT_TYPE_C] [int] null
  , [EVENT_SUBTYPE_C] [int] null
  , [EFFECTIVE_TIME] [datetime] null
  , [EVENT_TIME] [datetime] null
  , [DEPARTMENT_ID] [numeric](18, 0) null
  , [ROOM_ID] [varchar](18) null
  , [DIS_EVENT_ID] [numeric](18, 0) null
  , [IN_EVENT_TYPE_C] [int] null
  , [OUT_EVENT_TYPE_C] [int] null
  , [FROM_BASE_CLASS_C] [int] null
  , [TO_BASE_CLASS_C] [int] null
  , [FIRST_IP_IN_IP_YN] [varchar](1) null
  , [SDS_EFFECTIVE_DATE] [date] null
  , [EVENT_IS_UPGRADE] int null
  , [EVENT_IS_DOWNGRADE] int null
);
-- Find one day stays
--with OneDayStays as (
--  select
--      EVENT_ID as DIS_EVENT_ID
--    , cast(EFFECTIVE_TIME as date) as SDS_EFFECTIVE_DATE
--  from
--    ge.CLARITY_ADT as ca
--	left join @EventType as et
--      on ca.EVENT_TYPE_C = et.EVENT_TYPE_C
--  where et.EVENT_DESCRIPTION = 'Discharge'
-- )
insert into
    [gt1].[tClarityAdtInterpretation](   [EVENT_ID]
                                       , [PAT_ENC_CSN_ID]
                                       , [PAT_CLASS_C]
                                       , [PAT_SERVICE_C]
                                       , [EVENT_TYPE_C]
                                       , [EVENT_SUBTYPE_C]
                                       , [EFFECTIVE_TIME]
                                       , [EVENT_TIME]
                                       , [DEPARTMENT_ID]
                                       , [ROOM_ID]
                                       , [DIS_EVENT_ID]
                                       , [IN_EVENT_TYPE_C]
                                       , [OUT_EVENT_TYPE_C]
                                       , [FROM_BASE_CLASS_C]
                                       , [TO_BASE_CLASS_C]
                                       , [FIRST_IP_IN_IP_YN]
                                       , [SDS_EFFECTIVE_DATE]
									   , [EVENT_IS_UPGRADE]
									   , [EVENT_IS_DOWNGRADE]
                                     )
select
    ca.EVENT_ID
  , ca.PAT_ENC_CSN_ID
  --, ca.PAT_ID
  , ca.PAT_CLASS_C
  , ca.PAT_SERVICE_C
  , ca.EVENT_TYPE_C
  , ca.EVENT_SUBTYPE_C
  , ca.EFFECTIVE_TIME
  , ca.EVENT_TIME
  , ca.DEPARTMENT_ID
  , ca.ROOM_ID
  , ca.DIS_EVENT_ID
  , ca.IN_EVENT_TYPE_C
  , ca.OUT_EVENT_TYPE_C
  , ca.FROM_BASE_CLASS_C
  , ca.TO_BASE_CLASS_C
  , ca.FIRST_IP_IN_IP_YN
  , cast(EFFECTIVE_TIME as date) as SDS_EFFECTIVE_DATE
  , ud.EVENT_IS_UPGRADE
  , ud.EVENT_IS_DOWNGRADE
from ge.CLARITY_ADT as ca
inner join @EventType et
   on ca.EVENT_TYPE_C = et.EVENT_TYPE_C
left join gt1.tUpgradeDowngrades ud
   on ca.EVENT_ID = ud.EVENT_ID
left join ge.CLARITY_DEP cd
  on ca.DEPARTMENT_ID = cd.DEPARTMENT_ID
 where ca.EVENT_SUBTYPE_C <> 2	 
   and ca.PAT_ENC_CSN_ID is not null
   and ca.EFFECTIVE_TIME >= @CensusStartDate  
   and cd.SERV_AREA_ID = @ServiceAreas 

 create index ncix__tClarityAdtInterpretation__PAT_ENC_CSN_ID on gt1.tClarityAdtInterpretation(PAT_ENC_CSN_ID)
 create index ncix__tClarityAdtInterpretation__DEPARTMENT_ID on gt1.tClarityAdtInterpretation(DEPARTMENT_ID)

-- Find patient's current location

/*If this is an admission, use that time as the start time.
If this is anything else, increment it by one second so that when interval matching on the time of a transfer, patients will appear where they were.
For example, this means for a transfer at 4pm, at 4pm the patient will be listed in the department they were in prior to the transfer.*/
;
--------------------------------------------------------------------------------
print('-----------gt1.tADTInterval--------------');

if object_id('gt1.tADTInterval', 'U') is not null
    drop table gt1.tADTInterval;

create table [gt1].[tADTInterval]
(
    [EVENT_ID] [numeric](18, 0) not null
  , [PAT_ENC_CSN_ID] [numeric](18, 0) null
  , [PAT_CLASS_C] [varchar](66) null
  , [EVENT_TYPE_C] [int] null
  , [EVENT_SUBTYPE_C] [int] null
  , [EFFECTIVE_TIME] [datetime] null
  , [FROM_TIME] [numeric](18, 5) null
  , [DEPARTMENT_ID] [numeric](18, 0) null
  , [DIS_EVENT_ID] [numeric](18, 0) null
  , [PAT_SERVICE_C] [varchar](66) null
  , [ROOM_ID] [varchar](18) null
);

insert into
    [gt1].[tADTInterval](   [EVENT_ID]
                          , [PAT_ENC_CSN_ID]
                          , [PAT_CLASS_C]
                          , [EVENT_TYPE_C]
                          , [EVENT_SUBTYPE_C]
                          , [EFFECTIVE_TIME]
                          , [FROM_TIME]
                          , [DEPARTMENT_ID]
                          , [DIS_EVENT_ID]
                          , [PAT_SERVICE_C]
                          , [ROOM_ID]
                        )
select
    ca.EVENT_ID
  , ca.PAT_ENC_CSN_ID
  --, PAT_ID
  , ca.PAT_CLASS_C
  , ca.EVENT_TYPE_C
  , ca.EVENT_SUBTYPE_C
  , ca.EFFECTIVE_TIME
  , cast((case
              when ca.EFFECTIVE_TIME is not null
              then case
                       when ca.EVENT_TYPE_C = 1 --admission
                       then ca.EFFECTIVE_TIME
                       else dateadd(ss, 1, ca.EFFECTIVE_TIME) -- increment by one second
                   end
              else @MinDate
          end
         ) as numeric(18, 5)) as FROM_TIME
  --, null as TO_TIME
  , ca.DEPARTMENT_ID
  , ca.DIS_EVENT_ID
  , ca.PAT_SERVICE_C
  , ca.ROOM_ID
  from ge.CLARITY_ADT as ca
 where ca.EVENT_TYPE_C in (1, 2, 3, 5) --( 'Admission', 'Discharge', 'Transfer In', 'Patient Update' )
   and ca.EVENT_SUBTYPE_C <> 2 -- 'Cancelled' -- get rid of cancelled events
  and ca.EFFECTIVE_TIME >= @CensusStartDate
 order by
    ca.PAT_ENC_CSN_ID
  , FROM_TIME desc;

--create index iCSNtai on #AdtIntervals (PAT_ENC_CSN_ID);
--create index iETCtai on #AdtIntervals (EVENT_TYPE_C);
--create index iESCtai on #AdtIntervals (EVENT_SUBTYPE_C);

--------------------------------------------------------------------------------
/*
Loop backwards in time for each patient to find the time each event ended. 
If a patient isn't discharged, the TO_TIME for last event interval will be @EndTime).
*/

print('-----------gt1.ADTInterval--------------');

if object_id('gt1.ADTInterval', 'U') is not null
    drop table gt1.ADTInterval;

create table [gt1].[ADTInterval]
(
    [PAT_ENC_CSN_ID] [numeric](18, 0) null
  , [PAT_CLASS_C] [varchar](66) null
  , [EVENT_ID] [numeric](18, 0) not null
  , [EVENT_TYPE_C] [int] null
  , [EVENT_SUBTYPE_C] [int] null
  , [FROM_TIME] [numeric](18, 5) null
  , [TO_TIME] [smalldatetime] null
  , [DEPARTMENT_ID] [numeric](18, 0) null
  , [DIS_EVENT_ID] [numeric](18, 0) null
  , [PAT_SERVICE_C] [varchar](66) null
  , [ROOM_ID] [varchar](18) null
);

insert into
    [gt1].[ADTInterval](   [PAT_ENC_CSN_ID]
                         , [PAT_CLASS_C]
                         , [EVENT_ID]
                         , [EVENT_TYPE_C]
                         , [EVENT_SUBTYPE_C]
                         , [FROM_TIME]
                         , [TO_TIME]
                         , [DEPARTMENT_ID]
                         , [DIS_EVENT_ID]
                         , [PAT_SERVICE_C]
                         , [ROOM_ID]
                       )
select
    ai.PAT_ENC_CSN_ID
  --, ai.PAT_ID
  , ai.PAT_CLASS_C
  , ai.EVENT_ID
  , ai.EVENT_TYPE_C
  , ai.EVENT_SUBTYPE_C
  , ai.FROM_TIME
  , cast((case
              when ai.PAT_ENC_CSN_ID = lag(ai.PAT_ENC_CSN_ID) over (partition by ai.PAT_ENC_CSN_ID order by ai.EVENT_ID desc)
              then lag(cast(ai.FROM_TIME as numeric(18, 5))) over (partition by ai.PAT_ENC_CSN_ID order by ai.EVENT_ID desc)
              else @EndTime
          end
         ) as smalldatetime) as TO_TIME
  , ai.DEPARTMENT_ID
  , ai.DIS_EVENT_ID
  , ai.PAT_SERVICE_C
  , ai.ROOM_ID
  from gt1.tADTInterval as ai
 where ai.EVENT_TYPE_C <> 2 --'Discharge' -- Get rid of discharges. Patients still in the hospital will have a final time of @EndTime.
 order by
    ai.EVENT_ID;

--create index iCSNai on AdtIntervals (PAT_ENC_CSN_ID);
--create index iEIai on AdtIntervals (EVENT_ID);
--create index iETai on AdtIntervals (EVENT_TYPE_C)

-- drop table gt1.tADTIntervals

--------------------------------------------------------------------------------
/*
USE THIS PORTION IF YOU NEED HOURS PER GIVEN DAY
--Handle when patients cross midnight by splitting these entries into two separate entries.  We need this to calculate observation hours.
*/

print('-----------gt1.tAdtHourInterval--------------');

if object_id('gt1.tAdtHourInterval', 'U') is not null
  drop table gt1.tAdtHourInterval;
create table [gt1].[tAdtHourInterval]
(
    [FROM_TIME] [datetime] null
  , [TO_TIME] [datetime] null
  , [PAT_ENC_CSN_ID] [numeric](18, 0) null
  , [PAT_CLASS_C] [varchar](66) null
  , [EVENT_ID] [numeric](18, 0) not null
  , [EVENT_TYPE_C] [int] null
  , [EVENT_SUBTYPE_C] [int] null
  , [DEPARTMENT_ID] [numeric](18, 0) null
  , [DIS_EVENT_ID] [numeric](18, 0) null
  , [PAT_SERVICE_C] [varchar](66) null
);

declare @PatientClassObs int = (   select VariableValue
                                     from paval.variables.MasterGlobalReference
                                    where VariableName = '@PatientClassObs'
                               )

;
with SplitDates as
    (
        select
            case
                when floor(cast(TO_TIME as numeric(18, 5))) <> floor(cast(FROM_TIME as numeric(18, 5)))
                then case
                         when floor(cast(FROM_TIME as numeric(18, 5))) = lead(floor(cast(FROM_TIME as numeric(18, 5)))) over (partition by
                                                                                                                                  PAT_ENC_CSN_ID order by EVENT_ID
                                                                                                                             )
                         then cast(FROM_TIME as numeric(18, 5))
                         else floor(cast(FROM_TIME as numeric(18, 5))) + 1
                     end
                else cast(FROM_TIME as numeric(18, 5))
            end as FROM_TIME
          , TO_TIME
          , PAT_ENC_CSN_ID
          , PAT_CLASS_C
          , EVENT_ID
          , EVENT_TYPE_C
          , EVENT_SUBTYPE_C
          , DEPARTMENT_ID
          , DIS_EVENT_ID
          , PAT_SERVICE_C
          from gt1.ADTInterval
         where datediff(day, FROM_TIME, TO_TIME) <> 0
        union
        select
            cast(FROM_TIME as numeric(18, 5))
          , case
                when floor(cast(TO_TIME as numeric(18, 5))) <> floor(cast(FROM_TIME as numeric(18, 5)))
                then case
                         when floor(cast(FROM_TIME as numeric(18, 5))) = lead(floor(cast(FROM_TIME as numeric(18, 5)))) over (partition by
                                                                                                                                  PAT_ENC_CSN_ID order by EVENT_ID
                                                                                                                             )
                         then cast(TO_TIME as numeric(18, 5))
                         else dateadd(ms, -3, (floor(cast(FROM_TIME as numeric(18, 5))) + 1))
                     end
                else cast(TO_TIME as numeric(18, 5))
            end as TO_TIME
          , PAT_ENC_CSN_ID
          , PAT_CLASS_C
          , EVENT_ID
          , EVENT_TYPE_C
          , EVENT_SUBTYPE_C
          , DEPARTMENT_ID
          , DIS_EVENT_ID
          , PAT_SERVICE_C
          from gt1.ADTInterval
         where datediff(day, FROM_TIME, TO_TIME) <> 0
    )
insert into
    [gt1].[tAdtHourInterval](   [FROM_TIME]
                              , [TO_TIME]
                              , [PAT_ENC_CSN_ID]
                              , [PAT_CLASS_C]
                              , [EVENT_ID]
                              , [EVENT_TYPE_C]
                              , [EVENT_SUBTYPE_C]
                              , [DEPARTMENT_ID]
                              , [DIS_EVENT_ID]
                              , [PAT_SERVICE_C]
                            )
select distinct 
    cast(sd.FROM_TIME as datetime) as FROM_TIME
  , cast(sd.TO_TIME as datetime) as TO_TIME
  , sd.PAT_ENC_CSN_ID
  , sd.PAT_CLASS_C
  , sd.EVENT_ID
  , sd.EVENT_TYPE_C
  , sd.EVENT_SUBTYPE_C
  , sd.DEPARTMENT_ID
  , sd.DIS_EVENT_ID
  , sd.PAT_SERVICE_C
  from SplitDates as sd
  left join ge.OB_HSB_DELIVERY as hd
    on sd.PAT_ENC_CSN_ID = hd.DELIVERY_DATE_CSN
 where(sd.PAT_CLASS_C = @PatientClassObs or hd.OB_DEL_BIRTH_DTTM >= sd.FROM_TIME) 
   and hd.DELIVERY_DATE_CSN is not null
--order by sd.EVENT_ID

--create index iCSNtahi on #AdtHourIntervals (PAT_ENC_CSN_ID);
--create index iPCCtahi on #AdtHourIntervals (PAT_CLASS_C);

-------------------------------------------------------------------------------

print('-----------gt1.tAdtObs--------------');

if object_id('gt1.tAdtObs', 'U') is not null
    drop table gt1.tAdtObs;

create table [gt1].[tAdtObs]
(
    [EVENT_ID] [numeric](18, 0) not null
  , [EFFECTIVE_TIME] [datetime] null
  , [EVENT_TIME] [datetime] null
  , [DIS_EVENT_ID] [numeric](18, 0) null
  , [DEPARTMENT_ID] [numeric](18, 0) null
  , [EVENT_TYPE_C] [int] null
  , [EVENT_SUBTYPE_C] [int] null
  , [PAT_CLASS_C] [varchar](66) null
  , [PAT_SERVICE_C] [varchar](66) null
  , [PAT_ENC_CSN_ID] [numeric](18, 0) null
  , [NUM_OBS_PT_MIN] [int] null
);

insert into
    [dbo].[tAdtObs](   [EVENT_ID]
                     , [EFFECTIVE_TIME]
                     , [EVENT_TIME]
                     , [DIS_EVENT_ID]
                     , [DEPARTMENT_ID]
                     , [EVENT_TYPE_C]
                     , [EVENT_SUBTYPE_C]
                     , [PAT_CLASS_C]
                     , [PAT_SERVICE_C]
                     , [PAT_ENC_CSN_ID]
                     , [NUM_OBS_PT_MIN]
                   )

select distinct
    EVENT_ID
  , convert(datetime, TO_TIME) as EFFECTIVE_TIME
  , convert(datetime, TO_TIME) as EVENT_TIME
  , DIS_EVENT_ID
  , DEPARTMENT_ID
  , '100' as EVENT_TYPE_C --create new 
  , EVENT_SUBTYPE_C
  , PAT_CLASS_C
                          --, PAT_ID
  , PAT_SERVICE_C
  , PAT_ENC_CSN_ID
  , datediff(mi, convert(datetime, FROM_TIME), convert(datetime, TO_TIME)) as NUM_OBS_PT_MIN
  from gt1.tAdtHourInterval
 where PAT_CLASS_C = 104 --@PatientClassObs
union
select
    EVENT_ID
  , EFFECTIVE_TIME
  , EVENT_TIME
  , DIS_EVENT_ID
  , DEPARTMENT_ID
  , EVENT_TYPE_C
  , EVENT_SUBTYPE_C
  , PAT_CLASS_C
  --, PAT_ID
  , PAT_SERVICE_C
  , PAT_ENC_CSN_ID
  , null as NUM_OBS_PT_MIN
  from gt1.tClarityAdtInterpretation;


-- create index iCSNtao on #AdtObs (PAT_ENC_CSN_ID);


/*******************************GRAB DELIVERY INFO*****************************/


-- Get a baby ID for those cases where there's not a mom CSN
/* Group by CSN ID, to account for multiple births on a given day.  
We'll take the first birth time as the mom's location for both births - the reason for this is that if we just leave the
CSN with two separate birth times and both fall in the same ADT event range interval (more on that later) they get
grouped under one bucket and we lose multiplicity.
For matching purposes, we will want to move BIRTH_TIME back by 1 second */


/*TODO: convert UTC time to local timezone*/

--------------------------------------------------------------------------------
 
print('-----------gt1.tBabyDataFinal--------------');

if object_id('gt1.tBabyDataFinal', 'U') is not null
    drop table gt1.tBabyDataFinal;

create table gt1.[tBabyDataFinal]
(
    [FROM_TIME] [datetime] null
  , [TO_TIME] [datetime] null
  , [PAT_ENC_CSN_ID] [numeric](18, 0) null
  , [BABY_AMOUNT] [int] null
  , [EFFECTIVE_TIME] [datetime] null
  , [EVENT_TIME] [datetime] null
  , [EVENT_ID] [numeric](18, 0) null
  , [DIS_EVENT_ID] [numeric](18, 0) null
  , [DEPARTMENT_ID] [numeric](18, 0) null
  , [EVENT_TYPE_C] [int] null
  , [PAT_CLASS_C] [varchar](66) null
  , [PAT_SERVICE_C] [varchar](66) null
)

;
with OBDelivery as
    (
        select
            ohd.DELIVERY_DATE_CSN as PAT_ENC_CSN_ID
          , ohd.SUMMARY_BLOCK_ID as EPISODE_ID
          , ohd.OB_DEL_BIRTH_DTTM as OB_DEL_BIRTH_DTTM_UTC --added UTC at end, time is NOT converted to local
                                                           --, month(ohd.OB_DEL_BIRTH_DTTM) as BirthMonth  -- take this out for now until we can convert to local time
                                                           --, year(ohd.OB_DEL_BIRTH_DTTM) as BirthYear  
          , ohd.OB_DEL_EPIS_TYPE_C
          , e.OB_DELIVERY_BABY_ID as BABY_ID
          from ge.OB_HSB_DELIVERY as ohd
          left join ge.EPISODE as e
            on ohd.SUMMARY_BLOCK_ID = e.EPISODE_ID
         where ohd.OB_DEL_EPIS_TYPE_C = 10 --'Obstetrics - Delivery'
           and ohd.OB_DEL_BIRTH_DTTM > 0  
		   and ohd.DELIVERY_DATE_CSN is not null
    )
   , OBGroup as
    (
        select
            PAT_ENC_CSN_ID
          , OB_DEL_BIRTH_DTTM_UTC as BIRTH_DATE
          , month(min(OB_DEL_BIRTH_DTTM_UTC)) as MONTH_TEST
          , year(min(OB_DEL_BIRTH_DTTM_UTC)) as YEAR_TEST
          , dateadd(ss, -1, OB_DEL_BIRTH_DTTM_UTC) as BIRTH_TIME -- For matching purposes, move BIRTH_TIME back by 1 second
          , count(distinct BABY_ID) as NUMBER_OF_BABIES
		  from OBDelivery
         group by
            PAT_ENC_CSN_ID
          , OB_DEL_BIRTH_DTTM_UTC
    )

   /**********************************BABY/MOM TIME AND LOCATION******************************************/

   -- match the BirthTime for a given CSN's group within ADT events so that we can pinpoint mom's location

   , BabyData as
    (
        select
            ahi.PAT_ENC_CSN_ID
          , convert(numeric(18, 5), ahi.FROM_TIME) as FROM_TIME
          , ahi.TO_TIME
          , obg.BIRTH_TIME
          , convert(datetime, ahi.TO_TIME) as EFFECTIVE_TIME
          , convert(datetime, ahi.TO_TIME) as EVENT_TIME
          , ahi.EVENT_ID
          , ahi.DIS_EVENT_ID
          , ahi.DEPARTMENT_ID
          , ahi.EVENT_TYPE_C
          , ahi.PAT_CLASS_C
          --, ahi.PAT_ID, 
          , ahi.PAT_SERVICE_C
          from tAdtHourInterval as ahi
          left join OBGroup as obg
            on ahi.PAT_ENC_CSN_ID = obg.PAT_ENC_CSN_ID
         where obg.BIRTH_TIME between ahi.FROM_TIME and ahi.TO_TIME
    )  
   , BirthEventType as
    (
        select distinct
            convert(datetime, bd.FROM_TIME) as FROM_TIME
          , bd.TO_TIME
          , bd.PAT_ENC_CSN_ID
          , convert(datetime, bd.TO_TIME) as EFFECTIVE_TIME
          , convert(datetime, bd.TO_TIME) as EVENT_TIME
          , bd.EVENT_ID
          , bd.DIS_EVENT_ID
          , bd.DEPARTMENT_ID
          , '101' as EVENT_TYPE_C -- Make a new event type for birth
          , bd.PAT_CLASS_C
                                  --, bd.PAT_ID, 
          , bd.PAT_SERVICE_C
          from BabyData as bd
          left join gt1.tAdtHourInterval as ahi
            on bd.PAT_ENC_CSN_ID = ahi.PAT_ENC_CSN_ID
          left join ge.OB_HSB_DELIVERY as hd
            on ahi.PAT_ENC_CSN_ID = hd.DELIVERY_DATE_CSN
         where hd.DELIVERY_DATE_CSN is not null
		  
    )


   /*Because some mom's CSNs end up with no match, we do an outer join to drop in all mom's CSNs with dates and number babies
For moms with times already, this just adds in the number of babies on a given day
For moms not admitted at time of birth, this adds them in with linked CSN and number of babies.
For moms not in the system (ie babies without linked mom CSNs) this just groups the number of babies based on effective date.*/

   , NumberOfBabies as
    (
        select  
            bet.FROM_TIME
          , bet.TO_TIME
          , bet.PAT_ENC_CSN_ID
          , obg.BIRTH_DATE as EFFECTIVE_TIME
          , bet.EVENT_TIME
          , bet.EVENT_ID
          , bet.DIS_EVENT_ID
          , bet.DEPARTMENT_ID
          , '101' as EVENT_TYPE_C
          , bet.PAT_CLASS_C
          --, bet.PAT_ID, 
          , bet.PAT_SERVICE_C
          , obg.NUMBER_OF_BABIES as BABY_AMOUNT
          from BirthEventType as bet
          full join OBGroup as obg
            on bet.PAT_ENC_CSN_ID = obg.PAT_ENC_CSN_ID

    )   
 
insert into
    [gt1].[tBabyDataFinal](   [FROM_TIME]
                            , [TO_TIME]
                            , [PAT_ENC_CSN_ID]
                            , [BABY_AMOUNT]
                            , [EFFECTIVE_TIME]
                            , [EVENT_TIME]
                            , [EVENT_ID]
                            , [DIS_EVENT_ID]
                            , [DEPARTMENT_ID]
                            , [EVENT_TYPE_C]
                            , [PAT_CLASS_C]
                            , [PAT_SERVICE_C]
                          )
select
    nb.FROM_TIME
  , nb.TO_TIME
  , nb.PAT_ENC_CSN_ID
  , nb.BABY_AMOUNT
  , nb.EFFECTIVE_TIME
  , nb.EVENT_TIME
  , nb.EVENT_ID
  , nb.DIS_EVENT_ID
  , nb.DEPARTMENT_ID
  , nb.EVENT_TYPE_C
  , case
        when nb.PAT_CLASS_C > 0
        then nb.PAT_CLASS_C
        else case when nb.PAT_ENC_CSN_ID is not null then cadt.PAT_CLASS_C end
    end as PAT_CLASS_C
  --, nb.PAT_ID, 
  , nb.PAT_SERVICE_C
  from NumberOfBabies as nb
  left join ge.CLARITY_ADT as ca
    on nb.EVENT_ID = ca.EVENT_ID
  --left join ge.PAT_ENC_HSP as peh
  --  on nb.PAT_ENC_CSN_ID = peh.PAT_ENC_CSN_ID
  left join ge.CLARITY_ADT as cadt
    on nb.EVENT_ID = cadt.EVENT_ID
  where ca.PAT_ENC_CSN_ID is not null
union all
select
    null as FROM_TIME
  , null as TO_TIME
  , cai.PAT_ENC_CSN_ID
  , null as BABY_AMOUNT
  , cai.EFFECTIVE_TIME
  , cai.EVENT_TIME
  , cai.EVENT_ID
  , cai.DIS_EVENT_ID
  , cai.DEPARTMENT_ID
  , cai.EVENT_TYPE_C
  , cai.PAT_CLASS_C
  --, cai.PAT_ID, 
  , cai.PAT_SERVICE_C
  from gt1.tClarityAdtInterpretation as cai;

--------------------------------------------------------------------------------
print('-----------gt1.tADTDepartmentPatient--------------');

if object_id('gt1.tADTDepartmentPatient', 'U') is not null
    drop table gt1.tADTDepartmentPatient;

create table [gt1].[tADTDepartmentPatient]
(
    [EVENT_ID] [numeric](18, 0) not null
  , [PAT_ENC_CSN_ID] [numeric](18, 0) null
  , [PAT_CLASS_C] [varchar](66) null
  , [PAT_SERVICE_C] [varchar](66) null
  , [EVENT_TYPE_C] [int] null
  , [EVENT_SUBTYPE_C] [int] null
  , [EFFECTIVE_TIME] [datetime] null
  , [EVENT_TIME] [datetime] null
  , [DEPARTMENT_ID] [numeric](18, 0) null
  , [ROOM_ID] [varchar](18) null
  , [DIS_EVENT_ID] [numeric](18, 0) null
  , [IN_EVENT_TYPE_C] [int] null
  , [OUT_EVENT_TYPE_C] [int] null
  , [FROM_BASE_CLASS_C] [int] null
  , [TO_BASE_CLASS_C] [int] null
  , [FIRST_IP_IN_IP_YN] [varchar](1) null
  , [SDS_EFFECTIVE_DATE] [date] null
  , [EVENT_IS_UPGRADE] int null
  , [EVENT_IS_DOWNGRADE] int null
  , [NUM_OBS_PT_MIN] [int] null
  , [BABY_AMOUNT] [int] null
  , [ADT_PAYOR_ID] [int] null
  , [DEPARTMENT_NAME] [varchar](254) null
  , [REV_LOC_ID] [numeric](18, 0) null
  , [SERV_AREA_ID] [numeric](18, 0) null
  , [REV_LOC_NAME] [varchar](80) null
  , [SERV_AREA_NAME] [varchar](80) null
  , [HSP_ACCOUNT_ID] [numeric](18, 0) null
  , [DISCH_DISP_C] [varchar](66) null
  , [ADMISSION_PROV_ID] [varchar](18) null
  , [ADMISSION_PROV_NAME] [varchar](200) null
  , [PAT_CLASS_NAME] [varchar](254) null
  , [PAT_SERVICE_NAME] [varchar](254) null
  , [DISCH_DISP_C_NAME] [varchar](254) null
);
insert into
    [gt1].[tADTDepartmentPatient](   [EVENT_ID]
                                   , [PAT_ENC_CSN_ID]
                                   , [PAT_CLASS_C]
                                   , [PAT_SERVICE_C]
                                   , [EVENT_TYPE_C]
                                   , [EVENT_SUBTYPE_C]
                                   , [EFFECTIVE_TIME]
                                   , [EVENT_TIME]
                                   , [DEPARTMENT_ID]
                                   , [ROOM_ID]
                                   , [DIS_EVENT_ID]
                                   , [IN_EVENT_TYPE_C]
                                   , [OUT_EVENT_TYPE_C]
                                   , [FROM_BASE_CLASS_C]
                                   , [TO_BASE_CLASS_C]
                                   , [FIRST_IP_IN_IP_YN]
                                   , [SDS_EFFECTIVE_DATE]
								   , [EVENT_IS_UPGRADE]
								   , [EVENT_IS_DOWNGRADE]
                                   , [NUM_OBS_PT_MIN]
                                   , [BABY_AMOUNT]
                                   , [ADT_PAYOR_ID]
                                   , [DEPARTMENT_NAME]
                                   , [REV_LOC_ID]
                                   , [SERV_AREA_ID]
                                   , [REV_LOC_NAME]
                                   , [SERV_AREA_NAME]
                                   , [HSP_ACCOUNT_ID]
                                   , [DISCH_DISP_C]
                                   , [ADMISSION_PROV_ID]
                                   , [ADMISSION_PROV_NAME]
                                   , [PAT_CLASS_NAME]
                                   , [PAT_SERVICE_NAME]
                                   , [DISCH_DISP_C_NAME]
                                 )
select distinct
    cai.[EVENT_ID]
  , cai.[PAT_ENC_CSN_ID]
  , cai.[PAT_CLASS_C]
  , cai.[PAT_SERVICE_C]
  , cai.[EVENT_TYPE_C]
  , cai.[EVENT_SUBTYPE_C]
  , cai.[EFFECTIVE_TIME]
  , cai.[EVENT_TIME]
  , cai.[DEPARTMENT_ID]
  , cai.[ROOM_ID]
  , cai.[DIS_EVENT_ID]
  , cai.[IN_EVENT_TYPE_C]
  , cai.[OUT_EVENT_TYPE_C]
  , cai.[FROM_BASE_CLASS_C]
  , cai.[TO_BASE_CLASS_C]
  , cai.[FIRST_IP_IN_IP_YN]
  , cai.[SDS_EFFECTIVE_DATE]
  , cai.[EVENT_IS_UPGRADE]
  , cai.[EVENT_IS_DOWNGRADE]
  , ao.NUM_OBS_PT_MIN ---
  , bdf.BABY_AMOUNT   ---
  , null as ADT_PAYOR_ID
  , cd.DEPARTMENT_NAME
  , cd.REV_LOC_ID
  , cd.SERV_AREA_ID
  , cl.LOC_NAME as REV_LOC_NAME
  , csa.SERV_AREA_NAME
  , peh.HSP_ACCOUNT_ID
  , peh.DISCH_DISP_C
  , cs.PROV_ID as ADMISSION_PROV_ID
  , cs.PROV_NAME as ADMISSION_PROV_NAME
  , zcpc.NAME as PAT_CLASS_NAME
  , zcps.NAME as PAT_SERVICE_NAME
  , zcdd.NAME as DISCH_DISP_C_NAME
  from gt1.tClarityAdtInterpretation as cai
  left join gt1.tAdtObs as ao
    on cai.PAT_ENC_CSN_ID = ao.PAT_ENC_CSN_ID
  left join gt1.tBabyDataFinal as bdf
    on cai.PAT_ENC_CSN_ID = bdf.PAT_ENC_CSN_ID
  left join ge.CLARITY_DEP as cd
    on cai.DEPARTMENT_ID = cd.DEPARTMENT_ID
  left join ge.CLARITY_LOC as cl
    on cd.REV_LOC_ID = cl.LOC_ID
  left join ge.CLARITY_SA as csa
    on cd.SERV_AREA_ID = csa.SERV_AREA_ID
  left join ge.PAT_ENC_HSP as peh
    on cai.PAT_ENC_CSN_ID = peh.PAT_ENC_CSN_ID
  left join ge.CLARITY_SER as cs
    on peh.ADMISSION_PROV_ID = cs.PROV_ID
  left join ge.ZC_PAT_CLASS as zcpc
    on cai.PAT_CLASS_C = zcpc.ADT_PAT_CLASS_C
  left join ge.ZC_PAT_SERVICE as zcps
    on cai.PAT_SERVICE_C = zcps.HOSP_SERV_C
  left join ge.ZC_DISCH_DISP as zcdd
    on peh.DISCH_DISP_C = zcdd.DISCH_DISP_C;
	
--create index ncix__tADTDepartmentPatient__PAT_ENC_CSN_ID on gt1.tADTDepartmentPatient (PAT_ENC_CSN_ID)
--create index ncix__tADTDepartmentPatient__ADT_PAYOR_ID on gt1.tADTDepartmentPatient (ADT_PAYOR_ID)
--create index ncix__tADTDepartmentPatient__DEPARTMENT_ID on gt1.tADTDepartmentPatient (DEPARTMENT_ID)
--create index ncix__tADTDepartmentPatient__SERV_AREA_ID on gt1.tADTDepartmentPatient (SERV_AREA_ID)



--------------------------------------------------------------------------------
print('-----------gt1.ClarityAdt--------------');

if object_id('gt1.ClarityAdt', 'U') is not null
    drop table gt1.ClarityAdt;

create table [gt1].[ClarityAdt]
(
    [EVENT_ID] [numeric](18, 0) not null
  , [EFFECTIVE_DATE] [date] null
  , [EVENT_DISCHARGE_DATE] [date] null
  , [_SDS_Flag] [int] not null
  , [OBSERVATION_MINUTES] [int] null
  , [NUMBER_OF_BIRTHS] [int] null
  , [EVENT_DATE] [date] null
  , [EVENT_TYPE] [int] null
  , [EVENT_IS_ADMIT] [int] null
  , [EVENT_IS_DISCHARGE] [int] null
  , [EVENT_IS_TRANSFER_IN] [int] null
  , [EVENT_IS_TRANSFER_OUT] [int] null
  , [EVENT_PATIENT_CLASS_CHANGE] [int] null
  , [EVENT_IS_CENSUS] [int] null
  , [EVENT_IS_UPGRADE] int null
  , [EVENT_IS_DOWNGRADE] int null
  , [PAT_CLASS] [varchar](254) null
  , [SERVICE] [varchar](254) null
  , [CSN] [numeric](18, 0) null
  , [HAR] [numeric](18, 0) null
  , [ADT_PAYOR_ID] [numeric](18, 0) null
  , [ADT_PAYOR] [varchar](80) null
  , [DISCHARGE_DISPOSITION] [varchar](254) null
  , [DISCHARGE_DISPOSITION_ID] [varchar](66) null
  , [DEPARTMENT_ID] [numeric](18, 0) null
  , [DEPARTMENT] [varchar](100) null
  , [CENSUS_BEDS] [int] null
  , [REVENUE_LOCATION] [varchar](80) null
  , [SERVICE_AREA_ID] [numeric](18, 0) null
  , [SERVICE_AREA] [varchar](80) null
  , [ADMITTING_PROVIDER] [varchar](200) null
)

;

declare @PatClassObs int = variables.GetSingleValue('@vcgPtClassObservation');

;
with UpDown as (
select distinct
  cai.PAT_ENC_CSN_ID
from gt1.tClarityAdtInterpretation cai
where cai.EVENT_IS_UPGRADE = 1
   or cai.EVENT_IS_DOWNGRADE = 1
)
, ObsMinutes as (
select
  a.PAT_ENC_CSN_ID
, DateDiff( Minute, Min( a.EFFECTIVE_TIME ), Max( a.EFFECTIVE_TIME ) ) as ObsMinutes
from ge.CLARITY_ADT a
  inner join UpDown
    on UpDown.PAT_ENC_CSN_ID = a.PAT_ENC_CSN_ID
where a.PAT_CLASS_C = @PatClassObs
  and a.EVENT_SUBTYPE_C <> 2
group by a.PAT_ENC_CSN_ID
)
, DepartmentBedCount as  -- EY updated with Lehigh's logic
    (
		select distinct 
			d.DEPARTMENT_ID	as DepartmentID
		  , d.DEPT_ABBREVIATION
		  , max(e.STAFFED_BEDS) as DepartmentBedCount
		from ge.ED_IEV_EVENT_INFO e 
		left outer join ge.ED_IEV_PAT_INFO ep    
		  on e.EVENT_ID = ep.EVENT_ID
		left outer join ge.CLARITY_DEP d
		  on ep.DEPT_EVENT_DEP_ID = d.DEPARTMENT_ID
		where  ep.DEPT_EVENT_DEP_ID is not null
		group by d.DEPARTMENT_ID, d.DEPT_ABBREVIATION
    )


insert into
    [gt1].[ClarityAdt](   [EVENT_ID]
                        , [EFFECTIVE_DATE]
                        , [EVENT_DISCHARGE_DATE]
                        , [_SDS_Flag]
                        , [OBSERVATION_MINUTES]
                        , [NUMBER_OF_BIRTHS]
                        , [EVENT_DATE]
                        , [EVENT_TYPE]
                        , [EVENT_IS_ADMIT]
                        , [EVENT_IS_DISCHARGE]
                        , [EVENT_IS_TRANSFER_IN]
                        , [EVENT_IS_TRANSFER_OUT]
                        , [EVENT_PATIENT_CLASS_CHANGE]
                        , [EVENT_IS_CENSUS]
						, [EVENT_IS_UPGRADE]
						, [EVENT_IS_DOWNGRADE]
                        , [PAT_CLASS]
                        , [SERVICE]
                        , [CSN]
                        , [HAR]
                        , [ADT_PAYOR_ID]
                        , [ADT_PAYOR]
                        , [DISCHARGE_DISPOSITION]
                        , [DISCHARGE_DISPOSITION_ID]
                        , [DEPARTMENT_ID]
                        , [DEPARTMENT]
                        , [CENSUS_BEDS]
                        , [REVENUE_LOCATION]
                        , [SERVICE_AREA_ID]
                        , [SERVICE_AREA]
                        , [ADMITTING_PROVIDER]
                      )

select distinct
    dp.EVENT_ID
  , cast(dp.EFFECTIVE_TIME as date) as EFFECTIVE_DATE
  --,	cast(month(dp.EFFECTIVE_TIME) as varchar) + '-' + cast(year(dp.EFFECTIVE_TIME) as varchar) as EFFECTIVE_MONTH_YEAR
  --,	cast(year(dp.EFFECTIVE_TIME) as varchar) + cast(cast(month(dp.EFFECTIVE_TIME) as numeric) as varchar) as [%ADT_MONTH_YEAR_SORT]
  , dp.SDS_EFFECTIVE_DATE as EVENT_DISCHARGE_DATE
  , case
        when dp.EVENT_TYPE_C = 1 and cast(dp.EFFECTIVE_TIME as date) = dp.SDS_EFFECTIVE_DATE
        then 1
        else 0
    end as [_SDS_Flag]
  , IsNull( ObsMinutes.ObsMinutes, 0 ) as OBSERVATION_MINUTES
  , case when dp.BABY_AMOUNT > 0 then dp.BABY_AMOUNT else 0 end as NUMBER_OF_BIRTHS
  , cast(EVENT_TIME as date) as EVENT_DATE
  , dp.EVENT_TYPE_C as EVENT_TYPE
  , case
        when dp.IN_EVENT_TYPE_C = 1--and dp.TO_BASE_CLASS_C = 1 and dp.EVENT_TYPE_C in (1, 3, 5)
        then 1
    end as EVENT_IS_ADMIT
  , case
        when dp.OUT_EVENT_TYPE_C = 2 --and dp.FROM_BASE_CLASS_C = 1 and dp.EVENT_TYPE_C in (2, 4, 5)
        then 1
    end as EVENT_IS_DISCHARGE
  , case
        when dp.EVENT_TYPE_C = 3  
        then 1
    end as EVENT_IS_TRANSFER_IN
  , case
        when dp.EVENT_TYPE_C = 4  
        then 1
    end as EVENT_IS_TRANSFER_OUT
  , case
        when dp.EVENT_TYPE_C = 5
        then case
                 when dp.TO_BASE_CLASS_C = 1 and dp.FROM_BASE_CLASS_C <> 1
                 then 1
                 else case when dp.FROM_BASE_CLASS_C = 1 and dp.TO_BASE_CLASS_C <> 1 then -1 end
             end
    end as EVENT_PATIENT_CLASS_CHANGE
  , case when dp.EVENT_TYPE_C = 6 and dp.EVENT_TYPE_C is not null then 1 end as EVENT_IS_CENSUS
  , dp.EVENT_IS_UPGRADE
  , dp.EVENT_IS_DOWNGRADE
  , dp.PAT_CLASS_NAME as PAT_CLASS
  , dp.PAT_SERVICE_NAME as [SERVICE]
  , dp.PAT_ENC_CSN_ID as CSN
  , dp.HSP_ACCOUNT_ID as HAR
  , ha.PRIMARY_PAYOR_ID as ADT_PAYOR_ID
  , ce.PAYOR_NAME as ADT_PAYOR
  , dp.DISCH_DISP_C_NAME as DISCHARGE_DISPOSITION
  , dp.DISCH_DISP_C as DISCHARGE_DISPOSITION_ID
  , dp.DEPARTMENT_ID
  , dp.DEPARTMENT_NAME as DEPARTMENT
  , DepartmentBedCount as CENSUS_BEDS
  , dp.REV_LOC_NAME as REVENUE_LOCATION
  , dp.SERV_AREA_ID as SERVICE_AREA_ID
  , dp.SERV_AREA_NAME as SERVICE_AREA
  , dp.ADMISSION_PROV_NAME as ADMITTING_PROVIDER
  from [gt1].[tADTDepartmentPatient] as dp
  left join gt1.tAdtHourInterval as ahi
    on dp.PAT_ENC_CSN_ID = ahi.PAT_ENC_CSN_ID
  left join ge.HSP_ACCOUNT as ha
    on dp.ADT_PAYOR_ID = ha.HSP_ACCOUNT_ID
  left join ge.CLARITY_EPM as ce
    on dp.ADT_PAYOR_ID = ce.PAYOR_ID
  left join DepartmentBedCount dbc
    on dp.DEPARTMENT_ID = dbc.DepartmentID
  left join ObsMinutes
    on dp.PAT_ENC_CSN_ID = ObsMinutes.PAT_ENC_CSN_ID
 --left join @EventType as et
 --  on j.EVENT_TYPE_C = et.EVENT_TYPE_C
;

--create index iCSNca on ClarityAdt (CSN);
--create index iAPIca on ClarityAdt (ADT_PAYOR_ID);
--create index iDIca on ClarityAdt (DEPARTMENT_ID);
--create index iSAIca on ClarityAdt (SERVICE_AREA_ID);
--create index iETCca on ClarityAdt (EVENT_TYPE);
--create index iESCca on ClarityAdt (EVENT_SUBTYPE_C);

--drop table #AdtHourIntervals;
--drop table #AdtObs;
--drop table #BabyDataFinal;
--drop table #ClarityAdtInterpretation;

 
create index [ncix__ClarityAdt__DEPARTMENT_EFFECTIVE_DATE] on [paval].[gt1].[ClarityAdt] ([DEPARTMENT],[EFFECTIVE_DATE]) include ([EVENT_IS_UPGRADE], [EVENT_IS_DOWNGRADE])
create index [ncix__ClarityAdt__EFFECTIVE_DATE] on [paval].[gt1].[ClarityAdt] ([EFFECTIVE_DATE]) include ([EVENT_IS_UPGRADE], [EVENT_IS_DOWNGRADE], [DEPARTMENT])
 







GO

