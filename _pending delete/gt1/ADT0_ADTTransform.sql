SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
 


 CREATE procedure [gt1].[ADT0_ADTTransform] 
as 
 

/*
-->	Source tables used <--
	pa.ge.V_PAT_ADT_LOCATION_HX   

--> Target tables populated <--
	[gt1].[ADTHistory] 
	[gt1].[HospitalLOS] 
	[gt1].[DepartmentLOS] 
	[gt1].[ICULOS]
 
--> Performance benchmark <--
    2766818 rows in 38 seconds
*/
	
--declare and populate variables
declare @vADTTransferEventType int = (select cast(variables.GetSingleValue ('@vADTTransferEventType') as int))
      , @vADTAdmissionEventType int = (select cast(variables.GetSingleValue ('@vADTAdmissionEventType') as int))  
      , @vADTDischargeEventType int = (select cast(variables.GetSingleValue ('@vADTDischargeEventType') as int)) 

declare @ICUDepartments_DEPARTMENT_ID table (id int identity(1,1), Department_ID int)
insert into @ICUDepartments_DEPARTMENT_ID
select VariableValue from variables.GetTableValues ('@ICUDepartments_DEPARTMENT_ID')

  
 -- ADTHistory
 if exists (select * from information_schema.tables where TABLE_NAME = 'ADTHistory' and TABLE_SCHEMA = 'gt1')
  drop table gt1.[ADTHistory]
create table [gt1].[ADTHistory]  
(
    [EncounterCSN] [numeric](18, 0) NULL,
    [InTime] [datetime] NULL,
    [OutTime] [datetime] NULL,
    [EventID] [numeric](18, 0) NULL,
    [EventTypeID] [int] NULL, --RTEventTypeID
    --[ADT_BED_ID] [varchar](18) NULL,
    [DepartmentID] [numeric](18, 0) NULL,
	[DepartmentName] varchar(100) null,
    --[ADT_LOC_ID] [numeric](18, 0) NULL,
    --[ADT_ROOM_ID] [varchar](18) NULL,
    --[ADT_SERV_AREA_ID] [numeric](18, 0) NULL,
    --[ADT_BED_CSN] [numeric](18, 0) NULL,
    --[ADT_ROOM_CSN] [numeric](18, 0) NULL,
    [ICUDepartmentFlag] [int] NOT NULL,
    [TransferToICUFlag] [int] NOT NULL,
    [EventDurationMinute] [int] NULL,
    [AdmissionEventFlag] [int] NOT NULL
)  
insert into gt1.ADTHistory
(           
     [EncounterCSN]
    ,[InTime]
    ,[OutTime]
    ,[EventID]
    ,[EventTypeID] --RTEventTypeID
    --,[ADT_BED_ID]
    ,[DepartmentID]
	,[DepartmentName]
    --,[ADT_LOC_ID]
    --,[ADT_ROOM_ID]
    --,[ADT_SERV_AREA_ID]
    --,[ADT_BED_CSN]
    --,[ADT_ROOM_CSN]
    ,[ICUDepartmentFlag]
    ,[TransferToICUFlag]
    ,[EventDurationMinute]
    ,[AdmissionEventFlag]
)
select
  PAT_ENC_CSN as EncounterCSN, 
  IN_DTTM as InTime,
  OUT_DTTM as OutTime,
  EVENT_ID as EventID,
  EVENT_TYPE_C as EventTypeID, 
  --ADT_BED_ID,
  ADT_DEPARTMENT_ID as DepartmentID,
  ADT_DEPARTMENT_NAME as DepartmentName,
  --ADT_LOC_ID,
  --ADT_ROOM_ID,
  --ADT_SERV_AREA_ID,
  --ADT_BED_CSN,
  --ADT_ROOM_CSN, 
  iif(isnull(vdc.DEPARTMENT_ID,0) = 0, 0, 1) as ICUDepartmentFlag,
  iif(vpa.EVENT_TYPE_C = @vADTTransferEventType and DEPARTMENT_ID is not null, 1, 0) as TransferToICUFlag, 
  datediff(minute, vpa.IN_DTTM, vpa.OUT_DTTM) as EventDurationMinute, 
  iif(vpa.EVENT_TYPE_C = @vADTAdmissionEventType, 1, 0) as AdmissionEventFlag 
from pa.ge.V_PAT_ADT_LOCATION_HX vpa
left join @ICUDepartments_DEPARTMENT_ID vdc
  on vpa.ADT_DEPARTMENT_ID = vdc.DEPARTMENT_ID
--where Year(IN_DTTM) >= 2016 --Updated to run quicker with less data
 
--Hospital LOS
 if exists (select * from information_schema.tables where TABLE_NAME = 'HospitalLOS' and TABLE_SCHEMA = 'gt1')
  drop table gt1.[HospitalLOS]
create table [gt1].[HospitalLOS] 
(
	[PAT_ENC_CSN_ID] [numeric](18, 0) NULL,
	[MinDischargeEventInTime] [datetime] NULL,
	[MinAdmitEventInTime] [datetime] NULL,
	[LOSDays] [int] NULL--,
	--[LOSGroup] [varchar](50) NULL
)
;
with DischargeTimes as 
( 
	 select a.EncounterCSN as PAT_ENC_CSN_ID, iif(a.EventTypeID = @vADTDischargeEventType, a.InTime, '9/9/2099') as MinDischargeEventInTime from gt1.ADTHistory a
	  cross apply
	(select EncounterCSN, min(iif(EventTypeID = @vADTDischargeEventType, InTime, '9/9/2099')) as MinIT from gt1.ADTHistory group by EncounterCSN) b
	 where a.EncounterCSN = b.EncounterCSN 
	   and iif(a.EventTypeID = @vADTDischargeEventType, a.InTime, '9/9/2099') = B.MinIT
), AdmitTimes as
(
	 select a.EncounterCSN as PAT_ENC_CSN_ID, iif(a.EventTypeID = @vADTAdmissionEventType, a.InTime, '9/9/2099') as MinAdmitEventInTime from gt1.ADTHistory a
	  cross apply
	(select EncounterCSN, min(iif(EventTypeID = @vADTAdmissionEventType, InTime, '9/9/2099')) as MinIT from gt1.ADTHistory group by EncounterCSN) b
	 where a.EncounterCSN = b.EncounterCSN 
	   and iif(a.EventTypeID = @vADTAdmissionEventType, a.InTime, '9/9/2099') = B.MinIT
), LOSTimes as
(
	select at.PAT_ENC_CSN_ID, MinDischargeEventInTime, MinAdmitEventInTime, datediff(day,MinAdmitEventInTime, MinDischargeEventInTime) as LOSDays  -->  RT should this be datediff(minute,MinAdmitEventInTime, MinDischargeEventInTime) as LOSMinutes? do we need for later?
	  from DischargeTimes dt
	 inner join AdmitTimes at
		on dt.PAT_ENC_CSN_ID = at.PAT_ENC_CSN_ID
) 
insert into gt1.HospitalLOS
select 
	PAT_ENC_CSN_ID, 
	MinDischargeEventInTime, 
	MinAdmitEventInTime, 
	LOSDays  
--  LOSMinutes,
--	lgm.[Group] as LOSGroup 
from LOSTimes lt
--left join map.LOSGroupMap lgm 
--  on lt.LOSDays = lgm.[Key]
 
--Department LOS 
if exists (select * from information_schema.tables where TABLE_NAME = 'DepartmentLOS' and TABLE_SCHEMA = 'gt1')
  drop table gt1.DepartmentLOS 
create table gt1.DepartmentLOS  
(
	[PAT_ENC_CSN_ID] [numeric](18, 0) NULL,
	[ADT_LOS_DEPARTMENT_ID] [numeric](18, 0) NULL,
	[ICUDepartmentFlag] [int] NOT NULL,
	[DepartmentLOSMinute] [numeric](38, 0) NULL
) 
insert into gt1.DepartmentLOS
 (
  	[PAT_ENC_CSN_ID]
   ,[ADT_LOS_DEPARTMENT_ID]
   ,[ICUDepartmentFlag]
   ,[DepartmentLOSMinute]
)
select
  ah.EncounterCSN,
  ah.DepartmentID as ADT_LOS_DEPARTMENT_ID,
  ICUDepartmentFlag,
--  //In.Time as Department.Arrival.Time,
--  //Out.Time as Department.Departure.Time, 
   sum(datediff(minute, InTime, OutTime)) as DepartmentLOSMinutes
from gt1.ADTHistory ah
where ah.EventTypeID = @vADTAdmissionEventType
   or ah.EventTypeID = @vADTTransferEventType 
group by EncounterCSN 
	   , DepartmentID
	   , ICUDepartmentFlag
;
-- ICU_LOS
if exists (select * from information_schema.tables where TABLE_NAME = 'ICULOS' and TABLE_SCHEMA = 'gt1')
  drop table gt1.ICULOS 
create table [gt1].[ICULOS]
(
	[PAT_ENC_CSN_ID] [numeric](18, 0) NULL,
	[ICULOSDay] [int] NULL
) ON [PRIMARY]
insert into [gt1].[ICULOS]
(
	[PAT_ENC_CSN_ID]
   ,[ICULOSDay]
)
select
  EncounterCSN,
  sum(datediff(day, InTime, OutTime)) as ICULOSDays
from gt1.ADTHistory
where ICUDepartmentFlag = 1
group by EncounterCSN;
 
-- check keys and indexes
exec tools.CheckKeysAndIndexes













GO
