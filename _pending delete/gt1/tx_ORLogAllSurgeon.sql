if object_id('gt1.ORLogAllSurgeon', 'U') is not null drop table gt1.ORLogAllSurgeon;

create table gt1.ORLogAllSurgeon (
	LogID [numeric] (18,0) NOT NULL
  ,Panel [numeric] (18,0) NULL
  ,SurgeonID [numeric] (18,0) NULL
	,SurgeonName [varchar](254) NULL
	,Role [varchar](254) NULL
  ,StartTime [datetime] NULL
  ,EndTime [datetime] NULL
  ,TotalTime [numeric] (18,0) NULL
  ,Line [numeric] (18,0) NULL
	,StaffIsPrimarySurgeon [numeric] (18,0) NULL
);
with 
Panel1PrimarySurgeon as ( --PANEL 1 Logic - Duplicate if successful
	select
		LOG_ID as LogID
		,min(LINE) as Line
	from ge.OR_LOG_ALL_SURG
	where ROLE_C=1
	and PANEL=1
	group by LOG_ID
)
insert into gt1.ORLogAllSurgeon
(
	LogID
  ,Panel
  ,SurgeonID
	,SurgeonName
	,Role
  ,StartTime
  ,EndTime
  ,TotalTime
  ,Line
	,StaffIsPrimarySurgeon
)
select
	cast(surg.LOG_ID as numeric) as LogID
  ,surg.PANEL as Panel
  --,ROLE_C as StaffRoleID
  ,surg.SURG_ID as SurgeonID
	,ser.PROV_NAME as SurgeonName
	,'Surgeon' + role.NAME as Role
  ,surg.START_TIME as StartTime
  ,surg.END_TIME as EndTime
  ,surg.TOTAL_LENGTH as TotalTime
  ,surg.LINE as Line
	,iif(len(panel1.Line)>0 and surg.PANEL=1,1,0) as StaffIsPrimarySurgeon --PANEL 1 Logic - Duplicate if successful
from ge.OR_LOG_ALL_SURG surg
left join Panel1PrimarySurgeon panel1 on --PANEL 1 Logic - Duplicate if successful
	panel1.LogID = surg.LOG_ID
	and panel1.Line = surg.LINE
left join ge.ZC_OR_PANEL_ROLE role on
	role.ROLE_C = surg.ROLE_C
left join ge.CLARITY_SER ser on
	ser.PROV_ID = surg.SURG_ID
;
