SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
 
 
 
CREATE procedure [gt1].[OP1_OrderProcedure] as 

/*  --> Source Tables <--
		pa.ge.CL_PRL_SS
		pa.ge.ORDER_INSTANTIATED
		pa.ge.ORDER_PROC
		pa.ge.ORDER_PROC_2
		pa.ge.ORDER_SMARTSET 
		gt1.OrderMed  

	--> Target Table <--
		gt1.OrderProcedure 

	--> Performance Benchmark <--
		230740 rows ~10s
*/		

if OBJECT_ID('gt1.OrderProcedure', 'U') is not null drop table gt1.OrderProcedure;
create table gt1.OrderProcedure
(
  EncounterCSN numeric 
, ProcedureOrderTime datetime
, ProcedureResultTime datetime
, SpecimenTakenTime datetime
, BloodCxFlag int
, SepsisOrdersetUsedFlag int
, CVUltrasoundFlag int
, OrdersetName nvarchar(1000)
) 

declare @vAllProcID table (id int identity(1,1), PROC_ID numeric(18,0))
insert into @vAllProcID
select distinct PROC_ID from pa.ge.ORDER_PROC
 
declare @SepsisOrderSet table (id int identity(1,1), ProtocolID numeric(18,0))
insert into @SepsisOrderSet
select VariableValue from variables.GetTableValues ('@OrderSets_Protocol_ID')

declare @BloodCxProcID table (id int identity(1,1), PROC_ID numeric(18,0))
insert into @BloodCxProcID
select VariableValue from variables.GetTableValues ('@BloodCxProcID')

declare @CVUltrasoundFlag table (id int identity(1,1), PROC_ID numeric(18,0))
insert into @CVUltrasoundFlag
select VariableValue from variables.GetTableValues('@CVUltrasoundFlag')
 

insert into [gt1].[OrderProcedure]
(
	 [EncounterCSN]
    ,[ProcedureOrderTime]
    ,[ProcedureResultTime]
    ,[SpecimenTakenTime]
    ,[BloodCxFlag]
    ,[SepsisOrdersetUsedFlag]
	,[CVUltrasoundFlag]
    ,[OrdersetName]
)
select 
	op.PAT_ENC_CSN_ID as EncounterCSN,
	op.ORDER_TIME as ProcedureOrderTime,
	--op.RESULT_TIME as ProcedureResultTime, 
	max(op_child.RESULT_TIME) as ProcedureResultTime, -- Using child, not parent, result time
	op2.SPECIMN_TAKEN_TIME as SpecimenTakenTime, 
	max(iif(bcp.PROC_ID is not null,1,0)) as BloodCxFlag,
	max(iif(sos.ProtocolID is not null,1,0)) as SepsisOrdersetUsedFlag, 
	max(iif(usf.PROC_ID is not null, 1,0)) as CVUltrasoundFlag,
	isnull(cps.PROTOCOL_NAME, '*Unknown') as OrdersetName
from pa.ge.ORDER_PROC op
inner join @vAllProcID apid
	on op.PROC_ID = apid.PROC_ID
left join pa.ge.ORDER_SMARTSET oss
    on op.ORDER_PROC_ID = oss.ORDER_ID
left join pa.ge.ORDER_INSTANTIATED oi
    --on op.ORDER_PROC_ID = oi.INSTNTD_ORDER_ID -- Join on the ORDER_ID of instantiated table first
	on op.ORDER_PROC_ID = oi.ORDER_ID
-- parent child workflow
left join pa.ge.ORDER_PROC op_child
	ON oi.INSTNTD_ORDER_ID = op_child.ORDER_PROC_ID
--
left join pa.ge.ORDER_PROC_2 op2
	--on op.ORDER_PROC_ID = op2.ORDER_PROC_ID
	on oi.INSTNTD_ORDER_ID = op2.ORDER_PROC_ID -- Use child order
left join @BloodCxProcID bcp
    on op.PROC_ID = bcp.PROC_ID
left join @SepsisOrderSet sos
    on oss.SS_PRL_ID = sos.ProtocolID
left join @CVUltrasoundFlag usf
    on op.PROC_ID = usf.PROC_ID
left join pa.ge.CL_PRL_SS cps
    on oss.SS_PRL_ID = cps.PROTOCOL_ID
group by op.ORDER_PROC_ID,
	op.PAT_ENC_CSN_ID,
	op.PROC_ID,
	op.PROC_CODE,  
	op.ORDER_STATUS_C,
	op.ORDER_TIME,
	--op.RESULT_TIME, 
	--op_child.RESULT_TIME,
	op2.SPECIMN_TAKEN_TIME ,
	sos.ProtocolID, 
	cps.PROTOCOL_NAME
	  
create nonclustered index [ncix__OrderProcedure__EncounterCSN] on [gt1].[OrderProcedure] ([EncounterCSN]) on [PRIMARY]
create nonclustered index [ncix__OrderProcedure__BloodCxFlag__SpecimenTakenTime] on [gt1].[OrderProcedure] ([BloodCxFlag], [SpecimenTakenTime]) include ([EncounterCSN], [OrdersetName], [ProcedureResultTime], [SepsisOrdersetUsedFlag]) on [PRIMARY]; 

-- check keys and indexes
exec tools.CheckKeysAndIndexes








GO
