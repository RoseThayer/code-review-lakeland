if object_id('gt1.ORCasePrimarySurgeon', 'U') is not null drop table gt1.ORCasePrimarySurgeon;

create table gt1.ORCasePrimarySurgeon (
	CaseID [numeric] (18,0) NOT NULL
  ,Panel [numeric] (18,0) NOT NULL
  ,SurgeonID [numeric] (18,0) NOT NULL
	,SurgeonName [varchar](254) NULL
  ,Line [numeric] (18,0) NOT NULL
	,StaffIsPrimarySurgeon [numeric] (18,0) NOT NULL
);
with
Panel1PrimarySurgeon as ( 
	select
		OR_CASE_ID as CaseID
		,min(LINE) as Line
	from ge.OR_CASE_ALL_SURG
	where ROLE_C=1
	and PANEL=1
	group by OR_CASE_ID
)
insert into gt1.ORCasePrimarySurgeon
(
	CaseID
  ,Panel
  ,SurgeonID
	,SurgeonName
  ,Line
	,StaffIsPrimarySurgeon
)
select
	cast(surg.OR_CASE_ID as numeric) as CaseID
  ,surg.PANEL as Panel
  --,ROLE_C as StaffRoleID
  ,surg.SURG_ID as SurgeonID
	,ser.PROV_NAME as SurgeonName
  ,surg.LINE as Line
	,iif(len(panel1.Line)>0 and surg.PANEL=1,1,0) as StaffIsPrimarySurgeon --PANEL 1 Logic - Duplicate if successful
from ge.OR_CASE_ALL_SURG surg
left join Panel1PrimarySurgeon panel1 on --PANEL 1 Logic - Duplicate if successful
	panel1.CaseID = surg.OR_CASE_ID
	and panel1.Line = surg.LINE
left join ge.ZC_OR_PANEL_ROLE role on
	role.ROLE_C = surg.ROLE_C
left join ge.CLARITY_SER ser on
	ser.PROV_ID = surg.SURG_ID
;
