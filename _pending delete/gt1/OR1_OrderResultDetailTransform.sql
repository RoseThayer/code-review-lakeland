SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
 


 CREATE procedure [gt1].[OR1_OrderResultDetailTransform]
  as

   /*  --> Source Tables <--	
		pa.ge.ORDER_SMARTSET  
		pa.ge.ORDER_INSTANTIATED
		pa.ge.ORDER_RESULTS
		pa.ge.ORDER_PROC
		pa.ge.ORDER_PROC_2 
		pa.ge.CL_PRL_SS 
		pa.ge.CLARITY_EAP
		pa.ge.CLARITY_COMPONENT
   
	--> Target Table <--
		gt1.OrderResultDetail 

	--> Performance Benchmark <--
		5060756 rows ~1m
*/		
 
BEGIN
-- -- SET NOCOUNT ON added to prevent extra result sets from
-- -- interfering with SELECT statements.
 SET NOCOUNT ON;

 -- declare variables
declare @PO2ArterialComponentID as int =  (select variables.GetSingleValue ('@PO2ArterialComponentID')  )

declare @SepsisOrderSet table (id int identity(1,1), ProtocolID numeric(18,0))
insert into @SepsisOrderSet 
select VariableValue from variables.GetTableValues ('@OrderSets_Protocol_ID')

--declare @SepsisIPOrderSets as smallint = 0; -- No LVHN workflow
--declare @SepsisEDOrderSets as smallint = 0; -- No LVHN workflow

declare @WBCComponentID table (id int identity(1,1), ComponentID numeric(18,0))
insert into @WBCComponentID 
select VariableValue from variables.GetTableValues ('@WBC_Component_ID')

declare @BandsComponentID table (id int identity(1,1), ComponentID numeric(18,0))
insert into @BandsComponentID 
select VariableValue from variables.GetTableValues ('@Bands_Component_ID')

declare @BilirubinComponentID table (id int identity(1,1), ComponentID numeric(18,0))
insert into @BilirubinComponentID 
select VariableValue from variables.GetTableValues ('@BiliRubin_Component_ID')

declare @PlateletComponentID table (id int identity(1,1), ComponentID numeric(18,0))
insert into @PlateletComponentID 
select VariableValue from variables.GetTableValues ('@Platelet_Component_ID')

declare @LactateComponentID table (id int identity(1,1), ComponentID numeric(18,0))
insert into @LactateComponentID 
select VariableValue from variables.GetTableValues ('@Lactate_Component_ID')

declare @CreatinineComponentID table (id int identity(1,1), ComponentID numeric(18,0))
insert into @CreatinineComponentID 
select VariableValue from variables.GetTableValues ('@Creatinine_Component_ID')

declare @PTINRComponentID table (id int identity(1,1), ComponentID numeric(18,0))
insert into @PTINRComponentID 
select VariableValue from variables.GetTableValues ('@INR_Component_ID')

declare @PTTComponentID table (id int identity(1,1), ComponentID numeric(18,0))
insert into @PTTComponentID 
select VariableValue from variables.GetTableValues ('@PTT_Component_ID')
 
if OBJECT_ID('gt1.OrderResultDetail', 'U') is not null drop table gt1.OrderResultDetail;
create table [gt1].[OrderResultDetail]
(
  [EncounterCSN] [numeric](18, 0) NULL,
  [ResultTime] [datetime] NULL,
  [OrderTime] [datetime] NULL,
  [OrderProcID] [numeric](18, 0) NOT NULL,
  [ProcedureName] [varchar](100) NULL,
  [ResultValue] [float] NULL,
  [SpecimenTakenTime] [datetime] NULL, 
  [ComponentID] [numeric](18, 0) NULL,
  [ComponentName] [varchar](75) NULL,
  [Wbc] [float] NULL,
  [Bands] [float] NULL,
  [Bilirubin] [float] NULL,
  [PlateletCount] [float] NULL,
  [Lactate] [float] NULL,
  [Creatinine] [float] NULL,
  [PtInr] [float] NULL,
  [Ptt] [float] NULL,
  PO2Arterial float null,
  [SepsisOrdersetUsedFlag] [int] NULL,
  [OrdersetName] [varchar](200) NULL
)

;
with OrderTab (ORDER_PROC_ID,SS_PRL_ID) as
(
select
     oss.SS_PRL_ID as SS_PRL_ID
   , oi.INSTNTD_ORDER_ID as ORDER_PROC_ID
from pa.ge.ORDER_SMARTSET oss
inner join pa.ge.ORDER_INSTANTIATED oi  
   on oss.ORDER_ID = oi.ORDER_ID

union

select
    oss.ORDER_ID as ORDER_PROC_ID
  , oss.SS_PRL_ID as SS_PRL_ID
from pa.ge.ORDER_SMARTSET oss
)

, OrderTabGroup as
(
select
    ot.ORDER_PROC_ID
  , max(ot.SS_PRL_ID) as SS_PRL_ID
  , max(iif(sos.ProtocolID is not null,1,0)) as SepsisOrdersetUsedFlag
--, Max(iif(SS_PRL_ID=@SepsisIPOrderSets,1,0)) as SepsisIPOrdersetUsedFlag -- No LVHN workflow
--, Max(iif(SS_PRL_ID=@SepsisEDOrderSets,1,0)) as SepsisEDOrdersetUsedFlag -- No LVHN workflow
from OrderTab ot
left join @SepsisOrderSet sos
  on ot.SS_PRL_ID = sos.ProtocolID
group by ORDER_PROC_ID
)

, OrderResult AS
(
select
	  ord.ORDER_PROC_ID as OrderProcID
	, ord.PAT_ENC_CSN_ID as EncounterCSN
	, ord.COMPONENT_ID as ComponentID
	, ord.RESULT_TIME as ResultTime
	, ord.ORD_NUM_VALUE as ResultValue
	, op.PROC_ID as ProcID
	, op.ORDER_TIME as OrderTime
	, op2.SPECIMN_TAKEN_TIME as SpecimenTakenTime 
	, otg.SS_PRL_ID
	, otg.SepsisOrdersetUsedFlag
	, coalesce(cps.PROTOCOL_NAME,'*Unknown') as OrdersetName
	--, SepsisIPOrdersetUsedFlag -- No LVHN workflow
	--, SepsisEDOrdersetUsedFlag  -- No LVHN workflow
from pa.ge.ORDER_RESULTS  ord  with (nolock, index( ncix__ORDER_RESULTS__ORDER_PROC_ID))  
--optimizer ignoring index, this hint forces its use.  Tested both ways, w forced indexing is ~25% faster.  Did not yet test forcing seek vs scan.
left join pa.ge.[ORDER_PROC]  op  --optimizer already using index here 
  on op.ORDER_PROC_ID = ord.ORDER_PROC_ID
left join pa.ge.ORDER_PROC_2 op2  with (nolock, index(ncix__ORDER_PROC_2__ORDER_PROC_ID)) 
  on ord.ORDER_PROC_ID = op2.ORDER_PROC_ID
left join OrderTabGroup AS otg
  on ord.ORDER_PROC_ID = otg.ORDER_PROC_ID
left join pa.ge.CL_PRL_SS cps
  on otg.SS_PRL_ID = cps.PROTOCOL_ID
  where ord.ORD_NUM_VALUE <> 9999999 
) 
insert into [gt1].[OrderResultDetail]
(
	  [EncounterCSN]
	, [ResultTime]
	, [OrderTime]
	, [OrderProcID]
	, [ProcedureName]
	, [ResultValue]
	, [SpecimenTakenTime] 
	, [ComponentID]
	, [ComponentName]
	, [Wbc]
	, [Bands]
	, [Bilirubin]
	, [PlateletCount]
	, [Lactate]
	, [Creatinine]
	, [PtInr]
	, [Ptt]
	, PO2Arterial
	, [SepsisOrdersetUsedFlag]
	, [OrdersetName]
)
select
    ord.EncounterCSN
  , ord.ResultTime
  , ord.OrderTime
  , ord.OrderProcID
  , coalesce(ce.PROC_NAME,'*Unspecified') AS ProcedureName
  , ord.ResultValue
  , ord.SpecimenTakenTime --RT: New for Lactate collection time
  , ord.ComponentID as ComponentID
  , coalesce(cc.NAME,'*Unspecified') as ComponentName
  , iif(wbc.ComponentID is not null, ResultValue, NULL) as Wbc
  , iif(bnd.ComponentID is not null, ResultValue,NULL) as Bands
  , iif(bli.ComponentID is not null, ResultValue,NULL) as Bilirubin
  , iif(plt.ComponentID is not null, ResultValue,NULL) as PlateletCount
  , iif(lct.ComponentID is not null, ResultValue,NULL) as Lactate
  , iif(crt.ComponentID is not null, ResultValue,NULL) as Creatinine
  , iif(pnr.ComponentID is not null, ResultValue,NULL) as PtInr
  , iif(ptt.ComponentID is not null, ResultValue,NULL) as Ptt
  , iif(ord.ComponentID IN (@PO2ArterialComponentID), ResultValue,NULL) as PO2Arterial
  , ord.SepsisOrdersetUsedFlag
--, SepsisIPOrdersetUsedFlag -- No LVHN workflow
--, SepsisEDOrdersetUsedFlag -- No LVHN workflow
  , ord.OrdersetName
from OrderResult ord
left join pa.ge.CLARITY_EAP as ce with (nolock)
	on ord.ProcID = ce.PROC_ID
left join pa.ge.CLARITY_COMPONENT as cc with (nolock)
	on ord.ComponentID = cc.COMPONENT_ID 
left join @WBCComponentID wbc 
  on ord.ComponentID = wbc.ComponentID
left join @BandsComponentID bnd
  on ord.ComponentID = bnd.ComponentID
left join @BilirubinComponentID bli
  on ord.ComponentID = bli.ComponentID
left join @PlateletComponentID plt
  on ord.ComponentID = plt.ComponentID
left join @LactateComponentID lct
  on ord.ComponentID = lct.ComponentID
left join @CreatinineComponentID crt
  on ord.ComponentID = crt.ComponentID
left join @PTINRComponentID pnr
  on ord.ComponentID = pnr.ComponentID
left join @PTTComponentID ptt
  on ord.ComponentID = ptt.ComponentID
  
 end
 
 -- check keys and indexes
exec tools.CheckKeysAndIndexes







GO
