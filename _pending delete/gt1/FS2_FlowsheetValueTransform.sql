SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
 
 
CREATE procedure [gt1].[FS2_FlowsheetValueTransform] as
 
 /*
	--> Source Tables used: <--
	    gt1.FlowsheetDetail

	--> Target tables created: <--
		padev.gt1.FlowsheetValue

	--> Performance benchmark: <--
		8717986 rows in 1:08
*/

BEGIN

SET NOCOUNT ON;

if OBJECT_ID('gt1.FlowsheetValue', 'U') is not null 
   drop table gt1.FlowsheetValue;
create table [gt1].[FlowsheetValue]
(
	[EncounterCSN] [numeric](18, 0) NOT NULL,
	[FlowsheetRecordedTime] [datetime] NULL,
	[MinSystolicBloodPressure] [int] NULL,
	[MaxSystolicBloodPressure] [int] NULL,
	[MinTemperatureF] [float] NULL,
	[MaxTemperatureF] [float] NULL,
	[MinTemperatureC] [float] NULL,
	[MaxTemperatureC] [float] NULL,
	[MinPulse] [float] NULL,
	[MaxPulse] [float] NULL,
	[MinRespirationRate] [float] NULL,
	[MaxRespirationRate] [float] NULL,
	[MinScvO2] [float] NULL,
	[minCVP] [float] NULL,
	[MaxCVP] [float] NULL,
	[MinMAP] [float] NULL,
	[MaxMAP] [float] NULL,
	[MinUrine] [float] NULL,
	[MaxUrine] [float] NULL,
	[MinWeight] [float] NULL,
	[MaxWeight] [float] NULL,
	[VitalsRecordedFlag] [int] NOT NULL
) 
;With FlowsheetValue
as
(
SELECT  
   EncounterCSN 
  ,FlowsheetRecordedTime 
  ,min(SystolicBloodPressure) as MinSystolicBloodPressure
  ,max(SystolicBloodPressure) as MaxSystolicBloodPressure
  --,min(DiastolicBloodPressure) as MinDiastolicBloodPressure
  --,max(DiastolicBloodPressure) as MaxDiastolicBloodPressure
  ,min(TemperatureF) as MinTemperatureF
  ,max(TemperatureF) as MaxTemperatureF
  ,min(TemperatureC) as MinTemperatureC
  ,max(TemperatureC) as MaxTemperatureC
  ,min(Pulse) as MinPulse
  ,max(Pulse) as MaxPulse
  ,min(RespirationRate) as MinRespirationRate
  ,max(RespirationRate) as MaxRespirationRate
  --,min(FIO2) as MinFIO2
  --,max(FIO2) as MaxFIO2
  ,min(ScvO2) as MinScvO2
  --,max(ScvO2) as MaxScvO2
  --,min(PRBC) as MinPRBC
  --,max(PRBC) as MaxPRBC
  ,min(CVP) as MinCVP
  ,max(CVP) as MaxCVP
  ,min(MAP) as MinMAP
  ,max(MAP) as MaxMAP
  ,min(Urine) as MinUrine
  ,max(Urine) as MaxUrine
  ,min(WeightKg) as MinWeight
  ,max(WeightKg) as MaxWeight
  --,min(PassiveLegRaise) as MinPassiveLegRaise
  --,max(PassiveLegRaise) as MaxPassiveLegRaise
  --,min(SepsisRiskScore) as MinSepsisRiskScore
  --,max(SepsisRiskScore) as MaxSepsisRiskScore
  --,min(MDSuspectsSepsis) as MinMDSuspectsSepsis
  --,max(MDSuspectsSepsis) as MaxMDSuspectsSepsis
from gt1.FlowsheetDetail
group by EncounterCSN, EncounterCSN, FlowsheetRecordedTime
)
insert into [gt1].[FlowsheetValue]
(	 [EncounterCSN]
    ,[FlowsheetRecordedTime]
    ,[MinSystolicBloodPressure]
    ,[MaxSystolicBloodPressure]
    ,[MinTemperatureF]
    ,[MaxTemperatureF]
    ,[MinTemperatureC]
    ,[MaxTemperatureC]
    ,[MinPulse]
    ,[MaxPulse]
    ,[MinRespirationRate]
    ,[MaxRespirationRate]
	,[MinScvO2]
    ,[minCVP]
    ,[MaxCVP]
    ,[MinMAP]
    ,[MaxMAP]
    ,[MinUrine]
    ,[MaxUrine]
    ,[MinWeight]
    ,[MaxWeight]
    ,[VitalsRecordedFlag]
) 
select  
   EncounterCSN 
  ,FlowsheetRecordedTime 
  ,MinSystolicBloodPressure
  ,MaxSystolicBloodPressure
  --,MinDiastolicBloodPressure
  --,MaxDiastolicBloodPressure
  ,MinTemperatureF
  ,MaxTemperatureF
  ,MinTemperatureC
  ,MaxTemperatureC
  ,MinPulse
  ,MaxPulse
  ,MinRespirationRate
  ,MaxRespirationRate
  --,MinFIO2
  --,MaxFIO2
  ,MinScvO2
  --,MaxScvO2
  --,MinPRBC
  --,MaxPRBC
  ,minCVP
  ,MaxCVP
  ,MinMAP
  ,MaxMAP
  ,MinUrine
  ,MaxUrine
  ,MinWeight
  ,MaxWeight
  --,MinPassiveLegRaise
  --,MaxPassiveLegRaise
  --,MinSepsisRiskScore
  --,MaxSepsisRiskScore
  --,MinMDSuspectsSepsis
  --,MaxMDSuspectsSepsis
  ,CASE 
   when (MinPulse IS NOT NULL ) then 1
   When (MinRespirationRate IS NOT NULL ) then 1
   when (MinTemperatureC IS NOT NULL ) then 1
   else 0
   END AS VitalsRecordedFlag
  from FlowsheetValue;

END

 create index ncix__FlowsheetValue_EncounterCSN on gt1.FlowsheetValue (EncounterCSN)

-- check keys and indexes
exec tools.CheckKeysAndIndexes
 



GO
