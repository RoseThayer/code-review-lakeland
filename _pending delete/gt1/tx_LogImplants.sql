--if exists (select * from information_schema.tables where TABLE_NAME = 'LogImplants' and TABLE_SCHEMA = 'gt1')
--	drop table gt1.LogImplants
if object_id('gt1.LogImplants', 'U') is null drop table gt1.LogImplants;

create table [gt1].[LogImplants]
(
    LogID 										numeric(18,0)    NOT NULL
	, LogImplantArea 						varchar(254)         NULL
	, LogImplantAction 					varchar(254)       	 NULL
	, LogImplantReasonWasted 		varchar(254)         NULL
	, LogImplantNumberUsed 			numeric(6,0)         NULL
	, LogImplantCurrentStatus 	varchar(254)         NULL
	, LogImplantID 							numeric(20,0)        NULL
	, LogImplantExpirationDate 	date 							   NULL
	, LogImplantManufacturer 		varchar(254)         NULL
	, LogImplantName 						varchar(254)         NULL
	, LogImplantType 						varchar(254)         NULL
	, LogImplantChargeable? 		numeric(2,0)         NULL
	, LogImplantModelNumber 		numeric(30,0)        NULL
	, LogImplantSerialNumber 		numeric(30,0)        NULL
	, LogImplantLotNumber 			numeric(30,0)        NULL
	, LogImplantSite 						varchar(254)         NULL
	, LogImplantSize 						varchar(254)         NULL
	, LogImplantLaterality 			varchar(254)         NULL
)

insert into [gt1].[LogImplants]
(
    LogID
	, LogImplantArea
	, LogImplantAction
	, LogImplantReasonWasted
	, LogImplantNumberUsed
	, LogImplantCurrentStatus
	, LogImplantID
	, LogImplantExpirationDate
	, LogImplantManufacturer
	, LogImplantName
	, LogImplantType
	, LogImplantChargeable?
	, LogImplantModelNumber
	, LogImplantSerialNumber
	, LogImplantLotNumber
	, LogImplantSite
	, LogImplantSize
	, LogImplantLaterality
)

select
    olli.LOG_ID as LogID
	, oli.IMPLANT_ID as as LogImplantID
	, isNull(oli.IMPLANT_AREA_C,'*Unspecified') as LogImplantArea
	, isNull(oli.IMPLANT_ACTION_C,'*Unspecified') as LogImplantAction
	,	isNull(oli.IMPLANT_RSN_WSTD_C,'*Unspecified') as LogImplantReasonWasted
	, oli.IMPLANT_NUM_USED as LogImplantNumberUsed
	, isNull(oi.STATUS_C,'*Unspecified') as LogImplantCurrentStatus
	, oi.EXPIRATION_DATE as LogImplantExpirationDate
  , isNull(oi.MANUFACTURER_C,'*Unspecified') as LogImplantManufacturer
	, oi.IMPLANT_NAME as LogImplantName
	, isNull(oi.IMPLANT_TYPE_C,'*Unspecified') as LogImplantType
	, iif(oi.CHARGEABLE_YN = 'Y',1,0) as LogImplantChargeable?
	, oi.MODEL_NUMBER as LogImplantModelNumber
	, oi.SERIAL_NUMBER as LogImplantSerialNumber
	, oi.LOT_NUMBER as LogImplantLotNumber
	, oi.IMPLANT_SIZE as LogImplantSize
	, isNull(oi.IMPLANT_LAT_C,'*Unspecified')
from
  ge.OR_LOG_LN_IMPLANT olli
	  left join ge.OR_LNLG_IMPLANTS oli
		  on olli.IMPLANTS_ID = oli.RECORD_ID
		left join ge.OR_IMP oi
			on oli.IMPLANT_ID = oi.IMPLANT_ID
		left join ge.ZC_IMPLANT_AREA ziarea
			on olli.IMPLANT_AREA_C = ziarea.IMPLANT_AREA_C
		left join ge.ZC_IMPLANT_ACTION ziaction
			on olli.IMPLANT_ACTION_C = ziaction.IMPLANT_ACTION_C
		left join ge.ZC_OR_RSN_WASTE zorw
			on olli.IMPLANT_RSN_WSTD_C = zorw.REASON_WATED_C
		left join ge.ZC_OR_STATUS zos
			on oi.STATUS_C = zos.STATUS_C
		left join ge.ZC_OR_MANUFACTURER zom
			on oi.MANUFACTURER_C = zom.MANUFACTURER_C
		left join ge.ZC_OR_IMPLANT_TYPE zoit
			on oi.IMPLANT_TYPE_C = zoit.IMPLANT_TYPE_C
		left join ge.ZC_OR_LRB zol
			on oi.IMPLANT_LAT_C = zol.LRB_C
;
