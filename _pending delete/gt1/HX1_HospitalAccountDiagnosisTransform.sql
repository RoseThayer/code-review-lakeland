SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [gt1].[HX1_HospitalAccountDiagnosisTransform] as
 
/*
--> Source tables used: <--
   pa.ge.HSP_ACCT_DX_LIST  
   pa.ge.PAT_ENC_HSP  
   pa.ge.PATIENT
   pa.ge.CLARITY_EDG  
   pa.ge.ZC_EDG_CODE_SET  
   pa.ge.ZC_DX_POA  

--> Target tables created <--
	gt1.HospitalAccountDiagnosis
	 
--> Performance Baseline <--
	5047777 rows : 42 s
*/
 
-- declare variables
declare @vSepsisICD10Codes table (id int identity(1,1), REF_BILL_CODE varchar(254)) --RT 12/21 Confirmed Lehigh Values 
insert into @vSepsisICD10Codes   
select VariableValue from variables.GetTableValues('@CurrentICD10_DX_ID')

declare @vSepsisICD9Codes table (id int identity(1,1), REF_BILL_CODE varchar(254)) --RT 12/21 Confirmed Lehigh Values 
insert into @vSepsisICD9Codes  
select VariableValue from variables.GetTableValues('@vSepsisICD9Codes')

-- HospitalAccountDiagnosis
if OBJECT_ID('gt1.HospitalAccountDiagnosis', 'U') is not null drop table gt1.HospitalAccountDiagnosis;
create table [gt1].[HospitalAccountDiagnosis]
(
	[HospitalAccountID] [numeric](18, 0) NOT NULL,
	[DxLine] [int] NOT NULL,
	[DiagnosisID] [numeric](18, 0) NULL,
	--[FinalDXPOAID] [int] NULL,
	[EncounterCSN] [numeric](18, 0) NULL,
	[RefBillCode] [varchar](254) NULL,
	--[RefBillCodeSetID] [int] NULL,
	[DxIsPrimary] [varchar](1) NOT NULL,
	[DxIsSecondary] [varchar](1) NOT NULL,
	[DxCode] [varchar](100) NULL,
	[DxName] [varchar](200) NULL,
	[DxCodeSet] [varchar](254) NULL,
	[DxPresentOnArrival] [varchar](254) NULL,
	[SepsisDxFlag] [int] NOT NULL,
	--[AlteredMentalStatusDxFlag] [int] NOT NULL,
	[SepsisDxName] [varchar](200) NULL, 
	[PatientMRN] numeric(18,0)
)  
insert into [gt1].[HospitalAccountDiagnosis]
(	
	 [HospitalAccountID]
	,[DxLine]
	,[DiagnosisID]
	--,[FinalDXPOAID]
	,[EncounterCSN]
	,[RefBillCode]
	--,[RefBillCodeSetID]
	,[DxIsPrimary]
	,[DxIsSecondary]
	,[DxCode]
	,[DxName]
	,[DxCodeSet]
	,[DxPresentOnArrival]
	,[SepsisDxFlag]
	--,[AlteredMentalStatusDxFlag]
	,[SepsisDxName]
	,[PatientMRN]
)
select  
  had.HSP_ACCOUNT_ID as HospitalAccountID,
  had.LINE as DxLine,
  had.DX_ID as DiagnosisID,
  --had.FINAL_DX_POA_C as FinalDXPOAID,
  peh.PAT_ENC_CSN_ID as EncounterCSN,
  ce.REF_BILL_CODE as RefBillCode, 
  --ce.REF_BILL_CODE_SET_C as RefBillCodeSetID, 
  iif(had.LINE = 1, 'Y' ,'N') as DxIsPrimary, 
  iif(had.LINE > 1, 'Y', 'N') as DxIsSecondary,  
  iif(ce.REF_BILL_CODE_SET_C = 1, '0.00', cast(ce.REF_BILL_CODE as varchar(100))) as DxCode, 
  ce.DX_NAME as DxName, 
  zec.[NAME] as DxCodeSet, 
  zdp.[NAME] as DxPresentOnArrival, 
  case when (REF_BILL_CODE_SET_C = 1 and vi9.id is not null) or 
       (REF_BILL_CODE_SET_C = 2 and vi10.id is not null) then 1
	   else 0 end as SepsisDxFlag,  
  case when (REF_BILL_CODE_SET_C = 1 and vi9.id is not null) or 
       (REF_BILL_CODE_SET_C = 2 and vi10.id is not null) then ce.DX_NAME 
	   end as SepsisDxName, 
  pt.PAT_MRN_ID	        	      
from pa.ge.HSP_ACCT_DX_LIST had
left join pa.ge.PAT_ENC_HSP peh
  on had.HSP_ACCOUNT_ID = peh.HSP_ACCOUNT_ID
left join pa.ge.PATIENT pt
  on peh.PAT_ID = pt.PAT_ID
left join pa.ge.CLARITY_EDG ce
  on had.DX_ID = ce.DX_ID
left join pa.ge.ZC_EDG_CODE_SET zec
  on ce.REF_BILL_CODE_SET_C = zec.EDG_CODE_SET_C
left join pa.ge.ZC_DX_POA zdp
  on had.FINAL_DX_POA_C = zdp.DX_POA_C
left join @vSepsisICD10Codes vi10
  on ce.REF_BILL_CODE = vi10.REF_BILL_CODE
left join @vSepsisICD9Codes vi9
	on ce.REF_BILL_CODE = vi9.REF_BILL_CODE
 
create clustered index [cix__HospitalAccountDiagnosis__HospitalAccount_ID] on [gt1].[HospitalAccountDiagnosis]
(
	[HospitalAccountID] ASC,
	[DxLine] ASC,
	[EncounterCSN] ASC
) 
 
 create index ncix__HospitalAccountDiagnosis__SepsisDxFlag
 on gt1.HospitalAccountDiagnosis (SepsisDxFlag)
 include (EncounterCSN)

 create index ncix__HospitalAccountDiagnosis__SepsisDxFlag_ext
     on gt1.HospitalAccountDiagnosis (SepsisDxFlag)
	 include (DxLine, EncounterCSN, DXisPrimary, DxIsSecondary, DxCode, DxName, DxCodeset, DxPresentOnArrival, SepsisDxName)


 -- check keys and indexes
exec tools.CheckKeysAndIndexes










GO
