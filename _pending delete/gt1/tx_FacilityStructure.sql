--if exists (select * from information_schema.tables where TABLE_NAME = 'DepartmentInfo' and TABLE_SCHEMA = 'gt1')
--drop table gt1.DepartmentInfo

if object_id('gt1.DepartmentInfo', 'U') is not null drop table gt1.DepartmentInfo;

  create table [gt1].[DepartmentInfo]
  (
      DepartmentID           varchar(254)    NOT NULL
    , DepartmentName         varchar(254)          NULL
    , DepartmentAbbreviation varchar(254)          NULL
    , DepartmentSpecialty    varchar(254)          NULL
    , LocationID              varchar(254)        NULL
    , LocationName           varchar(254)          NULL
    , ServiceAreaID          varchar(254)        NULL
    , ServiceAreaName        varchar(254)          NULL
    , HospitalID              varchar(254)        NULL
  )

insert into [gt1].[DepartmentInfo]
(
      DepartmentID
    , DepartmentName
    , DepartmentAbbreviation
    , DepartmentSpecialty
    , LocationID
    , LocationName
    , ServiceAreaID
    , ServiceAreaName
    , HospitalID
)
  select
      dep.DEPARTMENT_ID        as DepartmentID
    , dep.DEPARTMENT_NAME      as DepartmentName
    , dep.DEPT_ABBREVIATION		  as DepartmentAbbreviation
    , dep.SPECIALTY            as DepartmentSpecialty
    , dep.REV_LOC_ID               as LocationID
    , loc.LOC_NAME             as LocationName
    , dep.SERV_AREA_ID         as ServiceAreaID
    , sa.SERV_AREA_NAME        as ServiceAreaName
    , loc.HOSP_PARENT_LOC_ID   as HospitalID
  from
    ge.CLARITY_DEP dep
      left join ge.CLARITY_LOC loc
        on dep.REV_LOC_ID = loc.LOC_ID
      left join ge.CLARITY_SA sa
        on dep.SERV_AREA_ID = sa.SERV_AREA_ID
;

  --Creating OR Facility Structure
if object_id('gt1.ORFacility', 'U') is not null drop table gt1.ORFacility;

  create table [gt1].[ORFacility]
(
      LocationID    varchar(254)  NOT NULL
    , DepartmentID  varchar(254)   NOT NULL
    , LocationName varchar(254)        NULL
    , RoomID        varchar(254)      NULL
    , RoomName     varchar(254)        NULL
)

insert into [gt1].[ORFacility]
(
    LocationID
  , DepartmentID
  , LocationName
  , RoomID
  , RoomName
)
  select
      ol.LOC_ID as LocationID
    , isNull(loc.LOC_NAME,'*Unspecified') as LocationName
    , ol.OR_DEPARTMENT_ID as DepartmentID
    , os.PROV_ID as RoomID
    , cs.PROV_NAME as RoomName
    from
      ge.OR_LOC ol
        left join ge.CLARITY_LOC loc
          on ol.LOC_ID = loc.LOC_ID
        left join ge.OR_SER_SURG_SRVC os
          on ol.LOC_ID = os.LOC_ID
        left join ge.CLARITY_SER cs
          on os.PROV_ID = cs.PROV_ID
;
