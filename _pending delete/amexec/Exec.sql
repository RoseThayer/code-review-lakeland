alter procedure amExec.Model as

declare @UnknownLabel varchar(8) = '*Unknown';
declare @UnspecifiedLabel varchar(12) = '*Unspecified';
declare @NotApplicableLabel varchar(15) = '*Not Applicable';
declare @OtherLabel varchar(6) = '*Other';
declare @SchuylkillLabel varchar(10) = 'Schuylkill';

-- amExec.Census

if object_id('tempdb..##AdmitTypeMap') is not null
  drop table ##AdmitTypeMap
  ;
create table ##AdmitTypeMap (
  AdmitType varchar(80)
, AdmitTypeBudget varchar(80)
    constraint PK__AdmitTypeMap__AdmitType
    primary key clustered ( AdmitType ))
;
insert into ##AdmitTypeMap
values
  ( 'Ambulatory Overnight', 'Ambulatory Overnight' )
, ( 'Acute', 'Inpatient Acute Admits' )
, ( 'Rehab Inpatient', 'REHAB Visits' )
, ( 'TSU Inpatient', 'Inpatient TSU Admits' )
, ( 'Observation', 'Observation Visits' )
;

if object_id('amExec.Census', 'U') is not null
  drop table amExec.Census
  ;

select
  adt.CSN as EncounterCSN
, eb.HospitalAccountId
, eb.HospitalLocation
, eb.Location
, eb.HospitalEntity
, eb.AdmitHospital
, eb.AdmitHospitalEntity
, eb.DischargeHospital
, eb.DischargeHospitalEntity
, isnull(eb.AdmitLocation, @UnknownLabel)                           as AdmitLocation
, isnull(eb.DischargeLocation, @UnknownLabel)                       as DischargeLocation
, adt.EVENT_ID                                                      as EventID
, adt.EFFECTIVE_DATE                                                as EventEffectiveDate
, adt.EVENT_DATE                                                    as EventDate
, adt.OBSERVATION_MINUTES                                           as ObservationMinutes
, adt.NUMBER_OF_BIRTHS                                              as NumberOfBirths
, adt.EVENT_TYPE                                                    as EventType
, adt.EVENT_IS_ADMIT                                                as EventIsAdmit
, adt.EVENT_IS_DISCHARGE                                            as EventIsDischarge
, adt.EVENT_IS_TRANSFER_IN                                          as EventIsTransferIn
, adt.EVENT_IS_TRANSFER_OUT                                         as EventIsTransferOut
, adt.EVENT_PATIENT_CLASS_CHANGE                                    as EventPatientClassChange
, adt.EVENT_IS_UPGRADE                                              as EventIsUpgrade
, adt.EVENT_IS_DOWNGRADE                                            as EventIsDowngrade
, adt.EVENT_IS_CENSUS                                               as EventIsCensus
, adt.PAT_CLASS                                                     as EventPatientClass
, adt.DEPARTMENT_ID                                                 as EventDepartmentID
, adt.DEPARTMENT                                                    as EventDepartment
, adt.CENSUS_BEDS                                                   as CensusBeds
, adt.REVENUE_LOCATION                                              as RevenueLocation
, adt.SERVICE_AREA_ID                                               as EventServiceAreaID
, adt.SERVICE_AREA                                                  as EventServiceArea

, eb.ServiceAreaID
, eb.BedId
, eb.PatientId
, eb.HARPrimaryPayorID                                              as PayorId
, eb.HARPrimaryPayor                                                as Payor
, eb.HARPrimaryPayorAbbr                                            as PayorAbbr
, eb.HARPrimaryPayorFinancialClass                                  as PayorFinancialClass
, eb.HospitalAccountClassID
, eb.HospitalAccountClass
, isnull(eb.AdmitType, @OtherLabel)                                 as AdmitType
, atm.AdmitTypeBudget
, eb.AdmitTime
, eb.AdmitDate
, eb.AdmitSourceId
, eb.AdmitProviderId
, admit_prov.PROV_NAME                                              as AdmitProvider
, eb.FirstAdmitAllowedDepartmentID                                  as AdmitDepartmentId
, admit_dep.DEPARTMENT_NAME                                         as AdmitDepartment
, eb.DischargeTime
, eb.DischargeDate
, eb.DischargeProviderId
, disch_prov.PROV_NAME                                              as DischargeProvider
, eb.LastDepartmentId
, eb.DischargeDepartmentID
, disch_dep.DEPARTMENT_NAME                                         as DischargeDepartment
, eb.DischargeDispositionID
, eb.DischargeDisposition
, eb.DeceasedPatient
, eb.AdmissionConfirmed
, eb.InpatientConfirmedAdmission
, eb.OutpatientConfirmedVisit
, iif(eb.InpatientConfirmedAdmission = 1 
    or eb.OutpatientConfirmedVisit = 1
    , 1
    , 0 
  )                                                                 as ConfirmedEncounter
, eb.PatientClassId
, eb.PatientClass
, eb.IPLOS
, eb.SameDayStay
, p.PatientMRN
, p.PatientSex
, p.PatientEthnicGroup
, c.DRGType
, c.DRGIDTypeID
, c.DRGWeight
, eb.ValidationHospitalLocation
into amExec.Census
--from gt2.EncounterBase as eb
from gt3.LVHEncounterVolume eb
  inner join gt1.ClarityAdt adt
    on eb.EncounterCSN = adt.CSN
  left join gt1.Patient as p
    on p.PatientID = eb.PatientId
  left join ge.CLARITY_SER as admit_prov
    on admit_prov.PROV_ID = eb.AdmitProviderId
  left join ge.CLARITY_SER as disch_prov
    on disch_prov.PROV_ID = eb.DischargeProviderId
  left join ge.CLARITY_DEP as admit_dep
    on admit_dep.DEPARTMENT_ID = eb.FirstAdmitAllowedDepartmentID
  left join ge.CLARITY_DEP as disch_dep
    on disch_dep.DEPARTMENT_ID = eb.DischargeDepartmentID
  left join gt1.CMI as c
    on c.HospitalAccountID = eb.HospitalAccountId
  left join ##AdmitTypeMap as atm
    on atm.AdmitType = eb.AdmitType
;

-- amExec.DrgLos

if object_id('amExec.DrgLos', 'U') is not null
  drop table amExec.DrgLos
  ;

select
  eb.EncounterCSN
, eb.PatientId
, eb.IPLOS
, eb.AdmitDate
, eb.DischargeDate
, eb.HospitalLocation
, eb.HospitalEntity
, c.DRGType
, c.DRGIDTypeID
, c.DRGWeight
into amExec.DrgLos
from gt2.EncounterBase as eb
left join gt1.CMI as c
  on eb.HospitalAccountID = c.HospitalAccountID
left join palvhs.dbo.PAT_ENC_HSP sk
  on eb.EncounterCSN = sk.PAT_ENC_CSN_ID
where eb.IPLOS is not null
or c.DRGWeight is not null
;

-- amExec.HospitalOutpatient

if object_id('amExec.HospitalOutpatient', 'U') is not null
  drop table amExec.HospitalOutpatient
  ;

select
  oe.EncounterCSN
, oe.HospitalLocation
, oe.HospitalEntity
, oe.HospitalAccountID
, oe.PatientId
, oe.HARPrimaryPayorID                                              as PayorID
, oe.HARPrimaryPayor                                                as Payor
, oe.HARPrimaryPayorAbbr                                            as PayorAbbr
, oe.HARPrimaryPayorFinancialClass                                  as PayorFinancialClass
, oe.ProviderID                                                     as ProviderID
, cs.PROV_NAME                                                      as Provider
, oe.HospitalEncounterServiceID
, oe.HospitalAccountClass
, oe.PrimaryHospitalAccountServiceID                                as ServiceId
, oe.PrimaryHospitalAccountService                                  as Service
, oe.AdmitDate                                                      as Date
, oe.VisitDepartmentID                                              as DepartmentID
, oe.VisitType                                                      as VisitType
, cd.DEPARTMENT_NAME                                                as Department
, iif(oe.OutpatientConfirmedVisit = 1 
    and oe.RecurringVisitAccount <> 1
    , 1
    , 0
  )                                                                 as IsOutpatientVisit
, oe.RecurringVisitAccount                                          as IsRecurringVisit
, oe.OutpatientAccount
, oe.RecurringServiceDate
, oe.RecurringTransactionID
, oe.RecurringDepartmentID
, oe.RecurringServiceArea
, p.PatientMRN
, p.PatientSex
, p.PatientEthnicGroup
into amExec.HospitalOutpatient
from gt2.OutpatientEncounter as oe
left join gt1.PATIENT as p
  on p.PatientId = oe.PatientId
left join ge.CLARITY_DEP as cd
  on cd.DEPARTMENT_ID = oe.VisitDepartmentID
left join ge.CLARITY_SER as cs
  on cs.PROV_ID = oe.ProviderID
left join palvhs.dbo.PAT_ENC_HSP as sk
  on oe.EncounterCSN = sk.PAT_ENC_CSN_ID
;

-- amExec.HB

if Object_Id('amExec.HB', 'U') is not null
  drop table amExec.HB
  ;

with HospitalLocation as (
  select
    COST_CNTR_ID as CostCenterID
  , case
      when ccc.RPT_GRP_TWO = 2
        then 'LVH'
      when ccc.RPT_GRP_TWO = 21
        then 'LVHM'
      else @UnknownLabel
    end as Hospital
  from ge.CL_COST_CNTR ccc
)
select
  hb.CSN                                                            as EncounterCSN
, hb.HospitalAccount                                                as HospitalAccountId
, case
    --when hb.DischargeDepartment = @SchuylkillLabel then @SchuylkillLabel
    --when TxTypeID = 2 then IsNull( zdrg.Name, @UnknownLabel )
    --else IsNull( HospitalLocation.Hospital, @UnknownLabel )
    when hb.DischargeDepartment = @SchuylkillLabel 
    then @SchuylkillLabel
    when hb.TxTypeID = 2 
    then 
      case
        when hb.LocationID is null
        then @UnspecifiedLabel
        when zdrg.RPT_GRP_SIX is null
        then @UnknownLabel
        else zdrg.NAME
      end
    when hb.TxTypeID = 1
    then  
      case 
        when hb.CostCenterID is null
        then @UnspecifiedLabel
        else HospitalLocation.Hospital
      end
    else @UnknownLabel    
  end                                                               as HospitalLocation
, case
    --when hb.DischargeDepartment = @SchuylkillLabel then @SchuylkillLabel
    --when TxTypeID = 2 then IsNull( ec.Name, @UnknownLabel )
    --else IsNull( HospitalLocation.Hospital, @UnknownLabel )
    when hb.DischargeDepartment = @SchuylkillLabel 
    then @SchuylkillLabel
    when hb.TxTypeID = 2 
    then 
      case
        when hb.LocationID is null
        then @UnspecifiedLabel
        when ec.RPT_GRP_SIX is null
        then @UnknownLabel
        else ec.NAME
      end
    when hb.TxTypeID = 1
    then  
      case 
        when hb.CostCenterID is null
        then @UnspecifiedLabel
        else HospitalLocation.Hospital
      end
    else @UnknownLabel
  end                                                               as HospitalEntity
, hb.PostDate
, hb.AdmitDepartmentID
, hb.AdmitDepartment
, hb.DischargeDepartmentID
, hb.DischargeDepartment
, hb.ServiceDate
, hb.BillingProviderID
, hb.BillingProvider
, hb.FinancialClassId
, hb.FinancialClass
, hb.CostCenterID
, hb.CostCenter
, hb.ServiceAreaID
, hb.ServiceArea
, hb.HBPaymentAmount                                                as CashPosted
, hb.HBChargeAmount                                                 as Revenue
, hb.Payor
, hb.PayorAbbr
, ae.AdmitDate
, ae.DischargeDate
, p.PatientMRN
, p.PatientSex
, p.PatientEthnicGroup
, hb.TxTypeID
, hb.TxType
into amExec.HB
from gt1.HBTransactions as hb
left join gt2.EncounterBase as ae
  on hb.CSN = ae.EncounterCSN
left join gt1.PATIENT as p
  on p.PatientId = ae.PatientId
left join HospitalLocation
  on hb.CostCenterID = HospitalLocation.CostCenterID
left join ge.CLARITY_LOC cl
  on hb.LocationID = cl.LOC_ID
left join ge.ZC_DEP_RPT_GRP_6 zdrg
  on cl.RPT_GRP_SIX = zdrg.RPT_GRP_SIX
left join variables.EntityCrosswalk ec
  on cl.RPT_GRP_SIX = ec.RPT_GRP_SIX
;

-- amExec.PB

if object_id('amExec.PB', 'U') is not null
  drop table amExec.PB
  ;

select
  pt.PatientID
, pt.EncounterCSN
, pt.HospitalLocationName
, pt.HospitalLocationNameWithID
, pt.HospitalEntity
, pt.HospitalAccountID
, pt.FinancialClass
, pt.CurrentFinancialClass
, pt.OriginalPayorID
, pt.OriginalPayor
, pt.OriginalPayorAbbr
, pt.CurrentPayorID
, pt.CurrentPayor
, pt.CurrentPayorAbbr
, pt.PostDate
, pt.ServiceDate
, pt.BillingProviderID
, pt.BillingProvider
, pt.PBBillArea                                                     as BillArea
, pt.PBBillAreaName                                                 as BillAreaName
, pt.PBBillAreaNameWithID                                           as BillAreaNameWithID
, pt.PaymentAmount                                                  as CashPosted
, pt.ChargeAmount                                                   as Revenue
, pt.DepartmentID
, pt.Department
, p.PatientMRN
, p.PatientSex
, p.PatientEthnicGroup
into amExec.PB
from gt1.PBTransactions as pt
  left join ge.CLARITY_DEP dep
    on dep.DEPARTMENT_ID = pt.DepartmentID
  left join gt1.Patient as p
    on p.PatientID = pt.PatientID
;


-- amExec.Appointment

if object_id('amExec.Appointment', 'U') is not null
  drop table amExec.Appointment
  ;

select
  a.VisitID                                                         as EncounterCSN
, a.HospitalLocationID
, iif(sk.PAT_ENC_CSN_ID is not null
    , @SchuylkillLabel
    , a.HospitalLocation
  )                                                                 as HospitalLocation
, iif(sk.PAT_ENC_CSN_ID is not null
    , @SchuylkillLabel
    , a.HospitalEntity
  )                                                                 as HospitalEntity
, a.VisitDate
, a.VisitMadeDate
, a.VisitDepartmentID
, a.VisitDepartment
, a.VisitType
, a.VisitProviderID
, a.VisitProvider
, a.VisitPayorID
, a.VisitPayor
, a.VisitPayorAbbr
, a.VisitPayorFinancialClass
, a.VisitStatus
, p.PatientMRN
, p.PatientSex
, p.PatientEthnicGroup
, a.ScheduleLag
, case
    when a.ScheduleLag <= 6 
      then '0 - 6 days'
    when a.ScheduleLag <= 14 
      then '7 - 14 days'
    when a.ScheduleLag <= 30 
      then '15 - 30 days'
    when a.ScheduleLag > 30 
      then '> 30 days'
  end                                                               as SchedulLagGroup
into amExec.Appointment
from gt1.Appointments as a
left join gt1.Patient as p
  on a.PatientID = p.PatientID
left join palvhs.dbo.F_SCHED_APPT as sk
  on sk.PAT_ENC_CSN_ID = a.VisitID
;

-- amExec.ProviderAccessibility
if object_id('amExec.ProviderAccessibility', 'U') is not null
  drop table amExec.ProviderAccessibility
  ;

select
  pa.ProviderID
, pa.ProviderName
, pa.ProviderType
, pa.DepartmentID                                                   as VisitDepartmentID
, pa.Department                                                     as VisitDepartment
, pa.SearchDate                                                     as VisitDate
, pa.SlotDate
, pa.SlotTime
, pa.SlotHour
, pa.DaysWait
, pa.SlotLength
, pa.HospitalLocationID
, pa.HospitalLocation
, pa.HospitalEntity
into amExec.ProviderAccessibility
from gt1.ProviderAccessibility pa

go

-- amExec.DepartmentInfo

if object_id('amExec.DepartmentInfo', 'V') is not null
drop view amExec.DepartmentInfo
;

go

create view amExec.DepartmentInfo
as
select
di.DepartmentID
, di.ADTAdmitDepartment
, di.DepartmentName
, di.DepartmentAbbreviation
, di.DepartmentSpecialty
, di.LocationID
, di.LocationName
, di.HospitalID
, di.HospitalName
, di.ServiceAreaID
, di.ServiceAreaName
, di.NumberCensusBeds
from gt1.DepartmentInfo as di
;

go
