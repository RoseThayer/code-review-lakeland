-- ================================================
--
-- ================================================
USE padev
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Prominence Advisors
-- Create date: 5/24/2016
-- Description:	Populates the following tables:
--              gt1.ORAuditLog
--              gt1.ORAuditCase
--              gt1.LogPrimaryProcedures and gt1.LogProcedures
--              gt1.ORLogAllSurgeons
--              gt1.ORCasePrimarySurgeons --will need to be configured before this portion will run
--              gt1.ORAllStaffInfo
--              gt1.ORCharges
--              gt1.ORCaseTrackingAllEvents
--              gt1.ORCaseTracking
-- =============================================
ALTER PROCEDURE gt1.ORBaseData
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

  /*************************Variable Declaration**********************************/
  --the following are used in the ORCaseTracking section - set these to the appropriate values
      declare @vcgOREventPatInRoom varchar(66) = '10';
      declare @vcgOREventPatOutRoom varchar(66) = '110';
      declare @vcgOREventPatInPACU varchar(66) = '120';
      declare @vcgOREventPatOutPACU varchar(66) = '130';
      declare @vcgOREventPatInPreop varchar(66) = '140';
      declare @vcgOREventProcedureStart varchar(66) = '150';
      declare @vcgOREventProcedureFinish varchar(66) = '160';
	    declare @vcgOREventPatPreProcComplete varchar(66) = '170';


  /***********************Create gt1.ORAuditLog*********************************/
  --in case we need to recreate or change the table definition during testing
  --run-time: 26 seconds

  /*
    drop table gt1.ORAuditLog;
    create table gt1.ORAuditLog (
        LOG_ID           varchar(18) not null
      , AUDIT_ACTION     varchar(200) null
      , AUDIT_ACTION_C   int null
      , AUDIT_DATE       date null
      , AUDIT_DATE_TIME  datetime null
      , AUDIT_USER       varchar(100) null
      , AUDIT_COMMENTS   varchar(2000) null
    );
  */
  
  --if the destination table already exists truncate it
    if object_id('padev.gt1.ORAuditLog') is not null
    truncate table padev.gt1.ORAuditLog;

  --Populate gt1.ORAuditLog
  insert into padev.gt1.ORAuditLog
  select 
      orlat.LOG_ID
    , case 
        when orlat.AUDIT_ACTION_C is null
        then '*Unspecified'
        else case 
          when zaud.NAME is null 
          then '*Unknown'
          else zaud.NAME 
        end
      end                                  as AUDIT_ACTION 
    , orlat.AUDIT_ACTION_C
    , cast(orlat.AUDIT_DATE as date)       as AUDIT_DATE
    , orlat.AUDIT_DATE                     as AUDIT_DATE_TIME
    , case 
        when orlat.AUDIT_USER_ID is null
        then '*Unspecified'
        else case 
          when emp.NAME is null 
          then '*Unknown'
          else emp.NAME 
        end
      end                                 as AUDIT_USER     
    , orlat.AUDIT_COMMENTS  
  from 
    padb.dbo.OR_LOG_AUDIT_TRAIL                            as orlat
    left join padb.dbo.ZC_OR_AUDIT_ACTION                  as zaud
      on orlat.AUDIT_ACTION_C = zaud.AUDIT_ACTION_C
    left join padb.dbo.CLARITY_EMP                         as emp
      on orlat.AUDIT_USER_ID = emp.USER_ID
  ;



  /***********************Create gt1.ORAuditCase*********************************/
  --run-time: 12 seconds
  /*
    drop table padev.gt1.ORAuditCase;
    create table padev.gt1.ORAuditCase (
      OR_CASE_ID          varchar(18) not null
    , AUDIT_ACTION        varchar(200) null
    , AUDIT_ACTION_C      int null
    , AUDIT_DATE          date null
    , AUDIT_DATE_TIME     datetime null
    , AUDIT_USER          varchar(100) null
    , AUDIT_COMMENTS      varchar(2000) null
    , AUDIT_UNSCHED_DATE  date null
    , AUDIT_UNSCHED_TIME  time null
    , AUDIT_UNSCHED_OR    varchar(18) null
    , AUDIT_SCHED_TO_TIM  time null
    , AUDIT_SCHED_TO_DAT  date null
    , AUDIT_SCHED_TO_OR   varchar(18) null
    );
  */    
  
  --if the destination table already exists truncate it
    if object_id('padev.gt1.ORAuditCase') is not null
    truncate table padev.gt1.ORAuditCase;

  --Populate gt1.ORAuditCase
  insert into padev.gt1.ORAuditCase
  select 
    ortrl.OR_CASE_ID
    , case 
        when ortrl.AUDIT_ACTION_C is null
        then '*Unspecified'
        else case 
          when zaud.NAME is null 
          then '*Unknown'
          else zaud.NAME 
        end
      end                                    as AUDIT_ACTION 
    , ortrl.AUDIT_ACTION_C
    , cast(ortrl.AUDIT_DATE as date)         as AUDIT_DATE
    , ortrl.AUDIT_DATE                       as AUDIT_DATE_TIME
    , case 
        when ortrl.AUDIT_USER_ID is null
        then '*Unspecified'
        else case 
          when emp.NAME is null 
          then '*Unknown'
          else emp.NAME 
        end
      end                                    as AUDIT_USER   
    , ortrl.AUDIT_COMMENTS
    --data in the date and time fields below appears to be date only or time only
    --wo we'll format them that way to remove extraneous data
    , cast(ortrl.AUDIT_UNSCHED_DATE as date) as AUDIT_UNSCHED_DATE
    , cast(ortrl.AUDIT_UNSCHED_TIME as time) as AUDIT_UNSCHED_TIME
    , ortrl.AUDIT_UNSCHED_OR
    , cast(ortrl.AUDIT_SCHED_TO_TIM as time) as AUDIT_SCHED_TO_TIM
    , cast(ortrl.AUDIT_SCHED_TO_DAT as date) as AUDIT_SCHED_TO_DAT
    , ortrl.AUDIT_SCHED_TO_OR
  from
    padb.dbo.OR_CASE_AUDIT_TRL  as ortrl
    left join padb.dbo.ZC_OR_AUDIT_ACTION                  as zaud
      on ortrl.AUDIT_ACTION_C = zaud.AUDIT_ACTION_C
    left join padb.dbo.CLARITY_EMP                         as emp
      on ortrl.AUDIT_USER_ID = emp.USER_ID

  /**********Create gt1.LogPrimaryProcedures and gt1.LogProcedures******************/
  --run-time: 2 seconds


  /*
    drop table gt1.LogPrimaryProcedures;
    create table gt1.LogPrimaryProcedures (
        LOG_ID            varchar(18) not null
      , PROC_NAME         varchar(2000) null 
    );

    drop table gt1.LogProcedures;
    create table gt1.LogProcedures (
        LOG_ID            varchar(18) not null
      , PROC_NAME         varchar(2000) null 
      , ALL_PROCS_PANEL   int null
      , OR_PROC_ID        varchar(254) null
      , ORDINAL           int null
    );
  */

    --if the destination table already exists truncate it
    if object_id('padev.gt1.LogPrimaryProcedures') is not null
    truncate table padev.gt1.LogPrimaryProcedures;

    if object_id('padev.gt1.LogProcedures') is not null
    truncate table padev.gt1.LogProcedures;

  --Populate gt1.LogPrimaryProcedures 
  /*
    create initial temp table for the or data
    depending on performance we may want to add indexes to the temp tables or 
    try a different solution
  */
  
  select 
      orl.LOG_ID
    , case 
        when orl.OR_PROC_ID is null
        then '*Unspecified'
        else case 
          when orp.PROC_NAME is null 
          then '*Unknown'
          else orp.PROC_NAME 
        end
      end                                    as PROC_NAME
    , orl.ALL_PROCS_PANEL
    , orl.OR_PROC_ID
    , orl.ORDINAL
    , orl.LINE  
  into 
    padev.#ORLogProcedures
  from 
    padb.dbo.OR_LOG_ALL_PROC              as orl
    left join padb.dbo.OR_PROC            as orp
      on orl.OR_PROC_ID = orp.OR_PROC_ID
  ; 

  --create the temp table for the anesthesia data
  select 
      an.AN_LOG_ID
    , an.AN_PROC_NAME
  into 
    padev.#ANProcedures
  from 
    padb.dbo.F_AN_RECORD_SUMMARY as an
  /* adjust for customers section from original script - this column does not exist in our data currently
    inner join padb.dbo.OR_LOG as orl
    on an.AN_LOG_ID = orl.LOG_ID
    and orl.OR_CASE_ID is not null
  */
  where 
    AN_LOG_ID in (select distinct LOG_ID from padev.#ORLogProcedures)
  ;

  with PrimaryProcedures as (
    select 
        AN_LOG_ID
      , AN_PROC_NAME
    from 
      padev.#ANProcedures
  )

  /*
    There can be multiple procedures that are in the 1st panel and have an Ordinal position of 1 
    so we need to take the one of those that is on the first line
  */
  , ORLogPrimaryProcedures as (
    select distinct
        LOG_ID
      , PROC_NAME
      , LINE
      , row_number() over(partition by LOG_ID order by LINE) as ROW_N --so we can limit to the first row per log later
    from 
      padev.#ORLogProcedures
    where LOG_ID not in (select AN_LOG_ID from PrimaryProcedures) 
      and ORDINAL=1
      and ALL_PROCS_PANEL = 1
  )
  
  insert into padev.gt1.LogPrimaryProcedures 
  select 
      LOG_ID
    , PROC_NAME 
  from 
    ORLogPrimaryProcedures
  where 
    ROW_N = 1
  
  union all

  select 
      AN_LOG_ID  as LOG_ID
    , AN_PROC_NAME as PROC_NAME
  from
    PrimaryProcedures
  ;
  


  --Populate gt1.LogProcedures
  insert into padev.gt1.LogProcedures
  select
      LOG_ID
    , PROC_NAME
    , ALL_PROCS_PANEL
    , OR_PROC_ID
    , ORDINAL
    --, LINE  
  from 
    padev.#ORLogProcedures

  union all

  select 
      AN_LOG_ID       as LOG_ID
    , AN_PROC_NAME    as PROC_NAME
    , null            as ALL_PROCS_PANEL
    , null            as OR_PROC_ID
    , null            as ORDINAL
  from 
    padev.#ANProcedures
  ;


  drop table padev.#ANProcedures;
  drop table padev.#ORLogProcedures;

  /***********************Create gt1.ORLogAllSurgeons*************************/
  --in case we need to recreate or change the table definition during testing
  --run-time 1 second
  /*
    drop table padev.gt1.ORLogAllSurgeons;
    create table padev.gt1.ORLogAllSurgeons (
        LOG_ID                            varchar(18) not null
        , STAFF_TYPE                      varchar(300) null
        , SURG_ID                         varchar(18) null
        , STAFF_NAME                      varchar(254) null
        , START_TIME                      datetime null
        , END_TIME                        datetime null
        , TOTAL_LENGTH                    int null
        , STAFF_IS_SURGEON                int null
        , LINE                            int null
        , STAFF_IS_PRIMARY_SURGEON        int null
        , STAFF_IS_PANEL1_PRIMARY_SURGEON int null
        , STAFF_IS_PANEL2_PRIMARY_SURGEON int null
        , STAFF_IS_PANEL3_PRIMARY_SURGEON int null
        , STAFF_IS_PANEL4_PRIMARY_SURGEON int null
        , STAFF_IS_PANEL5_PRIMARY_SURGEON int null
    );
  */
  
  --if the destination table already exists truncate it
  if object_id('padev.gt1.ORLogAllSurgeons') is not null
  truncate table padev.gt1.ORLogAllSurgeons;

  --Populate gt1.ORAuditLog
  insert into padev.gt1.ORLogAllSurgeons
  select 
      orl.LOG_ID
    --add the surgeon type to the STAFF_TYPE when we can find a match, otherwise just list 'Surgeon'
    , 'Surgeon' + 
      case 
        when len(
                case 
                  when orl.ROLE_C is null
                  then ''
                  else case 
                    when zrole.NAME is null 
                    then ''
                    else zrole.NAME 
                  end
                end
                ) > 0 
        then ' - ' + zrole.NAME
        else ''
      end                                 as STAFF_TYPE
    , orl.SURG_ID
    , case 
        when orl.SURG_ID is null
        then '*Unspecified'
        else case 
          when ser.PROV_NAME is null 
          then '*Unknown'
          else ser.PROV_NAME 
        end
      end                                 as STAFF_NAME
    , orl.START_TIME
    , orl.END_TIME
    , orl.TOTAL_LENGTH
    , 1                                   as STAFF_IS_SURGEON
    , orl.LINE                            as LINE
--    , orl.PANEL--FOR TESTING
--    , orl.ROLE_C
    , pp.STAFF_IS_PANEL1_PRIMARY_SURGEON  as STAFF_IS_PRIMARY_SURGEON
    , pp.STAFF_IS_PANEL1_PRIMARY_SURGEON
    , pp.STAFF_IS_PANEL2_PRIMARY_SURGEON
    , pp.STAFF_IS_PANEL3_PRIMARY_SURGEON
    , pp.STAFF_IS_PANEL4_PRIMARY_SURGEON
    , pp.STAFF_IS_PANEL5_PRIMARY_SURGEON
  from  
    padb.dbo.OR_LOG_ALL_SURG        as orl
    left join padb.dbo.ZC_OR_PANEL_ROLE as zrole
      on orl.ROLE_C = zrole.ROLE_C
    left join padb.dbo.CLARITY_SER      as ser
      on orl.SURG_ID = ser.PROV_ID
    /*
      the following query sets a 1 in the appropriate column for the primary surgeons for panels 1-5
      using the row_number() over construct keeps us from having to create five separate queries
      run the subquery alone with the PANEL column uncommented to get a better look at what's happening here
    */
    left join (select
                    LOG_ID
                  , LINE
                  --, PANEL
                  , case when PANEL = 1 then row_number() over(partition by LOG_ID, PANEL order by LINE) else null end as STAFF_IS_PANEL1_PRIMARY_SURGEON
                  , case when PANEL = 2 then row_number() over(partition by LOG_ID, PANEL order by LINE) else null end as STAFF_IS_PANEL2_PRIMARY_SURGEON
                  , case when PANEL = 3 then row_number() over(partition by LOG_ID, PANEL order by LINE) else null end as STAFF_IS_PANEL3_PRIMARY_SURGEON
                  , case when PANEL = 3 then row_number() over(partition by LOG_ID, PANEL order by LINE) else null end as STAFF_IS_PANEL4_PRIMARY_SURGEON
                  , case when PANEL = 3 then row_number() over(partition by LOG_ID, PANEL order by LINE) else null end as STAFF_IS_PANEL5_PRIMARY_SURGEON
                  --, 1 as FLAG
                from 
                  padb.dbo.OR_LOG_ALL_SURG
                where
                  ROLE_C = 1
                  and PANEL between 1 and 5
                  )               as pp
      on orl.LOG_ID = pp.LOG_ID
        and orl.LINE = pp.LINE

  ;
  /***********************Create gt1.ORCasePrimarySurgeons****************************/
  /*
    This section was commented out in the qvs version of OR Base Data with a note of: 
      adjust for customers

      The source table OR_CASE_ALL_SURG does not exist in our current data set so 
      I've approximated what we would need to build this portion but did not test it
  */

  --in case we need to recreate or change the table definition during testing
  /*
    drop table padev.gt1.ORCasePrimarySurgeons;
    create table padev.gt1.ORCasePrimarySurgeons (
      CASE_ID             varchar(18) null --VERIFY
      , STAFF_TYPE        varchar(300) null
      , SURG_ID           varchar(18) null
      , STAFF_NAME        varchar(254) null
      , STAFF_IS_SURGEON  int null
    );
  */
  /*
  --if the destination table already exists truncate it
  if object_id('padev.gt1.ORCasePrimarySurgeons') is not null
  truncate table padev.gt1.ORCasePrimarySurgeons;

  --Populate gt1.ORCasePrimarySurgeons
  insert into padev.gt1.ORCasePrimarySurgeons
  select
      orc.CASE_ID
    --add the surgeon type to the STAFF_TYPE when we can find a match, otherwise just list 'Surgeon'
    , 'Surgeon' + 
      case 
        when len(
                case 
                  when orc.ROLE_C is null
                  then ''
                  else case 
                    when zrole.NAME is null 
                    then ''
                    else zrole.NAME 
                  end
                end
                ) > 0 
        then ' - ' + zrole.NAME
        else ''
      end                               as STAFF_TYPE
    , orc.SURG_ID
    , case 
        when orc.SURG_ID is null
        then '*Unspecified'
        else case 
          when ser.PROV_NAME is null 
          then '*Unknown'
          else ser.PROV_NAME 
        end
      end                               as STAFF_NAME
    , 1                                 as STAFF_IS_SURGEON
  from
    padb.dbo.OR_CASE_ALL_SURG as orc
    left join padb.dbo.ZC_OR_PANEL_ROLE as zrole
      on orc.ROLE_C = zrole.ROLE_C
    left join padb.dbo.CLARITY_SER as ser
      on orc.SURG_ID = ser.PROV_ID
  where
    orc.ROLE = 1
    AND orc.PANEL = 1
    and orc.LINE = 1
  ;

  */

  /***********************Create gt1.ORAllStaffInfo*****************************/
    --in case we need to recreate or change the table definition during testing
    --run-time 7 seconds
  /*
    drop table padev.gt1.ORAllStaffInfo;
    create table padev.gt1.ORAllStaffInfo (
        LOG_ID                varchar(18) not null
      , STAFF_RECORD_ID       varchar(18) null
      , STAFF_TYPE_ID         int null
      , STAFF_TYPE            varchar(254) null
      , STAFF_ID              varchar(18) null
      , STAFF_NAME            varchar(254) null
      , STAFF_START_TIME      datetime null
      , STAFF_END_TIME        datetime null
      , STAFF_TOTAL_TIME      int null
      , STAFF_IS_SURGEON      int null
      , STAFF_IS_ANESTHESIA   int null
    );
  */
  
  --if the destination table already exists truncate it
  if object_id('padev.gt1.ORAllStaffInfo') is not null
  truncate table padev.gt1.ORAllStaffInfo;

  --Populate gt1.ORAllStaffInfo
  
  with ORStaffInfo as (
    select 
      LOG_ID
        /*
          The following column is just a generic row number.
        
          I'm guessing this was used here in the qvs because of the lack of the STAFF_ID 
          column in the source data we have to use.  

          When we move this to a customer's server we'll want to verify things work as they should here
          and we could likely move this out of the with clause as we won't need to use the row_number function
          and longer.
       */
      , row_number() over (order by (SELECT 1))as STAFF_RECORD_ID 
      --, orls.STAFF_ID --adjust for customer
    from 
      padb.dbo.OR_LOG_LN_STAFF
  )
  insert into padev.gt1.ORAllStaffInfo
  select 
      orls.LOG_ID
    , orls.STAFF_RECORD_ID
    , orss.STAFF_TYPE_C                 as STAFF_TYPE_ID --renaming as we'll be combining with values from other tables later
    , case 
        when orss.STAFF_TYPE_C is null
        then '*Unspecified'
        else case 
          when zost.NAME is null 
          then '*Unknown'
          else zost.NAME 
        end
      end                                 as STAFF_TYPE
    , orss.STAFF_MEMBER_ID                as STAFF_ID --renaming as we'll be combining with values from other tables later
    , case 
        when orss.STAFF_MEMBER_ID is null
        then '*Unspecified'
        else case 
          when ser.PROV_NAME is null 
          then '*Unknown'
          else ser.PROV_NAME 
        end
      end                               as STAFF_NAME
    , orst.STAFF_START_TIME
    , orst.STAFF_END_TIME
    , orst.STAFF_TOTAL_TIME
    , null                              as STAFF_IS_SURGEON
    , null                              as STAFF_IS_ANESTHESIA
  from
    ORStaffInfo orls
    left join padb.dbo.OR_LNLG_SURG_STAFF as orss   
      on orls.STAFF_RECORD_ID = orss.RECORD_ID
    left join padb.dbo.ZC_OR_STAFF_TYPE as zost     
      on orss.STAFF_TYPE_C = zost.SURG_STAFF_REQ_C --this mapping load not in source qvs - VERIFY
    left join padb.dbo.CLARITY_SER as ser         
      on orss.STAFF_MEMBER_ID = ser.PROV_ID
    left join padb.dbo.OR_LNLG_SRGSTF_TMS as orst --staff times
      on orls.STAFF_RECORD_ID = orst.RECORD_ID
  
  union all
 
  --add surgeons to staff info
  select distinct
      LOG_ID
    , null              as STAFF_RECORD_ID
    , null              as STAFF_TYPE_ID 
    , STAFF_TYPE
    , SURG_ID           as STAFF_ID
    , STAFF_NAME
    , START_TIME        as STAFF_START_TIME
    , END_TIME          as STAFF_END_TIME
    , TOTAL_LENGTH      as STAFF_TOTAL_TIME
    , STAFF_IS_SURGEON
    , null              as STAFF_IS_ANESTHESIA
  from 
    padev.gt1.ORLogAllSurgeons
  

  union all

  --add AN staff
  select distinct 
      olla.LOG_ID
    , olas.RECORD_ID                      as STAFF_RECORD_ID
    , olas.ANESTH_STAFF_C                 as STAFF_TYPE_ID 
    , case 
        when olas.ANESTH_STAFF_C is null
        then '*Unspecified'
        else case 
          when zcat.NAME is null 
          then '*Unknown'
          else zcat.NAME 
        end
      end                                 as STAFF_TYPE
    , olas.ANESTH_STAFF_ID                as STAFF_ID
    , case 
        when olas.ANESTH_STAFF_ID is null
        then '*Unspecified'
        else case 
          when ser.PROV_NAME is null 
          then '*Unknown'
          else ser.PROV_NAME 
        end
      end                                 as STAFF_NAME
    , olat.ANES_STAFF_ST_TIME             as STAFF_START_TIME
    , olat.ANES_STAFF_END_TIM             as STAFF_END_TIME
    , olat.ANES_STAFF_TOT_TIM             as STAFF_TOTAL_TIME
    , null                                as STAFF_IS_SURGEON
    , 1                                   as STAFF_IS_ANESTHESIA
  from 
    padb.dbo.OR_LNLG_ANES_STAFF as olas
    left join padb.dbo.ZC_OR_ANSTAFF_TYPE zcat
      on olas.ANESTH_STAFF_C = zcat.ANEST_STAFF_REQ_C
    left join padb.dbo.CLARITY_SER as ser         
      on olas.ANESTH_STAFF_ID = ser.PROV_ID
    left join padb.dbo.OR_LNLG_ANESTF_TMS as olat
      on olas.RECORD_ID = olat.RECORD_ID
    inner join padb.dbo.OR_LOG_LN_ANESSTAF as olla
      on olas.RECORD_ID = olla.ANESTHESIA_STAFF_I

  union all

  --add AN resp staff
  select distinct 
      olla.LOG_ID
    , olar.RECORD_ID                      as STAFF_RECORD_ID
    , olar.ANES_STAFF_TYPE_C              as STAFF_TYPE_ID 
    , case 
        when olar.ANES_STAFF_TYPE_C is null
        then '*Unspecified'
        else case 
          when zcat.NAME is null 
          then '*Unknown'
          else zcat.NAME 
        end
      end                                 as STAFF_TYPE
    , olar.ANES_STAFF_ID                  as STAFF_ID
    , case 
        when olar.ANES_STAFF_ID is null
        then '*Unspecified'
        else case 
          when ser.PROV_NAME is null 
          then '*Unknown'
          else ser.PROV_NAME 
        end
      end                                 as STAFF_NAME
    , olat.ANES_STAFF_ST_TIME             as STAFF_START_TIME
    , olat.ANES_STAFF_END_TIM             as STAFF_END_TIME
    , olat.ANES_STAFF_TOT_TIM             as STAFF_TOTAL_TIME
    , null                                as STAFF_IS_SURGEON
    , 1                                   as STAFF_IS_ANESTHESIA
  from 
    padb.dbo.OR_LNLG_ANES_RESP as olar
    left join padb.dbo.ZC_OR_ANSTAFF_TYPE zcat
      on olar.ANES_STAFF_TYPE_C = zcat.ANEST_STAFF_REQ_C
    left join padb.dbo.CLARITY_SER as ser         
      on olar.ANES_STAFF_ID = ser.PROV_ID
    left join padb.dbo.OR_LNLG_ANESTF_TMS as olat
      on olar.RECORD_ID = olat.RECORD_ID
    inner join padb.dbo.OR_LOG_LN_ANESSTAF olla
      on olar.RECORD_ID = olla.ANESTHESIA_STAFF_I
  where
    len(olar.ANES_STAFF_ID) > 0
 ;


  /***********************Create gt1.ORCharges********************************/
  --in case we need to recreate or change the table definition during testing
  --run-time: 9 minutes
/*  
    drop table padev.gt1.ORCharges;
    create table padev.gt1.ORCharges (
      TX_ID                   int not null         
    , CHARGE_COST_CENTER      varchar(254) null
	  , PROCEDURE_DESC          varchar(255) null
	  , PROC_ID                 int null
    , PROC_NAME               varchar(100) null
	  , QUANTITY                float null
    , REVENUE_CODE_NAME       varchar(254) null
    , LOC_NAME                varchar(80)
	  , SERVICE_DATE            datetime null
	  , TX_AMOUNT               float null
	  , TX_POST_DATE            datetime null
    , CHARGE_TX_SOURCE        varchar(254)
	  , SUP_ID                  varchar(18) null
	  , OPTIME_LOG_ID           varchar(18) null
	  , COST                    float null
	  --, IMPLANT_ID --looks like this column is missing from test data
	  , CHARGE_TX_TYPE          varchar(254) null
    , HAR_PAYOR_ID            numeric(18,0) null
    , HAR_PAYOR_NAME          varchar(80) null
    );
 */
  
  --if the destination table already exists truncate it
  if object_id('padev.gt1.ORCharges') is not null
  truncate table padev.gt1.ORCharges;

  --Populate gt1.ORCharges
  insert into padev.gt1.ORCharges
  select 
      hspt.TX_ID 
    , case 
        when hspt.COST_CNTR_ID is null
        then '*Unspecified'
        else case 
          when mccc.COST_CENTER_NAME is null 
          then '*Unknown'
          else mccc.COST_CENTER_NAME 
        end
      end                                 as CHARGE_COST_CENTER
	  , hspt.PROCEDURE_DESC 
	  , hspt.PROC_ID
    , case 
        when hspt.PROC_ID is null
        then '*Unspecified'
        else case 
          when meap.PROC_NAME is null 
          then '*Unknown'
          else meap.PROC_NAME 
        end
      end                                 as PROC_NAME
	  , QUANTITY 
    , case 
        when hspt.UB_REV_CODE_ID is null
        then '*Unspecified'
        else case 
          when mrev.REVENUE_CODE_NAME is null 
          then '*Unknown'
          else mrev.REVENUE_CODE_NAME 
        end
      end                                 as REVENUE_CODE_NAME
    , case 
        when hspt.REVENUE_LOC_ID is null
        then '*Unspecified'
        else case 
          when mloc.LOC_NAME is null 
          then '*Unknown'
          else mloc.LOC_NAME 
        end
      end                                 as LOC_NAME
	  , hspt.SERVICE_DATE
	  , hspt.TX_AMOUNT
	  , hspt.TX_POST_DATE
    , case 
        when hspt.TX_SOURCE_HA_C is null
        then '*Unspecified'
        else case 
          when zhas.NAME is null 
          then '*Unknown'
          else zhas.NAME 
        end
      end                                 as CHARGE_TX_SOURCE
	  , hspt.SUP_ID
	  , hspt.OPTIME_LOG_ID
	  , hspt.COST
	  --, IMPLANT_ID --looks like this column is missing from our data
	  , case 
        when hspt.TX_TYPE_HA_C is null
        then '*Unspecified'
        else case 
          when zhat.NAME is null 
          then '*Unknown'
          else zhat.NAME 
        end
      end                                 as CHARGE_TX_TYPE
    --, HSP_ACCOUNT_ID
	  , case 
        when hspt.HSP_ACCOUNT_ID is null
        then NULL --'*Unspecified'
        else case 
          when hspa.PRIMARY_PAYOR_ID is null 
          then NULL --'*Unknown'
          else hspa.PRIMARY_PAYOR_ID 
        end
      end                                 as HAR_PAYOR_ID
    , case
        when hspa.PRIMARY_PAYOR_ID is null
        then '*Unspecified'
        else case
          when mepm.PAYOR_NAME is null
          then '*Unknown'
          else mepm.PAYOR_NAME
        end
       end                                  as HAR_PAYOR_NAME
  from padb.dbo.HSP_TRANSACTIONS                    as hspt
    inner join padb.dbo.OR_LOG                      as olog --only want transactions where a LOG_ID exists on OR_LOG
      on hspt.OPTIME_LOG_ID = olog.LOG_ID
    left join padb.dbo.CL_COST_CNTR                          as mccc
      on hspt.COST_CNTR_ID = mccc.COST_CNTR_ID
    left join padb.dbo.CLARITY_EAP                           as meap
      on hspt.PROC_ID = meap.PROC_ID
    left join padb.dbo.CL_UB_REV_CODE                        as mrev
      on hspt.UB_REV_CODE_ID = mrev.UB_REV_CODE_ID
    left join padb.dbo.CLARITY_LOC                           as mloc
      on hspt.REVENUE_LOC_ID = mloc.LOC_ID
    left join padb.dbo.ZC_TX_SOURCE_HA                       as zhas
      on hspt.TX_SOURCE_HA_C = zhas.TX_SOURCE_HA_C
    left join padb.dbo.ZC_TX_TYPE_HA                         as zhat
      on hspt.TX_TYPE_HA_C = zhat.TX_TYPE_HA_C
    left join padb.dbo.HSP_ACCOUNT                           as hspa
      on hspt.HSP_ACCOUNT_ID = hspa.HSP_ACCOUNT_ID
    left join padb.dbo.CLARITY_EPM                           as mepm
      on hspa.PRIMARY_PAYOR_ID = mepm.PAYOR_ID



  /***********************Create gt1.ORCaseTrackingAllEvents**********************/
    --in case we need to recreate or change the table definition during testing
    --run-time 3 seconds
  /*
    drop table padev.gt1.ORCaseTrackingAllEvents;
    create table padev.gt1.ORCaseTrackingAllEvents (
        LOG_ID varchar(18) not null
      , TRACKING_EVENT_C varchar(66) null
      , CASE_TRACKING_EVENT_TIME datetime null
    );
  */
  
  --if the destination table already exists truncate it
  if object_id('padev.gt1.ORCaseTrackingAllEvents') is not null
  truncate table padev.gt1.ORCaseTrackingAllEvents;

  --Populate gt1.ORCaseTrackingAllEvents
  insert into padev.gt1.ORCaseTrackingAllEvents
  select 
      LOG_ID
    , TRACKING_EVENT_C
    /*
      we do not have the necessary table in our db to create the following field
	  , case 
        when olct.TRACKING_EVENT_C is null
        then '*Unspecified'
        else case 
          when zcpe.NAME is null 
          then '*Unknown'
          else zcpe.NAME 
        end
      end                                 as CASE_TRACKING_EVENT
      */
    , min(TRACKING_TIME_IN)       as CASE_TRACKING_EVENT_TIME
  from 
    padb.dbo.OR_LOG_CASE_TIMES olct
    /* 
      we do not have the necessary table in our db for the following join 
    left join padb.dbo.ZC_OR_PAT_EVENTS zcpe
      on olcd.TRACKING_EVENT_C = zcpe.TRACKING_EVENT_C
    */
  where
    TRACKING_TIME_IN > 0
  group by 
      LOG_ID
    , TRACKING_EVENT_C 
    --, CASE_TRACKING_EVENT
  ;

  /***********************Create gt1.ORCaseTracking*******************************/
  --in case we need to recreate or change the table definition during testing
  --run-time 2 seconds
  /*
    drop table padev.gt1.ORCaseTracking;
    create table padev.gt1.ORCaseTracking (
        LOG_ID                                varchar(18) not null
        , CASE_TRACKING_PAT_IN_PREOP_TIME     datetime null
        , CASE_TRACKING_PRE_PROC_COMP_TIME    datetime null
        , CASE_TRACKING_PAT_IN_ROOM_TIME      datetime null
        , CASE_TRACKING_PAT_OUT_ROOM_TIME     datetime null
        , CASE_TRACKING_PAT_IN_RECOVERY_TIME  datetime null
        , CASE_TRACKING_PAT_OUT_RECOVERY_TIME datetime null
        , CASE_TRACKING_PROC_START_TIME       datetime null
        , CASE_TRACKING_PROC_FINISH_TIME      datetime null
    );
  */
  

  --if the destination table already exists truncate it
  if object_id('padev.gt1.ORCaseTracking') is not null
  truncate table padev.gt1.ORCaseTracking;



  --Populate gt1.ORCaseTracking
  
  with tCaseTrack as (
    select 
        LOG_ID
      , TRACKING_EVENT_C
      , min(TRACKING_TIME_IN) as CASE_TRACKING_TIME
    from 
      padb.dbo.OR_LOG_CASE_TIMES olct
    where
      --the qvs is using Match() here which returns a number value...I'm thinking IN should be sufficient
      TRACKING_EVENT_C in(@vcgOREventPatInRoom,@vcgOREventPatOutRoom,@vcgOREventPatInPACU,@vcgOREventPatOutPACU,
	      @vcgOREventPatInPreop,@vcgOREventProcedureStart,@vcgOREventProcedureFinish,@vcgOREventPatPreProcComplete)
    group by 
        LOG_ID
      , TRACKING_EVENT_C 
    )
  insert into padev.gt1.ORCaseTracking
  select 
      LOG_ID
    --these could be stretched out to separate lines but it's easy to understand what's happening here when all on one line
    , min(case when TRACKING_EVENT_C = @vcgOREventPatInPreop then CASE_TRACKING_TIME end)         as CASE_TRACKING_PAT_IN_PREOP_TIME
    , min(case when TRACKING_EVENT_C = @vcgOREventPatPreProcComplete then CASE_TRACKING_TIME end) as CASE_TRACKING_PRE_PROC_COMP_TIME
    , min(case when TRACKING_EVENT_C = @vcgOREventPatInRoom then CASE_TRACKING_TIME end)          as CASE_TRACKING_PAT_IN_ROOM_TIME
    , min(case when TRACKING_EVENT_C = @vcgOREventPatOutRoom then CASE_TRACKING_TIME end)         as CASE_TRACKING_PAT_OUT_ROOM_TIME
    , min(case when TRACKING_EVENT_C = @vcgOREventPatInPACU then CASE_TRACKING_TIME end)          as CASE_TRACKING_PAT_IN_RECOVERY_TIME
    , min(case when TRACKING_EVENT_C = @vcgOREventPatOutPACU then CASE_TRACKING_TIME end)         as CASE_TRACKING_PAT_OUT_RECOVERY_TIME
    , min(case when TRACKING_EVENT_C = @vcgOREventProcedureStart then CASE_TRACKING_TIME end)     as CASE_TRACKING_PROC_START_TIME
    , min(case when TRACKING_EVENT_C = @vcgOREventProcedureFinish then CASE_TRACKING_TIME end)    as CASE_TRACKING_PROC_FINISH_TIME
    from 
      tCaseTrack
    group by 
      LOG_ID
  ;


  --template
  
  /***********************Create gt1.NEWTABLENAME******************************/
  --in case we need to recreate or change the table definition during testing
  /*
    drop table padev.gt1.NEWTABLENAME;
    create table padev.gt1.NEWTABLENAME (
        
    );
  */
  
  --if the destination table already exists truncate it
  --if object_id('padev.gt1.NEWTABLENAME') is not null
  --truncate table padev.gt1.NEWTABLENAME;

  --Populate gt1.NEWTABLENAME

END
GO