SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

 
CREATE procedure [gt2].[SF1_SepsisFlagTransform] 
as

/*  --> Source Tables <--
		pa.ge.CLARITY_DEP
		gt2.HospitalEncounter
		gt1.EDEvent
		gt1.HospitalAccountDiagnosis
		gt1.FlowsheetValue
		gt1.MedicationOrder
		gt1.OrderResult
		gt1.OrderMed
		gt1.OrderProcedure
		gt1.PatientEncounterCurrentMed
		gt1.ADTHistory 
		gt1.MedicationOrder
		gt1.MARGiven
		gt2.SIRSFlag
		gt2.SepsisFlag
		gt2.SevereSepsis
		gt2.SepticShock
		gt2.SepsisCombinedFlag
		gt2.IVAdminMedication
		gt2.SepsisADT
		gt2.AbxOrderedMedication 
		gt2.AbxAdminMedication
		gt2.IVAdminMedication
		gt2.BloodCxOrder
		gt2.VasopressorAdminMedication

	--> Target Table <--		
		gt2.SepsisADT
		gt2.SirsFlag
		gt2.SepsisFlag
		gt2.SepsisFlagTime
		gt2.SevereSepsis
		gt2.SepticShock
		gt2.SepsisCombinedFlag
		gt2.AbxOrderedMedication
		gt2.AbxAdminMedication
		gt2.BloodCxOrder
		gt2.IVAdminMedication
		gt2.VasopressorAdminMedication

	--> Performance benchmark <--
		total time ~3:33 
*/	


declare @ConfigSepsisStart DateTime = '2016-01-01'; -- This controls the main date filter for app
declare @ConfigSepsisEnd DateTime = GetDate();
 
print('-----------gt2.tHospitalEncounter--------------')
if OBJECT_ID('gt2.tHospitalEncounter', 'U') is not null drop table gt2.tHospitalEncounter;

 select distinct
  he.Pat_Enc_Csn_Id as EncounterCSN
, he.HospitalAdmissionDate
, he.HospitalAdmissionTime
, he.HospitalDischargeTime
, he.ADTPatientStatusID
, he.DEPARTMENT_ID as DepartmentID 
, Min( he.DEPARTMENT_ID ) over ( partition by he.PAT_ENC_CSN_ID ) as FirstEDDepartmentID
, Max( ed.EDArrival ) over ( partition by he.PAT_ENC_CSN_ID ) as EdArrival
, had.SepsisDxFlag
into gt2.tHospitalEncounter
from gt2.HospitalEncounter as he
 left join gt1.EDEvent as ed
   on ed.EncounterCSN = he.PAT_ENC_CSN_ID
 left join gt1.HospitalAccountDiagnosis as had
   on had.EncounterCSN = he.PAT_ENC_CSN_ID
   and had.SepsisDxFlag = 1 
where
 he.HospitalAdmissionTime >=  @ConfigSepsisStart
 and he.HospitalAdmissionTime <  @ConfigSepsisEnd

-- For performance may want to explore creating a primary key verses an index
create index ncix__tHospitalEncounter__EncounterCSN on gt2.tHospitalEncounter (EncounterCSN);

print('-----------gt2.tFlowsheetValue--------------')
if OBJECT_ID('gt2.tFlowsheetValue', 'U') is not null drop table gt2.tFlowsheetValue;

select
  fv.EncounterCSN
, fv.FlowsheetRecordedTime
, fv.MinTemperatureC
, fv.MaxTemperatureC
, fv.MaxPulse
, fv.MaxRespirationRate
, fv.MinSystolicBloodPressure
, fv.MaxSystolicBloodPressure
, fv.MinUrine
, fv.MinWeight
--, fv.MaxFIO2
, fv.MinMAP
, fv.MinCVP
, fv.MinScvO2
--, fv.MinPassiveLegRaise
, fv.VitalsRecordedFlag
into gt2.tFlowsheetValue
from gt1.FlowsheetValue as fv
  inner join gt2.tHospitalEncounter he
    on fv.EncounterCSN = he.EncounterCSN
;

create clustered index cix__EncounterCSN on gt2.tFlowsheetValue (EncounterCSN);  --EY:  Changed this to a clustered index which removed an RID Lookup & halved the logical reads
create index ncix__tFlowsheetValue__FlowsheetRecordedTime on gt2.tFlowsheetValue (FlowsheetRecordedTime);
create index ncix__tFlowsheetValue__VitalsRecordedFlag on gt2.tFlowsheetValue (VitalsRecordedFlag) include (EncounterCSN, FlowsheetRecordedTime) --EY: This index changed a high-percentage table scan to an 6% index seek, easy win.
create index ncix__tFlowsheetValue__MinSystolicBloodPressure on gt2.tFlowsheetValue (MinSystolicBloodPressure) include (EncounterCSN, FlowsheetRecordedTime, MaxSystolicBloodPressure)  --EY: This one's recommended in the execution plan, need to test & see if it really helps

print('-----------gt2.tOrderResult--------------')
if OBJECT_ID('gt2.tOrderResult', 'U') is not null drop table gt2.tOrderResult;

select
  ores.EncounterCSN
, ResultTime
, OrderTime
, SpecimenTakenTime
, MinWBC
, MaxWBC
, MaxBands
, MaxCreatinine
, MaxBilirubin
, MinPlateletCount
, MaxLactate
, MaxPTINR
, MaxPTT
--, MaxPO2Arterial
, SepsisOrdersetUsedFlag
, OrdersetName
into gt2.tOrderResult
from gt1.OrderResult as ores 
  inner join gt2.tHospitalEncounter he
    on ores.EncounterCSN = he.EncounterCSN
;

create index ncix__tOrderResult__EncounterCSN on gt2.tOrderResult (EncounterCSN);
create index ncix__tOrderResult__ResultTime on gt2.tOrderResult (ResultTime);
create index ncix__tOrderResult__SpecimenTakenTime on gt2.tOrderResult (SpecimenTakenTime); 

-- These are the simple SIRS flags we can pull directly from Flowsheet data
print('-----------gt2.SirsFlag--------------')

if OBJECT_ID('gt2.SirsFlag', 'U') is not null drop table gt2.SirsFlag;
--Potential optimization: Consider converting float datatype to tinyint.
declare @HypothermiaMinTemp float = (select variables.GetSingleValue('@HypothermiaMinTemp'));
declare @HyperthermiaMaxTemp float = (select variables.GetSingleValue('@HyperthermiaMaxTemp'));
declare @TachycardiaMaxPulse float = (select variables.GetSingleValue('@TachycardiaMaxPulse'));
declare @TachypneaMaxRespRate float = (select variables.GetSingleValue('@TachypneaMaxRespRate'));
declare @HypotensionMinSBP float = (select variables.GetSingleValue('@HypotensionMinSBP'));
declare @WBCRecordedWithinVitalsHours float = (select variables.GetSingleValue('@WBCRecordedWithinVitalsHours'));
declare @LeukocytosisMaxWBC float = (select variables.GetSingleValue('@LeukocytosisMaxWBC'));
declare @LeukopeniaMinWBC float = (select variables.GetSingleValue('@LeukopeniaMinWBC'));
declare @ElevatedBandsMaxBands float = (select variables.GetSingleValue('@ElevatedBandsMaxBands'));
declare @MinimumSIRSCriteria float = (select variables.GetSingleValue('@MinimumSIRSCriteria'));

with FlowsheetFlag as
(
select
  EncounterCSN
, FlowsheetRecordedTime as RecordedTime
, case when MinTemperatureC < @HypothermiaMinTemp then 1 else null end as HypothermiaFlag 
, case when MaxTemperatureC > @HyperthermiaMaxTemp then 1 else null end as HyperthermiaFlag 
, case when MaxPulse > @TachycardiaMaxPulse then 1 else null end as TachycardiaFlag 
, case when MaxRespirationRate > @TachypneaMaxRespRate then 1 else null end as TachypneaFlag 
, case when MinSystolicBloodPressure < @HypotensionMinSBP and MinSystolicBloodPressure > 0 then 1 else null end as HypotensionFlag 
from gt2.tFlowsheetValue   
) 

, WBCIntervalAll as
(
select distinct
  vitals.EncounterCSN
, vitals.FlowsheetRecordedTime as RecordedTime
, wbc.ResultTime
, wbc.MinWBC
, wbc.MaxWBC
, Abs( DateDiff( second, vitals.FlowsheetRecordedTime, wbc.ResultTime ) ) as TimeDiff
from gt2.tFlowsheetValue vitals
  inner join gt2.tOrderResult wbc
    on vitals.EncounterCSN = wbc.EncounterCSN
    and vitals.VitalsRecordedFlag = 1
    and wbc.MinWBC is not null
    and Abs( DateDiff( second, wbc.ResultTime, vitals.FlowsheetRecordedTime ) / 3600.0 ) <= @WBCRecordedWithinVitalsHours
)

, MinTimeDiff as 
( 
select distinct  
  EncounterCSN
, RecordedTime
, min( TimeDiff ) as MinTimeDiff
from WBCIntervalAll
group by EncounterCSN, RecordedTime
) 
, WBCInterval as 
( 
select distinct  
  wia.EncounterCSN, 
  wia.RecordedTime, 
  wia.TimeDiff, 
  wia.ResultTime as WBCResultTime, 
  wia.MinWBC, 
  wia.MaxWBC   
from WBCIntervalAll wia
inner join MinTimeDiff mtd
   on wia.EncounterCSN = mtd.EncounterCSN
  and wia.RecordedTime = mtd.RecordedTime
  and wia.TimeDiff = mtd.MinTimeDiff
)   


, BandsFlag as
(
select
  wi.EncounterCSN
, wi.RecordedTime
, bands.ResultTime as BandsResultTime
from WBCInterval as wi
  inner join gt2.tOrderResult as bands
    on bands.EncounterCSN = wi.EncounterCSN
    and bands.MaxBands is not null
    and bands.ResultTime = wi.WBCResultTime
where wi.MaxWBC < @LeukocytosisMaxWBC and wi.MinWBC > @LeukopeniaMinWBC
  and MaxBands > @ElevatedBandsMaxBands
) 

, SirsFlagAll as (
select
  coalesce( fsf.EncounterCSN, wi.EncounterCSN, bf.EncounterCSN ) as EncounterCSN
, coalesce( fsf.RecordedTime, wi.RecordedTime, bf.RecordedTime ) as RecordedTime
, coalesce (WBCResultTime, BandsResultTime) as ResultTime -- EY: modifications per 'Phase 2: Update Time of SIRS flag' card
, IsNull( fsf.HypothermiaFlag, 0 ) as HypothermiaFlag
, IsNull( fsf.HyperthermiaFlag, 0 ) as HyperthermiaFlag
, IsNull( fsf.TachycardiaFlag, 0 ) as TachycardiaFlag
, IsNull( fsf.TachypneaFlag, 0 ) as TachypneaFlag
, IsNull( fsf.HypotensionFlag, 0 ) as HypotensionFlag
, Iif( wi.MaxWBC > @LeukocytosisMaxWBC, 1, 0 ) as LeukocytosisFlag 
, Iif( wi.MinWBC < @LeukopeniaMinWBC, 1, 0 ) as LeukopeniaFlag
, Iif( bf.RecordedTime is not null, 1, 0 ) as ElevatedBandsFlag
from FlowsheetFlag fsf
  full outer join WBCInterval wi
    on fsf.EncounterCSN = wi.EncounterCSN
    and fsf.RecordedTime = wi.RecordedTime
  full outer join BandsFlag as bf
    on fsf.EncounterCSN = bf.EncounterCSN
    and fsf.RecordedTime = bf.RecordedTime
where coalesce( HypothermiaFlag, HyperthermiaFlag, TachycardiaFlag, TachypneaFlag, HypotensionFlag ) is not null
   or MaxWBC > @LeukocytosisMaxWBC 
   or MinWBC < @LeukopeniaMinWBC 
   or bf.RecordedTime is not null
)  

select
  EncounterCSN
, case when RecordedTime < ResultTime and (HypothermiaFlag + HyperthermiaFlag + TachycardiaFlag + TachypneaFlag) >= 2 then RecordedTime
       when RecordedTime >= ResultTime then RecordedTime 
	   when ResultTime > RecordedTime then ResultTime
	   when ResultTime is null then RecordedTime
   end as RecordedTime  -- EY: modifications per 'Phase 2: Update Time of SIRS flag' card
, RecordedTime as FlowsheetRecordedTime
, HypothermiaFlag
, HyperthermiaFlag
, TachycardiaFlag
, TachypneaFlag
, HypotensionFlag
, LeukocytosisFlag
, LeukopeniaFlag
, ElevatedBandsFlag
, Iif( HypothermiaFlag
     + HyperthermiaFlag
     + TachycardiaFlag
     + TachypneaFlag
     -- + HypotensionFlag -- Removed per Lehigh SIRS specification
     + LeukocytosisFlag
     + LeukopeniaFlag
     + ElevatedBandsFlag >= @MinimumSIRSCriteria, 1, 0 ) as MinSirsCriteriaFlag
into gt2.SirsFlag
from  SirsFlagAll
;

create index ncix__SirsFlag__EncounterCSN on gt2.SirsFlag (EncounterCSN);
create index ncix__SirsFlag__RecordedTime on gt2.SirsFlag (RecordedTime);
create index ncix__SirsFlag__HypotensionFlag on gt2.SirsFlag (HypotensionFlag) include (EncounterCSN, REcordedTime)
create index ncix__SirsFlag__MinSirsCriteriaFlag on gt2.SirsFlag (MinSirsCriteriaFlag) include (EncounterCSN, REcordedTime)

print('-----------gt2.SepsisFlag--------------')

declare @SepsisFlagAbxOrderHoursBeforeSIRS int = (select variables.GetSingleValue('@SepsisFlagAbxOrderHoursBeforeSIRS')); --24
declare @SepsisFlagAbxOrderHoursAfterSIRS int = (select variables.GetSingleValue('@SepsisFlagAbxOrderHoursAfterSIRS')); --6

if OBJECT_ID('gt2.SepsisFlag', 'U') is not null drop table gt2.SepsisFlag;

select distinct
  sff.EncounterCSN
, sff.RecordedTime as SIRSRecordedTime
, count(1) as AbxCount
, min( abx.MedicationOrderTime ) as RecordedTime
, max( mo.SepsisOrdersetUsedFlag ) as AbxOrdersetUsedFlag
, max( mo.OrdersetName ) as AbxOrdersetName
into gt2.SepsisFlag
from gt2.SirsFlag as sff
  inner join gt1.OrderMed as abx
    on sff.EncounterCSN = abx.PAT_ENC_CSN_ID
    and abx.AbxFlag = 1
    and DateDiff( second, sff.RecordedTime, abx.MedicationOrderTime ) / 3600.0 between -@SepsisFlagAbxOrderHoursBeforeSIRS and @SepsisFlagAbxOrderHoursAfterSIRS
    and sff.MinSirsCriteriaFlag = 1
  left join gt1.MedicationOrder as mo
    on abx.ORDER_MED_ID = mo.ORDER_MED_ID
group by sff.EncounterCSN, sff.RecordedTime

create index ncix__SepsisFlag__EncounterCSN on gt2.SepsisFlag (EncounterCSN);
create index ncix__SepsisFlag__RecordedTime on gt2.SepsisFlag (RecordedTime);

-------------------------------------------------------------------------------
/* SB: 2017-01-06 - Temporarily removed by Lehigh request.
print('-----------gt2.UrineValue--------------')

 if OBJECT_ID('gt2.UrineValue', 'U') is not null drop table gt2.UrineValue;

with UrineValue as
(
select
  sf.EncounterCSN
, fv.FlowsheetRecordedTime as UrineRecordedTime
, DateAdd( hour, @HourstoCombineUrineRecordings, fv.FlowsheetRecordedTime ) as EndTime
, fv.MinUrine
from gt2.tFlowsheetValue fv
  inner join (select distinct EncounterCSN from gt2.SepsisFlag ) as sf
    on sf.EncounterCSN = fv.EncounterCSN
where MinUrine is not null
)
, UrineFilter as
(
select
  ui1.EncounterCSN
, ui1.UrineRecordedTime
from UrineValue as ui1
  inner join UrineValue as ui2
    on ui1.EncounterCSN = ui2.EncounterCSN
    and ui1.UrineRecordedTime between ui2.UrineRecordedTime and ui2.EndTime
group by ui1.EncounterCSN, ui1.UrineRecordedTime
having count(1) > 1
)
  ui1.EncounterCSN
, ui1.UrineRecordedTime as UrineInterval
, ui2.UrineRecordedTime
, ui2.MinUrine
, case when DateDiff( second
                    , Lag( ui2.UrineRecordedTime, 1, ui2.UrineRecordedTime )
                        over ( partition by ui1.EncounterCSN, ui1.UrineRecordedTime order by ui2.UrineRecordedTime asc )
                    , ui2.UrineRecordedTime ) <= 3600
                    then 1 else 0 end as UrineRecordedHourlyFlag
into gt2.UrineValue
from UrineValue as ui1
  inner join UrineFilter as uf
    on ui1.EncounterCSN = uf.EncounterCSN
  and ui1.UrineRecordedTime = uf.UrineRecordedTime
  inner join UrineValue as ui2
    on ui1.EncounterCSN = ui2.EncounterCSN
    and ui1.UrineRecordedTime between ui2.UrineRecordedTime and ui2.EndTime
order by ui1.EncounterCSN, ui1.UrineRecordedTime, ui2.UrineRecordedTime
;
--CB:  Consider primary key for EncounterCSN
create index idx_EncounterCSN on gt2.UrineValue (EncounterCSN);
create index idx_UrineInterval on gt2.UrineValue (UrineInterval);

go
--------------------------------------------------------------------------------
-- We'll keep Urine Values around so we can use to check four hourly urine output recorded later on
print('-----------gt2.UrineOutput--------------')
if OBJECT_ID('gt2.UrineOutput', 'U') is not null drop table gt2.UrineOutput;
-- Get the most recent weight for each urinte output
with WeightValue as
(
select distinct
  uv.EncounterCSN
, fv.FlowsheetRecordedTime as WeightRecordedTime
, fv.MinWeight
from gt2.UrineValue as uv
  inner join gt2.tFlowsheetValue fv
    on uv.EncounterCSN = fv.EncounterCSN
where
  fv.MinWeight is not null and fv.MinWeight > 0
)

, UrineOutputRaw as
(
select
  uv.EncounterCSN
, uv.UrineInterval as UrineIntervalFirstRecordedTime
, sum(uv.MinUrine) as UrineOutput
, DateDiff( second, min(uv.UrineRecordedTime), max(uv.UrineRecordedTime) ) / 3600.0 as UrineOutputIntervalHours
, min(uv.UrineRecordedHourlyFlag) as UrineRecordedHourlyFlag
, min( he.HospitalAdmissionDate ) as Start
from gt2.UrineValue as uv
  inner join gt2.tHospitalEncounter as he
    on uv.EncounterCSN = he.EncounterCSN
group by uv.EncounterCSN, uv.UrineInterval
)
, UrineOutput as
(
select
  uo.EncounterCSN
, uo.UrineIntervalFirstRecordedTime
, uo.UrineOutput
, uo.UrineOutputIntervalHours
, uo.UrineRecordedHourlyFlag
, first_value( WeightRecordedTime ) over ( partition by uo.EncounterCSN, uo.UrineIntervalFirstRecordedTime order by wv.WeightRecordedTime desc ) as WeightRecordedTime
, first_value( MinWeight ) over ( partition by uo.EncounterCSN, uo.UrineIntervalFirstRecordedTime order by wv.WeightRecordedTime desc ) as MinWeight
from UrineOutputRaw as uo
  inner join WeightValue as wv
    on wv.EncounterCSN = uo.EncounterCSN
    and wv.WeightRecordedTime between Start and UrineIntervalFirstRecordedTime
)


select distinct
  EncounterCSN
, UrineIntervalFirstRecordedTime

, UrineOutputIntervalHours
, UrineRecordedHourlyFlag
, WeightRecordedTime
, MinWeight
, UrineOutput / MinWeight / UrineOutputIntervalHours as UrineOutputPerHour
into gt2.UrineOutput
from UrineOutput
;

create index idx_EncounterCSN on gt2.UrineOutput (EncounterCSN);
create index idx_UrineIntervalFirstRecordedTime on gt2.UrineOutput (UrineIntervalFirstRecordedTime);

*/
--------------------------------------------------------------------------------
print('-----------gt2.SevereSepsis--------------') 
 
declare @SevereSepsisMinUrineOutput float = (select variables.GetSingleValue('@SevereSepsisMinUrineOutput'));
declare @SevereSepsisMaxCreatinine float = (select variables.GetSingleValue('@SevereSepsisMaxCreatinine'));
declare @MinimumSevereSepsisCriteria float = (select variables.GetSingleValue('@MinimumSevereSepsisCriteria'));
declare @SevereSepsisMaxBilirubin float = (select variables.GetSingleValue('@SevereSepsisMaxBilirubin'));
declare @SevereSepsisMinPlateletCount float = (select variables.GetSingleValue('@SevereSepsisMinPlateletCount'));
declare @SevereSepsisMaxLactate float = (select variables.GetSingleValue('@SevereSepsisMaxLactate'));
declare @SevereSepsisMaxINR float = (select variables.GetSingleValue('@SevereSepsisMaxINR'));
declare @SevereSepsisMaxPTT float = (select variables.GetSingleValue('@SevereSepsisMaxPTT'));
declare @SepticShockMinSBPDrop float = (select variables.GetSingleValue('@SepticShockMinSBPDrop'));
declare @SepticShockMinMAP float = (select variables.GetSingleValue('@SepticShockMinMAP'));
declare @HoursAfterSepsisFlagCheckSS float = (select variables.GetSingleValue('@HoursAfterSepsisFlagCheckSS'));

if OBJECT_ID('gt2.SevereSepsis', 'U') is not null drop table gt2.SevereSepsis;

with BP as
(
select
  fv.EncounterCSN
, fv.FlowsheetRecordedTime as RecordedTime
, fv.MinSystolicBloodPressure
, fv.MaxSystolicBloodPressure
, case when Lag( fv.MaxSystolicBloodPressure, 1 ) over ( partition by fv.EncounterCSN order by fv.EncounterCSN, fv.FlowsheetRecordedTime )
       - fv.MinSystolicBloodPressure > @SepticShockMinSBPDrop then 1 else 0 end as SBPDropFlag 
from gt2.tFlowsheetValue as fv
where fv.MinSystolicBloodPressure is not null
),
Meds as (
select
  coalesce( om.PAT_ENC_CSN_ID, pcm.PAT_ENC_CSN_ID ) as EncounterCSN
, Iif( Max( om.CoumadinFlag ) = 1 or Max( pcm.MedicationIsCoumadin ) = 1, 1, 0 ) as CoumadinFlag
, Iif( Max( om.HeparinFlag ) = 1 or Max( pcm.MedicationIsHeparin ) = 1, 1, 0 ) as HeparinFlag
from gt1.OrderMed om
full outer join gt1.PatientEncounterCurrentMed pcm  
  on om.PAT_ENC_CSN_ID = pcm.PAT_ENC_CSN_ID
group by coalesce( om.PAT_ENC_CSN_ID, pcm.PAT_ENC_CSN_ID )
)

--SEVERE SEPSIS
, SevereSepsisBase as
(
select distinct
hypo.EncounterCSN
,hypo.FlowsheetRecordedTime as RecordedTime
,'Hypotension' as Indicator
from (select distinct EncounterCSN from gt2.SepsisFlag) as sirs
	left join gt2.SirsFlag as hypo
    on sirs.EncounterCSN = hypo.EncounterCSN
    --and Abs( DateDiff( second, sirs.RecordedTime, hypo.RecordedTime ) / 3600.0 ) <= @HoursAfterSepsisFlagCheckSS -- applied later in logic
	and hypo.HypotensionFlag =1

union all

select distinct
map.EncounterCSN
,map.FlowsheetRecordedTime as RecordedTime
,'Low MAP' as Indicator
from (select distinct EncounterCSN from gt2.SepsisFlag) as sirs
	left join gt2.tFlowsheetValue as map
    on sirs.EncounterCSN = map.EncounterCSN
    --and Abs( DateDiff( second, sirs.RecordedTime, hypo.RecordedTime ) / 3600.0 ) <= @HoursAfterSepsisFlagCheckSS -- applied later in logic
	and MinMAP < @SepticShockMinMAP

union all

select distinct
bp.EncounterCSN
,bp.RecordedTime as RecordedTime
,'SBP Drop' as Indicator
from (select distinct EncounterCSN from gt2.SepsisFlag) as sirs
	left join BP as bp
    on sirs.EncounterCSN = bp.EncounterCSN
    --and Abs( DateDiff( second, sirs.RecordedTime, hypo.RecordedTime ) / 3600.0 ) <= @HoursAfterSepsisFlagCheckSS -- applied later in logic
	and SBPDropFlag =1

union all

select distinct
c.EncounterCSN
,c.ResultTime as RecordedTime
,'High Creatinine' as Indicator
from (select distinct EncounterCSN from gt2.SepsisFlag) as sirs
	left join gt2.tOrderResult as c
    on sirs.EncounterCSN = c.EncounterCSN
    --and Abs( DateDiff( second, sirs.RecordedTime, hypo.RecordedTime ) / 3600.0 ) <= @HoursAfterSepsisFlagCheckSS -- applied later in logic
	and cast( c.MaxCreatinine as float ) > @SevereSepsisMaxCreatinine

union all

select distinct
b.EncounterCSN
,b.ResultTime as RecordedTime
,'High Bilirubin' as Indicator
from (select distinct EncounterCSN from gt2.SepsisFlag) as sirs
	left join gt2.tOrderResult as b
    on sirs.EncounterCSN = b.EncounterCSN
    --and Abs( DateDiff( second, sirs.RecordedTime, hypo.RecordedTime ) / 3600.0 ) <= @HoursAfterSepsisFlagCheckSS -- applied later in logic
    and cast( b.MaxBilirubin as float ) > @SevereSepsisMaxBilirubin

union all

select distinct
p.EncounterCSN
,p.ResultTime as RecordedTime
,'Low Platelet Count' as Indicator
from (select distinct EncounterCSN from gt2.SepsisFlag) as sirs
	left join gt2.tOrderResult as p
    on sirs.EncounterCSN = p.EncounterCSN
    --and Abs( DateDiff( second, sirs.RecordedTime, hypo.RecordedTime ) / 3600.0 ) <= @HoursAfterSepsisFlagCheckSS -- applied later in logic
    and cast( p.MinPlateletCount as float ) < @SevereSepsisMinPlateletCount

union all


select distinct
l.EncounterCSN
,l.ResultTime as RecordedTime
,'High Lactate Severe' as Indicator
from (select distinct EncounterCSN from gt2.SepsisFlag) as sirs
	left join gt2.tOrderResult as l
    on sirs.EncounterCSN = l.EncounterCSN
    --and Abs( DateDiff( second, sirs.RecordedTime, hypo.RecordedTime ) / 3600.0 ) <= @HoursAfterSepsisFlagCheckSS -- applied later in logic
    and cast( l.MaxLactate as float ) > @SevereSepsisMaxLactate

union all

select distinct
ptt.EncounterCSN
,ptt.ResultTime as RecordedTime
,'High PTT' as Indicator
from (select distinct EncounterCSN from gt2.SepsisFlag) as sirs
	left join gt2.tOrderResult as ptt
    on sirs.EncounterCSN = ptt.EncounterCSN
    --and Abs( DateDiff( second, sirs.RecordedTime, hypo.RecordedTime ) / 3600.0 ) <= @HoursAfterSepsisFlagCheckSS -- applied later in logic
    and cast( ptt.MaxPTT as float ) > @SevereSepsisMaxPTT
	inner join Meds meds
	on sirs.EncounterCSN = meds.EncounterCSN
where meds.HeparinFlag = 0 and cast( ptt.MaxPTT as float ) > @SevereSepsisMaxPTT

union all

select distinct
inr.EncounterCSN
,inr.ResultTime as RecordedTime
,'High INR' as Indicator
from (select distinct EncounterCSN from gt2.SepsisFlag) as sirs
	left join gt2.tOrderResult as inr
    on sirs.EncounterCSN = inr.EncounterCSN
    --and Abs( DateDiff( second, sirs.RecordedTime, hypo.RecordedTime ) / 3600.0 ) <= @HoursAfterSepsisFlagCheckSS -- applied later in logic
    and cast( inr.MaxPTINR as float ) > @SevereSepsisMaxINR
	inner join Meds meds
	on sirs.EncounterCSN = meds.EncounterCSN
where meds.CoumadinFlag = 0 and cast( inr.MaxPTINR as float ) > @SevereSepsisMaxINR
)

select distinct
  ssb.EncounterCSN as EncounterCSN
, ssb.RecordedTime as RecordedTime
, ssb.Indicator as IndicatorType
into gt2.SevereSepsis
from SevereSepsisBase ssb
inner join gt2.SepsisFlag as sf
	on ssb.EncounterCSN = sf.EncounterCSN
	and Abs( DateDiff( second, ssb.RecordedTime, sf.RecordedTime ) / 3600.0 ) <= @HoursAfterSepsisFlagCheckSS
	--and Abs( DateDiff( second, ssb.RecordedTime, sf.RecordedTime ) / 3600.0 ) > 0 --RT Removed per Lehigh Quality Team

create index ncix__SevereSepsis__EncounterCSN on gt2.SevereSepsis (EncounterCSN);
create index ncix_SevereSepsis__RecordedTime on gt2.SevereSepsis (RecordedTime);

--------------------------------------------------------------------------------
print('-----------gt2.SepticShock--------------')

declare @SepticShockMaxLactate tinyint = (select variables.GetSingleValue('@SepticShockMaxLactate'));
declare @SepticShockLactateHours int = (select VariableValue from variables.MasterGlobalReference where VariableName = '@SepticShockLactateHours')

if OBJECT_ID('gt2.SepticShock', 'U') is not null drop table gt2.SepticShock;

with BP as (
select
  fv.EncounterCSN
, fv.FlowsheetRecordedTime as RecordedTime
, fv.MinSystolicBloodPressure
, fv.MaxSystolicBloodPressure
, case when Lag( fv.MaxSystolicBloodPressure, 1 ) over ( partition by fv.EncounterCSN order by fv.EncounterCSN, fv.FlowsheetRecordedTime )
       - fv.MinSystolicBloodPressure > @SepticShockMinSBPDrop then 1 else 0 end as SBPDropFlag --RT: 12/21 Updated for Lehigh MW
from gt2.tFlowsheetValue as fv
where fv.MinSystolicBloodPressure is not null
)

, SevereFlagTime as (
select distinct
  EncounterCSN
, RecordedTime
from gt2.SevereSepsis
)

, IVAdminStopped as (
select distinct
  mg.PAT_ENC_CSN_ID as EncounterCSN
, severe.RecordedTime
, mg.MedicationStoppedTime as IVAdminStopped
from SevereFlagTime as severe
  inner join gt1.MARGiven as mg
    on severe.EncounterCSN = mg.PAT_ENC_CSN_ID
    -- -3/+6 hours
    and DateDiff( second, severe.RecordedTime, mg.MedicationTakenTime ) / 3600.0 between -3 and 6 -- Todo: Variables
where mg.IVFluidTakenFlag = 1
)

--SEPTIC SHOCK
, SepticShockBase as (
select distinct
  hypo.EncounterCSN
, hypo.FlowsheetRecordedTime as RecordedTime
, 'Hypotension' as Indicator
from gt2.SirsFlag as hypo
where hypo.HypotensionFlag = 1

union all

select distinct
  map.EncounterCSN
, map.FlowsheetRecordedTime as RecordedTime
, 'Low MAP' as Indicator
from gt2.tFlowsheetValue as map
where MinMAP < @SepticShockMinMAP

union all

select distinct
  bp.EncounterCSN
, bp.RecordedTime as RecordedTime
, 'SBP Drop' as Indicator
from BP as bp
where SBPDropFlag = 1
)

select distinct
  ssb.EncounterCSN as EncounterCSN
, ssb.RecordedTime as RecordedTime
, ssb.Indicator as IndicatorType
into gt2.SepticShock
from SepticShockBase ssb
inner join IVAdminStopped as severe
  on ssb.EncounterCSN = severe.EncounterCSN
  -- Septic Shock must be after severe sepsis
  and DateDiff( second, severe.RecordedTime, ssb.RecordedTime ) > 0
  -- And Hypo, Low MAP, and SBP Drop must be within +1 hour of stop time for associated IV admin
  and DateDiff( second, severe.IVAdminStopped, ssb.RecordedTime ) / 3600.0 between 0 and 1 -- Todo: Variablize

union

-- Lactate > 4 within +6 hours of severe sepsis flag

select distinct
  l.EncounterCSN
, l.ResultTime as RecordedTime
, 'High Lactate Shock' as Indicator
from SevereFlagTime as severe
  inner join gt2.tOrderResult as l
    on severe.EncounterCSN = l.EncounterCSN
    and Cast( l.MaxLactate as float ) >= @SepticShockMaxLactate
    and DateDiff( second, severe.RecordedTime, l.ResultTime ) > 0
    and DateDiff( second, severe.RecordedTime, l.ResultTime ) / 3600.0 <= @SepticShockLactateHours

create index ncix__SepticShock__EncounterCSN on gt2.SepticShock (EncounterCSN);
create index ncix__SepticShock__RecordedTime on gt2.SepticShock (RecordedTime);


--------------------------------------------------------------------------------
print('-----------gt2.SepsisCombinedFlag--------------')

if OBJECT_ID('gt2.SepsisCombinedFlag', 'U') is not null drop table gt2.SepsisCombinedFlag;

with Flag as
(
select distinct
  EncounterCSN
, RecordedTime as FlagTime
, 'SIRS' as FlagType
, 1 as FlagTypeId
from gt2.SIRSFlag
where MinSirsCriteriaFlag = 1

union all

select distinct
  EncounterCSN
, RecordedTime as FlagTime
, 'Sepsis' as FlagType
, 2 as FlagTypeId
from gt2.SepsisFlag

union all

select distinct
  ss.EncounterCSN
, ss.RecordedTime as FlagTime
, 'Severe Sepsis' as FlagType
, 3 as FlagTypeId
from gt2.SevereSepsis as ss
--inner join gt2.SepsisFlag as sf --RT: 1/17 Change Severe Sepsis to Sepsis based time Phase 1 
--on ss.EncounterCSN = sf.EncounterCSN
--and Abs( DateDiff( second, ss.RecordedTime, sf.RecordedTime ) / 3600.0 ) <= @HoursAfterSepsisFlagCheckSS

union all

select distinct
  ss.EncounterCSN
, ss.RecordedTime as FlagTime
, 'Septic Shock' as FlagType
, 4 as FlagTypeId
from gt2.SepticShock as ss
inner join gt2.SevereSepsis as ssf --RT: 1/17 Change Septic Shock to Severe Sepsis based time Phase 1 
on ss.EncounterCSN = ssf.EncounterCSN
and DateDiff( second, ssf.RecordedTime, ss.RecordedTime ) / 3600.0 > 0 
and DateDiff( second, ssf.RecordedTime, ss.RecordedTime ) / 3600.0 <= @HoursAfterSepsisFlagCheckSS

-- Remove ED Arrival from flags for Lehigh: SB 2017-01-13
-- union all

-- select distinct
--   EncounterCSN
-- , EDArrival as FlagTime
-- , 'ED Arrival' as FlagType
-- , -2 as FlagTypeId
-- from gt2.tHospitalEncounter
-- where EDArrival > 0 --and FirstEDDepartmentID = @cgGHSEDDepartmentID --Remove for Lehigh

)

select
  EncounterCSN
, FlagTime
, FlagType
, FlagTypeId
into gt2.SepsisCombinedFlag
from Flag
;

create index ncix__SepsisCombinedFlag__EncounterCSN on gt2.SepsisCombinedFlag (EncounterCSN);
create index ncix__SepsisCombinedFlag__FlagTime on gt2.SepsisCombinedFlag (FlagTime);
create index ncix__SepsisCombinedFlag__FlagTypeID on gt2.SepsisCombinedFlag (FlagTypeId) include (EncounterCSN, FlagTime)

print('-----------gt2.tFlagTime--------------')
declare @Sepsis3HourBundle int = (select variables.GetSingleValue('@Sepsis3HourBundle'));

if OBJECT_ID('gt2.tFlagTime', 'U') is not null drop table gt2.tFlagTime;

select distinct
  scf.EncounterCSN
, scf.FlagTime
into gt2.tFlagTime
from gt2.SepsisCombinedFlag scf
;

create index ncix__tFlagTime__EncounterCSN on gt2.tFlagTime (EncounterCSN);
create index ncix__tFlagTime__FlagTime on gt2.tFlagTime (FlagTime);

print('-----------gt2.SepsisADT--------------')
declare @ADTAdmissionEventType int = (select variables.GetSingleValue('@ADTAdmissionEventType'));

if OBJECT_ID('gt2.SepsisADT', 'U') is not null drop table gt2.SepsisADT;

with AdtHistory as
(
select
  adt.EncounterCSN
, adt.InTime
, adt.OutTime
, adt.EventID
, adt.EventTypeID
, adt.TransferToICUFlag
, adt.ICUDepartmentFlag
, adt.DepartmentID
, adt.DepartmentName
from gt1.ADTHistory as adt
  inner join gt2.tHospitalEncounter as he
    on adt.EncounterCSN  = he.EncounterCSN
union all

select
  he.EncounterCSN
, he.HospitalAdmissionTime as InTime
, he.HospitalDischargeTime as OutTime
, -1 as EventID
, 7 as EventTypeID
, 0 as TransferToICUFlag
, 0 as ICUDepartmentFlag
, he.DepartmentID
, dep.DEPARTMENT_NAME as DepartmentName
from gt2.tHospitalEncounter as he
	left join pa.ge.CLARITY_DEP as dep
		on he.DepartmentID = dep.DEPARTMENT_ID
where ADTPatientStatusID = 6

)

, SepsisADT as
(
select
  adt.EncounterCSN
, ft.FlagTime
, case when adt.EventTypeID in ( 7, @ADTAdmissionEventType ) then Cast( 0 as DateTime )
  else adt.InTime end as InTime
, OutTime as OutTime
, adt.EventID
, adt.TransferToICUFlag
, adt.ICUDepartmentFlag
, adt.DepartmentID
, adt.DepartmentName
from ADTHistory as adt
  inner join gt2.tFlagTime as ft
    on adt.EncounterCSN = ft.EncounterCSN
    and (ft.FlagTime >= InTime and ft.FlagTime < OutTime ) 
)

select
  EncounterCSN
, FlagTime
, EventID
, TransferToICUFlag
, ICUDepartmentFlag
, DepartmentID
, DepartmentName
into gt2.SepsisADT
from
(
select
  adt.EncounterCSN
, adt.FlagTime
, adt.EventID
, adt.TransferToICUFlag
, adt.ICUDepartmentFlag
, adt.DepartmentID
, adt.DepartmentName
from SepsisADT as adt

union all

select
  ft.EncounterCSN
, ft.FlagTime
, case when ft.FlagTime > he.HospitalDischargeTime then -2 else -1 end as EventID
, 0 as TransferToICUFlag
, 0 as ICUDepartmentFlag
, case when ft.FlagTime > he.HospitalDischargeTime then -2 else -1 end as DepartmentID
, case when ft.FlagTime > he.HospitalDischargeTime then '-2' else '-1' end as DepartmentName
from gt2.tFlagTime ft
  inner join gt2.tHospitalEncounter as he
    on ft.EncounterCSN = he.EncounterCSN
  left join SepsisADT as adt
    on ft.EncounterCSN = adt.EncounterCSN
    and ft.FlagTime = adt.FlagTime
-- Missed flags are flags that don't appear in adt
where adt.EncounterCSN is null
) as t

create index ncix__SepsisADT__EncounterCSN on gt2.SepsisADT (EncounterCSN);
create index ncix__SepsisADT__FlagTime on gt2.SepsisADT (FlagTime);

--------------------------------------------------------------------------------
print('-----------gt2.AbxOrderedMedication--------------')

if OBJECT_ID('gt2.AbxOrderedMedication', 'U') is not null drop table gt2.AbxOrderedMedication;

select distinct
  om.PAT_ENC_CSN_ID as EncounterCSN
, ft.FlagTime
, om.MedicationOrderTime as AbxOrderTime
, om.MedicationName as AbxOrderName
, om.MedicationGroup as AbxOrderGroup
, abx.SepsisOrdersetUsedFlag as AbxOrdersetUsedFlag
, abx.OrdersetName as AbxOrdersetName
into gt2.AbxOrderedMedication
from gt1.MedicationOrder as abx
  inner join gt1.OrderMed om
    on abx.ORDER_MED_ID = om.ORDER_MED_ID
  inner join gt2.tFlagTime as ft
    on ft.EncounterCSN = om.PAT_ENC_CSN_ID
    and om.AbxFlag = 1
    and Abs( DateDiff( second, om.MedicationOrderTime, ft.FlagTime ) / 3600.0 ) <= @Sepsis3HourBundle
;

--------------------------------------------------------------------------------

print('-----------gt2.AbxAdminMedication--------------')

if OBJECT_ID('gt2.AbxAdminMedication', 'U') is not null drop table gt2.AbxAdminMedication;

select distinct
  mg.PAT_ENC_CSN_ID as EncounterCSN
, ft.FlagTime
, mg.MedicationTakenTime as AbxAdminTime
, mg.MedicationOrderTime as AbxOrderTime
, mg.MedicationName as AbxAdminName
, mg.MedicationGroup as AbxAdminGroup
, mg.AbxTakenFlag as AbxAdminOrdersetUsedFlag 
, mg.OrdersetName as AbxAdminOrdersetName
into gt2.AbxAdminMedication
from  gt1.MARGiven as mg
  inner join gt2.tFlagTime as ft
    on ft.EncounterCSN = mg.PAT_ENC_CSN_ID
   -- and Abs( DateDiff( second, mg.MedicationTakenTime, ft.FlagTime ) / 3600.0 ) <= @Sepsis3HourBundle
    and ((datediff(second, ft.FlagTime, mg.MedicationTakenTime) / 3600.0 ) <= 3)
	and ((datediff(second, ft.FlagTime, mg.MedicationTakenTime) / 3600.0 ) >= -24)
	and mg.AbxTakenFlag = 1
  inner join gt2.SepsisCombinedFlag scf
    on ft.EncounterCSN = scf.EncounterCSN
    and ft.FlagTime = scf.FlagTime
	and scf.FlagTypeID = 2 -- Sepsis	

create index ncix__AbxAdminMedication__EncounterCSN on gt2.AbxAdminMedication (EncounterCSN);
create index ncix__AbxAdminMedication__FlagTime on gt2.AbxAdminMedication (FlagTime);


--------------------------------------------------------------------------------

print('-----------gt2.BloodCxOrder--------------')

declare @HoursAfterSepsisFlagBloodCx int = (select variables.GetSingleValue('@HoursAfterSepsisFlagBloodCx'));
declare @HoursBeforeSepsisFlagBloodCx int = (select variables.GetSingleValue('@HoursBeforeSepsisFlagBloodCx'));

if OBJECT_ID('gt2.BloodCxOrder', 'U') is not null drop table gt2.BloodCxOrder;


--CREATE NONCLUSTERED INDEX idx_BloodCxFlag2 ON [gt1].[OrderProcedure] ([BloodCxFlag],[SpecimenTakenTime]) INCLUDE ([EncounterCSN],[ProcedureOrderTime],[ProcedureResultTime],[SepsisOrdersetUsedFlag],[OrdersetName])

select
  op.EncounterCSN
, ft.FlagTime
, op.ProcedureOrderTime as BloodCxOrderTime
, op.ProcedureResultTime as BloodCxResultTime
, op.SpecimenTakenTime as BloodCxCollectedTime
, op.BloodCxFlag
, op.SepsisOrdersetUsedFlag as BloodCxOrdersetUsedFlag
, op.OrdersetName as BloodCxOrdersetName
into gt2.BloodCxOrder
from gt2.tFlagTime as ft
  inner join gt1.OrderProcedure as op 
    on ft.EncounterCSN = op.EncounterCSN
    and DateDiff( second, ft.FlagTime, op.SpecimenTakenTime ) / 3600.0 between -@HoursBeforeSepsisFlagBloodCx and @HoursAfterSepsisFlagBloodCx
where op.BloodCxFlag = 1
  and SpecimenTakenTime is not null;

create index ncix__BloodOrder__EncounterCSN on gt2.BloodCxOrder (EncounterCSN);
create index ncix__BloodOrder__FlagTime on gt2.BloodCxOrder (FlagTime);


--------------------------------------------------------------------------------

print('-----------gt2.IVAdminMedication--------------')

if OBJECT_ID('gt2.IVAdminMedication', 'U') is not null drop table gt2.IVAdminMedication;

select distinct
  mg.PAT_ENC_CSN_ID as EncounterCSN
, ft.FlagTime
, mg.MEDICATION_ID as IVFluidMedicationID
, mg.MedicationName as IVFluidName
, mg.MedicationGroup as IVFluidGroup
, mg.MedicationTakenTime as IVFluidGivenTime
, mg.MedicationOrderTime as IVFluidOrderTime
, mg.SepsisOrdersetUsedFlag as IVAdminOrdersetUsedFlag
, mg.OrdersetName as IVAdminOrdersetName
into gt2.IVAdminMedication
from gt2.tFlagTime as ft
  inner join gt1.MARGiven as mg
  on ft.EncounterCSN = mg.PAT_ENC_CSN_ID
  -- Three hours before or 6 hours after
  and DateDiff( second, ft.FlagTime, mg.MedicationTakenTime ) / 3600.0 between -3 and 6
where mg.IVFluidTakenFlag = 1
;

create index ncix__IVAdminMedication__EncounterCSN on gt2.IVAdminMedication (EncounterCSN);
create index ncix__IVAdminMedication__FlagTime on gt2.IVAdminMedication (FlagTime);


--------------------------------------------------------------------------------

print('-----------gt2.VasopressorAdminMedication--------------')
declare @Sepsis6HourBundle int = (select variables.GetSingleValue('@Sepsis6HourBundle'));

if OBJECT_ID('gt2.VasopressorAdminMedication', 'U') is not null drop table gt2.VasopressorAdminMedication;

select distinct
  ft.EncounterCSN
, ft.FlagTime
, mg.MedicationName as VasopressorAdminName
, mg.MedicationGroup as VasopressorAdminGroup
, mg.MedicationTakenTime as VasopressorAdminTime
, mg.MedicationOrderTime as VasopressorOrderTime
, mg.SepsisOrdersetUsedFlag as VasopressorAdminOrdersetUsedFlag
, mg.OrdersetName as VasopressorAdminOrdersetName
into gt2.VasopressorAdminMedication
from gt2.tFlagTime as ft
  inner join gt1.MARGiven as mg
    on ft.EncounterCSN = mg.PAT_ENC_CSN_ID
    and DateDiff( second, ft.FlagTime, mg.MedicationTakenTime ) / 3600.0 between 0 and @Sepsis6HourBundle
where mg.VasopressorTakenFlag = 1
;

create index ncix__VasopressorAdminMedication__EncounterCSN on gt2.VasopressorAdminMedication (EncounterCSN);
create index ncix__VasopressorAdminMedication__FlagTime on gt2.VasopressorAdminMedication (FlagTime);


--------------------------------------------------------------------------------
print('-----------gt2.SepsisFlagTime--------------')

declare @NormalUrineOutputHours int = (select variables.GetSingleValue('@NormalUrineOutputHours'));
declare @NormalUrineOutputCount int = (select variables.GetSingleValue('@NormalUrineOutputCount'));

if OBJECT_ID('gt2.SepsisFlagTime', 'U') is not null drop table gt2.SepsisFlagTime;

with CVP as (
select
  coalesce( fv.EncounterCSN, po.EncounterCSN ) as EncounterCSN
, coalesce( fv.FlowsheetRecordedTime, po.SpecimenTakenTime ) as RecordedTime
, fv.MinCVP
, fv.MinScvO2
, po.SpecimenTakenTime
, po.CVUltrasoundFlag
from gt2.tFlowsheetValue as fv
  full outer join gt1.OrderProcedure as po
    on fv.EncounterCSN = po.EncounterCSN
    and po.CVUltrasoundFlag = 1 and po.SpecimenTakenTime is not null
where MinCVP is not null or MinScvO2 is not null
)
, CVPInterval as (
select distinct
  cvp.EncounterCSN
, ft.FlagTime
, Min( Iif( cvp.MinCVP is not null, cvp.RecordedTime, null ) ) as CVPRecordedTime
, Max( Iif( cvp.MinCVP is not null, 1, 0 ) ) as CVPRecordedFlag

, Min( Iif( cvp.MinScvO2 is not null, cvp.RecordedTime, null ) ) as ScvO2RecordedTime
, Max( Iif( cvp.MinScvO2 is not null, 1, 0 ) ) as ScvO2RecordedFlag

, Min( Iif( cvp.CVUltrasoundFlag is not null, cvp.RecordedTime, null ) ) as CVUltrasoundRecordedTime
, Max( Iif( cvp.CVUltrasoundFlag is not null, 1, 0 ) ) as CVUltrasoundRecordedFlag

from gt2.tFlagTime as ft
  inner join CVP as cvp
    on ft.EncounterCSN = cvp.EncounterCSN
    and DateDiff( second, ft.FlagTime, cvp.RecordedTime ) / 3600.0 between 0 and @Sepsis6HourBundle
where cvp.MinScvO2 is not null or cvp.MinCVP is not null
group by cvp.EncounterCSN, ft.FlagTime
)
, CVPFiltered as (
select
  EncounterCSN
, FlagTime
, CVPRecordedTime
, CVPRecordedFlag
, ScvO2RecordedTime
, ScvO2RecordedFlag
, CVUltrasoundRecordedTime
, CVUltrasoundRecordedFlag
, 1 as MeasureCVPScvO2Flag
 , (select Max( d ) from (values (CVPRecordedTime), (ScvO2RecordedTime),(CVUltrasoundRecordedTime)) as value( d )) as CVPScvO2RecordedTime
from CVPInterval
where CVPRecordedFlag + ScvO2RecordedFlag + CVUltrasoundRecordedFlag >= 2
--+ PassiveLegRaiseRecordedFlag + CVUltrasoundRecordedFlag >= 2 -- Not in LVHN workflow

)
, PersistentHypotention as ( -- Defined as 2 readings of hypotension within +1 hour of IV admin
select
  iv.EncounterCSN
, iv.FlagTime
, 1 as PersistentHypotentionFlag
from gt2.IVAdminMedication iv
  inner join gt2.tFlowsheetValue fv
    on iv.EncounterCSN = fv.EncounterCSN
    and DateDiff( second, iv.IVFluidGivenTime, fv.FlowsheetRecordedTime ) between 0 and 3600
where MinSystolicBloodPressure < @HypotensionMinSBP
group by iv.EncounterCSN, iv.FlagTime
having count( distinct fv.FlowsheetRecordedTime ) >= 2
)

/* SB: 2017-01-06 - Removed per lehigh request
, UrineOutputNormal as (
select
  uo.EncounterCSN
, ft.FlagTime
, Min( uo.UrineIntervalFirstRecordedTime ) as UrineOutputFirstRecordedTime
from gt2.UrineOutput as uo
  inner join gt2.tFlagTime as ft
    on uo.EncounterCSN = ft.EncounterCSN
    and Abs( DateDiff( second, uo.UrineIntervalFirstRecordedTime, ft.FlagTime ) / 3600.0 ) <= @Sepsis6HourBundle
where UrineOutputPerHour >= @SevereSepsisMinUrineOutput
group by uo.EncounterCSN, ft.FlagTime
)
, UrineInterval as (
select
  uo1.EncounterCSN
, uo1.UrineOutputFirstRecordedTime
, Max( uo2.UrineOutputFirstRecordedTime ) as UrineOutputLastRecordedTime
, Count( uo2.UrineOutputFirstRecordedTime ) as UrineOutputRecordedCount
, Min( he.HospitalAdmissionDate ) as AdmissionTime
from UrineOutputNormal as uo1
  inner join UrineOutputNormal as uo2
    on uo1.EncounterCSN = uo2.EncounterCSN
    and uo1.UrineOutputFirstRecordedTime between uo2.UrineOutputFirstRecordedTime and DateAdd( hour, @NormalUrineOutputHours, uo2.UrineOutputFirstRecordedTime )
  inner join gt2.tHospitalEncounter as he
    on he.EncounterCSN = uo1.EncounterCSN
group by uo1.EncounterCSN, uo1.UrineOutputFirstRecordedTime
having Count( uo2.EncounterCSN ) >= @NormalUrineOutputCount
)

-- Grab the First abnormal Urine recordings that happened before the Urine Output became Normal
, AbnormalUrineFirstRecordedAfterSepsis1 as
(
select
 uo.EncounterCSN
, ft.FlagTime
, Min(uo.UrineIntervalFirstRecordedTime) as AbnormalUrineOutputFirstRecordedTime
from gt2.UrineOutput as uo
 inner join gt2.tFlagTime as ft
   on uo.EncounterCSN = ft.EncounterCSN
   and Abs( DateDiff( second, uo.UrineIntervalFirstRecordedTime, ft.FlagTime ) / 3600.0 ) <= @Sepsis6HourBundle
where uo.UrineOutputPerHour < @SevereSepsisMinUrineOutput
group by uo.EncounterCSN, ft.FlagTime
)
, AbnormalUrineFirstRecordedAfterSepsis2 as
(
select
 au.EncounterCSN
, au.FlagTime
, au.AbnormalUrineOutputFirstRecordedTime
, Min( ui.UrineOutputLastRecordedTime ) as UrineOutputLastRecordedTime
from AbnormalUrineFirstRecordedAfterSepsis1 as au
 inner join UrineInterval as ui
   on au.EncounterCSN = ui.EncounterCSN
   and au.AbnormalUrineOutputFirstRecordedTime between ui.AdmissionTime and ui.UrineOutputLastRecordedTime
group by au.EncounterCSN, au.FlagTime, au.AbnormalUrineOutputFirstRecordedTime
)
, AbnormalUrineFirstRecordedAfterSepsis as
(
select
 au.EncounterCSN
, au.FlagTime
, au.AbnormalUrineOutputFirstRecordedTime
, au.UrineOutputLastRecordedTime
, uv.UrineRecordedTime
, case when DateDiff( second
                   , Lag( uv.UrineRecordedTime, 1, uv.UrineRecordedTime )
                   over ( partition by  au.EncounterCSN, au.FlagTime order by au.EncounterCSN, au.FlagTime, au.AbnormalUrineOutputFirstRecordedTime, uv.UrineRecordedTime asc )
                   , uv.UrineRecordedTime ) <= 3600
                   then 1 else 0 end as UrineRecordedHourlyFlag
from AbnormalUrineFirstRecordedAfterSepsis2 as au
 inner join gt2.UrineValue as uv
   on au.EncounterCSN = uv.EncounterCSN
   and uv.UrineRecordedTime between au.AbnormalUrineOutputFirstRecordedTime and au.UrineOutputLastRecordedTime
)
*/

--FlagTime----------------------------------------------------------------------

, FlagTime as (
select distinct
  ft.EncounterCSN
, ft.FlagTime as SepsisFlagTime
, DateDiff( second, ft.FlagTime, Min( Iif( IcuTransfer.InTime > NonIcuFlag.FlagTime, IcuTransfer.InTime, null ) ) ) / 60.0 as SepsisToICUMinutes

-- Lactate Results
, Min( lactate.ResultTime ) as LactateResultTime
, min( lactate.SpecimenTakenTime ) as LactateCollectionTime 
, Min( lactate.OrderTime ) as LactateOrderTime
, Min( DateDiff( second, ft.FlagTime, lactate.SpecimenTakenTime ) / 60.0 ) as SepsisToLactateMinutes 
, Max( IsNull( lactate.SepsisOrdersetUsedFlag, 0 ) ) as LactateOrdersetUsedFlag
, Max( lactate.OrdersetName ) as LactateOrdersetName
, Max( Iif( lactate.MaxLactate is not null, 1, 0 ) ) as LactateFlag


-- ABX Order
, Max( aom.AbxOrdersetUsedFlag ) as AbxOrderOrdersetUsedFlag
, Max( aom.AbxOrdersetName ) as AbxOrdersetName
, DateDiff( second, ft.FlagTime, min( aom.AbxOrderTime ) ) / 60.0 as AbxFirstOrderTime
, Max( case when aom.EncounterCSN is null then 0 else 1 end ) as AbxOrderFlag

-- ABX Admin
, Min( aam.AbxAdminTime ) as AbxFirstAdminTime
, Min( aam.AbxOrderTime ) as AbxFirstOrderedTime
, Max( aam.AbxAdminOrdersetUsedFlag ) as AbxAdminOrdersetUsedFlag
, Max( aam.AbxAdminOrdersetName ) as AbxAdminOrdersetName
, DateDiff( second, ft.FlagTime, min( aam.AbxAdminTime ) ) / 60.0 as SepsisToAbxAdminMinutes
, Max( case when aam.EncounterCSN is null then 0 else 1 end ) as AbxAdminFlag

-- IV Given
, Min( iv.IVFluidGivenTime ) as IVFluidFirstGivenTime
, Min( iv.IVFluidOrderTime ) as IVFluidFirstOrderTime
, Max( iv.IVAdminOrdersetUsedFlag ) as IVAdminOrdersetUsedFlag
, Max( iv.IVAdminOrdersetName ) as IVAdminOrdersetName
, DateDiff( second, ft.FlagTime, min( iv.IVFluidGivenTime ) ) / 60.0 as SepsisToIVAdminMinutes
, Max( case when iv.EncounterCSN is null then 0 else 1 end ) as IVAdminFlag

-- Blood CX
, Min( bo.BloodCxCollectedTime ) as BloodCxFirstCollectedTime
, Min( bo.BloodCxOrderTime ) as BloodCxFirstOrderTime
, DateDiff( second, ft.FlagTime, Min( bo.BloodCxCollectedTime ) ) / 60.0 as SepsisToBloodCxMinutes
, Max( bo.BloodCxOrdersetUsedFlag ) as BloodCxOrdersetUsedFlag
, Max( bo.BloodCxOrdersetName ) as BloodCxOrdersetName
, Max( case when bo.EncounterCSN is null then 0 else 1 end ) as BloodCxFlag

-- Vasopressor Admin
, Min( vaso.VasopressorAdminTime ) as VasopressorFirstAdminTime
, Min( vaso.VasopressorOrderTime ) as VasopressorFirstOrderTime
, DateDiff( second, ft.FlagTime, Min( vaso.VasopressorAdminTime ) ) / 60.0 as SepsisToVasopressorAdminMinutes
, Max( vaso.VasopressorAdminOrdersetUsedFlag ) as VasopressorAdminOrdersetUsedFlag
, Max( vaso.VasopressorAdminOrdersetName ) as VasopressorAdminOrdersetName
, Max( case when vaso.EncounterCSN is null then 0 else 1 end ) as VasopressorAdminFlag

, Max( IsNull( ph.PersistentHypotentionFlag, 0 ) ) as PersistentHypotentionFlag

-- Elevated Lactate Flag
, Max( case when elevatedLactate.EncounterCSN is null then 0 else 1 end ) as ElevatedLactateFlag

-- Lactate Remeasure
, Min( lactate.ResultTime ) as LactateFirstRemeasuredResultTime
, Min( remeasure.SpecimenTakenTime ) as  LactateFirstRemeasuredTime 
, Min( remeasure.OrderTime ) as LactateFirstRemeasuredOrderTime
, DateDiff( second, ft.FlagTime, Min( remeasure.SpecimenTakenTime ) ) / 60.0 as SepsisToRemeasureLactateMinutes -- RT: Updated to use collection not result time for Lactate compliance
, Max( remeasure.SepsisOrdersetUsedFlag ) as RemeasureLactateOrdersetUsedFlag
, Max( remeasure.OrdersetName ) as RemeasureLactateOrdersetName
, Max( case when remeasure.EncounterCSN is null then 0 else 1 end ) as RemeasureLactateFlag

-- CVP

, Min( cvp.CVPScvO2RecordedTime ) as CVPScvO2RecordedTime
, DateDiff( second, ft.FlagTime, Min( cvp.CVPScvO2RecordedTime ) ) / 60.0 as SepsisToScvO2RecordedMinutes
, Max( cvp.MeasureCVPScvO2Flag ) as MeasureCVPScvO2Flag

, Max( IsNull( sixHourSepsisOrderSet.SepsisOrdersetUsedFlag, 0 ) ) as SixHourSepsisOrderSetUsed
, DateDiff( second, ft.FlagTime, Min( sixHourSepsisOrderSet.OrderTime ) ) / 60.0 as SixHourSepsisOrderSetMinutes

, Max( IsNull( threeHourSepsisOrderSet.SepsisOrdersetUsedFlag, 0 ) ) as ThreeHourSepsisOrderSetUsed
, DateDiff( second, ft.FlagTime, Min( threeHourSepsisOrderSet.OrderTime ) ) / 60.0 as ThreeHourSepsisOrderSetMinutes

-- Removed per LVHN request
-- -- Urine Output
-- , Min( uo.UrineIntervalFirstRecordedTime ) as UrineOutputFirstRecordedTime
-- , DateDiff( second, ft.FlagTime, Min( uo.UrineIntervalFirstRecordedTime ) ) / 60.0 as SepsisToUrineOutputFirstRecordedMinutes
-- , Max( case when uo.EncounterCSN is null then 0 else 1 end ) as HourlyUrineOutputFlag

---- Abnormal Urine
---- TODO: Should these be concated to UO above?
-- , Min( au.AbnormalUrineOutputFirstRecordedTime ) as UrineOutputFirstRecordedTime
-- , DateDiff( second, ft.FlagTime, Min( au.AbnormalUrineOutputFirstRecordedTime ) ) / 60.0 as SepsisToUrineOutputFirstRecordedMinutes
-- , Max( case when au.EncounterCSN is null then 0 else 1 end ) as HourlyUrineOutputFlag

from gt2.tFlagTime as ft
  left join gt2.tOrderResult as lactate
    on lactate.EncounterCSN = ft.EncounterCSN
	-- RT: Updated for collection time for Lactate
      and Abs( DateDiff( second, ft.FlagTime, lactate.SpecimenTakenTime ) / 3600.0 ) <= @Sepsis3HourBundle
     and lactate.MaxLactate is not null
  left join gt2.SepsisADT as NonIcuFlag
    on ft.EncounterCSN = NonIcuFlag.EncounterCSN
    and ft.FlagTime = NonIcuFlag.FlagTime
    and NonIcuFlag.ICUDepartmentFlag = 0
  left join gt1.ADTHistory as IcuTransfer
    on IcuTransfer.EncounterCSN = ft.EncounterCSN
    and IcuTransfer.InTime > NonIcuFlag.FlagTime
    and IcuTransfer.TransferToICUFlag = 1
  left join gt2.AbxOrderedMedication as aom
    on aom.EncounterCSN = ft.EncounterCSN
    and aom.FlagTime = ft.FlagTime
  left join gt2.AbxAdminMedication as aam
    on aam.EncounterCSN = ft.EncounterCSN
    and aam.FlagTime = ft.FlagTime
  left join gt2.IVAdminMedication as iv
    on iv.EncounterCSN = ft.EncounterCSN
    and iv.FlagTime = ft.FlagTime
  left join gt2.BloodCxOrder as bo
    on bo.EncounterCSN = ft.EncounterCSN
    and bo.FlagTime = ft.FlagTime
  left join gt2.VasopressorAdminMedication as vaso
    on vaso.EncounterCSN = ft.EncounterCSN
    and vaso.FlagTime = ft.FlagTime
  left join PersistentHypotention ph
    on ft.EncounterCSN = ph.EncounterCSN
	and ft.FlagTime = ph.FlagTime
  left join gt2.SevereSepsis as elevatedLactate
    on elevatedLactate.RecordedTime = ft.FlagTime
    and elevatedLactate.EncounterCSN = ft.EncounterCSN
    and elevatedLactate.IndicatorType = 'High Lactate Severe' 
  left join gt2.tOrderResult as remeasure
    on remeasure.EncounterCSN = elevatedLactate.EncounterCSN
    and DateDiff( second, elevatedLactate.RecordedTime, remeasure.SpecimenTakenTime ) / 3600.0 between 0 and @Sepsis6HourBundle
    and remeasure.MaxLactate is not null
  left join CVPFiltered as cvp
    on cvp.EncounterCSN = ft.EncounterCSN
    and cvp.FlagTime = ft.FlagTime
	-- Urine removed per LVHN request
  -- left join gt2.UrineOutput as uo
  --   on uo.EncounterCSN = ft.EncounterCSN
  --   and Abs( DateDiff( second, uo.UrineIntervalFirstRecordedTime, ft.FlagTime ) / 3600.0 ) <= @Sepsis6HourBundle
  --   and UrineRecordedHourlyFlag = 1
  -- --left join AbnormalUrineFirstRecordedAfterSepsis as au
  -- --  on au.EncounterCSN = ft.EncounterCSN
  -- --  and au.FlagTime = ft.FlagTime
  left join gt2.tOrderResult sixHourSepsisOrderSet
    on sixHourSepsisOrderSet.EncounterCSN = ft.EncounterCSN
    and sixHourSepsisOrderSet.SepsisOrderSetUsedFlag = 1
    and DateDiff( second, ft.FlagTime, sixHourSepsisOrderSet.OrderTime ) / 3600.0 between 0 and @Sepsis6HourBundle
  left join gt2.tOrderResult threeHourSepsisOrderSet
    on threeHourSepsisOrderSet.EncounterCSN = ft.EncounterCSN
    and threeHourSepsisOrderSet.SepsisOrderSetUsedFlag = 1
    and DateDiff( second, ft.FlagTime, threeHourSepsisOrderSet.OrderTime ) / 3600.0 between 0 and @Sepsis3HourBundle
group by ft.EncounterCSN, ft.FlagTime
)



, FT as (
select
  *

, Iif( SepsisToLactateMinutes between 0 and 180, 1, 0 ) as ThreeHourLactateFlag
, Iif( SepsisToIVAdminMinutes between 0 and 180, 1, 0 ) as ThreeHourIVAdminFlag
--, case when SepsisToBloodCxMinutes < SepsisToAbxAdminMinutes then 1 else 0 end as BloodCxFlag
from FlagTime
)
, BundleCompliance as (
select
  *

, case when ThreeHourSepsisOrderSetUsed = 1 or (  ThreeHourLactateFlag = 1 and BloodCxFlag = 1 and AbxAdminFlag = 1 and ThreeHourIVAdminFlag = 1 )
    then 1 else 0 end as ThreeHourBundleFlag

, Iif( ThreeHourSepsisOrderSetUsed = 1, ThreeHourSepsisOrderSetMinutes, null ) as ThreeHourOrderSetMinutes
, Iif( ThreeHourLactateFlag = 1 and BloodCxFlag = 1 and AbxAdminFlag = 1 and ThreeHourIVAdminFlag = 1
     , ( select Max( m ) from (values (SepsisToLactateMinutes), (SepsisToBloodCxMinutes), (SepsisToAbxAdminMinutes), (SepsisToIVAdminMinutes) ) as value(m))
     , null ) ThreeHourIndividualMinutes

, case when SixHourSepsisOrderSetUsed = 1
        or ( ( PersistentHypotentionFlag = 0 or VasopressorAdminFlag = 1 )
          and ( ElevatedLactateFlag = 0 or RemeasureLactateFlag = 1 ) )
     then 1 else 0 end as SixHourBundleFlag

, Iif( SixHourSepsisOrderSetUsed = 1, SixHourSepsisOrderSetMinutes, null ) as SixHourOrderSetMinutes

, case when PersistentHypotentionFlag = 0 and ElevatedLactateFlag = 0 then 0
       when PersistentHypotentionFlag = 1 and VasopressorAdminFlag = 1 and ElevatedLactateFlag = 0 then SepsisToVasopressorAdminMinutes
       when PersistentHypotentionFlag = 0 and ElevatedLactateFlag = 1 and RemeasureLactateFlag = 1 then SepsisToRemeasureLactateMinutes
       when PersistentHypotentionFlag = 1 and VasopressorAdminFlag = 1 and ElevatedLactateFlag = 1 and RemeasureLactateFlag = 1
         then (select Max( m ) from (values (SepsisToVasopressorAdminMinutes), (SepsisToRemeasureLactateMinutes) ) as value(m))
       else null
     end as SixHourIndividualMinutes

, case when LactateFlag = 1 and BloodCxFlag = 1 and AbxAdminFlag = 1 then 1 else 0 end as LactateBloodCxAbxAdminFlag
from FT
)

select
  *

, (select Min( m ) from (values (ThreeHourOrderSetMinutes), (ThreeHourIndividualMinutes)) as value(m)) as ThreeHourBundleMinutes
, (select Min( m ) from (values (SixHourOrderSetMinutes), (SixHourIndividualMinutes)) as value(m)) as SixHourBundleMinutes

into gt2.SepsisFlagTime
from BundleCompliance
;

create index ncix__SepsisFlagTime__EncounterCSN on gt2.SepsisFlagTime (EncounterCSN);
create index ncix__SepsisFlagTime__FlagTime on gt2.SepsisFlagTime (SepsisFlagTime);
create index ncix__SepsisFlagTime__LactateFlag on gt2.SepsisFlagTime (LactateFlag) include (EncounterCSN)

-- -- Drop Temp Tables
--drop table gt2.tHospitalEncounter;
--drop table gt2.tFlowsheetValue;
--drop table gt2.tOrderResult;
--drop table gt2.tFlagTime;


-- check keys and indexes
exec tools.CheckKeysAndIndexes


 











GO
