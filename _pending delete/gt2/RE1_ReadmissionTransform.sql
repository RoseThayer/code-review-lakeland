SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
 
 
CREATE procedure [gt2].[RE1_ReadmissionTransform]
as

/*
    --> Source Tables <--
		pa.ge.PATIENT
		pa.ge.PAT_ENC_HSP
		pa.ge.HSP_ACCOUNT
		pa.ge.LV_READMISSION_RATE_ORDER_ADT
		pa.ge.LV_READMISSION_RATE_DISCHARGES
		gt1.Encounter  

	--> Target Table <--
		gt2.Readmission

	--> Performance Benchmark <--
		1022 rows ~1s
*/	
  
declare @ReadmissionDays numeric = (select VariableValue from variables.MasterGlobalReference where VariableName = '@ReadmissionDays')

if OBJECT_ID('gt2.Readmission', 'U') is not null drop table gt2.Readmission;
;with Admission as
(
	select distinct                              
	    oa.EVENT_TYPE_C
	  , oa.EFFECTIVE_TIME
	  , oa.PAT_ID as PatientID
	  , oa.PAT_ENC_CSN_ID as EncounterCSN
	  , be.ORIG_ADMSN_DTTM as AdmissionDate
      , be.DSCG_DTTM as DischargeDate
	  , be.MRN as PatientMRN
	  --, PatientMRN	
	  --, EncounterCSN
	from pa.ge.LV_READMISSION_RATE_ORDER_ADT oa
    left join pa.ge.LV_READMISSION_RATE_DISCHARGES be 
	  on oa.PAT_ENC_CSN_ID = be.ENC
    left join pa.ge.HSP_ACCOUNT ha
	  on oa.PAT_ENC_CSN_ID = ha.PRIM_ENC_CSN_ID
    where oa.NAME = 'Inpatient'   
	  and (ha.ACCT_BASECLS_HA_C is null or ha.ACCT_BASECLS_HA_C = 1)
), 
	IndexedAdmission as
(
	select distinct
		--ROW_NUMBER() over (partition by PatientMRN order by PatientMRN, AdmissionDate) as ix
		row_number() over (order by PatientID, EFFECTIVE_TIME) as ix --new_row                                
	  , EVENT_TYPE_C
	  , EFFECTIVE_TIME
	  , PatientID
	  , EncounterCSN
	  , AdmissionDate
      , DischargeDate
	  , PatientMRN
	  --, PatientMRN	
	  --, EncounterCSN
	from Admission 
	--order by oa.PAT_ID, oa.PAT_ENC_CSN_ID, oa.EFFECTIVE_TIME
)   

select distinct 
	  t1.PatientMRN, 
	  t1.EncounterCSN as IndexEncounterCSN,
	  ha1.HSP_ACCOUNT_ID as IndexHospitalAccountID,
	  t1.AdmissionDate as IndexAdmissionDate,
	  t1.DischargeDate as IndexDischargeDate,
	  t1.ix as IndexFlag, 
	  t2.EncounterCSN as ReadmissionEncounterCSN,
	  ha2.HSP_ACCOUNT_ID as ReadmissionHospitalAccountID,
	  t2.AdmissionDate as ReadmissionAdmissionDate,
	  t2.DischargeDate as ReadmissionDischargeDate,
	  t2.ix as ReadmissionVisitNumber, 
	  datediff(MINUTE, t1.DischargeDate, t2.AdmissionDate) / 60.0 / 24 AS DaysDifference
into gt2.Readmission
from   IndexedAdmission t1
inner join IndexedAdmission t2
   on t1.ix = t2.ix-1
  and t2.EVENT_TYPE_C = 1
  and t1.PatientID = t2.PatientID
 left join pa.ge.PAT_ENC_HSP ha1
   on t1.EncounterCSN = ha1.PAT_ENC_CSN_ID 
 left join pa.ge.PAT_ENC_HSP ha2
   on t2.EncounterCSN = ha2.PAT_ENC_CSN_ID                  
where cast(t2.AdmissionDate as date) >= cast(t1.DischargeDate as date)
  and cast(t2.AdmissionDate as date) <= dateadd(day,@ReadmissionDays,t1.DischargeDate)

--order by t1.PatientMRN, t1.ix
 
create index ncix__Readmission__IndexEncounterCSN on gt2.Readmission (IndexEncounterCSN)
 
 -- check keys and indexes
exec tools.CheckKeysAndIndexes












GO
