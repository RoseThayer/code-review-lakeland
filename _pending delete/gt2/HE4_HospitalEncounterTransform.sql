SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [gt2].[HE4_HospitalEncounterTransform]
 as
 set nocount on
/*
-->	Source tables used <--
    PA.ge.CLARITY_DEP  
    pa.ge.ZC_PAT_STATUS  
    pa.ge.ZC_DISCH_DISP  
    pa.ge.ZC_MC_ADM_SOURCE  
    pa.ge.ZC_ACCT_BASECLS_HA_C  
    pa.ge.ZC_ED_DISPOSITION  
    pa.ge.CLARITY_SER  
    pa.ge.ZC_FIN_CLASS  
    pa.ge.PATIENT
	gt1.Encounter
	map.LOSGroup
 
--> Target table created <--
	gt2.HospitalEncounter

--> Performance benchmark <--
	1373451 rows in ~30 seconds w 2015-limited PAT_ENC_HSP
	entire data set ~4:30 at time of dev
*/	
 

declare @vcgWICDepartmentID int  = (select cast(pa.variables.GetSingleValue ('@vcgWICDepartmentID') as int)) 
      , @vcgPatientStatusHOV int = (select cast(pa.variables.GetSingleValue ('@vcgPatientStatusHOV') as int))  
      , @vcgIPPatientClassID int = (select cast(pa.variables.GetSingleValue ('@vcgIPPatientClassID') as int))  
      , @vcgOutpatientPatientClassID int = (select cast(pa.variables.GetSingleValue ('@vcgOutpatientPatientClassID') as int))   
      , @vcgObservationPatientClassID int =(select cast(pa.variables.GetSingleValue ('@vcgObservationPatientClassID') as int))    
      , @vcgEmergencyPatientClassID int =(select cast(pa.variables.GetSingleValue ('@vcgEmergencyPatientClassID') as int))  
      , @vcgADTBaseClassObservation varchar(100) = (select pa.variables.GetSingleValue ('@vcgADTBaseClassObservation'))   

declare @vMortalityDischargeDisposition table (DISCH_DISP_C int)
insert into @vMortalityDischargeDisposition
select VariableValue from variables.GetTableValues ('@DischargeDisp_DISCH_DISP_C') 
 
--HospitalEncounter 
if exists (select * from information_schema.tables where TABLE_NAME = 'HospitalEncounter' and TABLE_SCHEMA = 'gt2')
   drop table gt2.HospitalEncounter 
create table [gt2].[HospitalEncounter]
(
	[PAT_ENC_CSN_ID] [numeric](18, 0) NOT NULL,
	[PAT_ID] [varchar](18) NULL,
	[DEPARTMENT_ID] [numeric](18, 0) NULL,
	[WICDepartment] [varchar](1)  NULL,
	[HSP_ACCOUNT_ID] [numeric](18, 0) NULL,
	[DischargeDepartment] [varchar](254) NULL,
	[ADTPatientStatusID] [int] NULL,
	[ADTPatientStatus] [varchar](50) NULL,
	[HOVEncounter] [varchar](1)  NULL,
	[INPATIENT_DATA_ID] [varchar](18) NULL,
	[DISCH_DISP_C] [varchar](66) NULL,
	[DischargeDisposition] [varchar](254) NULL,
	[HospitalAdmissionTime] [datetime] NULL,
	[HospitalAdmissionDate] [date] NULL,
	[AdmissionDAT] [numeric](18, 0) NULL,
	[HospitalDischargeTime] [datetime] NULL,
	[DischargeDateTimeNowIfNull] [datetime]  NULL,
	[DischargeDateTodayIfNull] [date] NULL,
	[ADMISSION_SOURCE_C] [varchar](100) NULL,
	[Discharged] [varchar](3)  NULL,
	[LengthOfStay] [int] NULL,
	--[LengthOfStayNowIfNull] [varchar](50) NULL,
	[LengthOfStayDaysNowIfNull] [varchar](30) NULL,
	[AdmissionSource] [varchar](254) NULL,
	[HasDNRStatus] [varchar](1) NULL,
	[CodeStatuses] [varchar](500) NULL,
	[HasMortalityDischargeDisposition] [varchar](1) NOT NULL,
	[PatientClass] [nvarchar](50) NULL,
	[InpatientEncounter] [varchar](1)  NULL,
	[OutpatientEncounter] [varchar](1)  NULL,
	[ObservationEncounter] [varchar](1)  NULL,
	[EmergencyEncounter] [varchar](1)  NULL,
	[BaseClass] varchar(100) NULL,
	[EDDispositionID] [varchar](66) NULL,
	[EDDisposition] [varchar](50) NULL,
	[EDDepartureTime] [datetime] NULL,
	[EDDispTime] [datetime] NULL,
	[EDEpisodeID] [numeric](18, 0) NULL,
	[AdmissionProviderID] [varchar](18) NULL,
	[AdmissionProvider] [varchar](254) NULL,
	[DischargeProviderID] [varchar](18) NULL,
	[DischargeProvider] [varchar](254) NULL,
	[IPAdmitDateTime] [datetime] NULL,
	[IPAdmitDate] [date] NULL,
	[INP_ADM_EVENT_ID] [numeric](18, 0) NULL,
	[EMER_ADM_EVENT_ID] [numeric](18, 0) NULL,
	[EMER_ADM_DATE] [datetime] NULL,
	[CoumadinFlag] int null, 
	[HeparinFlag] int null,
	[FinancialClass] [varchar](254) NULL,
	[BirthDate] [datetime] NULL,
	[PatientMRN] [varchar](25) NULL,
	[Age] [int] NULL,
	[AgeAtDischarge] [int] NULL,
	[Adult] [varchar](3)  NULL,
	[LengthOfStayGroup] [varchar](50) NULL,
	[FirstIPDepartmentID] [numeric](18, 0) NULL,
	[FirstEDDepartmentID] [numeric](18, 0) NULL,
	[OBS_EVENT_ID] [numeric](18, 0) NULL,
	[FirstObsDepartmentID] [numeric](18, 0) NULL
) 

insert into gt2.HospitalEncounter 
(	 [PAT_ENC_CSN_ID]
    ,[PAT_ID]
    ,[DEPARTMENT_ID]
    ,[WICDepartment]
    ,[HSP_ACCOUNT_ID]
    ,[ADTPatientStatusID]
    ,[HOVEncounter]
    ,[INPATIENT_DATA_ID]
    ,[DISCH_DISP_C]
    ,[HospitalAdmissionTime]
    ,[HospitalAdmissionDate]
	,[AdmissionDAT]
    ,[HospitalDischargeTime]
    ,[DischargeDateTimeNowIfNull]
    ,[DischargeDateTodayIfNull]
    ,[ADMISSION_SOURCE_C]
    ,[Discharged]
    ,[LengthOfStay]
   -- ,[LengthOfStayNowIfNull]
    ,[LengthOfStayDaysNowIfNull]
    ,[HasDNRStatus]
    ,[CodeStatuses]
    ,[EDDispositionID]   
    ,[EDDepartureTime]
    ,[EDDispTime]
    ,[EDEpisodeID]
    ,[AdmissionProviderID]
    ,[DischargeProviderID]
    ,[IPAdmitDateTime]
    ,[IPAdmitDate]
    ,[INP_ADM_EVENT_ID]
    ,[EMER_ADM_EVENT_ID]
    ,[EMER_ADM_DATE]
	,[CoumadinFlag]
	,[HeparinFlag]
    ,[Age]
    ,[AgeAtDischarge]
    ,[Adult]
    ,[DischargeDepartment] 
    ,[ADTPatientStatus]
    ,[DischargeDisposition]
    ,[AdmissionSource]
    ,[HasMortalityDischargeDisposition]
    ,[PatientClass]  
    ,[InpatientEncounter]
    ,[OutpatientEncounter]
    ,[ObservationEncounter]
    ,[EmergencyEncounter]  	 
	,[EDDisposition]
    ,[AdmissionProvider]
	,[DischargeProvider]
    ,[FinancialClass]
    ,[BirthDate]
    ,[PatientMRN]
    ,[LengthOfStayGroup]
    ,[FirstIPDepartmentID]
    ,[OBS_EVENT_ID]
    ,[FirstObsDepartmentID]
    ,[FirstEDDepartmentID]
)   
select 
 	 enc.PAT_ENC_CSN_ID 
   , enc.PAT_ID 
   , enc.DEPARTMENT_ID 
   , case when enc.DEPARTMENT_ID = @vcgWICDepartmentID then 1 else 0 end as WICDepartment 
   , enc.HSP_ACCOUNT_ID 
   , enc.ADT_PATIENT_STAT_C as ADTPatientStatusID 
   , case when enc.ADT_PATIENT_STAT_C = @vcgPatientStatusHOV then 1 else 0 end as HOVEncounter 
   , enc.INPATIENT_DATA_ID 
   , enc.DISCH_DISP_C 
   , enc.HOSP_ADMSN_TIME as HospitalAdmissionTime 
   , cast(enc.HOSP_ADMSN_TIME as date) as HospitalAdmissionDate   
   , cast(121531 - CAST(CONVERT(datetime,enc.PAT_ENC_DATE_REAL) as float) as float) as AdmissionDAT 
   , enc.HOSP_DISCH_TIME as HospitalDischargeTime 
   , isnull(enc.HOSP_DISCH_TIME, getdate()) as DischargeDateTimeNowIfNull 
   , isnull(Cast(enc.HOSP_DISCH_TIME as date), ( cast(getdate() as date) )) as DischargeDateTodayIfNull 
   , enc.ADMISSION_SOURCE_C 
   , iif(enc.HOSP_DISCH_TIME > 0, 1, 0) as Discharged 
   , case when enc.HOSP_DISCH_TIME > 0 then 
		(case when enc.INP_ADM_DATE< enc.OP_ADM_DATE or enc.OP_ADM_DATE is null 
		      then
				 (case when (CAST(ROUND((datediff(DAY,enc.INP_ADM_DATE,enc.HOSP_DISCH_TIME)), 1) AS numeric)) = 0 
					   then 1
					   else (CAST(ROUND((datediff(DAY,enc.INP_ADM_DATE,enc.HOSP_DISCH_TIME)), 1) AS numeric)) 
				  end)
			  else
				 (case when (CAST(ROUND((datediff(DAY,enc.OP_ADM_DATE,enc.HOSP_DISCH_TIME)), 1) AS numeric)) = 0 
				       then 1
					   else (CAST(ROUND((datediff(DAY,enc.OP_ADM_DATE,enc.HOSP_DISCH_TIME)), 1) AS numeric)) 
				  end) 
		 end)
	 end as LengthOfStay  
 --  , enc.LengthOfStayNowIfNull as LengthOfStayNowIfNull 
   , isnull(convert(varchar, enc.HOSP_DISCH_TIME, 120), cast(datediff(day, enc.HOSP_ADMSN_TIME, Getdate()) as varchar(50))) as LengthOfStayDaysNowIfNull 
   , enc.[HasDNRStatus] 
   , enc.CodeStatuses 
   , enc.ED_DISPOSITION_C as EDDispositionID 
   , Cast(enc.ED_DEPARTURE_TIME as datetime) as EDDepartureTime 
   , Cast(enc.ED_DISP_TIME as datetime) as EDDispTime 
   , enc.ED_EPISODE_ID as EDEpisodeID 
   , enc.ADMISSION_PROV_ID as AdmissionProviderID 
   , enc.DISCHARGE_PROV_ID as DischargeProviderID 
   , Cast(enc.INP_ADM_DATE as datetime) as IPAdmitDateTime 
   , Cast(enc.INP_ADM_DATE as date) as IPAdmitDate 
   , enc.INP_ADM_EVENT_ID 
   , enc.EMER_ADM_EVENT_ID 
   , enc.EMER_ADM_DATE 
   , enc.CoumadinFlag
   , enc.HeparinFlag
   , ( CONVERT( int, CONVERT( char(8), enc.HOSP_ADMSN_TIME, 112 ) ) - CONVERT( char(8), pt.BIRTH_DATE, 112 ) ) / 10000 as Age
   , ( CONVERT( int, CONVERT( char(8), enc.HOSP_DISCH_TIME, 112 ) ) - CONVERT( char(8), pt.BIRTH_DATE, 112 ) ) / 10000 as AgeAtDischarge
   , case when (( CONVERT( int, CONVERT( char(8), enc.HOSP_ADMSN_TIME, 112 ) ) - CONVERT( char(8), pt.BIRTH_DATE, 112 ) ) / 10000 ) >= 18 then 'Yes' else 'No' end as Adult 
   , cd.DEPARTMENT_NAME as DischargeDepartment 
   --cd.DEPARTMENT_ID as DischargeDepartmentId,      
   , zps.[NAME] as ADTPatientStatus 
   , zdd.[NAME] as DischargeDisposition 
   , zma.[NAME] as AdmissionSource 
   , case when vmd.DISCH_DISP_C is null then 'N' else 'Y' end as HasMortalityDischargeDisposition 
   --zab.[NAME] as BaseClass,  
   , zpc.[NAME] as PatientClass 
   , case when zpc.ADT_PAT_CLASS_C = @vcgIPPatientClassID then 'Y' else 'N' end as InpatientEncounter 
   , case when zpc.ADT_PAT_CLASS_C = @vcgOutpatientPatientClassID then 'Y' else 'N' end as OutpatientEncounter 
   , case when zpc.ADT_PAT_CLASS_C = @vcgObservationPatientClassID then 'Y' else 'N' end as ObservationEncounter 
   , case when zpc.ADT_PAT_CLASS_C = @vcgEmergencyPatientClassID then 'Y' else 'N' end as EmergencyEncounter 
   , zed.[NAME] as EDDisposition 
   , csa.PROV_NAME as AdmissionProvider 
   , csd.PROV_NAME as DischargeProvider 
   , zfc.[NAME] as FinancialClass 
   , pt.BIRTH_DATE as BirthDate 
   , pt.PAT_MRN_ID as PatientMRN 
   , '999' as LengthOfStayGroup -- currently handled in UI of Sepsis app
   , cai.DEPARTMENT_ID as FirstIPDepartmentID 
   , cai.EVENT_ID as OBS_EVENT_ID 
   , cai.DEPARTMENT_ID as FirstObsDepartmentID 
   , cae.DEPARTMENT_ID as FirstEDDepartmentID 
from  gt1.Encounter enc with (nolock)
left join PA.ge.CLARITY_DEP cd  with (nolock)
  on enc.DEPARTMENT_ID = cd.DEPARTMENT_ID
left join pa.ge.ZC_PAT_STATUS zps with (nolock)
  on enc.ADT_PATIENT_STAT_C = zps.ADT_PATIENT_STAT_C
left join pa.ge.ZC_DISCH_DISP zdd with (nolock)
  on enc.DISCH_DISP_C = zdd.DISCH_DISP_C
left join pa.ge.ZC_MC_ADM_SOURCE zma with (nolock)
  on enc.ADMISSION_SOURCE_C = zma.ADMISSION_SOURCE_C
left join @vMortalityDischargeDisposition vmd
  on enc.DISCH_DISP_C = vmd.DISCH_DISP_C
--left join pa.ge.ZC_ACCT_BASECLS_HA_C zab
--  on enc.ACCT_BASECLS_HA_C = zab.ACCT_BASECLS_HA_C
left join PA.ge.ZC_PAT_CLASS zpc with (nolock)
  on enc.ADT_PAT_CLASS_C = zpc.ADT_PAT_CLASS_C 
left join pa.ge.ZC_ED_DISPOSITION zed with (nolock)
  on enc.ED_DISPOSITION_C = zed.ED_DISPOSITION_C
left join pa.ge.CLARITY_SER csa with (nolock)
  on enc.ADMISSION_PROV_ID = csa.PROV_ID
left join pa.ge.CLARITY_SER csd with (nolock)
  on enc.DISCHARGE_PROV_ID = csd.PROV_ID
left join pa.ge.ZC_FIN_CLASS zfc with (nolock)
  on enc.ACCT_FIN_CLASS_C = zfc.FIN_CLASS_C
left join pa.ge.PATIENT pt with (nolock)
  on enc.PAT_ID = pt.PAT_ID
--left join map.LOSGroupMap lgm
--  on LengthOfStayNowIfNull = lgm.[Key]
left join pa.ge.CLARITY_ADT cai with (nolock)
  on enc.INP_ADM_EVENT_ID = cai.EVENT_ID
left join pa.ge.CLARITY_ADT cae with (nolock)
  on enc.EMER_ADM_EVENT_ID = cae.EVENT_ID
 
--print datediff(second, @start, getdate())
--print '--------- table complete - adding indices ---------'
--set @start = getdate()

 alter table gt2.HospitalEncounter add primary key (PAT_ENC_CSN_ID)
 create index ncix__HospitalEncounter__PAT_ENC_CSN_ID on gt2.HospitalEncounter(PAT_ENC_CSN_ID) include (DEPARTMENT_ID, ADTPatientStatusID, HospitalAdmissionTime, HospitalAdmissionDate, HospitalDischargeTime)
 create index ncix__HospitalEncounter__HSP_ACCOUNT_ID on gt2.HospitalEncounter(HSP_ACCOUNT_ID)
 create index ncix__HospitalEncounter__PAT_ID on gt2.HospitalEncounter(PAT_ID)
 create index ncix__HospitalEncounter__HospitalAdmissionDate on gt2.HospitalEncounter (HospitalAdmissionDate) include (PAT_ENC_CSN_ID, PAT_ID, DischargeDepartment, DischargeDisposition, HospitalAdmissionTime, HospitalDischargeTime, Birthdate, Age, Adult)
 create index ncix__HospitalEncounter__HospitalAdmissionTime on gt2.hospitalEncounter (HospitalAdmissionTime) include (PAT_ENC_CSN_ID, DEPARTMENT_ID, ADTPatientStatusID, HospitalAdmissionDate, HospitalDischargeTime)
 

--print datediff(second, @start, getdate())
--print '--------- indexing complete ---------'
 
-- check keys and indexes
exec tools.CheckKeysAndIndexes











GO
