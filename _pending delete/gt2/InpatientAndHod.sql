alter procedure [gt2].[InpatientAndHod] as

--select * from paval.tools.GetMissingIndexInformation order by 5 desc

declare @UnknownLabel varchar(8) = '*Unknown';
declare @UnspecifiedLabel varchar(12) = '*Unspecified';
declare @NotApplicable varchar(15) = '*Not Applicable';
declare @SelfPayLabel varchar(8) = 'Self-pay';

declare @ApplicationStartDate datetime = variables.GetSingleValue('@vcgApplicationStartDate');

declare @ADTCancelledEvent tinyint = ( select variables.GetSingleValue('@vcgADTCancelledEvent'))
;

declare @TECDepartmentID tinyint = ( select variables.GetSingleValue('@vcgTECDepartmentID'))
;
declare @TransferInEventTypeID tinyint = ( select variables.GetSingleValue('@vcgTransferInEventTypeID'))
;

declare @AdmissionPreAdmitPtStatus tinyint = ( select variables.GetSingleValue('@vcgAdmissionPreAdmitPtStatus'))
;
declare @CompletedPatientStatus tinyint = ( select variables.GetSingleValue('CompletedPatientStatus'))
;
declare @OutpatientAccountBaseClass tinyint = ( select variables.GetSingleValue('@vcgOutpatientAccountBaseClass'))
;
declare @InpatientBaseClassID tinyint = ( select variables.GetSingleValue('@vcgInpatientBaseClassID'))
;
declare @RecurringVisitAccountClass tinyint = ( select variables.GetSingleValue('@vcgRecurringVisitAccountClass'))
;
declare @DeceasedDisposition tinyint = ( select variables.GetSingleValue('@vcgDeceasedDisposition'))
;

declare @AdmissionExcludeConfStatus table ( ADMIT_CONF_STAT_C tinyint )
;
insert into @AdmissionExcludeConfStatus
select *
from variables.GetTableValues('@vcgAdmissionExcludeConfStatus')
;

declare @OutpatientExcludeService table ( PRIM_SVC_HA_C varchar(66))
;
insert into @OutpatientExcludeService
select *
from variables.GetTableValues('@vcgOutpatientExcludeService')
;

declare @BillingStatusToExclude table ( ACCT_BILLSTS_HA_C tinyint )
;
insert into @BillingStatusToExclude
select *
from variables.GetTableValues('@vcgBillingStatusToExclude')
;

declare @AdmissionExcludePtStatus table ( ADT_PATIENT_STAT_C tinyint )
;
insert into @AdmissionExcludePtStatus
select *
from variables.GetTableValues('@vcgAdmissionExcludePtStatus')
;

declare @OutpatientExcludeConfStatus table ( ADMIT_CONF_STAT_C tinyint )
;
insert into @OutpatientExcludeConfStatus
select *
from variables.GetTableValues('@vcgOutpatientExcludeConfStatus')
;

declare @InpatientExcludeService table ( HOSP_SERV_C varchar(66))
;
insert into @InpatientExcludeService
select *
from variables.GetTableValues('@vcgInpatientExcludeService')
;

declare @AdmitType table (
  ACCT_CLASS_HA_C varchar(66)
, Description varchar(80))
;

insert into @AdmitType
select
  VariableValue
, 'Acute'
from variables.GetTableValues('@vcgInpatientAcuteAccount')
union all
select
  VariableValue
, 'Ambulatory Overnight'
from variables.GetTableValues('@vcgInpatientAmbOverAccount')
union all
select
  VariableValue
, 'Observation'
from variables.GetTableValues('@vcgInpatientObsAccount')
union all
select
  VariableValue
, 'TSU Inpatient'
from variables.GetTableValues('@vcgInpatientSNFAccount')
union all
select
  VariableValue
, 'Rehab Inpatient'
from variables.GetTableValues('@vcgInpatientRehabAccount')
;


declare @EDDispositionGroup table (
  ED_DISPOSITION_C varchar(66)
, Description varchar(80))
;

insert into @EDDispositionGroup
select
  VariableValue
, 'Discharge'
from variables.GetTableValues('@vcgEDDischargeDisposition')
union all
select
  VariableValue
, 'Admit'
from variables.GetTableValues('@vcgEDAdmitDisposition')
union all
select
  VariableValue
, 'AMA'
from variables.GetTableValues('@vcgEDAMADisposition')
union all
select
  VariableValue
, 'LWBS'
from variables.GetTableValues('@vcgEDLWBSDisposition')
union all
select
  VariableValue
, 'LBTC'
from variables.GetTableValues('@vcgEDLBTCDisposition')
union all
select
  VariableValue
, 'Eloped'
from variables.GetTableValues('@vcgEDElopedDisposition')
;

declare @LastUpdateTS datetime = getdate();

if object_id('gt2.EncounterBase', 'U') is not null
  drop table gt2.EncounterBase
  ;

with FirstAdmit
as ( select distinct
       adt.PAT_ENC_CSN_ID
     , first_value(adt.FROM_TIME) over ( partition by adt.PAT_ENC_CSN_ID order by adt.FROM_TIME )                         as FirstAdmitDepartmentTime
     , first_value(adt.DEPARTMENT_ID) over ( partition by adt.PAT_ENC_CSN_ID order by adt.FROM_TIME )                     as FirstAdmitAllowedDepartmentID
     , first_value(adt.EVENT_ID) over ( partition by adt.PAT_ENC_CSN_ID order by adt.FROM_TIME )                          as FirstAdmitAllowedEvent
     from gt1.ADTInterval adt
)
, HospitalLocation as (
select distinct
  rgs.DEPARTMENT_ID
, rgs.RPT_GRP_SIX as HospitalLocationID
, IsNull( rgs.HospitalLocation, @UnknownLabel ) as HospitalLocation
, rgs.REV_LOC_ID as LocationID
, Iif( rgs.REV_LOC_ID is null, @UnknownLabel, rgs.Location ) as Location
, Iif( rgs.RPT_GRP_SIX is null, @UnknownLabel, rgs.HospitalEntity ) as HospitalEntity
from variables.RptGrpSix rgs
)
select
  -- top 10
  --   count(1)
  peh.PAT_ENC_CSN_ID                                                                                                      as EncounterCSN
, hspa.PRIM_ENC_CSN_ID                                                                                                    as PrimaryHospitalAccountCSN
, peh.ACCOM_REASON_C                                                                                                      as AccommodationReasonID
, iif(peh.ACCOM_REASON_C is null, @UnspecifiedLabel, isnull(zc_accom.NAME, @UnknownLabel))                                as AccommodationReason
, dep.SERV_AREA_ID                                                                                                        as ServiceAreaID
, peh.BED_ID                                                                                                              as BedId
, peh.PAT_ID                                                                                                              as PatientId
, peh.HSP_ACCOUNT_ID                                                                                                      as HospitalAccountId
, hspa.ACCT_BASECLS_HA_C                                                                                                  as AccountBaseClassId
, peh.HOSP_SERV_C                                                                                                         as HospitalEncounterServiceID
, iif(peh.HOSP_SERV_C is null, @UnspecifiedLabel, isnull(zc_pat_service.NAME, @UnknownLabel))                             as HospitalEncounterService
, hspa.PRIMARY_PAYOR_ID                                                                                                   as HARPrimaryPayorID
, iif(hspa.PRIMARY_PAYOR_ID is null, @SelfPayLabel, isnull( epm.PAYOR_NAME, @UnknownLabel))                               as HARPrimaryPayor
, iif(hspa.PRIMARY_PAYOR_ID is null, @SelfPayLabel, iif(zfc.FINANCIAL_CLASS is null, @UnknownLabel, zfc.NAME))            as HARPrimaryPayorFinancialClass
, iif(hspa.PRIMARY_PLAN_ID is null, @SelfPayLabel, iif(epp.RPT_GRP_ONE is null, @UnknownLabel, epp.RPT_GRP_ONE))          as HARPrimaryPayorAbbr
, hspa.PRIM_SVC_HA_C                                                                                                      as PrimaryHospitalAccountServiceID
, iif(hspa.PRIM_SVC_HA_C is null, @UnspecifiedLabel, isnull(zps.NAME, @UnknownLabel))                                     as PrimaryHospitalAccountService
, hspa.ACCT_CLASS_HA_C                                                                                                    as HospitalAccountClassID
, iif(hspa.ACCT_CLASS_HA_C is null, @UnspecifiedLabel, isnull(zc_acct_class.NAME, @UnknownLabel))                         as HospitalAccountClass
, iif(peh.ADT_PATIENT_STAT_C = @CompletedPatientStatus, 1, 0)                                                             as CompletedPatientStatus
, iif(peh.ADT_PATIENT_STAT_C = @AdmissionPreAdmitPtStatus, 1, 0)                                                          as PreAdmit
, iif(InpatientExcludeService.HOSP_SERV_C is null, 1, 0)                                                                  as InpatientService
, iif(OutpatientExcludeService.PRIM_SVC_HA_C is null, 1, 0)                                                               as OutpatientService
, iif(BillingStatusToExclude.ACCT_BILLSTS_HA_C is null, 1, 0)                                                             as BillingIncomplete
, iif(hspa.ACCT_BASECLS_HA_C = @OutpatientAccountBaseClass, 1, 0)                                                         as OutpatientAccount
, iif( sk.PAT_ENC_CSN_ID is null
       and hspa.ACCT_CLASS_HA_C = @RecurringVisitAccountClass
       and hspa.ACCT_BASECLS_HA_C = @OutpatientAccountBaseClass, 1, 0)                                                    as RecurringVisitAccount
, peh.ED_EPISODE_ID                                                                                                       as EDEpisodeID
, peh.ED_DISPOSITION_C                                                                                                    as EDDispositionID
, iif(peh.ED_DISPOSITION_C is null, @UnspecifiedLabel, isnull(zc_ed_disp.NAME, @UnknownLabel))                            as EDDispositionType
, EDDispositionGroup.Description                                                                                          as EDDispositionGroup
, peh.INPATIENT_DATA_ID                                                                                                   as InpatientDataId
, peh.ADT_ARRIVAL_TIME                                                                                                    as ArrivalTime
, cast(peh.ADT_ARRIVAL_TIME as date)                                                                                      as ArrivalDate
, cast(peh.INP_ADM_DATE as date)                                                                                          as IPAdmitDate
, cast(peh.OP_ADM_DATE as date)                                                                                           as OPAdmitDate
, peh.ED_DEPARTURE_TIME                                                                                                   as EDDepartureTime
, cast(peh.ED_DEPARTURE_TIME as date)                                                                                     as EDDepartureDate
, peh.EMER_ADM_DATE                                                                                                       as EmergencyAdmitDate
, peh.HOSP_ADMSN_TIME                                                                                                     as AdmitTime
, cast(peh.HOSP_ADMSN_TIME as date)                                                                                       as AdmitDate
, peh.HOSP_DISCH_TIME                                                                                                     as DischargeTime
, cast(peh.HOSP_DISCH_TIME as date)                                                                                       as DischargeDate
, iif(hspa.ACCT_BASECLS_HA_C = @InpatientBaseClassID, 1, 0)                                                               as InpatientAccount
, iif(AdmissionExcludeConfStatus.ADMIT_CONF_STAT_C is null, 1, 0)                                                         as AdmissionConfirmed
, iif(
    AdmissionExcludePtStatus.ADT_PATIENT_STAT_C is null
    and InpatientExcludeService.HOSP_SERV_C is null
    and hspa.ACCT_BASECLS_HA_C = @InpatientBaseClassID
    and AdmissionExcludeConfStatus.ADMIT_CONF_STAT_C is null
  , 1
  , 0)                                                                                                                    as InpatientConfirmedAdmission
, iif(
    OutpatientExcludeService.PRIM_SVC_HA_C is null
    and InpatientExcludeService.HOSP_SERV_C is null
    and hspa.ACCT_BASECLS_HA_C = @OutpatientAccountBaseClass
    and hspa.ACCT_CLASS_HA_C <> @RecurringVisitAccountClass
    and OutpatientExcludeConfStatus.ADMIT_CONF_STAT_C is null
  , 1
  , 0)                                                                                                                    as OutpatientConfirmedVisit
, peh.ADMIT_SOURCE_C                                                                                                      as AdmitSourceId
, peh.ADMISSION_PROV_ID                                                                                                   as AdmitProviderId
, peh.DISCHARGE_PROV_ID                                                                                                   as DischargeProviderId
, iif(peh.DISCH_DISP_C = @DeceasedDisposition, 1, 0)                                                                      as DeceasedPatient
, peh.DEPARTMENT_ID                                                                                                       as LastDepartmentId
, hspa.DISCH_DEPT_ID                                                                                                      as DischargeDepartmentID
, peh.DISCH_DISP_C                                                                                                        as DischargeDispositionID
, iif(peh.DISCH_DISP_C is null, @UnspecifiedLabel, isnull(zdd.NAME, @UnknownLabel))                                       as DischargeDisposition
, peh.ADT_PAT_CLASS_C                                                                                                     as PatientClassId
, iif(peh.ADT_PAT_CLASS_C is null, @UnspecifiedLabel, isnull(zc_patclass.NAME, @UnknownLabel))                            as PatientClass
, FirstAdmit.FirstAdmitDepartmentTime
, FirstAdmit.FirstAdmitAllowedDepartmentID
, FirstAdmit.FirstAdmitAllowedEvent

, AdmitType.Description                                                                                                   as AdmitType

, iif(sk.PAT_ENC_CSN_ID is not null, 'Schuylkill', isnull(HospitalLocation.HospitalLocation, @UnknownLabel))              as HospitalLocation
, iif(sk.PAT_ENC_CSN_ID is not null, 'Schuylkill', isnull(HospitalLocation.Location, @UnknownLabel))                      as Location
, iif(sk.PAT_ENC_CSN_ID is not null, 'Schuylkill', isnull(HospitalLocation.HospitalEntity, @UnknownLabel))                as HospitalEntity
, iif(sk.PAT_ENC_CSN_ID is not null, 'Schuylkill', isnull(AdmitHospitalLocation.HospitalLocation, @UnknownLabel))         as AdmitHospital
, iif(sk.PAT_ENC_CSN_ID is not null, 'Schuylkill', isnull(AdmitHospitalLocation.Location, @UnknownLabel))                 as AdmitLocation
, iif(sk.PAT_ENC_CSN_ID is not null, 'Schuylkill', isnull(AdmitHospitalLocation.HospitalEntity, @UnknownLabel))           as AdmitHospitalEntity
, iif(sk.PAT_ENC_CSN_ID is not null, 'Schuylkill', isnull(DischargeHospitalLocation.HospitalLocation, @UnknownLabel))     as DischargeHospital
, iif(sk.PAT_ENC_CSN_ID is not null, 'Schuylkill', isnull(DischargeHospitalLocation.Location, @UnknownLabel))             as DischargeLocation
, iif(sk.PAT_ENC_CSN_ID is not null, 'Schuylkill', isnull(DischargeHospitalLocation.HospitalEntity, @UnknownLabel))       as DischargeHospitalEntity

, iif(cast(peh.INP_ADM_DATE as date) = cast(peh.HOSP_DISCH_TIME as date)
    , 1
    , datediff(dd, peh.INP_ADM_DATE, peh.HOSP_DISCH_TIME))                                                                as IPLOS
, iif(cast(peh.INP_ADM_DATE as date) = cast(peh.HOSP_DISCH_TIME as date), 1, 0)                                           as SameDayStay
, peh.ADT_PATIENT_STAT_C                                                                                                  as ADTPatientStatusCode
, hspaloc.RPT_GRP_SIX                                                                                                     as HARLocationID
, @LastUpdateTS                                                                                                           as LastUpdateTS
into gt2.EncounterBase -- Todo: rewrite as insert into
from ge.PAT_ENC_HSP as peh
  left join palvhs.dbo.PAT_ENC_HSP sk
    on peh.PAT_ENC_CSN_ID = sk.PAT_ENC_CSN_ID
  Left join ge.PATIENT_3 p
    on peh.PAT_ID = p.PAT_ID
  left join ge.HSP_ACCOUNT as hspa
    on hspa.HSP_ACCOUNT_ID = peh.HSP_ACCOUNT_ID
  left join ge.CLARITY_LOC hspaloc
    on hspaloc.LOC_ID = hspa.LOC_ID
  left join FirstAdmit
    on FirstAdmit.PAT_ENC_CSN_ID = peh.PAT_ENC_CSN_ID
  left join ge.CLARITY_DEP dep
    on dep.DEPARTMENT_ID = peh.DEPARTMENT_ID
  left join ge.ZC_ACCOMMODATION zc_accom
    on peh.ACCOM_REASON_C = zc_accom.ACCOMMODATION_C
  left join ge.ZC_ED_DISPOSITION zc_ed_disp
    on zc_ed_disp.ED_DISPOSITION_C = peh.ED_DISPOSITION_C
  left join ge.ZC_PAT_CLASS zc_patclass
    on zc_patclass.ADT_PAT_CLASS_C = peh.ADT_PAT_CLASS_C
  left join ge.ZC_PAT_SERVICE zc_pat_service
    on zc_pat_service.HOSP_SERV_C = peh.HOSP_SERV_C
  --  left join ge.ZC_PRIM_SVC_HA zc_prim_svc
  --   on zc_prim_svc.PRIM_SVC_HA_C = hspa.PRIM_SVC_HA_C
  left join ge.ZC_ACCT_CLASS_HA zc_acct_class
    on hspa.ACCT_CLASS_HA_C = zc_acct_class.ACCT_CLASS_HA_C
  left join ge.ZC_DISCH_DISP as zdd
    on zdd.DISCH_DISP_C = peh.DISCH_DISP_C
  left join @AdmissionExcludeConfStatus AdmissionExcludeConfStatus
    on AdmissionExcludeConfStatus.ADMIT_CONF_STAT_C = peh.ADMIT_CONF_STAT_C
  left join @OutpatientExcludeService OutpatientExcludeService
    on OutpatientExcludeService.PRIM_SVC_HA_C = hspa.PRIM_SVC_HA_C
  left join @BillingStatusToExclude BillingStatusToExclude
    on BillingStatusToExclude.ACCT_BILLSTS_HA_C = hspa.ACCT_BILLSTS_HA_C
  left join @AdmissionExcludePtStatus AdmissionExcludePtStatus
    on AdmissionExcludePtStatus.ADT_PATIENT_STAT_C = peh.ADT_PATIENT_STAT_C
  left join @OutpatientExcludeConfStatus OutpatientExcludeConfStatus
    on OutpatientExcludeConfStatus.ADMIT_CONF_STAT_C = peh.ADMIT_CONF_STAT_C
  left join @InpatientExcludeService InpatientExcludeService
    on InpatientExcludeService.HOSP_SERV_C = peh.HOSP_SERV_C
  left join @AdmitType AdmitType
    on AdmitType.ACCT_CLASS_HA_C = hspa.ACCT_CLASS_HA_C
  left join @EDDispositionGroup EDDispositionGroup
    on EDDispositionGroup.ED_DISPOSITION_C = peh.ED_DISPOSITION_C  
  left join ge.CLARITY_EPP as epp
    on epp.BENEFIT_PLAN_ID = hspa.PRIMARY_PLAN_ID
  left join ge.CLARITY_EPM as epm
    on hspa.PRIMARY_PAYOR_ID = epm.PAYOR_ID
  left join ge.ZC_FINANCIAL_CLASS zfc
    on zfc.FINANCIAL_CLASS = epm.FINANCIAL_CLASS
  left join ge.ZC_PAT_SERVICE as zps
    on zps.HOSP_SERV_C = hspa.PRIM_SVC_HA_C
  left join HospitalLocation
    on peh.DEPARTMENT_ID = HospitalLocation.DEPARTMENT_ID
  left join HospitalLocation AdmitHospitalLocation
    on isnull(FirstAdmit.FirstAdmitAllowedDepartmentID, peh.DEPARTMENT_ID) = AdmitHospitalLocation.DEPARTMENT_ID 
  left join HospitalLocation DischargeHospitalLocation
    on isnull(FirstAdmit.FirstAdmitAllowedDepartmentID, peh.DEPARTMENT_ID) = DischargeHospitalLocation.DEPARTMENT_ID
where peh.HOSP_ADMSN_TIME >= @ApplicationStartDate
  and (p.IS_TEST_PAT_YN is null or p.IS_TEST_PAT_YN = 'N')
;

create nonclustered index ncix__EncounterBase__EncounterCSN
on gt2.EncounterBase ( EncounterCSN )
;
create nonclustered index ncix__EncounterBase__InpatientConfirmedAdmission
on gt2.EncounterBase ( InpatientConfirmedAdmission )
;
create nonclustered index ncix__EncounterBase__EDEpisodeID
on gt2.EncounterBase ( EDEpisodeID )
;
create nonclustered index ncix__EncounterBase__CompletedPatientStatus
on gt2.EncounterBase ( CompletedPatientStatus )
;
create nonclustered index ncix__EncounterBase__BillingIncomplete
on gt2.EncounterBase ( BillingIncomplete )
;
create nonclustered index ncix__EncounterBase__HospitalAccountID
on gt2.EncounterBase ( HospitalAccountId )
;
create nonclustered index ncix__EncounterBase__RecurringVisitAccount
on gt2.EncounterBase ( RecurringVisitAccount )
;
create nonclustered index ncix__EncounterBase__OutpatientConfirmedVisit
on gt2.EncounterBase ( OutpatientConfirmedVisit )
;

-- gt2.OutpatientEncounter

if object_id('gt2.OutpatientEncounter', 'U') is not null
  drop table gt2.OutpatientEncounter
  ;

with hbtx
as ( select
       hbtx.HospitalAccount                                                                                               as HospitalAccountID
     , hbtx.ServiceDate                                                                                                   as RecurringServiceDate
     , first_value(hbtx.TxID) over ( partition by
                                       hbtx.HospitalAccount
                                     , hbtx.ServiceDate
                                     order by hbtx.TxID )                                                                 as RecurringTransactionID
     , first_value(hbtx.DepartmentID) over ( partition by
                                               hbtx.HospitalAccount
                                             , hbtx.ServiceDate
                                             order by hbtx.TxID )                                                         as RecurringDepartmentID
     , first_value(hbtx.ServiceArea) over ( partition by
                                              hbtx.HospitalAccount                                                        
                                            , hbtx.ServiceDate
                                            order by hbtx.TxID )                                                          as RecurringServiceArea
     from gt1.HBTransactions as hbtx
       inner join gt2.EncounterBase as eb
         on eb.HospitalAccountId = hbtx.HospitalAccount
            and eb.RecurringVisitAccount = 1
     where hbtx.TxID is not null
           and hbtx.TxTypeID = 1
           and hbtx.Quantity > 0
           and hbtx.ServiceDate < getdate())
select
  eb.EncounterCSN
, eb.PrimaryHospitalAccountCSN
, eb.PatientId
, eb.HARPrimaryPayorID
, eb.HARPrimaryPayor
, eb.HARPrimaryPayorAbbr
, eb.HARPrimaryPayorFinancialClass
, eb.HospitalAccountId
, eb.HospitalEncounterServiceID
, eb.HospitalAccountClass
, eb.PrimaryHospitalAccountServiceID
, eb.PrimaryHospitalAccountService
, eb.AdmitDate
, eb.DischargeDate
, eb.OutpatientAccount
, eb.RecurringVisitAccount
, eb.OutpatientConfirmedVisit
, eb.LastDepartmentId                                                                                                     as VisitDepartmentID
, eb.Location
, eb.HospitalLocation
, eb.HospitalEntity
, eb.AdmitHospital
, eb.AdmitLocation
, eb.AdmitHospitalEntity
, eb.DischargeHospital
, eb.DischargeLocation
, eb.DischargeHospitalEntity
, eb.DischargeProviderId
, eb.AdmitProviderId
, fsa.PROV_ID                                                                                                             as VisitProviderId
, isnull(fsa.PROV_ID, isnull(eb.AdmitProviderId, eb.DischargeProviderId))                                                 as ProviderId
, hbtx.RecurringServiceDate
, hbtx.RecurringTransactionID
, hbtx.RecurringDepartmentID
, hbtx.RecurringServiceArea
, iif( eb.HospitalLocation = 'Schuylkill', 'Schuylkill', cp.PRC_NAME )                                                    as VisitType
, @LastUpdateTS                                                                                                           as LastUpdateTS
into gt2.OutpatientEncounter
from gt2.EncounterBase as eb
  left join hbtx
    on eb.HospitalAccountId = hbtx.HospitalAccountID
       and eb.RecurringVisitAccount = 1
  left join ge.F_SCHED_APPT as fsa
    on eb.EncounterCSN = fsa.PAT_ENC_CSN_ID
  left join ge.CLARITY_PRC as cp
    on cp.PRC_ID = fsa.PRC_ID
where ( eb.OutpatientConfirmedVisit = 1
        and eb.RecurringVisitAccount <> 1 )
      or ( eb.RecurringVisitAccount = 1
           and hbtx.RecurringTransactionID is not null )
;

/*
-- gt2.PatientDayTransaction

declare @RevenueCodeExcludeCensus table ( RevenueCode varchar(30))
;
insert into @RevenueCodeExcludeCensus
select *
from variables.GetTableValues('RevenueCodeExcludeCensus')
;
declare @PtDayRevenueCodeMin varchar(30) = variables.GetSingleValue('PtDayRevenueCodeMin')
;
declare @PtDayRevenueCodeMax varchar(30) = variables.GetSingleValue('PtDayRevenueCodeMax')
;


if object_id('gt2.PatientDayTransaction', 'U') is not null
  drop table gt2.PatientDayTransaction
  ;

select
  hbtx.HospitalAccount                                                                           as HospitalAccountID
, hbtx.TxID                                                                                      as TransactionID
, hbtx.InactiveTxInd
, hbtx.TxTypeID
, hbtx.ServiceDate
, hbtx.Quantity
, hbtx.HBChargeAmount
, hbtx.CostCenterID
, hbtx.DepartmentID                                                                              as ServiceDepartmentId
, hbtx.ServiceArea
, hbtx.RevenueCode
--, hbtx.CostCenterAssociatedDepartmentID
, hbtx.ProcedureCode
, iif(RevenueCodeExcludeCensus.RevenueCode is not null, 1, 0)                                    as RevenueCodeExcludeCensus
, iif(hbtx.RevenueCode > @PtDayRevenueCodeMin and hbtx.RevenueCode < @PtDayRevenueCodeMax, 1, 0) as RevenueCodeCensus
, eb.PrimaryHospitalAccountCSN
, eb.AdmitDate
, eb.AccountBaseClassId
, eb.InpatientService
, eb.PrimaryHospitalAccountServiceID
, eb.PrimaryHospitalAccountService
, eb.InpatientAccount
, eb.PatientClass
, eb.HARPrimaryPayorID
, eb.HARPrimarPayor
, eb.Payor
into gt2.PatientDayTransaction
from gt1.HBTransactions as hbtx
  inner join gt2.EncounterBase as eb
    on eb.HospitalAccountId = hbtx.HospitalAccount
  left join @RevenueCodeExcludeCensus RevenueCodeExcludeCensus
    on hbtx.RevenueCode = RevenueCodeExcludeCensus.RevenueCode
where 1 = 1
      and hbtx.TxTypeID = 1
      and hbtx.TxID > 0
      and hbtx.InactiveTxInd <> 1
--and CostCenterAssociatedDepartmentID > 0
--and RevenueCodeExcludeCensus.RevenueCode is null
--  and hbtx.RevenueCode > @PtDayRevenueCodeMin and hbtx.RevenueCode < @PtDayRevenueCodeMax
;
*/
 

go

if object_id('gt2.AdmissionEncounter', 'V') is not null
drop view gt2.AdmissionEncounter
;

go

create view gt2.AdmissionEncounter
as
select
*
, iif(
eb.AdmissionConfirmed = 1
and eb.PreAdmit <> 1
and eb.BillingIncomplete <> 1
and eb.EDEpisodeID is not null
and eb.EmergencyAdmitDate is not null
, 1
, 0) as EDArrival
, iif(
eb.AdmissionConfirmed = 1
and eb.PreAdmit <> 1
and eb.BillingIncomplete <> 1
and eb.EDEpisodeID is not null
and eb.IPAdmitDate is not null
, 1
, 0) as EDIPAdmit
, iif(
eb.AdmissionConfirmed = 1
and eb.PreAdmit <> 1
and eb.BillingIncomplete <> 1
and eb.EDEpisodeID is not null
and eb.OPAdmitDate is not null
and eb.IPAdmitDate is null
, 1
, 0) as EDOPAdmit
from gt2.EncounterBase as eb
where eb.InpatientConfirmedAdmission = 1
or eb.EDEpisodeID is not null
or eb.CompletedPatientStatus = 1
or eb.BillingIncomplete <> 1
;

go