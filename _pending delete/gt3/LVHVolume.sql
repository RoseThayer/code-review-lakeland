alter procedure gt3.LVHVolume as 

/*
  Issues:
  - none of our HospitalEntity or HospitalLocation fields line up with the validation query.
    - validation query is looking at LOC_ID on HSP_ACCOUNT and getting the RPT_GRP_SIX from there
    - we look to either PAT_ENC_HSP.DEPARTMENT_ID or FirstAdmit.DEPARTMENT_ID (for Admit/Discharge hospital)
      - is there a bug in DischargeHospitalLocation logic? It is joining to FirstAdmit, like AdmitHospitalLocation, but
        shouldn't it join to the discharge department on PAT_ENC_HSP.DEPARTMENT_ID?
  - acute admits need additional rows removed. Validation query is removing financial divisions of newborn, IRF (Rehab) and
      TSU as well as NULL (basically anything that didn't get a financial division assignment). I have pulled out newborn,
      IRF and TSU but to remove NULL means we actually need to implement the financial division logic and then eliminate the
      rows without a financial division. To do so involves adding new tables.
  - Observation numbers tie out to Lehigh October 2016 validations
  - Ambulatory Overnight numbers tie out to Lehigh October 2016 validations when the duplicate HARs are removed from their
    validation file
  - there are Acute and Observation rows where the admit date is null, this should be addressed
*/

declare @UnknownLabel varchar(8) = '*Unknown';
declare @UnspecifiedLabel varchar(12) = '*Unspecified';
declare @NotApplicable varchar(15) = '*Not Applicable';
declare @SelfPayLabel varchar(8) = 'Self-pay';
declare @OtherLabel varchar(6) = '*Other';
declare @SchuylkillLabel varchar(10) = 'Schuylkill';

declare @DeceasedDisposition tinyint = (select variables.GetSingleValue('@vcgDeceasedDisposition'));

declare @OutpatientAccountBaseClass tinyint = (select variables.GetSingleValue('@vcgOutpatientAccountBaseClass'));

declare @InpatientBaseClassID tinyint = (select variables.GetSingleValue('@vcgInpatientBaseClassID'));

declare @RecurringVisitAccountClass tinyint = (select variables.GetSingleValue('@vcgRecurringVisitAccountClass'));

declare @AdmissionExcludeConfStatus table ( 
  ADMIT_CONF_STAT_C tinyint 
)
;
insert into @AdmissionExcludeConfStatus
select *
from variables.GetTableValues('@vcgAdmissionExcludeConfStatus')
;

declare @AdmissionExcludePtStatus table ( 
  ADT_PATIENT_STAT_C tinyint 
)
;
insert into @AdmissionExcludePtStatus
select *
from variables.GetTableValues('@vcgAdmissionExcludePtStatus')
;

declare @OutpatientExcludeConfStatus table ( 
  ADMIT_CONF_STAT_C tinyint 
)
;
insert into @OutpatientExcludeConfStatus
select *
from variables.GetTableValues('@vcgOutpatientExcludeConfStatus')
;

declare @OutpatientExcludeService table (   
  PRIM_SVC_HA_C varchar(66)
)
;
insert into @OutpatientExcludeService
select *
from variables.GetTableValues('@vcgOutpatientExcludeService')
;

declare @InpatientExcludeService table ( 
  HOSP_SERV_C varchar(66)
)
;
insert into @InpatientExcludeService
select *
from variables.GetTableValues('@vcgInpatientExcludeService')
;

declare @AdmitType table (
    ACCT_CLASS_HA_C varchar(66)
  , Description varchar(80)
)
;

insert into @AdmitType
select
    VariableValue
  , 'Acute'
from variables.GetTableValues('@vcgInpatientAcuteAccount')
--union all
--select 107, 'Acute' -- Newborn
--union all
--select 117, 'Acute' -- TSU Inpatient
--union all
--select 133, 'Acute' -- Rehab Inpatient
union all
select
    VariableValue
  , 'Ambulatory Overnight'
from variables.GetTableValues('@vcgInpatientAmbOverAccount')
union all
select 106, 'Ambulatory Overnight' -- Hospital Outpatient Surgery
union all
select 108, 'Ambulatory Overnight' -- Surgery Admit
union all
select
    VariableValue
  , 'Observation'
from variables.GetTableValues('@vcgInpatientObsAccount')
union all
select
    VariableValue
  , 'TSU Inpatient'
from variables.GetTableValues('@vcgInpatientSNFAccount')
union all
select
    VariableValue
  , 'Rehab Inpatient'
from variables.GetTableValues('@vcgInpatientRehabAccount')
;

declare @LastUpdateTS datetime = getdate();

if object_id('gt3.LVHEncounterVolume', 'U') is not null drop table gt3.LVHEncounterVolume;

-- Acute Admits (runtime: 1m)
with FirstAdmit as ( 
  select distinct
      adt.PAT_ENC_CSN_ID
    , first_value(adt.FROM_TIME) over ( 
        partition by adt.PAT_ENC_CSN_ID 
        order by adt.FROM_TIME 
      )                                                                     as FirstAdmitDepartmentTime
    , first_value(adt.DEPARTMENT_ID) over ( 
        partition by adt.PAT_ENC_CSN_ID 
        order by adt.FROM_TIME 
      )                                                                     as FirstAdmitAllowedDepartmentID
    , first_value(adt.EVENT_ID) over ( 
        partition by adt.PAT_ENC_CSN_ID 
        order by adt.FROM_TIME 
      )                                                                     as FirstAdmitAllowedEvent
  from gt1.ADTInterval adt
)
, HospitalLocation as (
  select distinct
      rgs.DEPARTMENT_ID
    , rgs.RPT_GRP_SIX                                                       as HospitalLocationID
    , IsNull(rgs.HospitalLocation, @UnknownLabel)                           as HospitalLocation
    , rgs.REV_LOC_ID                                                        as LocationID
    , Iif(rgs.REV_LOC_ID is null, @UnknownLabel, rgs.Location)              as Location
    , Iif(rgs.RPT_GRP_SIX is null, @UnknownLabel, rgs.HospitalEntity)       as HospitalEntity
  from variables.RptGrpSix rgs
)
select 
  --  case  
  --    when loc.RPT_GRP_SIX = 1
  --      then 'LVH'
  --    when loc.RPT_GRP_SIX = 2
  --      then 'LVHM'
  --    else '*Unknown'
  --  end as Entity
  --, case
  --    when har.ACCT_BASECLS_HA_C in (1)
  --      and har.ACCT_CLASS_HA_C not in (128, 129, 131, 132) 
  --    then 'Admit'
  --    else '*Unknown'
  --  end as Metric
  --, count(distinct har.HSP_ACCOUNT_ID)
    har.HSP_ACCOUNT_ID                                                      as HospitalAccountID
  , peh.PAT_ENC_CSN_ID                                                      as EncounterCSN
  , iif(sk.PAT_ENC_CSN_ID is not null
      , @SchuylkillLabel
      , isnull(HospitalLocation.HospitalLocation, @UnknownLabel)
    )                                                                       as HospitalLocation
  , iif(sk.PAT_ENC_CSN_ID is not null
      , @SchuylkillLabel
      , isnull(HospitalLocation.Location, @UnknownLabel)
    )                                                                       as Location
  , iif(sk.PAT_ENC_CSN_ID is not null
      , @SchuylkillLabel
      , isnull(HospitalLocation.HospitalEntity, @UnknownLabel)
    )                                                                       as HospitalEntity
  , iif(sk.PAT_ENC_CSN_ID is not null
      , @SchuylkillLabel
      , isnull(AdmitHospitalLocation.HospitalLocation, @UnknownLabel)
    )                                                                       as AdmitHospital
  , iif(sk.PAT_ENC_CSN_ID is not null
      , @SchuylkillLabel
      , isnull(AdmitHospitalLocation.HospitalEntity, @UnknownLabel)
    )                                                                       as AdmitHospitalEntity
  , iif(sk.PAT_ENC_CSN_ID is not null
      , @SchuylkillLabel
      , isnull(DischargeHospitalLocation.HospitalLocation, @UnknownLabel)
    )                                                                       as DischargeHospital
  , iif(sk.PAT_ENC_CSN_ID is not null
      , @SchuylkillLabel
      , isnull(DischargeHospitalLocation.HospitalEntity, @UnknownLabel)
    )                                                                       as DischargeHospitalEntity
  , iif(sk.PAT_ENC_CSN_ID is not null
      , @SchuylkillLabel
      , isnull(AdmitHospitalLocation.Location, @UnknownLabel)
    )                                                                       as AdmitLocation
  , iif(sk.PAT_ENC_CSN_ID is not null
      , @SchuylkillLabel
      , isnull(DischargeHospitalLocation.Location, @UnknownLabel)
    )                                                                       as DischargeLocation
  , dep.SERV_AREA_ID                                                        as ServiceAreaID
  , peh.BED_ID                                                              as BedId
  , peh.PAT_ID                                                              as PatientId
  , har.PRIMARY_PAYOR_ID                                                    as HARPrimaryPayorID
  , iif(har.PRIMARY_PAYOR_ID is null
      , @SelfPayLabel
      , isnull(epm.PAYOR_NAME, @UnknownLabel)
    )                                                                       as HARPrimaryPayor
  , iif(har.PRIMARY_PAYOR_ID is null
      , @SelfPayLabel
      , iif(zfc.FINANCIAL_CLASS is null
          , @UnknownLabel
          , zfc.NAME
        )
    )                                                                       as HARPrimaryPayorFinancialClass
  , iif(har.PRIMARY_PLAN_ID is null
      , @SelfPayLabel
      , iif(epp.RPT_GRP_ONE is null
          , @UnknownLabel
          , epp.RPT_GRP_ONE
        )
    )                                                                       as HARPrimaryPayorAbbr
  , har.ACCT_CLASS_HA_C                                                     as HospitalAccountClassID
  , iif(har.ACCT_CLASS_HA_C is null  
      , @UnspecifiedLabel
      , isnull(zc_acct_class.NAME, @UnknownLabel)
    )                                                                       as HospitalAccountClass
  --, isnull(AdmitType.Description, 'Acute ' + har.ACCT_CLASS_HA_C)           as AdmitType
  --, AdmitType.Description                                                   as AdmitType
  , cast('Acute' as varchar(80))                                            as AdmitType -- can't use the config variable as there are nuances beyond ACCT_CLASS_HA_C
  , case
      when EDADMITS.INP_ADM_DATE is not null
        then EDADMITS.INP_ADM_DATE
      else har3.IP_ADMIT_DATE_TIME
    end                                                                     as AdmitTime
  , case
      when EDADMITS.INP_ADM_DATE is not null
        then cast(EDADMITS.INP_ADM_DATE as date)
      else cast(har3.IP_ADMIT_DATE_TIME as date)
    end                                                                     as AdmitDate
  , peh.ADMIT_SOURCE_C                                                      as AdmitSourceId
  , peh.ADMISSION_PROV_ID                                                   as AdmitProviderId  
  , FirstAdmit.FirstAdmitAllowedDepartmentID
  , case
      when EDADMITS.DISCH_DATE_TIME is not null
        then EDADMITS.DISCH_DATE_TIME
      else peh.HOSP_DISCH_TIME -- should we be using HSP_ACCOUNT.DISCH_DATE_TIME instead? 
    end                                                                     as DischargeTime
  , case
      when EDADMITS.DISCH_DATE_TIME is not null
        then cast(EDADMITS.DISCH_DATE_TIME as date)
      else cast(peh.HOSP_DISCH_TIME as date) -- should we be using HSP_ACCOUNT.DISCH_DATE_TIME instead? 
    end                                                                     as DischargeDate
  , peh.DISCHARGE_PROV_ID                                                   as DischargeProviderId
  , peh.DEPARTMENT_ID                                                       as LastDepartmentId
  , har.DISCH_DEPT_ID                                                       as DischargeDepartmentID
  , peh.DISCH_DISP_C                                                        as DischargeDispositionID
  , iif(peh.DISCH_DISP_C is null
      , @UnspecifiedLabel
      , isnull(zdd.NAME, @UnknownLabel)
    )                                                                       as DischargeDisposition
  , iif(peh.DISCH_DISP_C = @DeceasedDisposition
      , 1
      , 0
    )                                                                       as DeceasedPatient
  , iif(AdmissionExcludeConfStatus.ADMIT_CONF_STAT_C is null
      , 1
      , 0
    )                                                                       as AdmissionConfirmed
  , iif(
      AdmissionExcludePtStatus.ADT_PATIENT_STAT_C is null
        and InpatientExcludeService.HOSP_SERV_C is null
        and har.ACCT_BASECLS_HA_C = @InpatientBaseClassID
        and AdmissionExcludeConfStatus.ADMIT_CONF_STAT_C is null
      , 1
      , 0
    )                                                                       as InpatientConfirmedAdmission
  , iif(
      OutpatientExcludeService.PRIM_SVC_HA_C is null
        and InpatientExcludeService.HOSP_SERV_C is null
        and har.ACCT_BASECLS_HA_C = @OutpatientAccountBaseClass
        and har.ACCT_CLASS_HA_C <> @RecurringVisitAccountClass
        and OutpatientExcludeConfStatus.ADMIT_CONF_STAT_C is null
    , 1
    , 0
    )                                                                       as OutpatientConfirmedVisit 
  , peh.ADT_PAT_CLASS_C                                                     as PatientClassId
  , iif(peh.ADT_PAT_CLASS_C is null
      , @UnspecifiedLabel
      , isnull(zc_patclass.NAME, @UnknownLabel)
    )                                                                       as PatientClass
  , iif(cast(peh.INP_ADM_DATE as date) = 
      case
        when EDADMITS.DISCH_DATE_TIME is not null
          then cast(EDADMITS.DISCH_DATE_TIME as date)
        else cast(peh.HOSP_DISCH_TIME as date) -- should we be using HSP_ACCOUNT.DISCH_DATE_TIME instead? 
      end 
      , 1
      , datediff(dd
          , peh.INP_ADM_DATE
          , case
              when EDADMITS.DISCH_DATE_TIME is not null
                then cast(EDADMITS.DISCH_DATE_TIME as date)
              else cast(peh.HOSP_DISCH_TIME as date) -- should we be using HSP_ACCOUNT.DISCH_DATE_TIME instead? 
            end 
        )
    )                                                                       as IPLOS
  , iif(cast(peh.INP_ADM_DATE as date) = 
      case
        when EDADMITS.DISCH_DATE_TIME is not null
          then cast(EDADMITS.DISCH_DATE_TIME as date)
        else cast(peh.HOSP_DISCH_TIME as date) -- should we be using HSP_ACCOUNT.DISCH_DATE_TIME instead? 
      end  
      , 1
      , 0
    )                                                                       as SameDayStay
  , case  
      when loc.RPT_GRP_SIX = 1
        then 'LVH'
      when loc.RPT_GRP_SIX = 2
        then 'LVHM'
      else '*Unknown'
    end                                                                     as ValidationHospitalLocation
  , cast('LVH Acute Logic' as varchar(25))                                  as Source
  , @LastUpdateTS                                                           as LastUpdateTS
into gt3.LVHEncounterVolume
from ge.HSP_ACCOUNT har
left join gt1.LVHFinanceDivisionByHAR findiv
  on har.HSP_ACCOUNT_ID = findiv.HSP_ACCOUNT_ID
left join (
  select distinct 
      har.HSP_ACCOUNT_ID
    , peh.INP_ADM_DATE
    , adt.EFFECTIVE_TIME
    , har.ADM_DATE_TIME
    , har.DISCH_DATE_TIME
  from ge.hsp_account har
  inner join ge.CLARITY_ADT adt 
    on har.PRIM_ENC_CSN_ID = adt.PAT_ENC_CSN_ID
  inner join ge.pat_enc_hsp peh 
    on har.PRIM_ENC_CSN_ID = peh.PAT_ENC_CSN_ID
  left join ge.HSP_ACCT_SBO sbo 
    on sbo.HSP_ACCOUNT_ID = har.HSP_ACCOUNT_ID
  where 
    adt.EVENT_TYPE_C = 1  -- admission
      and har.ADM_DATE_TIME < '2017-07-01' -- variable-ize, ask Chris G. what this represents
      and peh.INP_ADM_DATE >= '2017-07-01' -- variable-ize, ask Chris G. what this represents
      and adt.DEPARTMENT_ID in (
          10001079  -- cc ED
        , 10002029  -- 17 ed
        , 10003025  -- M-ED
      )
    and har.ACCT_BASECLS_HA_C = 1 -- inpatient
    and adt.EVENT_SUBTYPE_C <> 2  -- exclude cancelled events
    and sbo.SBO_HAR_TYPE_C = 0  -- single billing office HAR type: HB/mixed
) EDADMITS
  on EDADMITS.hsp_account_id = har.HSP_ACCOUNT_ID
left join ge.HSP_ACCT_SBO sbo 
    on sbo.HSP_ACCOUNT_ID = har.HSP_ACCOUNT_ID
left join ge.HSP_ACCOUNT_3 har3
  on har.HSP_ACCOUNT_ID = har3.HSP_ACCOUNT_ID
inner join ge.CLARITY_LOC loc         
  on loc.loc_id = har.loc_id
left join ge.pat_enc_hsp peh        
  on peh.hsp_account_id = har.hsp_account_id
left join ge.PATIENT pat
  on pat.PAT_ID = har.PAT_ID
left join ge.PATIENT_3 pat3
  on pat3.PAT_ID = har.PAT_ID
-- start of additional tables
left join palvhs.dbo.PAT_ENC_HSP sk
  on peh.PAT_ENC_CSN_ID = sk.PAT_ENC_CSN_ID
left join ge.CLARITY_DEP dep
  on dep.DEPARTMENT_ID = peh.DEPARTMENT_ID
left join ge.CLARITY_EPP as epp
  on epp.BENEFIT_PLAN_ID = har.PRIMARY_PLAN_ID
left join ge.CLARITY_EPM as epm
  on har.PRIMARY_PAYOR_ID = epm.PAYOR_ID
left join ge.ZC_FINANCIAL_CLASS zfc
  on zfc.FINANCIAL_CLASS = epm.FINANCIAL_CLASS
left join ge.ZC_ACCT_CLASS_HA zc_acct_class
  on har.ACCT_CLASS_HA_C = zc_acct_class.ACCT_CLASS_HA_C
left join ge.ZC_DISCH_DISP as zdd
    on zdd.DISCH_DISP_C = peh.DISCH_DISP_C
left join ge.ZC_PAT_CLASS zc_patclass
    on zc_patclass.ADT_PAT_CLASS_C = peh.ADT_PAT_CLASS_C
left join @AdmitType AdmitType
    on AdmitType.ACCT_CLASS_HA_C = har.ACCT_CLASS_HA_C
left join @AdmissionExcludeConfStatus AdmissionExcludeConfStatus
    on AdmissionExcludeConfStatus.ADMIT_CONF_STAT_C = peh.ADMIT_CONF_STAT_C
left join @AdmissionExcludePtStatus AdmissionExcludePtStatus
    on AdmissionExcludePtStatus.ADT_PATIENT_STAT_C = peh.ADT_PATIENT_STAT_C
left join @InpatientExcludeService InpatientExcludeService
    on InpatientExcludeService.HOSP_SERV_C = peh.HOSP_SERV_C
left join @OutpatientExcludeService OutpatientExcludeService
    on OutpatientExcludeService.PRIM_SVC_HA_C = har.PRIM_SVC_HA_C
left join @OutpatientExcludeConfStatus OutpatientExcludeConfStatus
    on OutpatientExcludeConfStatus.ADMIT_CONF_STAT_C = peh.ADMIT_CONF_STAT_C
left join FirstAdmit
  on FirstAdmit.PAT_ENC_CSN_ID = peh.PAT_ENC_CSN_ID
left join HospitalLocation
  on peh.DEPARTMENT_ID = HospitalLocation.DEPARTMENT_ID
left join HospitalLocation AdmitHospitalLocation
  on isnull(FirstAdmit.FirstAdmitAllowedDepartmentID, peh.DEPARTMENT_ID) = AdmitHospitalLocation.DEPARTMENT_ID 
left join HospitalLocation DischargeHospitalLocation
  on isnull(FirstAdmit.FirstAdmitAllowedDepartmentID, peh.DEPARTMENT_ID) = DischargeHospitalLocation.DEPARTMENT_ID
where
  har.ACCT_BASECLS_HA_C = 1 -- inpatient 
    and har.ACCT_CLASS_HA_C not in (128, 129, 131, 132) -- IP Hospice, Good Shepherd, IP Hospice (17th St.), Good Shepherd
    and not (
      findiv.FINANCE_DIVISION is null 
        or findiv.FINANCE_DIVISION = 'TSU'
        or findiv.FINANCE_DIVISION = 'IRF'
        or findiv.FINANCE_DIVISION = '0.New Born'
        or findiv.FINANCE_DIVISION = '0.New Born 2'
        or findiv.FINANCE_DIVISION = '0.New Born 3'
        or findiv.FINANCE_DIVISION = 'M TSU'
        or findiv.FINANCE_DIVISION = 'M IRF'
        or findiv.FINANCE_DIVISION = '0.M New Born'
        or findiv.FINANCE_DIVISION = '0.M New Born 2'
        or findiv.FINANCE_DIVISION = '0.M New Born 3'
    )
    and (
      (
        loc.rpt_grp_six = 1 -- LVH
          and har.SERV_AREA_ID = 10
          and sbo.SBO_HAR_TYPE_C = 0
          and har.COMBINE_ACCT_ID is null 
      ) or (
        loc.rpt_grp_six = 2 -- LVHM
          and peh.pat_id is not null 
      )
    )
    and (pat3.IS_TEST_PAT_YN = 'N' or pat3.IS_TEST_PAT_YN is null)
;

-- Ambulatory Overnight and Observation (runtime: )
with FirstAdmit as ( 
  select distinct
      adt.PAT_ENC_CSN_ID
    , first_value(adt.FROM_TIME) over ( 
        partition by adt.PAT_ENC_CSN_ID 
        order by adt.FROM_TIME 
      )                                                                     as FirstAdmitDepartmentTime
    , first_value(adt.DEPARTMENT_ID) over ( 
        partition by adt.PAT_ENC_CSN_ID 
        order by adt.FROM_TIME 
      )                                                                     as FirstAdmitAllowedDepartmentID
    , first_value(adt.EVENT_ID) over ( 
        partition by adt.PAT_ENC_CSN_ID 
        order by adt.FROM_TIME 
      )                                                                     as FirstAdmitAllowedEvent
  from gt1.ADTInterval adt
)
, HospitalLocation as (
  select distinct
      rgs.DEPARTMENT_ID
    , rgs.RPT_GRP_SIX                                                       as HospitalLocationID
    , IsNull(rgs.HospitalLocation, @UnknownLabel)                           as HospitalLocation
    , rgs.REV_LOC_ID                                                        as LocationID
    , Iif(rgs.REV_LOC_ID is null, @UnknownLabel, rgs.Location)              as Location
    , Iif(rgs.RPT_GRP_SIX is null, @UnknownLabel, rgs.HospitalEntity)       as HospitalEntity
  from variables.RptGrpSix rgs
)
insert into gt3.LVHEncounterVolume
select
    har.HSP_ACCOUNT_ID                                                      as HospitalAccountID
  , peh.PAT_ENC_CSN_ID                                                      as EncounterCSN
  , iif(sk.PAT_ENC_CSN_ID is not null
      , @SchuylkillLabel
      , isnull(HospitalLocation.HospitalLocation, @UnknownLabel)
    )                                                                       as HospitalLocation
  , iif(sk.PAT_ENC_CSN_ID is not null
      , @SchuylkillLabel
      , isnull(HospitalLocation.Location, @UnknownLabel)
    )                                                                       as Location
  , iif(sk.PAT_ENC_CSN_ID is not null
      , @SchuylkillLabel
      , isnull(HospitalLocation.HospitalEntity, @UnknownLabel)
    )                                                                       as HospitalEntity
  , iif(sk.PAT_ENC_CSN_ID is not null
      , @SchuylkillLabel
      , isnull(AdmitHospitalLocation.HospitalLocation, @UnknownLabel)
    )                                                                       as AdmitHospital
  , iif(sk.PAT_ENC_CSN_ID is not null
      , @SchuylkillLabel
      , isnull(AdmitHospitalLocation.HospitalEntity, @UnknownLabel)
    )                                                                       as AdmitHospitalEntity
  , iif(sk.PAT_ENC_CSN_ID is not null
      , @SchuylkillLabel
      , isnull(DischargeHospitalLocation.HospitalLocation, @UnknownLabel)
    )                                                                       as DischargeHospital
  , iif(sk.PAT_ENC_CSN_ID is not null
      , @SchuylkillLabel
      , isnull(DischargeHospitalLocation.HospitalEntity, @UnknownLabel)
    )                                                                       as DischargeHospitalEntity
  , iif(sk.PAT_ENC_CSN_ID is not null
      , @SchuylkillLabel
      , isnull(AdmitHospitalLocation.Location, @UnknownLabel)
    )                                                                       as AdmitLocation
  , iif(sk.PAT_ENC_CSN_ID is not null
      , @SchuylkillLabel
      , isnull(DischargeHospitalLocation.Location, @UnknownLabel)
    )                                                                       as DischargeLocation
  , dep.SERV_AREA_ID                                                        as ServiceAreaID
  , peh.BED_ID                                                              as BedId
  , peh.PAT_ID                                                              as PatientId
  , har.PRIMARY_PAYOR_ID                                                    as HARPrimaryPayorID
  , iif(har.PRIMARY_PAYOR_ID is null
      , @SelfPayLabel
      , isnull(epm.PAYOR_NAME, @UnknownLabel)
    )                                                                       as HARPrimaryPayor
  , iif(har.PRIMARY_PAYOR_ID is null
      , @SelfPayLabel
      , iif(zfc.FINANCIAL_CLASS is null
          , @UnknownLabel
          , zfc.NAME
        )
    )                                                                       as HARPrimaryPayorFinancialClass
  , iif(har.PRIMARY_PLAN_ID is null
      , @SelfPayLabel
      , iif(epp.RPT_GRP_ONE is null
          , @UnknownLabel
          , epp.RPT_GRP_ONE
        )
    )                                                                       as HARPrimaryPayorAbbr
  , har.ACCT_CLASS_HA_C                                                     as HospitalAccountClassID
  , iif(har.ACCT_CLASS_HA_C is null  
      , @UnspecifiedLabel
      , isnull(zc_acct_class.NAME, @UnknownLabel)
    )                                                                       as HospitalAccountClass
  --, isnull(AdmitType.Description, 'AO/OBV ' + har.ACCT_CLASS_HA_C)          as AdmitType
  , AdmitType.Description                                                   as AdmitType
  , peh.OP_ADM_DATE                                                         as AdmitTime
  , cast(peh.OP_ADM_DATE as date)                                           as AdmitDate
  , peh.ADMIT_SOURCE_C                                                      as AdmitSourceId
  , peh.ADMISSION_PROV_ID                                                   as AdmitProviderId  
  , FirstAdmit.FirstAdmitAllowedDepartmentID
  , peh.HOSP_DISCH_TIME                                                     as DischargeTime -- should we be using HSP_ACCOUNT.DISCH_DATE_TIME instead? 
  , cast(peh.HOSP_DISCH_TIME as date)                                       as DischargeDate -- should we be using HSP_ACCOUNT.DISCH_DATE_TIME instead? 
  , peh.DISCHARGE_PROV_ID                                                   as DischargeProviderId
  , peh.DEPARTMENT_ID                                                       as LastDepartmentId
  , har.DISCH_DEPT_ID                                                       as DischargeDepartmentID
  , peh.DISCH_DISP_C                                                        as DischargeDispositionID
  , iif(peh.DISCH_DISP_C is null
      , @UnspecifiedLabel
      , isnull(zdd.NAME, @UnknownLabel)
    )                                                                       as DischargeDisposition
  , iif(peh.DISCH_DISP_C = @DeceasedDisposition
      , 1
      , 0
    )                                                                       as DeceasedPatient
  , iif(AdmissionExcludeConfStatus.ADMIT_CONF_STAT_C is null
      , 1
      , 0
    )                                                                       as AdmissionConfirmed
  , iif(
      AdmissionExcludePtStatus.ADT_PATIENT_STAT_C is null
        and InpatientExcludeService.HOSP_SERV_C is null
        and har.ACCT_BASECLS_HA_C = @InpatientBaseClassID
        and AdmissionExcludeConfStatus.ADMIT_CONF_STAT_C is null
      , 1
      , 0
    )                                                                       as InpatientConfirmedAdmission
  , iif(
      OutpatientExcludeService.PRIM_SVC_HA_C is null
        and InpatientExcludeService.HOSP_SERV_C is null
        and har.ACCT_BASECLS_HA_C = @OutpatientAccountBaseClass
        and har.ACCT_CLASS_HA_C <> @RecurringVisitAccountClass
        and OutpatientExcludeConfStatus.ADMIT_CONF_STAT_C is null
    , 1
    , 0
    )                                                                       as OutpatientConfirmedVisit 
  , peh.ADT_PAT_CLASS_C                                                     as PatientClassId
  , iif(peh.ADT_PAT_CLASS_C is null
      , @UnspecifiedLabel
      , isnull(zc_patclass.NAME, @UnknownLabel)
    )                                                                       as PatientClass
  , iif(cast(peh.INP_ADM_DATE as date) = cast(peh.HOSP_DISCH_TIME as date) -- should we be using HSP_ACCOUNT.DISCH_DATE_TIME instead? 
      , 1
      , datediff(dd
          , peh.INP_ADM_DATE
          , cast(peh.HOSP_DISCH_TIME as date) -- should we be using HSP_ACCOUNT.DISCH_DATE_TIME instead?             
        )
    )                                                                       as IPLOS
  , iif(cast(peh.INP_ADM_DATE as date) = cast(peh.HOSP_DISCH_TIME as date) -- should we be using HSP_ACCOUNT.DISCH_DATE_TIME instead? 
      , 1
      , 0
    )                                                                       as SameDayStay
  , case  
      when loc.RPT_GRP_SIX = 1
        then 'LVH'
      when loc.RPT_GRP_SIX = 2
        then 'LVHM'
      else '*Unknown'
    end                                                                     as ValidationHospitalLocation
  , cast('LVH AO/OBV Logic' as varchar(25))                                 as Source
  , @LastUpdateTS                                                           as LastUpdateTS
from ge.HSP_ACCOUNT har
inner join ge.PAT_ENC_HSP peh
  on peh.HSP_ACCOUNT_ID = har.HSP_ACCOUNT_ID
      and har.ACCT_CLASS_HA_C = peh.ADT_PAT_CLASS_C
      and har.PRIM_ENC_CSN_ID = peh.PAT_ENC_CSN_ID
left join ge.HSP_ACCOUNT_3 har3
  on har.HSP_ACCOUNT_ID = har3.HSP_ACCOUNT_ID
left outer join ge.PATIENT_3 pat3
  on  pat3.PAT_ID = har.PAT_ID
left join ge.CLARITY_LOC loc         
  on loc.loc_id = har.loc_id
left join ge.HSP_ACCT_SBO sbo 
    on sbo.HSP_ACCOUNT_ID = har.HSP_ACCOUNT_ID
left join (
  select
      peh.HSP_ACCOUNT_ID
    , peh.PAT_ENC_CSN_ID
    , peh.HOSPITAL_AREA_ID
    , peh.BED_ID
    , loc.LOC_NAME
    , peh.INP_ADM_DATE
    , peh.OP_ADM_DATE
    , peh.ADT_PAT_CLASS_C
  from ge.V_PAT_ENC_HSP    peh
  left join ge.CLARITY_LOC loc
    on loc.LOC_ID = peh.HOSPITAL_AREA_ID
) venc 
  on venc.HSP_ACCOUNT_ID = har.HSP_ACCOUNT_ID
left join (
  select
      tx.hsp_account_id
    , sum(tx.tx_amount) genamt
  from ge.hsp_transactions tx
  where
    tx.TX_TYPE_HA_C = 1 -- charges
  group by
    hsp_account_id
) genamt
  on genamt.HSP_ACCOUNT_ID = har.HSP_ACCOUNT_ID
left join palvhs.dbo.PAT_ENC_HSP sk
  on peh.PAT_ENC_CSN_ID = sk.PAT_ENC_CSN_ID
left join ge.CLARITY_DEP dep
  on dep.DEPARTMENT_ID = peh.DEPARTMENT_ID
left join ge.CLARITY_EPP as epp
  on epp.BENEFIT_PLAN_ID = har.PRIMARY_PLAN_ID
left join ge.CLARITY_EPM as epm
  on har.PRIMARY_PAYOR_ID = epm.PAYOR_ID
left join ge.ZC_FINANCIAL_CLASS zfc
  on zfc.FINANCIAL_CLASS = epm.FINANCIAL_CLASS
left join ge.ZC_ACCT_CLASS_HA zc_acct_class
  on har.ACCT_CLASS_HA_C = zc_acct_class.ACCT_CLASS_HA_C
left join ge.ZC_DISCH_DISP as zdd
    on zdd.DISCH_DISP_C = peh.DISCH_DISP_C
left join ge.ZC_PAT_CLASS zc_patclass
    on zc_patclass.ADT_PAT_CLASS_C = peh.ADT_PAT_CLASS_C
left join @AdmitType AdmitType
    on AdmitType.ACCT_CLASS_HA_C = har.ACCT_CLASS_HA_C
left join @AdmissionExcludeConfStatus AdmissionExcludeConfStatus
    on AdmissionExcludeConfStatus.ADMIT_CONF_STAT_C = peh.ADMIT_CONF_STAT_C
left join @AdmissionExcludePtStatus AdmissionExcludePtStatus
    on AdmissionExcludePtStatus.ADT_PATIENT_STAT_C = peh.ADT_PATIENT_STAT_C
left join @InpatientExcludeService InpatientExcludeService
    on InpatientExcludeService.HOSP_SERV_C = peh.HOSP_SERV_C
left join @OutpatientExcludeService OutpatientExcludeService
    on OutpatientExcludeService.PRIM_SVC_HA_C = har.PRIM_SVC_HA_C
left join @OutpatientExcludeConfStatus OutpatientExcludeConfStatus
    on OutpatientExcludeConfStatus.ADMIT_CONF_STAT_C = peh.ADMIT_CONF_STAT_C
left join FirstAdmit
  on FirstAdmit.PAT_ENC_CSN_ID = peh.PAT_ENC_CSN_ID
left join HospitalLocation
  on peh.DEPARTMENT_ID = HospitalLocation.DEPARTMENT_ID
left join HospitalLocation AdmitHospitalLocation
  on isnull(FirstAdmit.FirstAdmitAllowedDepartmentID, peh.DEPARTMENT_ID) = AdmitHospitalLocation.DEPARTMENT_ID 
left join HospitalLocation DischargeHospitalLocation
  on isnull(FirstAdmit.FirstAdmitAllowedDepartmentID, peh.DEPARTMENT_ID) = DischargeHospitalLocation.DEPARTMENT_ID
where 
  (   
    -- Ambulatory Overnight
    (
      har.ACCT_BASECLS_HA_C = 2 --@OutpatientAccountBaseClass -- outpatient
        and har.acct_class_ha_c in (106, 108, 130) -- Hospital Outpatient Surgery, Surgery Admit, Outpatient in a Bed
        --and AdmitType.Description = 'Ambulatory Overnight'
        and sbo.SBO_HAR_TYPE_C = 0 -- todo: convert to config variable
        and har3.COMBINE_RSN_C is null
        and (
          convert(varchar, peh.OP_ADM_DATE, 101) <> convert(varchar, har.DISCH_DATE_TIME, 101) 
            or har.DISCH_DATE_TIME is null
        ) -- this part drops us from 90,000+ HARs down to about 8,000 HARs, eliminating same-day discharges
        and (
          (
            loc.rpt_grp_six = 1 -- LVH
              and genamt.HSP_ACCOUNT_ID is not null -- require hospital billing transactions, removes about 330 HARs
              and peh.OP_ADM_DATE is not null -- require an OP_ADM_DATE (from validation query, doesn't seem to actually have an effect)
              and venc.ADT_PAT_CLASS_C in (106, 108, 130) -- Hospital Outpatient Surgery, Surgery Admit, Outpatient in a Bed
                                                          -- not sure why we need this but it knocks out about 35 HARs
          ) 
          or (
            loc.rpt_grp_six = 2 -- LVHM
          )
        ) 
        --and cast(peh.OP_ADM_DATE as date) between '2016-10-01' and '2016-10-31'               
    ) 
    -- Observation
    or (
      har.ACCT_BASECLS_HA_C = 2 --@OutpatientAccountBaseClass -- outpatient
        and har.acct_class_ha_c in (104) -- Observation
        --and AdmitType.Description = 'Observation'
        and sbo.SBO_HAR_TYPE_C = 0 -- todo: convert to config variable
        and har3.COMBINE_RSN_C is null    
        and (
          (
            loc.rpt_grp_six = 1 -- LVH
              and genamt.HSP_ACCOUNT_ID is not null
              and peh.OP_ADM_DATE is not null
              and venc.ADT_PAT_CLASS_C in (104) -- Observation
              --and (
              --  cast(peh.OP_ADM_DATE as date) between '2016-10-01' and '2016-10-31' 
              --    --or har.DISCH_DATE_TIME is null -- including this brings in HARs where the admit date is outside the validation 10/1 - 10/31 date range. Asked Chris G. if this is really necessary
              --)
          ) 
          or (
            loc.rpt_grp_six = 2 -- LVHM
              --and cast(peh.OP_ADM_DATE as date) between '2016-10-01' and '2016-10-31'
          )
        )                
    )    
  )
  and (pat3.IS_TEST_PAT_YN = 'N' OR pat3.IS_TEST_PAT_YN IS NULL) -- exclude test patients  
;

-- TSU Inpatient and Rehab Inpatient
insert into gt3.LVHEncounterVolume 
select 
    eb.HospitalAccountId
  , eb.EncounterCSN
  , eb.HospitalLocation
  , eb.Location
  , eb.HospitalEntity
  , eb.AdmitHospital
  , eb.AdmitHospitalEntity
  , eb.DischargeHospital
  , eb.DischargeHospitalEntity
  , eb.AdmitLocation
  , eb.DischargeLocation
  , eb.ServiceAreaID
  , eb.BedId
  , eb.PatientId
  , eb.HARPrimaryPayorID                                                    as PayorId
  , eb.HARPrimaryPayor                                                      as Payor
  , eb.HARPrimaryPayorFinancialClass                                        as PayorFinancialClass
  , eb.HARPrimaryPayorAbbr                                                  as PayorAbbr  
  , eb.HospitalAccountClassID
  , eb.HospitalAccountClass
  --, isnull(eb.AdmitType, 'TSU/Rehab')                                     as AdmitType
  , eb.AdmitType                                                            as AdmitType
  , eb.AdmitTime
  , eb.AdmitDate
  , eb.AdmitSourceId
  , eb.AdmitProviderId
  , eb.FirstAdmitAllowedDepartmentID
  , eb.DischargeTime
  , eb.DischargeDate
  , eb.DischargeProviderId
  , eb.LastDepartmentId
  , eb.DischargeDepartmentID
  , eb.DischargeDispositionID
  , eb.DischargeDisposition
  , eb.DeceasedPatient
  , eb.AdmissionConfirmed
  , eb.InpatientConfirmedAdmission
  , eb.OutpatientConfirmedVisit
  , eb.PatientClassId
  , eb.PatientClass
  , eb.IPLOS
  , eb.SameDayStay
  , case
      when eb.HARLocationID = 1
        then 'LVH'
      when eb.HARLocationID = 2
        then 'LVHM'
      else '*Unknown'
    end                                                                     as ValidationHospitalLocation
  , cast('gt2.EncounterBase' as varchar(25))                                as Source
  , @LastUpdateTS                                                           as LastUpdateTS
from gt2.EncounterBase eb
where
  eb.AdmitType in (
      'TSU Inpatient'
    , 'Rehab Inpatient'
  )
;

declare @HAR table ( 
  HospitalAccountID numeric(18,0)
  index ncix__HAR__HospitalAccountID nonclustered (HospitalAccountID)
)
;
insert into @HAR
select distinct 
  ev.HospitalAccountID
from gt3.LVHEncounterVolume ev
;

-- everything else 
insert into gt3.LVHEncounterVolume 
select 
    eb.HospitalAccountId
  , eb.EncounterCSN
  , eb.HospitalLocation
  , eb.Location
  , eb.HospitalEntity
  , eb.AdmitHospital
  , eb.AdmitHospitalEntity
  , eb.DischargeHospital
  , eb.DischargeHospitalEntity
  , eb.AdmitLocation
  , eb.DischargeLocation
  , eb.ServiceAreaID
  , eb.BedId
  , eb.PatientId
  , eb.HARPrimaryPayorID                                                    as PayorId
  , eb.HARPrimaryPayor                                                      as Payor
  , eb.HARPrimaryPayorFinancialClass                                        as PayorFinancialClass
  , eb.HARPrimaryPayorAbbr                                                  as PayorAbbr  
  , eb.HospitalAccountClassID
  , eb.HospitalAccountClass
  --, isnull(eb.AdmitType, 'TSU/Rehab')                                     as AdmitType
  , 'Census'                                                                as AdmitType
  , eb.AdmitTime
  , eb.AdmitDate
  , eb.AdmitSourceId
  , eb.AdmitProviderId
  , eb.FirstAdmitAllowedDepartmentID
  , eb.DischargeTime
  , eb.DischargeDate
  , eb.DischargeProviderId
  , eb.LastDepartmentId
  , eb.DischargeDepartmentID
  , eb.DischargeDispositionID
  , eb.DischargeDisposition
  , eb.DeceasedPatient
  , eb.AdmissionConfirmed
  , eb.InpatientConfirmedAdmission
  , eb.OutpatientConfirmedVisit
  , eb.PatientClassId
  , eb.PatientClass
  , eb.IPLOS
  , eb.SameDayStay
  , case
      when eb.HARLocationID = 1
        then 'LVH'
      when eb.HARLocationID = 2
        then 'LVHM'
      else '*Unknown'
    end                                                                     as ValidationHospitalLocation
  , cast('gt2.EncounterBase' as varchar(25))                                as Source
  , @LastUpdateTS                                                           as LastUpdateTS
from gt2.EncounterBase eb
where
  eb.HospitalAccountId is not null
    and not exists (
      select null
      from @HAR ev
      where
        ev.HospitalAccountID = eb.HospitalAccountId
    )
;

/*
  select 
      datepart(yyyy, AdmitDate) as year
    , datepart(mm, AdmitDate) as month
    , ValidationHospitalLocation
    , AdmitType
    , source
    , count(distinct HospitalAccountID)
  from gt3.LVHEncounterVolume
  where
    AdmitDate between '2016-10-01' and '2016-10-31'
  group by
      datepart(yyyy, AdmitDate)
    , datepart(mm, AdmitDate)
    , ValidationHospitalLocation
    , AdmitType
    , source
  order by
    1, 2, 3

  select count(distinct HospitalAccountID) from gt3.LVHEncounterVolume;
  select count(distinct HospitalAccountID) 
  from gt3.LVHEncounterVolume ev
  inner join ge.PAT_ENC_HSP peh
    on peh.PAT_ENC_CSN_ID = ev.EncounterCSN
  where
    peh.HOSP_ADMSN_TIME >= '2015-02-01'
  ;
  select count(distinct HospitalAccountID) from gt2.EncounterBase;

*/

go

