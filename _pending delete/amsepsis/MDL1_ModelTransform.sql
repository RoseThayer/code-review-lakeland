SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

 
  
 CREATE procedure [gt2].[MDL1_ModelTransform] as

/*  --> Source Tables <--
    pa.ge.CLARITY_DEP
    pa.ge.HSP_ACCOUNT
    pa.ge.PATIENT
    pa.ge.ED_IEV_PAT_INFO
    gt1.ADTHistory
    gt1.HospitalAccountDiagnosis
    gt1.ICULOS
    gt2.HospitalEncounter
    gt1.Patient
    gt1.PatientRace
    gt2.Readmission
    gt2.SepsisADT
    gt2.SepsisCombinedFlag
    gt2.SepsisFlag
    gt2.SepsisFlagTime
    gt2.SepticShock
    gt2.SevereSepsis
    gt2.SirsFlag

  --> Target Table <--
	amSepsis.tFlagTime
	amSepsis.tFlag
	amSepsis.ttHospitalEncounter
	amSepsis.tSepsisADTEvent
	amSepsis.tHospitalEncounter
	amSepsis.HospitalAccountDxList
	amSepsis.SepsisReadmissions
	amSepsis.FlagTime
	amsepsis.FlagIndicatorDetails


  --> Performance benchmark <--
    total time ~3:33
*/

declare @ConfigSepsisStart DateTime = '2016-01-01'; -- most limits occur in sepsis flags, but this does limit the diagnosis based table

--------------------------------------------------------------------------------
print( '----------amSepsis.tFlagTime--------------' );
if OBJECT_ID('amSepsis.tFlagTime', 'U') is not null drop table amSepsis.tFlagTime;

with FlagTimes as (
select
  EncounterCSN
, SepsisFlagTime as FlagTime
, SepsisToICUMinutes
, LactateResultTime
, LactateCollectionTime
, LactateOrderTime as LactateOrderTime
, SepsisToLactateMinutes
, Cast( LactateFlag as int ) as LactateFlag
, Cast( LactateOrdersetUsedFlag as int ) as LactateOrdersetUsedFlag
, LactateOrdersetName
, AbxFirstOrderTime
--, SepsisToAbxOrderMinutes
, AbxFirstOrderedTime as AbxAdminFirstOrderedTime
, AbxFirstAdminTime
, SepsisToAbxAdminMinutes
, Cast( AbxOrderFlag as int ) as AbxOrderFlag
, Cast( AbxOrderOrdersetUsedFlag as int ) as AbxOrderOrdersetUsedFlag
, AbxOrdersetName
, Cast( AbxAdminFlag as int ) as AbxAdminFlag
, Cast( AbxAdminOrdersetUsedFlag as int ) as AbxAdminOrdersetUsedFlag
, AbxAdminOrdersetName
, IVFluidFirstGivenTime
, IVFluidFirstOrderTime as IVFluidFirstOrderedTime
, SepsisToIVAdminMinutes
, Cast( IVAdminFlag as int ) as IVAdminFlag
, Cast( IVAdminOrdersetUsedFlag as int ) as IVAdminOrdersetUsedFlag
, IVAdminOrdersetName
, BloodCxFirstCollectedTime
, BloodCxFirstOrderTime as BloodCxFirstOrderedTime
, SepsisToBloodCxMinutes
, Cast( BloodCxFlag as int ) as BloodCxFlag
, Cast( BloodCxOrdersetUsedFlag as int ) as BloodCxOrdersetUsedFlag
, BloodCxOrdersetName
, Cast( ThreeHourBundleFlag as int ) as ThreeHourBundleFlag
, ThreeHourBundleMinutes
, VasopressorFirstAdminTime
, VasopressorFirstOrderTime as VasopressorFirstOrderedTime
, SepsisToVasopressorAdminMinutes
, Cast( VasopressorAdminFlag as int ) as VasopressorAdminFlag
, Cast( VasopressorAdminOrdersetUsedFlag as int ) as VasopressorAdminOrdersetUsedFlag
, VasopressorAdminOrdersetName
, PersistentHypotentionFlag
, CVPScvO2RecordedTime
, Cast( MeasureCVPScvO2Flag as int ) as MeasureCVPScvO2Flag
, SepsisToScvO2RecordedMinutes
, Cast( ElevatedLactateFlag as int ) as ElevatedLactateFlag
, LactateFirstRemeasuredTime
, LactateFirstRemeasuredResultTime
, LactateFirstRemeasuredOrderTime as LactateFirstRemeasuredOrderTime
, SepsisToRemeasureLactateMinutes
, Cast( RemeasureLactateFlag as int ) as RemeasureLactateFlag
, Cast( RemeasureLactateOrdersetUsedFlag as int ) as RemeasureLactateOrdersetUsedFlag
, RemeasureLactateOrdersetName
-- , UrineOutputFirstRecordedTime
-- , Cast( HourlyUrineOutputFlag as int ) as HourlyUrineOutputFlag
-- , SepsisToUrineOutputFirstRecordedMinutes
, Cast( SixHourBundleFlag as int ) as SixHourBundleFlag
, SixHourBundleMinutes
, Cast( LactateBloodCxAbxAdminFlag as int ) as LactateBloodCxAbxAdminFlag
, Iif( LactateOrdersetUsedFlag = 1, LactateOrderTime, null ) as LactateOrdersetTime
, Iif( AbxOrderOrdersetUsedFlag = 1, AbxFirstOrderTime, null ) as AbxOrderOrdersetTime
--, Iif( AbxAdminOrdersetUsedFlag = 1, AbxAdminFirstOrderedTime, null ) as AbxAdminOrdersetTime
--, Iif( IVAdminOrdersetUsedFlag = 1, IVFluidFirstOrderedTime, null ) as IVAdminOrdersetTime
--, Iif( BloodCxOrdersetUsedFlag = 1, BloodCxFirstOrderedTime, null ) as BloodCxOrdersetTime
--, Iif( VasopressorAdminOrdersetUsedFlag = 1, VasopressorFirstOrderedTime, null ) as VasopressorAdminOrdersetTime
, Iif( RemeasureLactateOrdersetUsedFlag = 1, LactateFirstRemeasuredOrderTime, null ) as RemeasureLactateOrdersetTime
from gt2.SepsisFlagTime
)
, FirstLactateFlag as (
select
  EncounterCSN
, Min(FlagTime) as FlagTime
, 1 as FirstLactateFlag
from FlagTimes
where LactateFlag = 1
group by EncounterCSN
)
, FlagTimes2 as (
select
  ft.*
, ( select Min(t) from (values
      (LactateOrderTime)
    , (AbxFirstOrderTime)
    , (AbxAdminFirstOrderedTime)
    , (IVFluidFirstOrderedTime)
    , (BloodCxFirstOrderedTime)
    , (VasopressorFirstOrderedTime)
    , (LactateFirstRemeasuredOrderTime))
    as value(t) ) as OrdersetTime
, ( select Max(t) from (values
      (LactateResultTime)
    , (BloodCxFirstCollectedTime)
    , (AbxFirstAdminTime)
    , (IVFluidFirstGivenTime)
    , (VasopressorFirstAdminTime)
    , (CVPScvO2RecordedTime)
    , (LactateFirstRemeasuredTime)
    -- , (UrineOutputFirstRecordedTime) -- removed urine per LVHN request
    )
    as value(t) ) as LastProtocolCompletedTime
, flf.FirstLactateFlag
, Iif( flf.FirstLactateFlag = 1 and ft.ElevatedLactateFlag = 1, 1, 0 ) as FirstLactateElevatedFlag
from FlagTimes as ft
  left join FirstLactateFlag as flf
    on ft.EncounterCSN = flf.EncounterCSN
)

select
  ft.*
  , DateDiff( minute, OrdersetTime, LastProtocolCompletedTime ) as OrderSetToLastActionDurationMinutes
into amSepsis.tFlagTime
from FlagTimes2 as ft
;

create index idx_EncounterCSN on amSepsis.tFlagTime (EncounterCSN);

--------------------------------------------------------------------------------
print( 'amSepsis.tFlag' )
if OBJECT_ID('amSepsis.tFlag', 'U') is not null drop table amSepsis.tFlag;

declare @cgGHSEDDepartmentID int = (select variables.GetSingleValue('@cgGHSEDDepartmentID'));
declare @cgEDArrivalEventID int = (select variables.GetSingleValue('@cgEDArrivalEventID'));
declare @cgEDTriageStartedEventID int = (select variables.GetSingleValue('@cgEDTriageStartedEventID'));

declare @LevelOfSeverity table
(
  LevelOfSeverity varchar(255)
, FlagTypeId int
);

insert into @LevelOfSeverity values
  ( 'SIRS', 1 )
, ( 'Sepsis', 2 )
, ( 'Severe Sepsis', 3 )
, ( 'Septic Shock', 4 )
, ( 'Undetermined', -1 )
, ( 'Undetermined', -2 )
;

select
scf.EncounterCSN
, scf.FlagTime
, scf.FlagType
, scf.FlagTypeId
, ls.LevelOfSeverity
, iif( scf.FlagTypeId = 1, 1, 0 ) as SIRSFlag
, iif( scf.FlagTypeId = 2, 1, 0 ) as SepsisFlag
, iif( scf.FlagTypeId = 3, 1, 0 ) as SevereSepsisFlag
, iif( scf.FlagTypeId = 4, 1, 0 ) as SepticShockFlag
, Iif( Min( Iif( scf.FlagTypeId = '1', FlagTime, Null ) ) over ( partition by scf.EncounterCSN ) = scf.FlagTime, 1, 0 ) FirstSIRSFlag
, Iif( Min( Iif( scf.FlagTypeId = '-1', FlagTime, Null ) ) over ( partition by scf.EncounterCSN ) = scf.FlagTime, 1, 0 ) BPAFlag
, Iif( Min( Iif( scf.FlagTypeId = '-2', FlagTime, Null ) ) over ( partition by scf.EncounterCSN ) = scf.FlagTime, 1, 0 ) EDArrivalFlag
into amSepsis.tFlag
from gt2.SepsisCombinedFlag as scf
left join @LevelOfSeverity as ls
on ls.FlagTypeId = scf.FlagTypeId;


--------------------------------------------------------------------------------
if OBJECT_ID('amSepsis.ttHospitalEncounter', 'U') is not null drop table amSepsis.ttHospitalEncounter;
print( '----------amSepsis.ttHopsitalEncounter--------------' );

with Csn as (
select distinct
  EncounterCSN
from amsepsis.tFlagTime
)
, Ed as (
select distinct
  pat.PAT_ENC_CSN_ID as EncounterCSN
, Min( triage.EVENT_TIME ) as EDTriageStarted
, Min( arrive.EVENT_TIME ) as EDArrivalTime
from pa.ge.ED_IEV_PAT_INFO as pat
  left join pa.ge.ED_IEV_EVENT_INFO as triage on pat.EVENT_ID = triage.EVENT_ID and triage.EVENT_TYPE = @cgEDTriageStartedEventID
  left join pa.ge.ED_IEV_EVENT_INFO as arrive on pat.EVENT_ID = arrive.EVENT_ID and arrive.EVENT_TYPE = @cgEDArrivalEventID
group by pat.PAT_ENC_CSN_ID
)
, EncounterAgg as (
select
  EncounterCSN
, Max( Iif( LactateOrdersetUsedFlag
          + AbxOrderOrdersetUsedFlag
          + AbxAdminOrdersetUsedFlag
          + IVAdminOrdersetUsedFlag
          + BloodCxOrdersetUsedFlag
          + VasopressorAdminOrdersetUsedFlag
          + RemeasureLactateOrdersetUsedFlag
          > 0
          , 1, 0
) ) as EncounterOrdersetUsedFlag
, Min(OrdersetTime) as EncounterOrdersetTime
, Max(Iif(LactateFlag = 1 and BloodCxFlag = 1 and AbxAdminFlag = 1, 1, 0)) as LactateBloodCxAbxAdminFlag
, Min( AbxAdminFirstOrderedTime ) as EncounterAbxAdminFirstOrderedTime
, Min( AbxFirstAdminTime ) as EncounterAbxFirstAdminTime
from amSepsis.tFlagTime
group by EncounterCSN
)

select
  he.PAT_ENC_CSN_ID as EncounterCSN
, he.HSP_ACCOUNT_ID as HospitalAccountID
, he.PAT_ID as PatientID
, he.DischargeDepartment
, he.DischargeDisposition
, he.HospitalAdmissionTime
, he.HospitalAdmissionDate
, he.AdmissionDAT
, he.HospitalDischargeTime
, he.AdmissionSource
, he.HasDNRStatus
, he.CodeStatuses
, he.HasMortalityDischargeDisposition
, he.PatientClass
, he.ADTPatientStatus
, he.BaseClass
, he.FinancialClass
, he.FirstEDDepartmentID
, he.CoumadinFlag
, he.HeparinFlag
, he.LengthOfStay
, he.LengthOfStayGroup
, ha.TOT_CHGS as HospitalAccountTotalCharges
, ed.EDArrivalTime
, ed.EDTriageStarted
, pat.BIRTH_DATE as PatientBirthDate
, ( CONVERT( int, CONVERT( char(8), he.HospitalAdmissionTime, 112 ) ) - CONVERT( char(8), pat.BIRTH_DATE, 112 ) ) / 10000 as AgeAtAdmission
, ea.EncounterOrdersetUsedFlag
, ea.EncounterOrdersetTime
, ea.EncounterAbxAdminFirstOrderedTime
, ea.EncounterAbxFirstAdminTime
into amSepsis.ttHospitalEncounter
from gt2.HospitalEncounter as he
  inner join Csn on he.PAT_ENC_CSN_ID = Csn.EncounterCSN
  left join pa.ge.HSP_ACCOUNT as ha on he.HSP_ACCOUNT_ID = ha.HSP_ACCOUNT_ID
  left join ed on he.PAT_ENC_CSN_ID = ed.EncounterCSN
  left join pa.ge.PATIENT as pat on he.PAT_ID = pat.PAT_ID
  left join EncounterAgg as ea on he.PAT_ENC_CSN_ID = ea.EncounterCSN
;

create index idx_EncounterCSN on amSepsis.ttHospitalEncounter (EncounterCSN);


--------------------------------------------------------------------------------
print( 'amSepsis.tSepsisADTEvent')
if OBJECT_ID('amSepsis.tSepsisADTEvent', 'U') is not null drop table amSepsis.tSepsisADTEvent;

with ADTHistory as (
select distinct
  adth.InTime
, adth.OutTime
, adth.EventDurationMinute as EventDurationMinutes
, adth.EventID
, adth.EventTypeID
, adth.DepartmentID
, adth.DepartmentName
, adth.ICUDepartmentFlag
, adth.TransferToICUFlag
from  gt1.ADTHistory adth
where len(DepartmentID)>0 -- RT remove pre and post admission events as duplicate Event IDs throw off our logic

union all

select
  null as InTime
, null as OutTime
, null as EventDurationMinutes
, -1 as EventID
, null as EventTypeID
, -1 as DepartmentID
, 'PRE ADMISSION' as DepartmentName
, 0 as ICUDepartmentFlag
, 0 as TransferToICUFlag

union all

select
  null as InTime
, null as OutTime
, null as EventDurationMinutes
, -2 as EventID
, null as EventTypeID
, -2 as DepartmentID
, 'POST DISCHARGE' as DepartmentName
, 0 as ICUDepartmentFlag
, 0 as TransferToICUFlag
)
select
  sadt.EncounterCSN
, sadt.FlagTime
, sadt.EventID
, adth.InTime
, adth.OutTime
, adth.EventDurationMinutes
, adth.EventTypeID
, adth.DepartmentID
, IsNull( adth.DepartmentName, '*Unknown') as DepartmentName
, case
    when sadt.FlagTime > hsp.HospitalDischargeTime or DEPARTMENT_NAME = '-2' then 'POST DISCHARGE' 
    when sadt.FlagTime < hsp.HospitalAdmissionTime or DEPARTMENT_NAME = '-1' then 'PRE ADMISSION' 
     when sadt.DepartmentName is null then '*Unknown'
    else sadt.DepartmentName
  end as InitialDepartmentName
, adth.ICUDepartmentFlag
, adth.TransferToICUFlag
into amSepsis.tSepsisADTEvent
from gt2.SepsisADT sadt
  inner join (select distinct EncounterCSN, FlagTime from amSepsis.tFlagTime) as ft
    on sadt.EncounterCSN = ft.EncounterCSN and sadt.FlagTime = ft.FlagTime
  left join ADTHistory adth
    on sadt.EventId = adth.EventId
  left join pa.ge.CLARITY_DEP dep
    on sadt.DepartmentID = dep.DEPARTMENT_ID
  left join amSepsis.ttHospitalEncounter hsp
    on sadt.EncounterCSN = hsp.EncounterCSN

--------------------------------------------------------------------------------
print( 'amSepsis.tHospitalEncounter' )
if OBJECT_ID('amSepsis.tHospitalEncounter', 'U') is not null drop table amSepsis.tHospitalEncounter;

declare @AgeGroup table
(
  low float
, high float
, AgeGroup varchar(255)
)

insert into @AgeGroup values
  ( 0,0,'<1' )
, ( 1,5,'1-5' )
, ( 6,12,'6-12' )
, ( 13,17,'13-17' )
, ( 18,30,'18-30' )
, ( 31,40,'31-40' )
, ( 41,50,'41-50' )
, ( 51,66,'51-66' )
, ( 67,10000,'67+' )
;

with hadx as (
select
  EncounterCSN
, Max( IsNull(SepsisDxFlag, 0) ) as SepsisDxFlag
from  gt1.HospitalAccountDiagnosis had
where SepsisDxFlag = 1
group by EncounterCSN
)
, FlagAgg as (
select
 EncounterCSN
, DateDiff( minute, Min( FlagTime), Max( FlagTime ) ) as SepsisDuration
, Max( FlagTypeId ) as FlagTypeId
, Min( Iif( FlagType = 'SIRS', FlagTime, null ) ) as FirstSIRSFlagTime
, Min( Iif( FlagType = 'Sepsis', FlagTime, null ) ) as FirstSepsisFlagTime
, Min( Iif( FlagType = 'Severe Sepsis', FlagTime, null ) ) as FirstSevereSepsisFlagTime
, Min( Iif( FlagType = 'Septic Shock', FlagTime, null ) ) as FirstSepticShockFlagTime
, Min( Iif( FlagType = 'BPA', FlagTime, null ) ) as FirstBPATime
from amSepsis.tFlag
group by EncounterCSN
)
, HighestSeverity as (
select distinct
  EncounterCSN, 
  max(FlagTime) as TimeOfMostSevereFlag, 
  min(FlagTypeId) as MostSevereFlagType   
from amsepsis.tFlag
group by EncounterCSN
)
--, HighestSeverity as (
--select distinct
--  EncounterCSN
-- , first_value( FlagTime ) over ( partition by EncounterCSN order by FlagTypeId desc, FlagTime asc ) as TimeOfMostSevereFlag
-- , first_value( FlagType ) over ( partition by EncounterCSN order by FlagTypeId desc, FlagTime asc ) as MostSevereFlagType
--from amSepsis.tFlag
--)

select
  he.EncounterCSN
, he.HospitalAccountID
, he.PatientID
, he.DischargeDepartment
, he.DischargeDisposition
, he.HospitalAdmissionTime
, he.HospitalAdmissionDate
, he.AdmissionDAT
, he.HospitalDischargeTime
, he.HospitalAccountTotalCharges
, he.AdmissionSource
, he.HasDNRStatus
, he.CodeStatuses
, he.HasMortalityDischargeDisposition
, he.PatientClass
, he.ADTPatientStatus as PatientStatus
, he.BaseClass
, he.AgeAtAdmission

, ag.AgeGroup as AgeBucket
, Iif( DateDiff( year, he.PatientBirthDate, he.HospitalAdmissionTime ) < 18, 1,  0 ) as AgeIsPediatric

, EDArrivalTime
, iif( EDArrivalTime > cast( 0 as DateTime ), 1, 0 ) as EDEncounter
, EDTriageStarted as EDTriageTime

, he.FinancialClass
, he.EncounterOrdersetUsedFlag
, he.EncounterOrdersetTime
-- , LactateBloodCxAbxAdminFlag
, he.CoumadinFlag
, he.HeparinFlag
, he.LengthOfStay
, he.LengthOfStayGroup
, he.EncounterAbxAdminFirstOrderedTime
, he.EncounterAbxFirstAdminTime
, fa.SepsisDuration
, fa.FirstSIRSFlagTime
, fa.FirstSepsisFlagTime
, fa.FirstSevereSepsisFlagTime
, fa.FirstSepticShockFlagTime
, DateDiff( minute, he.HospitalAdmissionTime, fa.FirstSIRSFlagTime ) as AdmissionToSIRSTime
, DateDiff( minute, he.HospitalAdmissionTime, fa.FirstSepsisFlagTime ) as AdmissionToSepsisTime
, DateDiff( minute, he.HospitalAdmissionTime, fa.FirstSevereSepsisFlagTime ) as AdmissionToSevereSepsisTime
, DateDiff( minute, he.HospitalAdmissionTime, fa.FirstSepticShockFlagTime ) as AdmissionToSepticShockTime
, DateDiff( minute, he.EDArrivalTime, EDTriageStarted ) as TimetoFirstVitalsTaken
, Iif( he.HasMortalityDischargeDisposition = 'Y' and DateDiff(second, fa.FirstSIRSFlagTime, he.HospitalDischargeTime ) / 3600.0 <= 3 , 1, 0 ) as ExpiredWithin3Hours
, Iif( he.HasMortalityDischargeDisposition = 'Y' and DateDiff( second, fa.FirstSIRSFlagTime, he.HospitalDischargeTime ) / 3600.0 <= 6, 1, 0 ) as ExpiredWithin6Hours
-- , DateDiff( minute, EDTriageTime, MDSuspectsSepsisfa.FirstRecordedTime ) as TriagetoAssessmentDurationMinutes -- not in LVHN workflow
-- , DateDiff( minute, MDSuspectsSepsisfa.FirstRecordedTime, EncounterOrdersetTime ) as AssessmentToOrderSetDurationMinutes -- not in LVHN workflow
-- , DateDiff( minute, fa.FirstBPATime, MDSuspectsSepsisfa.FirstRecordedTime ) as BPAFiredTimeToAssessmentDurationMinutes -- not in LVHN workflow
, FirstBPATime
, DateDiff( minute, fa.FirstBPATime, he.EncounterOrdersetTime ) as BPAFiredTimeToOrderSetDurationMinutes
, hs.TimeOfMostSevereFlag
, hs.MostSevereFlagType
, sadt.DepartmentID as FirstSIRSFlagADTDepartmentID
, sadt.InitialDepartmentName as FirstSIRSFlagADTDepartmentName
, iculos.ICULOSDay as ICULOSDays
, IsNull( hadx.SepsisDxFlag, 0 ) as SepsisDxFlag

into amSepsis.tHospitalEncounter
from amSepsis.ttHospitalEncounter as he
  left join @AgeGroup ag on he.AgeAtAdmission between ag.low and ag.high
  left join FlagAgg as fa
    on fa.EncounterCSN = he.EncounterCSN
  left join amSepsis.tSepsisADTEvent as sadt
    on sadt.EncounterCSN = he.EncounterCSN
    and sadt.FlagTime = fa.FirstSIRSFlagTime
  left join  gt1.ICULOS as iculos
    on he.EncounterCSN = iculos.PAT_ENC_CSN_ID
  left join hadx
    on he.EncounterCSN = hadx.EncounterCSN
  left join HighestSeverity as hs
    on hs.EncounterCSN = he.EncounterCSN

create index idx_EncounterCSN on amSepsis.tHospitalEncounter (EncounterCSN);

-- drop table amSepsis.ttHospitalEncounter

--------------------------------------------------------------------------------

print( '----------amSepsis.HospitalAccountDxList--------------' );
if OBJECT_ID('amSepsis.HospitalAccountDxList', 'U') is not null drop table amSepsis.HospitalAccountDxList;


select
hadx.EncounterCSN
, hadx.DxLine
, hadx.DxIsPrimary
, hadx.DxIsSecondary
, hadx.DxCode
, hadx.DxName
, hadx.SepsisDxName
, hadx.DxCodeSet
, hadx.DxPresentOnArrival
, hadx.SepsisDxFlag
, flag.FlagTime
, flag.FlagType
, flag.FlagTypeId
, flag.LevelOfSeverity
, flag.FirstSIRSFlag
, flag.BPAFlag
, flag.EDArrivalFlag
, flag.SIRSFlag
, flag.SepsisFlag
, flag.SevereSepsisFlag
, flag.SepticShockFlag
, he.FirstSIRSFlagADTDepartmentName
, he.HospitalAdmissionTime
, he.HospitalAdmissionDate
, he.HospitalDischargeTime
, he.DischargeDepartment
, he.AgeAtAdmission
, pat.PatientSex
, pat.PatientMRN
, patrace.PatientRace
, he.EDEncounter
, he.DischargeDisposition
into amSepsis.HospitalAccountDxList
/*
--RT new if we want all sepsis patients in diagnosis table
from amSepsis.tFlagTime as ft
 left join amsepsis.tFlag as flag
    on ft.EncounterCSN = flag.EncounterCSN
  left join  gt1.HospitalAccountDiagnosis as hadx
  on ft.EncounterCSN = hadx.EncounterCSN
  left join amSepsis.tHospitalEncounter as he
    on ft.EncounterCSN = he.EncounterCSN -- updated for ft base
  left join gt1.Patient as pat on pat.PatientId = he.PatientId
  left join gt1.PatientRace as patrace on patrace.PatientId = he.PatientId and patrace.PatientRaceLine = 1
*/
from  gt1.HospitalAccountDiagnosis as hadx
  inner join (select distinct EncounterCSN from amSepsis.tFlagTime) as ft
    on hadx.EncounterCSN = ft.EncounterCSN
  left join amsepsis.tFlag as flag
    on hadx.EncounterCSN = flag.EncounterCSN
  left join amSepsis.tHospitalEncounter as he
    on hadx.EncounterCSN = he.EncounterCSN
  left join  gt1.Patient as pat on pat.PatientId = he.PatientId
  left join  gt1.PatientRace as patrace on patrace.PatientId = he.PatientId and patrace.PatientRaceLine = 1

create index idx_EncounterCSN on amSepsis.HospitalAccountDxList (EncounterCSN);

--------------------------------------------------------------------------------
print( '----------amSepsis.SepsisReadmissions--------------' );
if OBJECT_ID('amSepsis.SepsisReadmissions', 'U') is not null drop table amSepsis.SepsisReadmissions;

;with Ed as (
select distinct
  pat.PAT_ENC_CSN_ID as EncounterCSN
, Min( triage.EVENT_TIME ) as EDTriageStarted
, Min( arrive.EVENT_TIME ) as EDArrivalTime
from pa.ge.ED_IEV_PAT_INFO as pat
  left join pa.ge.ED_IEV_EVENT_INFO as triage on pat.EVENT_ID = triage.EVENT_ID and triage.EVENT_TYPE = @cgEDTriageStartedEventID
  left join pa.ge.ED_IEV_EVENT_INFO as arrive on pat.EVENT_ID = arrive.EVENT_ID and arrive.EVENT_TYPE = @cgEDArrivalEventID
group by pat.PAT_ENC_CSN_ID
)

, ReadmissionPrimaryDiagnosis as (
select distinct
  readm.ReadmissionEncounterCSN
, hadx.DxName
from  gt1.HospitalAccountDiagnosis hadx
  left join gt2.Readmission as readm on hadx.EncounterCSN=readm.ReadmissionEncounterCSN
where hadx.DxIsPrimary = 'Y'
  and ReadmissionEncounterCSN is not null
)

select distinct
  hadx.EncounterCSN
, hadx.DxLine
, hadx.DxIsPrimary
, hadx.DxIsSecondary
, hadx.DxCode
, hadx.DxName
, hadx.SepsisDxName
, hadx.DxCodeSet
, hadx.DxPresentOnArrival
, hadx.SepsisDxFlag
, isnull(sadt.FirstSIRSFlagADTDepartmentName,'*Unknown') as FirstSIRSFlagADTDepartmentName
, he.HospitalAdmissionTime
, he.HospitalAdmissionDate
, he.HospitalDischargeTime
, he.DischargeDepartment
, he.Age as AgeAtAdmission
, he.Adult as Adult
, he.AdmissionDAT as IndexAdmissionDAT
, ag.AgeGroup as AgeBucket
, readm.ReadmissionAdmissionDate
, readm.ReadmissionDischargeDate
, readm.ReadmissionEncounterCSN
, readm.ReadmissionHospitalAccountID
, round(readm.DaysDifference,0) as ReadmissionDaysBetween
, readm.ReadmissionVisitNumber
, pat.PatientID as IndexPatientID
, pat.PatientSex
, pat.PatientMRN
, patrace.PatientRace
, iif(ed.EDArrivalTime > cast( 0 as DateTime ), 1, 0 ) as EDEncounter
, he.DischargeDisposition
, rpd.DxName as ReadmissionPrimaryDiagnosis
into amSepsis.SepsisReadmissions
from  gt1.HospitalAccountDiagnosis as hadx --RT start with all sepsis diagnosis
  left join gt2.Readmission as readm
    on hadx.EncounterCSN=readm.IndexEncounterCSN
  left join ReadmissionPrimaryDiagnosis rpd
    on readm.ReadmissionEncounterCSN = rpd.ReadmissionEncounterCSN
  left join gt2.HospitalEncounter as he
    on hadx.EncounterCSN = he.PAT_ENC_CSN_ID
  left join @AgeGroup ag on he.Age between ag.low and ag.high
  left join  gt1.Patient as pat on pat.PatientId = he.PAT_ID
  left join  gt1.PatientRace as patrace on patrace.PatientId = he.PAT_ID and patrace.PatientRaceLine = 1
  left join amSepsis.tHospitalEncounter as sadt on sadt.EncounterCSN = he.PAT_ENC_CSN_ID-- added in to populate FirstSIRSFLagADTDepartmentName after joining to gt2.HospitalEncounter rather than tHospitalEncounters
  left join ed on he.PAT_ENC_CSN_ID = ed.EncounterCSN -- added in to populate EDEncounter
where hadx.SepsisDxFlag=1
  and he.HospitalAdmissionDate >= @ConfigSepsisStart

--------------------------------------------------------------------------------
print( '----------amSepsis.FlagTime--------------' );
if OBJECT_ID('amSepsis.FlagTime', 'U') is not null drop table amSepsis.FlagTime;

select
  ft.EncounterCSN
, row_number() over (order by ft.EncounterCSN,ft.FlagTime) as EncounterFlagTimeID
, row_number() over (order by ft.EncounterCSN,ft.FlagTime,flag.FlagType) as EncounterFlagTimeTypeID
, ft.FlagTime
, ft.SepsisToICUMinutes
, ft.LactateResultTime
, ft.LactateOrderTime
, ft.LactateCollectionTime
, ft.SepsisToLactateMinutes
, ft.LactateFlag
, ft.LactateOrdersetUsedFlag
, ft.LactateOrdersetName
, ft.AbxFirstOrderTime
--, ft.SepsisToAbxOrderMinutes
, ft.AbxAdminFirstOrderedTime
, ft.AbxFirstAdminTime
, ft.SepsisToAbxAdminMinutes
, ft.AbxOrderFlag
, ft.AbxOrderOrdersetUsedFlag
, ft.AbxOrdersetName
, ft.AbxAdminFlag
, ft.AbxAdminOrdersetUsedFlag
, ft.AbxAdminOrdersetName
, ft.IVFluidFirstGivenTime
, ft.IVFluidFirstOrderedTime
, ft.SepsisToIVAdminMinutes
, ft.IVAdminFlag
, ft.IVAdminOrdersetUsedFlag
, ft.IVAdminOrdersetName
, ft.BloodCxFirstCollectedTime
, ft.BloodCxFirstOrderedTime
, ft.SepsisToBloodCxMinutes
, ft.BloodCxFlag
, ft.BloodCxOrdersetUsedFlag
, ft.BloodCxOrdersetName
, ft.ThreeHourBundleFlag
, ft.ThreeHourBundleMinutes
, ft.VasopressorFirstAdminTime
, ft.VasopressorFirstOrderedTime
, ft.SepsisToVasopressorAdminMinutes
, ft.VasopressorAdminFlag
, ft.PersistentHypotentionFlag
, ft.VasopressorAdminOrdersetUsedFlag
, ft.VasopressorAdminOrdersetName
, ft.CVPScvO2RecordedTime
, ft.MeasureCVPScvO2Flag
, ft.SepsisToScvO2RecordedMinutes
, ft.ElevatedLactateFlag
, ft.LactateFirstRemeasuredTime
, ft.LactateFirstRemeasuredOrderTime
, ft.SepsisToRemeasureLactateMinutes
, ft.RemeasureLactateFlag
, ft.RemeasureLactateOrdersetUsedFlag
, ft.RemeasureLactateOrdersetName
-- , ft.UrineOutputFirstRecordedTime
-- , ft.HourlyUrineOutputFlag
-- , ft.SepsisToUrineOutputFirstRecordedMinutes
, ft.SixHourBundleFlag
, ft.SixHourBundleMinutes
, ft.LactateBloodCxAbxAdminFlag
, ft.LactateOrdersetTime
, ft.AbxOrderOrdersetTime
, ft.RemeasureLactateOrdersetTime
, ft.FirstLactateFlag
, ft.OrdersetTime
, ft.LastProtocolCompletedTime
, ft.FirstLactateElevatedFlag
, ft.OrderSetToLastActionDurationMinutes


, he.HospitalAccountID
, he.PatientID
, he.DischargeDepartment
, he.DischargeDisposition
, he.HospitalAdmissionTime
, he.HospitalAdmissionDate
, he.AdmissionDAT
, he.HospitalDischargeTime
, he.HospitalAccountTotalCharges
, he.AdmissionSource
, he.HasDNRStatus

, he.CodeStatuses
, he.HasMortalityDischargeDisposition
, he.PatientClass
, he.PatientStatus
, he.BaseClass
, he.AgeAtAdmission

, he.AgeBucket
, he.AgeIsPediatric

, he.EDArrivalTime
, he.EDEncounter
, he.EDTriageTime

, he.FinancialClass
, he.EncounterOrdersetUsedFlag
, he.EncounterOrdersetTime
-- , he.LactateBloodCxAbxAdminFlag
, he.AdmissionToSIRSTime
, he.AdmissionToSepsisTime
, he.AdmissionToSevereSepsisTime
, he.AdmissionToSepticShockTime
, he.TimeToFirstVitalsTaken
, he.ExpiredWithin3Hours
, he.ExpiredWithin6Hours
, he.SepsisDuration
, he.FirstSIRSFlagTime
, he.FirstSepsisFlagTime
, he.FirstSevereSepsisFlagTime
, he.FirstSepticShockFlagTime
, he.FirstBPATime
, he.BPAFiredTimeToOrderSetDurationMinutes
, he.TimeOfMostSevereFlag
, he.MostSevereFlagType
, he.FirstSIRSFlagADTDepartmentID
, he.FirstSIRSFlagADTDepartmentName
, he.LengthOfStay as HospitalLOSDays
, he.LengthOfStayGroup as HospitalLOSGroup

, he.ICULOSDays as ICULOSDays
, he.SepsisDxFlag

, he.CoumadinFlag
, he.HeparinFlag

, he.EncounterAbxAdminFirstOrderedTime
, he.EncounterAbxFirstAdminTime

, pat.PatientName
, cast(pat.PatientBirthDate as datetime) as PatientBirthDate
, pat.PatientMRN
, pat.PatientSex
, pat.PatientEthnicGroup

, patrace.PatientRaceID
, patrace.PatientRace

, sadt.EventID
, sadt.InTime
, sadt.OutTime
, sadt.EventDurationMinutes
, sadt.EventTypeID
--, sadt.BedID
, sadt.DepartmentID
--, sadt.LocationID
--, sadt.RoomID
--, sadt.ServiceAreaID
--, sadt.BedCSN
--, sadt.RoomCSN
, sadt.ICUDepartmentFlag
, sadt.TransferToICUFlag
, sadt.DepartmentName
--, sadt.ServiceAreaName
--, sadt.LocationName
--, sadt.RoomName
--, sadt.BedName

, flag.FlagType
, flag.FlagTypeId
, flag.LevelOfSeverity
, flag.FirstSIRSFlag
, flag.BPAFlag
, flag.EDArrivalFlag
, flag.SIRSFlag
, flag.SepsisFlag
, flag.SevereSepsisFlag
, flag.SepticShockFlag

into amSepsis.FlagTime
from amSepsis.tFlagTime as ft
  left join amSepsis.tHospitalEncounter as he
    on ft.EncounterCSN = he.EncounterCSN
  left join  gt1.Patient as pat on pat.PatientId = he.PatientId
  left join  gt1.PatientRace as patrace on patrace.PatientId = he.PatientId and patrace.PatientRaceLine = 1
  left join amSepsis.tSepsisADTEvent sadt
    on sadt.EncounterCSN = ft.EncounterCSN
    and sadt.FlagTime = ft.FlagTime
  left join amSepsis.tFlag flag
    on ft.EncounterCSN = flag.EncounterCSN
    and ft.FlagTime = flag.FlagTime


--------------------------------------------------------------------------------
print( '----------amSepsis.FlagIndicatorDetails--------------' );
if OBJECT_ID('amSepsis.FlagIndicatorDetails', 'U') is not null drop table amSepsis.FlagIndicatorDetails;

declare @LeukocytosisMaxWBC float = (select variables.GetSingleValue('@LeukocytosisMaxWBC'));
declare @LeukopeniaMinWBC float = (select variables.GetSingleValue('@LeukopeniaMinWBC'));
declare @ElevatedBandsMaxBands float = (select variables.GetSingleValue('@ElevatedBandsMaxBands'));
declare @WBCRecordedWithinVitalsHours float = (select variables.GetSingleValue('@WBCRecordedWithinVitalsHours'));

with  WBCInterval as
(
select distinct
wbc.EncounterCSN
, wbc.ResultTime
, wbc.MinWBC
, wbc.MaxWBC
from gt2.tOrderResult wbc
  inner join gt2.SirsFlag sirs
    on wbc.EncounterCSN = sirs.EncounterCSN
  and Abs( DateDiff( second, wbc.ResultTime, sirs.RecordedTime ) / 3600.0 ) <= @WBCRecordedWithinVitalsHours
)
, BandsFlag as
(
select
  wi.EncounterCSN
, wi.ResultTime
from WBCInterval as wi
  inner join gt2.tOrderResult as bands
    on bands.EncounterCSN = wi.EncounterCSN
    and bands.MaxBands is not null
    and bands.ResultTime = wi.ResultTime
where wi.MaxWBC < @LeukocytosisMaxWBC and wi.MinWBC > @LeukopeniaMinWBC
  and MaxBands > @ElevatedBandsMaxBands
)

, IndicatorBase as (

--SIRS INDICATORS
--HypothermiaFlag
select distinct
  EncounterCSN
, FlowsheetRecordedTime as RecordedTime
,'Hypothermia' as IndicatorType
from gt2.SirsFlag
where HypothermiaFlag=1

union all

--HyperthermiaFlag
select distinct
  EncounterCSN
, FlowsheetRecordedTime as RecordedTime
,'Hyperthermia' as IndicatorType
from gt2.SirsFlag
where HyperthermiaFlag=1

union all

--TachycardiaFlag
select distinct
  EncounterCSN
, FlowsheetRecordedTime as RecordedTime
,'Tachycardia' as IndicatorType
from gt2.SirsFlag
where TachycardiaFlag=1

union all

--TachypneaFlag
select distinct
  EncounterCSN
, FlowsheetRecordedTime as RecordedTime
,'Tachypnea' as IndicatorType
from gt2.SirsFlag
where TachypneaFlag=1

union all

--HypotensionFlag
select distinct
  EncounterCSN
, FlowsheetRecordedTime as RecordedTime
,'Hypotension' as IndicatorType
from gt2.SirsFlag
where HypotensionFlag=1

union all

--LeukocytosisFlag
select distinct
 EncounterCSN,
 ResultTime as RecordedTime,
 'Leukocytosis' as IndicatorType
from WBCInterval
where MaxWBC < @LeukocytosisMaxWBC

union all

--LeukopeniaFlag
select distinct
 EncounterCSN,
 ResultTime as RecordedTime,
 'Leukopenia' as IndicatorType
from WBCInterval
where MinWBC > @LeukopeniaMinWBC

union all

--ElevatedBandsFlag
select distinct
 EncounterCSN,
 ResultTime as RecordedTime,
 'Elevated Bands' as IndicatorType
from BandsFlag

union all

--SEPSIS INDICATORS
--ABX
select distinct
  EncounterCSN
, RecordedTime as RecordedTime
, 'ABX Order' as IndicatorType
from gt2.SepsisFlag

union all

--SEVERE SEPSIS INDICATORS
--All combined in one table
select distinct
  EncounterCSN
, RecordedTime
, IndicatorType
from gt2.SevereSepsis

union all

--SEPTIC SHOCK INDICATORS
--All combined in one table
select distinct
  EncounterCSN
, RecordedTime
, IndicatorType
from gt2.SepticShock)

-- ALL INDICATOR TABLE
select distinct
 EncounterCSN
,RecordedTime
,IndicatorType
into amsepsis.FlagIndicatorDetails
from IndicatorBase

-- check keys and indexes
exec tools.CheckKeysAndIndexes











GO
