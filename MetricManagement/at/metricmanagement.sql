-- =============================================
-- Author:		Prominence Advisors
-- Create date: 9/1/2017
-- Description:	Populate the metric management transform tables


 
-- =============================================

USE padev
--SET ANSI_NULLS ON
--GO
--SET QUOTED_IDENTIFIER ON
GO



  
ALTER PROCEDURE atMM.MetricManagment --SET TO CREATE ON FIRST RUN INSTEAD OF ALTER
	-- Add the parameters for the stored procedure here
AS
BEGIN
  -- SET NOCOUNT ON added to prevent extra result sets from
  -- interfering with SELECT statements.
  SET NOCOUNT ON;

declare @vSaveDate date = convert(date , getdate());
declare @vSaveDateStr varchar(12)= replace(cast(@vSaveDate as varchar(12)),'-','') 
--print @vSaveDate;
--print @vSaveDateStr;




  /***********************Create at1.ApplicationMetric*************************/
    --drop table atMM.ApplicationMetric;
  if OBJECT_ID('atMM.ApplicationMetric') is null 
  create table atMM.ApplicationMetric (
    Application             varchar(max) null
  , MetricID                varchar(max) null
  , MetricType              varchar(max) null
  , InScope                 varchar(max) null
  , DefinitionStatus        varchar(max) null
  , Approver                varchar(max) null
  , Tab                     varchar(max) null
  , OperationalSME          varchar(max) null
  , TechnicalSME            varchar(max) null
  , Dimensions              varchar(max) null
  , Security                varchar(max) null
  , DisplayName             varchar(max) null
  , UIVariable              varchar(max) null
  , Visualization           varchar(max) null
  , Target                  varchar(max) null
  , SaveDate                date null
);

  /***********************Create at1.ApplicationUIVariable*************************/
    --drop table atMM.ApplicationUIVariable;
  if OBJECT_ID('atMM.ApplicationUIVariable') is null 
  create table atMM.ApplicationUIVariable (
    Application varchar(max) null
  , MetricID    varchar(max) null
  , UIVariable  varchar(max) null
  , SaveDate    date null
);

  /***********************Create at1.MetricDefinition*************************/
    --drop table atMM.MetricDefinition;
    if OBJECT_ID('atMM.MetricDefinition') is null 
    create table atMM.MetricDefinition (
      MetricID                 varchar(max) null
    , EnterpriseID             varchar(max) null
    , EnterpriseMetric         varchar(max) null
    , EnterpriseDuplicateID    varchar(max) null
    , OwningApplication        varchar(max) null
    , OwningApplicationPath    varchar(max) null
    , Metric                   varchar(max) null
    , AffiliateTeamApprover    varchar(max) null
    , DataSource               varchar(max) null
    , Calculation              varchar(max) null
    , ConfigurationVariable    varchar(max) null
    , EnterpriseField          varchar(max) null
    , SourceTableField         varchar(max) null
    , SourceValue              varchar(max) null
    , SQLLogic                 varchar(max) null
    , Timing                   varchar(max) null
    , DefinitionComplete       varchar(max) null
    , UsageCount               varchar(max) null
    , DuplicateName            varchar(max) null
    , SaveDate                 date         null
    );

/***********************Create at1.MetricDataSource*************************/
  --in case we need to recreate or change the table definition during testing
  
    --drop table atMM.MetricDataSource;
    if OBJECT_ID('atMM.MetricDataSource') is null 
    create table atMM.MetricDataSource (
      MetricID                 varchar(max) null
    , DataSource               varchar(max) null
    , SaveDate                 date         null
    );
  

/***********************Create at1.MetricConfigurationVariable*************************/

    --drop table atMM.MetricConfigurationVariable;
    if OBJECT_ID('atMM.MetricConfigurationVariable') is null 
    create table atMM.MetricConfigurationVariable (
      MetricID                 varchar(max) null
    , ConfigurationVariable    varchar(max) null
    , SaveDate                 date         null
    );


/***********************Create at1.MetricSourceTableField*************************/

    --drop table atMM.MetricSourceTableField;
    if OBJECT_ID('atMM.MetricSourceTableField') is null 
    create table atMM.MetricSourceTableField (
      MetricID                 varchar(max) null
    , SourceTable              varchar(max) null
    , SourceField              varchar(max) null
    , SaveDate                 date         null
    );


/***********************Create at1.MetricEnterpriseField*************************/

    --drop table atMM.MetricEnterpriseField;
    if OBJECT_ID('atMM.MetricEnterpriseField') is null 
    create table atMM.MetricEnterpriseField (
      MetricID                 varchar(max) null
    , EnterpriseLevel          varchar(max) null
    , EnterpriseTable          varchar(max) null
    , EnterpriseField          varchar(max) null
    , SaveDate                 date         null
    );

  /***********************Create at1.MetricDefinition*************************/


    --drop table atMM.Changes;
    if OBJECT_ID('atMM.Changes') is not null 
    truncate table atMM.Changes;

    if OBJECT_ID('atMM.Changes') is null 
    create table atMM.Changes (
      Application              varchar(max) null
    , DimensionID              varchar(max) null
    , MetricID                 varchar(max) null
    , ChangedTable             varchar(max) null
    , ChangedFromValue         varchar(max) null
    , ChangedToValue           varchar(max) null
    , ChangedField             varchar(max) null
    , SaveDate                 date
    );

  /***********************Create at1.Workbook*************************/
    --drop table atMM.Workbook;
  if OBJECT_ID('atMM.Workbook') is null 
  create table atMM.Workbook (
    Application varchar(max) null
  , Path        varchar(max) null
  , SaveDate    date null
);
  /****************************************************************************/

/* create temp table for metric list */
if OBJECT_ID('tempdb..#mmMetric') is not null drop table #mmMetric;
create table #mmMetric(
    Application             varchar(max) null
  , Version                 varchar(max) null
  , ID                      varchar(max) null
  , Metric                  varchar(max) null
  , MetricType              varchar(max) null
  , InScope                 varchar(max) null
  , DefinitionStatus        varchar(max) null
  , AffiliateTeamApprover   varchar(max) null
  , Approver                varchar(max) null
  , Tab                     varchar(max) null
  , OperationalSME          varchar(max) null
  , TechnicalSME            varchar(max) null
  , Calculation             varchar(max) null
  , DataSource              varchar(max) null
  , SourceTableField        varchar(max) null
  , SourceValue             varchar(max) null
  , SQLLogic                varchar(max) null
  , Dimensions              varchar(max) null
  , Security                varchar(max) null
  , Timing                  varchar(max) null
  , DisplayName             varchar(max) null
  , ConfigurationVariable   varchar(max) null
  , EnterpriseField         varchar(max) null
  , UIVariable              varchar(max) null
  , Visualization           varchar(max) null
  , Target                  varchar(max) null
  , EnterpriseID            varchar(max) null
  , DefinitionComplete      varchar(max) null
  , DefinitionAttempted     varchar(max) null
  , OwningApplication       varchar(max) null
  , MetricID                varchar(max) null
  , EnterpriseMetric        varchar(max) null
  , EnterpriseDuplicateID   varchar(max) null
)


  declare @sql varchar(max);


/*  clear the MetricDefinition rows for today if it they already exist in case this has been run more than once today  */
delete from atMM.MetricDefinition
where SaveDate = convert(date , getdate());
--print 'rows deleted is ' + cast(@@rowcount as char(3));

/*  clear the MetricDataSource rows for today if it they already exist in case this has been run more than once today  */
delete from atMM.MetricDataSource
where SaveDate = convert(date , getdate());

delete from atMM.MetricConfigurationVariable
where SaveDate = convert(date , getdate());

delete from atMM.MetricSourceTableField
where SaveDate = convert(date , getdate());

delete from atMM.MetricEnterpriseField
where SaveDate = convert(date , getdate());

delete from atMM.ApplicationMetric
where SaveDate = convert(date , getdate());

delete from atMM.ApplicationUIVariable
where SaveDate = convert(date , getdate());

delete from atMM.Workbook
where SaveDate = convert(date , getdate());

delete from atMM.Changes
where SaveDate = convert(date , getdate());

--delete from atMM.MetricDefinition
--where MetricID = 'Metric Management||';

/*  Load data from the Metric table  */
with tMetric as (
  select 
	    Application             
	  , Version
	  , ID
	  , Metric                
	  , MetricType            
	  , InScope               
	  , DefinitionStatus      
	  , AffiliateTeamApprover 
	  , Approver              
	  , Tab                   
	  , OperationalSME        
	  , TechnicalSME
	  , Calculation           
	  , DataSource            
	  , SourceTableField      
	  , SourceValue           
	  , SQLLogic              
	  , Dimensions                
	  , Security                  
	  , Timing                    
	  , DisplayName               
	  , ConfigurationVariable     
	  , replace(replace(replace(replace(EnterpriseField,')','.'),' ',''),',','.'),'(','') as EnterpriseField
	  , UIVariable                
	  , Visualization
	  , Target
	  , case 
		    when EnterpriseField is not null
		    then coalesce(Metric,'') + '|' + EnterpriseField
		    else null
		  end                       as EnterpriseID
	  , case 
		    when DefinitionStatus is null or DataSource is null or Calculation is null or SourceTableField is null or SourceValue is null 
		    then 'No'
		    else 'Yes'
		  end                       as DefinitionComplete
	  , case 
		    when DataSource is null and Calculation is null and SourceTableField is null and SourceValue is null 
		    then 'No'
		    else 'Yes'
		  end                       as DefinitionAttempted

   from aeMM.Metric
   where ID is not null
)
--select * from aeMM.Metric where ID is not null;
, tEnterprise as (
  select
    EnterpriseID
    , Application
    , DefinitionComplete
  from tMetric
  where EnterpriseID is not null
 )
 , tEnterpriseOwner as (
  select 
      EnterpriseID
    , Min(Application) as CompleteOwner
  from tEnterprise
  where DefinitionComplete = 'Yes'
  group by EnterpriseID
 )
 , tEnterpriseOwner2 as (
  select
      EnterpriseID
    , min(Application) as IncompleteOwner
  from tEnterprise
  where DefinitionComplete = 'No'
  group by EnterpriseID
 )
 , tEnterpriseOwner3 as (
  select
      tEnterPrise.EnterpriseID
    , tEnterpriseOwner.CompleteOwner
    , tEnterpriseOwner2.IncompleteOwner
  from tEnterprise
  left join tEnterpriseOwner
    on tEnterprise.EnterpriseID = tEnterpriseOwner.EnterpriseID
  left join tEnterpriseOwner2
    on tEnterprise.EnterpriseID = tEnterpriseOwner2.EnterpriseID
 )
 
, tMetric2 as (
 select 
  tMetric.* 
  , case when CompleteOwner is null then IncompleteOwner else CompleteOwner end as OwningApplication
 from tMetric
 left join tEnterpriseOwner3
 on tMetric.EnterpriseID = tEnterpriseOwner3.EnterpriseID
 )
, Metric as (
 select
  *
  , case 
		  when EnterpriseID is null or (DefinitionAttempted = 'Yes' and Application <> OwningApplication)
		  then coalesce(Application,'') + '|' + coalesce(Metric, '') + '|' + coalesce(EnterpriseField, '')
		  else 'Enterprise|' + coalesce(EnterpriseID,'')
		end                       as MetricID
  , case 
		  when EnterpriseID is null or (DefinitionAttempted = 'Yes' and Application <> OwningApplication)
		  then 'No'
		  else 'Yes'
		end                       as EnterpriseMetric
  , case 
		  when EnterpriseID is not null and (DefinitionAttempted = 'Yes' and Application <> OwningApplication)
		  then 'Enterprise|' + coalesce(EnterpriseID,'')
		  else null
		end                       as EnterpriseDuplicateID
 from tMetric2
 )
 insert into #mmMetric 
 select * from Metric
 ;

 --select * from #mmMetric;

with tMetricCount as (
  select 
    MetricID
    , Count(MetricID) as UsageCount
  from #mmMetric
  group by MetricID
 )
, tMetricDuplicate as (
  select
    MetricID
    , case when count(case when len(EnterpriseDuplicateID) = 0 then Metric else null end)>1 then 'Yes' else 'No' end as DuplicateName
  from #mmMetric
  group by MetricID
)
 insert into atMM.MetricDefinition
  select 
      #mmMetric.MetricID
    , #mmMetric.EnterpriseID
    , #mmMetric.EnterpriseMetric
    , #mmMetric.EnterpriseDuplicateID
    , #mmMetric.Application as OwningApplication
    , case 
          when #mmMetric.Application is null
          then '*Unspecified'
          else case 
            when wkb.Path is null 
            then '*Unknown'
            else wkb.Path
          end
        end                       as OwningApplicationPath
    , #mmMetric.Metric
    , case when AffiliateTeamApprover is null then '*Unspecified' else AffiliateTeamApprover end as AffiliateTeamApprover
    , case when DataSource is null then '*Unspecified' else DataSource end as DataSource
    , case when Calculation is null then '*Unspecified' else Calculation end as Calculation
    , case when ConfigurationVariable is null then '*Unspecified' else ConfigurationVariable end as ConfigurationVariable
    , case when EnterpriseField is null then '*Unspecified' else EnterpriseField end as EnterpriseField
    , case when SourceTableField is null then '*Unspecified' else SourceTableField end as SourceTableField
    , case when SourceValue is null then '*Unspecified' else SourceValue end as SourceValue
    , case when SQLLogic is null then '*Unspecified' else SQLLogic end as SQLLogic
    , case when Timing is null then '*Unspecified' else Timing end as Timing
    , #mmMetric.DefinitionComplete
    , tMetricCount.UsageCount
    , tMetricDuplicate.DuplicateName
    , convert(date , getdate()) as SaveDate
    -- if we want to mess with the date..., dateadd(day,-1,convert(date , getdate())) as SaveDate
  from #mmMetric
  left join aeMM.Workbook wkb
    on #mmMetric.Application = wkb.Application
  left join tMetricCount
    on #mmMetric.MetricID = tMetricCount.MetricID
  left join tMetricDuplicate
    on #mmMetric.MetricID = tMetricDuplicate.MetricID
  where #mmMetric.EnterpriseMetric = 'No' or #mmMetric.Application = OwningApplication
 ;
 --select * from aeMM.workbook;
 --select * from aeMM.Metric;
 --select * from atMM.MetricDefinition order by MetricID, SaveDate;
 /*
    @source_string is the [Custom Extract] field value from ExtractConfig
    @delimiter is the character that indicates the end of the sub string 
    we can use this to create multiple rows for each entry in the source string
*/
 with tMetricDataSource as (
  select distinct
      MetricID
    , rtrim(ltrim(x.result_string)) as DataSource
  from atMM.MetricDefinition
  cross apply tools.ParseOnString(atMM.MetricDefinition.DataSource,char(10)) x --atMM.MetricDefinition
  where convert(date , getdate()) = SaveDate --only pull today's rows here for saving 
  and len(rtrim(ltrim(x.result_string))) > 0
 )
 
-- select * from tMetricDataSource;
  insert into atMM.MetricDataSource
  select 
      tMetricDataSource.MetricID
    , tMetricDataSource.DataSource
    , convert(date , getdate()) as SaveDate
  from tMetricDataSource
 ;

 with tMetricConfigurationVariable as (
  select distinct
      MetricID
    , rtrim(ltrim(x.result_string)) as MetricConfigurationVariable
  from atMM.MetricDefinition
  cross apply tools.ParseOnString(atMM.MetricDefinition.ConfigurationVariable,char(10)) x 
  where convert(date , getdate()) = SaveDate --only pull today's rows here for saving
  and len(rtrim(ltrim(x.result_string))) > 0
 )
 
--select * from tMetricConfigurationVariable;
  insert into atMM.MetricConfigurationVariable
  select 
      MetricID
    , MetricConfigurationVariable
    , convert(date , getdate()) as SaveDate
  from tMetricConfigurationVariable
 ;

  with tMetricSourceTableField as (
  select distinct
      MetricID
    , rtrim(ltrim(x.result_string)) as SourceTableField
  from atMM.MetricDefinition
  cross apply tools.ParseOnString(atMM.MetricDefinition.SourceTableField,char(10)) x 
  where convert(date , getdate()) = SaveDate --only pull today's rows here for saving
  and len(rtrim(ltrim(x.result_string))) > 0
 )
  , ttMetricSourceTableField as (
  select 
      MetricID
      , substring(SourceTableField,0,charindex('.',SourceTableField)) as SourceTable
      , substring(SourceTableField,charindex('.',SourceTableField)+1,len(SourceTableField) - charindex('.',SourceTableField)+1)  as SourceField
    
  from tMetricSourceTableField
 )
 --select * from ttMetricSourceTableField;
  insert into atMM.MetricSourceTableField
  select 
      MetricID
    , case when SourceTable is null then '*Unspecified' else SourceTable end as SourceTable
    , case when SourceField is null then '*Unspecified' else SourceField end as SourceField
    , convert(date , getdate()) as SaveDate
  from ttMetricSourceTableField
 ;
  --select * from atMM.MetricSourceTableField;
  with tMetricEnterpriseField as (
  select distinct
      MetricID
    , rtrim(ltrim(x.result_string)) as EnterpriseField
  from atMM.MetricDefinition
  cross apply tools.ParseOnString(atMM.MetricDefinition.EnterpriseField,char(10)) x 
  where convert(date , getdate()) = SaveDate --only pull today's rows here for saving
  and len(rtrim(ltrim(x.result_string))) > 0
 )
  , ttMetricEnterpriseField as (
  select 
      MetricID
    --, charindex('.',EnterpriseField, charindex('.',EnterpriseField)+1) - (charindex('.',EnterpriseField)+1)+1 as seconddot
    --, len(EnterPriseField) - charindex('.',EnterpriseField, charindex('.',EnterpriseField)+1)+1 - (charindex('.',EnterpriseField))+1 as thirdfieldstart
    --, EnterpriseField as origEnterpriseField
    , substring(EnterpriseField,0,charindex('.',EnterpriseField)) as EnterpriseLevel
    , replace(substring(
       EnterpriseField
      ,charindex('.',EnterpriseField)+1
      ,charindex('.',EnterpriseField, charindex('.',EnterpriseField)+1) - (charindex('.',EnterpriseField)+1)+1)
      , '.', '')  as EnterpriseTable
    , right(EnterpriseField, len(EnterpriseField) - charindex('.',EnterpriseField, charindex('.',EnterpriseField)+1)+1 - (charindex('.',EnterpriseField))+1  )as EnterpriseField
  
  from tmetricEnterpriseField
 )
--select * from ttMetricEnterpriseField;
  insert into atMM.MetricEnterpriseField
  select 
      MetricID
    , case when EnterpriseLevel is null or ltrim(rtrim(EnterpriseLevel)) = '' then '*Unspecified' else EnterpriseLevel end as EnterpriseLevel
    , case when EnterpriseTable is null or ltrim(rtrim(EnterpriseTable)) = '' then '*Unspecified' else EnterpriseTable end as EnterpriseTable
    , case when EnterpriseField is null or ltrim(rtrim(EnterpriseField)) = '' then '*Unspecified' else EnterpriseField end as EnterpriseField
    , convert(date , getdate()) as SaveDate
  from ttMetricEnterpriseField
 ;
--select * from atMM.MetricEnterpriseField;

/********************  Changes ****************************/
--find the latest two dates
with tDates as (
  select distinct top(2) SaveDate from atMM.MetricDefinition order by SaveDate desc
  )
  --select * from tDates;

, tChanges as(
  select distinct
    MetricDefinition.*
  from atMM.MetricDefinition
  inner join tDates on MetricDefinition.SaveDate = tDates.SaveDate
  )
--select * from tChanges;
 , ttChanges as (
  select 
      MetricID
    , 'Enterprise Metric' as ChangedTable
    , SaveDate
    --, case when lag(AffiliateTeamApprover,1) over (partition by MetricID order by SaveDate) <> AffiliateTeamApprover then 1 else 0 end as flag
    , case when lag(AffiliateTeamApprover,1) over (partition by MetricID order by SaveDate) <> AffiliateTeamApprover then lag(AffiliateTeamApprover,1) over (partition by MetricID order by SaveDate) end as ChangedFromValue
    , case when lag(AffiliateTeamApprover,1) over (partition by MetricID order by SaveDate) <> AffiliateTeamApprover then AffiliateTeamApprover end as ChangedToValue
    , case when lag(AffiliateTeamApprover,1) over (partition by MetricID order by SaveDate) <> AffiliateTeamApprover then 'Affiliate Team Approver' end as ChangedField
  from tChanges
  --order by MetricID, SaveDate desc
  union all
  select 
      MetricID
    , 'Enterprise Metric' as ChangedTable
    , SaveDate
    , case when lag(DataSource,1) over (partition by MetricID order by SaveDate) <> DataSource then lag(DataSource,1) over (partition by MetricID order by SaveDate) end as ChangedFromValue
    , case when lag(DataSource,1) over (partition by MetricID order by SaveDate) <> DataSource then DataSource end as ChangedToValue
    , case when lag(DataSource,1) over (partition by MetricID order by SaveDate) <> DataSource then 'Data Source' end as ChangedField
  from tChanges
  union all
  select 
      MetricID
    , 'Enterprise Metric' as ChangedTable
    , SaveDate
    , case when lag(Calculation,1) over (partition by MetricID order by SaveDate) <> Calculation then lag(Calculation,1) over (partition by MetricID order by SaveDate) end as ChangedFromValue
    , case when lag(Calculation,1) over (partition by MetricID order by SaveDate) <> Calculation then Calculation end as ChangedToValue
    , case when lag(Calculation,1) over (partition by MetricID order by SaveDate) <> Calculation then 'Calculation' end as ChangedField
  from tChanges
  union all
  select 
      MetricID
    , 'Enterprise Metric' as ChangedTable
    , SaveDate
    , case when lag(ConfigurationVariable,1) over (partition by MetricID order by SaveDate) <> ConfigurationVariable then lag(ConfigurationVariable,1) over (partition by MetricID order by SaveDate) end as ChangedFromValue
    , case when lag(ConfigurationVariable,1) over (partition by MetricID order by SaveDate) <> ConfigurationVariable then ConfigurationVariable end as ChangedToValue
    , case when lag(ConfigurationVariable,1) over (partition by MetricID order by SaveDate) <> ConfigurationVariable then 'Configuration Variable' end as ChangedField
  from tChanges
  union all
  select 
      MetricID
    , 'Enterprise Metric' as ChangedTable
    , SaveDate
    , case when lag(EnterpriseField,1) over (partition by MetricID order by SaveDate) <> EnterpriseField then lag(EnterpriseField,1) over (partition by MetricID order by SaveDate) end as ChangedFromValue
    , case when lag(EnterpriseField,1) over (partition by MetricID order by SaveDate) <> EnterpriseField then EnterpriseField end as ChangedToValue
    , case when lag(EnterpriseField,1) over (partition by MetricID order by SaveDate) <> EnterpriseField then 'Enterprise Field' end as ChangedField
  from tChanges
  union all
  select 
      MetricID
    , 'Enterprise Metric' as ChangedTable
    , SaveDate
    , case when lag(SourceTableField,1) over (partition by MetricID order by SaveDate) <> SourceTableField then lag(SourceTableField,1) over (partition by MetricID order by SaveDate) end as ChangedFromValue
    , case when lag(SourceTableField,1) over (partition by MetricID order by SaveDate) <> SourceTableField then SourceTableField end as ChangedToValue
    , case when lag(SourceTableField,1) over (partition by MetricID order by SaveDate) <> SourceTableField then 'SourceTable Field' end as ChangedField
  from tChanges
  union all
  select 
      MetricID
    , 'Enterprise Metric' as ChangedTable
    , SaveDate
    , case when lag(SourceValue,1) over (partition by MetricID order by SaveDate) <> SourceValue then lag(SourceValue,1) over (partition by MetricID order by SaveDate) end as ChangedFromValue
    , case when lag(SourceValue,1) over (partition by MetricID order by SaveDate) <> SourceValue then SourceValue end as ChangedToValue
    , case when lag(SourceValue,1) over (partition by MetricID order by SaveDate) <> SourceValue then 'Source Value' end as ChangedField
  from tChanges
  union all
  select 
      MetricID
    , 'Enterprise Metric' as ChangedTable
    , SaveDate
    , case when lag(SQLLogic,1) over (partition by MetricID order by SaveDate) <> SQLLogic then lag(SQLLogic,1) over (partition by MetricID order by SaveDate) end as ChangedFromValue
    , case when lag(SQLLogic,1) over (partition by MetricID order by SaveDate) <> SQLLogic then SQLLogic end as ChangedToValue
    , case when lag(SQLLogic,1) over (partition by MetricID order by SaveDate) <> SQLLogic then 'SQL Logic' end as ChangedField
  from tChanges
  union all
  select 
      MetricID
    , 'Enterprise Metric' as ChangedTable
    , SaveDate
    , case when lag(Timing,1) over (partition by MetricID order by SaveDate) <> Timing then lag(Timing,1) over (partition by MetricID order by SaveDate) end as ChangedFromValue
    , case when lag(Timing,1) over (partition by MetricID order by SaveDate) <> Timing then Timing end as ChangedToValue
    , case when lag(Timing,1) over (partition by MetricID order by SaveDate) <> Timing then 'Timing' end as ChangedField
  from tChanges
  union all
  select 
      MetricID
    , 'Enterprise Metric' as ChangedTable
    , SaveDate
    , case when lag(DefinitionComplete,1) over (partition by MetricID order by SaveDate) <> DefinitionComplete then lag(DefinitionComplete,1) over (partition by MetricID order by SaveDate) end as ChangedFromValue
    , case when lag(DefinitionComplete,1) over (partition by MetricID order by SaveDate) <> DefinitionComplete then DefinitionComplete end as ChangedToValue
    , case when lag(DefinitionComplete,1) over (partition by MetricID order by SaveDate) <> DefinitionComplete then 'Definition Complete' end as ChangedField
  from tChanges
  )

  --select * from ttChanges;

  insert into atMM.Changes 
  select 
      null as Application
    , null as DimensionID
    , MetricID
    , ChangedTable
    , ChangedFromValue
    , ChangedToValue
    , ChangedField  
    , convert(date , getdate()) as SaveDate
  from ttChanges 
  where len(ChangedFromValue) > 0 or len(ChangedToValue) > 0
  ;
  --select * from atMM.Changes;


/************************* Application Metric ******************************/
insert into atMM.ApplicationMetric
select 
    Application
    , MetricID
    , case when MetricType is null then '*Unspecified' else MetricType end as MetricType
    , case when InScope is null then '*Unspecified' else InScope end as InScope
    , case when DefinitionStatus is null then '*Unspecified' else DefinitionStatus end as DefinitionStatus
    , case when Approver is null then '*Unspecified' else Approver end as Approver
    , case when Tab is null then '*Unspecified' else Tab end as Tab
    , case when OperationalSME is null then '*Unspecified' else OperationalSME end as OperationalSME
    , case when TechnicalSME is null then '*Unspecified' else TechnicalSME end as TechnicalSME
    , case when Dimensions is null then '*Unspecified' else Dimensions end as Dimensions
    , case when Security is null then '*Unspecified' else Security end as Security
    , case when DisplayName is null then '*Unspecified' else DisplayName end as DisplayName
    , case when UIVariable is null then '*Unspecified' else UIVariable end as UIVariable
    , case when Visualization is null then '*Unspecified' else Visualization end as Visualization
    , case when Target is null then '*Unspecified' else Target end as Target
    --, convert(date , dateadd(day,-1,getdate())) as SaveDate
    , convert(date , getdate()) as SaveDate
from #mmMetric;

--find the latest two dates
with tDates as (
  select distinct top(2) SaveDate from atMM.ApplicationMetric order by SaveDate desc
  )
  --select * from tDates;

, tChanges as(
  select distinct
    ApplicationMetric.*
  from atMM.ApplicationMetric
  inner join tDates on ApplicationMetric.SaveDate = tDates.SaveDate
  )
 
 , ttChanges as (
   select 
      Application
    , MetricID
    , 'Application Metric' as ChangedTable
    --, SaveDate
    , case when lag(InScope,1) over (partition by Application, MetricID order by SaveDate) <> InScope then lag(InScope,1) over (partition by Application, MetricID order by SaveDate) end as ChangedFromValue
    , case when lag(InScope,1) over (partition by Application, MetricID order by SaveDate) <> InScope then InScope end as ChangedToValue
    , case when lag(InScope,1) over (partition by Application, MetricID order by SaveDate) <> InScope then 'In Scope' end as ChangedField
  from tChanges
  union all
  select 
      Application
    , MetricID
    , 'Application Metric' as ChangedTable
    --, SaveDate
    , case when lag(DefinitionStatus,1) over (partition by Application, MetricID order by SaveDate) <> DefinitionStatus then lag(DefinitionStatus,1) over (partition by Application, MetricID order by SaveDate) end as ChangedFromValue
    , case when lag(DefinitionStatus,1) over (partition by Application, MetricID order by SaveDate) <> DefinitionStatus then DefinitionStatus end as ChangedToValue
    , case when lag(DefinitionStatus,1) over (partition by Application, MetricID order by SaveDate) <> DefinitionStatus then 'Definition Status' end as ChangedField
  from tChanges
  union all
  select 
      Application
    , MetricID
    , 'Application Metric' as ChangedTable
    --, SaveDate
    , case when lag(Approver,1) over (partition by Application, MetricID order by SaveDate) <> Approver then lag(Approver,1) over (partition by Application, MetricID order by SaveDate) end as ChangedFromValue
    , case when lag(Approver,1) over (partition by Application, MetricID order by SaveDate) <> Approver then Approver end as ChangedToValue
    , case when lag(Approver,1) over (partition by Application, MetricID order by SaveDate) <> Approver then 'Approver' end as ChangedField
  from tChanges
  union all
  select 
      Application
    , MetricID
    , 'Application Metric' as ChangedTable
    --, SaveDate
    , case when lag(Tab,1) over (partition by Application, MetricID order by SaveDate) <> Tab then lag(Tab,1) over (partition by Application, MetricID order by SaveDate) end as ChangedFromValue
    , case when lag(Tab,1) over (partition by Application, MetricID order by SaveDate) <> Tab then Tab end as ChangedToValue
    , case when lag(Tab,1) over (partition by Application, MetricID order by SaveDate) <> Tab then 'Tab' end as ChangedField
  from tChanges
  union all
  select 
      Application
    , MetricID
    , 'Application Metric' as ChangedTable
    --, SaveDate
    , case when lag(OperationalSME,1) over (partition by Application, MetricID order by SaveDate) <> OperationalSME then lag(OperationalSME,1) over (partition by Application, MetricID order by SaveDate) end as ChangedFromValue
    , case when lag(OperationalSME,1) over (partition by Application, MetricID order by SaveDate) <> OperationalSME then OperationalSME end as ChangedToValue
    , case when lag(OperationalSME,1) over (partition by Application, MetricID order by SaveDate) <> OperationalSME then 'Operational SME' end as ChangedField
  from tChanges
  union all
  select 
      Application
    , MetricID
    , 'Application Metric' as ChangedTable
    --, SaveDate
    , case when lag(TechnicalSME,1) over (partition by Application, MetricID order by SaveDate) <> TechnicalSME then lag(TechnicalSME,1) over (partition by Application, MetricID order by SaveDate) end as ChangedFromValue
    , case when lag(TechnicalSME,1) over (partition by Application, MetricID order by SaveDate) <> TechnicalSME then TechnicalSME end as ChangedToValue
    , case when lag(TechnicalSME,1) over (partition by Application, MetricID order by SaveDate) <> TechnicalSME then 'Technical SME' end as ChangedField
  from tChanges
  union all
  select 
      Application
    , MetricID
    , 'Application Metric' as ChangedTable
    --, SaveDate
    , case when lag(Dimensions,1) over (partition by Application, MetricID order by SaveDate) <> Dimensions then lag(Dimensions,1) over (partition by Application, MetricID order by SaveDate) end as ChangedFromValue
    , case when lag(Dimensions,1) over (partition by Application, MetricID order by SaveDate) <> Dimensions then Dimensions end as ChangedToValue
    , case when lag(Dimensions,1) over (partition by Application, MetricID order by SaveDate) <> Dimensions then 'Dimensions' end as ChangedField
  from tChanges
  union all
  select 
      Application
    , MetricID
    , 'Application Metric' as ChangedTable
    --, SaveDate
    , case when lag(Security,1) over (partition by Application, MetricID order by SaveDate) <> Security then lag(Security,1) over (partition by Application, MetricID order by SaveDate) end as ChangedFromValue
    , case when lag(Security,1) over (partition by Application, MetricID order by SaveDate) <> Security then Security end as ChangedToValue
    , case when lag(Security,1) over (partition by Application, MetricID order by SaveDate) <> Security then 'Security' end as ChangedField
  from tChanges
  union all
  select 
      Application
    , MetricID
    , 'Application Metric' as ChangedTable
    --, SaveDate
    , case when lag(DisplayName,1) over (partition by Application, MetricID order by SaveDate) <> DisplayName then lag(DisplayName,1) over (partition by Application, MetricID order by SaveDate) end as ChangedFromValue
    , case when lag(DisplayName,1) over (partition by Application, MetricID order by SaveDate) <> DisplayName then DisplayName end as ChangedToValue
    , case when lag(DisplayName,1) over (partition by Application, MetricID order by SaveDate) <> DisplayName then 'Display Name' end as ChangedField
  from tChanges
  union all
  select 
      Application
    , MetricID
    , 'Application Metric' as ChangedTable
    --, SaveDate
    , case when lag(UIVariable,1) over (partition by Application, MetricID order by SaveDate) <> UIVariable then lag(UIVariable,1) over (partition by Application, MetricID order by SaveDate) end as ChangedFromValue
    , case when lag(UIVariable,1) over (partition by Application, MetricID order by SaveDate) <> UIVariable then UIVariable end as ChangedToValue
    , case when lag(UIVariable,1) over (partition by Application, MetricID order by SaveDate) <> UIVariable then 'UI Variable' end as ChangedField
  from tChanges
  union all
  select 
      Application
    , MetricID
    , 'Application Metric' as ChangedTable
    --, SaveDate
    , case when lag(Visualization,1) over (partition by Application, MetricID order by SaveDate) <> Visualization then lag(Visualization,1) over (partition by Application, MetricID order by SaveDate) end as ChangedFromValue
    , case when lag(Visualization,1) over (partition by Application, MetricID order by SaveDate) <> Visualization then Visualization end as ChangedToValue
    , case when lag(Visualization,1) over (partition by Application, MetricID order by SaveDate) <> Visualization then 'Visualization' end as ChangedField
  from tChanges
  union all
  select 
      Application
    , MetricID
    , 'Application Metric' as ChangedTable
    --, SaveDate
    , case when lag(Target,1) over (partition by Application, MetricID order by SaveDate) <> Target then lag(Target,1) over (partition by Application, MetricID order by SaveDate) end as ChangedFromValue
    , case when lag(Target,1) over (partition by Application, MetricID order by SaveDate) <> Target then Target end as ChangedToValue
    , case when lag(Target,1) over (partition by Application, MetricID order by SaveDate) <> Target then 'Target' end as ChangedField
  from tChanges
  union all
  select 
      Application
    , MetricID
    , 'Application Metric' as ChangedTable
    --, SaveDate
    , case when lag(MetricType,1) over (partition by Application, MetricID order by SaveDate) <> MetricType then lag(MetricType,1) over (partition by Application, MetricID order by SaveDate) end as ChangedFromValue
    , case when lag(MetricType,1) over (partition by Application, MetricID order by SaveDate) <> MetricType then MetricType end as ChangedToValue
    , case when lag(MetricType,1) over (partition by Application, MetricID order by SaveDate) <> MetricType then 'Metric Type' end as ChangedField
  from tChanges

  )
  insert into atMM.Changes 
  select 
      Application
    , null as DimensionID
    , MetricID
    , ChangedTable
    , ChangedFromValue
    , ChangedToValue
    , ChangedField  
    , convert(date , getdate()) as SaveDate
  from ttChanges 
  where len(ChangedFromValue) > 0 or len(ChangedToValue) > 0
  ;

  --select * from atMM.Changes;

  /* create rows for all entries in the UIVariable field */
 with tApplicationUIVariable as (
  select distinct
      Application
    , MetricID
    , rtrim(ltrim(x.result_string)) as UIVariable
  from atMM.ApplicationMetric
  cross apply tools.ParseOnString(atMM.ApplicationMetric.UIVariable,char(10)) x 
  where convert(date , getdate()) = SaveDate --only pull today's rows here for saving 
  and len(rtrim(ltrim(x.result_string))) > 0
 )
 --select * from tApplicationUIVariable;

insert into atMM.ApplicationUIVariable
  select 
      Application
    , MetricID
    , UIVariable
    , convert(date , getdate()) as SaveDate
  from tApplicationUIVariable
  where len(UIVariable) > 0
 ;
 --select * from atMM.ApplicationUIVariable;

 /* save off app workbook list */
 insert into atMM.Workbook
  select 
    Application
    , Path
    , convert(date , getdate()) as SaveDate
  from aeMM.Workbook
  ;

  --select * from atMM.Workbook;


END
GO
