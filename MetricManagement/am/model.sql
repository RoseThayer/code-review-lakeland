-- =============================================
-- Author:		Prominence Advisors
-- Create date: 9/1/2017
-- Description:	Populate the data model for the metric management application

-- We currently have separate tables for the latest set of data and the history data
-- These could all easily be combined into a single table if needed and we could reference the SaveDate
-- to pull various dates in the UI

-- =============================================

USE padev
--SET ANSI_NULLS ON
--GO
--SET QUOTED_IDENTIFIER ON
GO



  
ALTER PROCEDURE amMM.MetricManagementModel --SET TO CREATE ON FIRST RUN INSTEAD OF ALTER
	-- Add the parameters for the stored procedure here
AS
BEGIN
  -- SET NOCOUNT ON added to prevent extra result sets from
  -- interfering with SELECT statements.
  SET NOCOUNT ON;



/************************** Metric Definition ***********************************/
--grab the latest date's worth of entries and save them
     --drop table amMM.MetricDefinition;
  if OBJECT_ID('amMM.MetricDefinition') is null 
  create table amMM.MetricDefinition (
    Metric                               varchar(max) null
  , [Metric Enterprise ID]               varchar(max) null
  , [Metric Is Enterprise]               varchar(max) null
  , [Metric Duplicate Enterprise ID]     varchar(max) null
  , [Metric Owning Application]          varchar(max) null
  , [Metric Owning Application Path]     varchar(max) null
  , [Metric Name]                        varchar(max) null
  , [Metric Affiliate Team Owner]        varchar(max) null
  , [Metric All Data Sources]            varchar(max) null
  , [Metric Calculation]                 varchar(max) null
  , [Metric All Configuration Variables] varchar(max) null
  , [Metric All Enterprise Fields]       varchar(max) null
  , [Metric All Source Table Fields]     varchar(max) null
  , [Metric Source Value]                varchar(max) null
  , [Metric SQL Logic]                   varchar(max) null
  , [Metric Timing]                      varchar(max) null
  , [Metric Definition Complete]         varchar(max) null
  , [Metric Usage Count]                 varchar(max) null
  , [Metric Duplicate Name]              varchar(max) null
  , [Metric Save Date]                   date
  );

/************************** History Metric Definition ***********************************/
     --drop table amMM.HistoryMetricDefinition;
  if OBJECT_ID('amMM.HistoryMetricDefinition') is null 
  create table amMM.HistoryMetricDefinition (
    [History Metric Date]                        varchar(max) null
  , [History Metric Enterprise ID]               varchar(max) null
  , [History Metric Is Enterprise]               varchar(max) null
  , [History Metric Duplicate Enterprise ID]     varchar(max) null
  , [History Metric Owning Application]          varchar(max) null
  , [History Metric Owning Application Path]     varchar(max) null
  , [History Metric Name]                        varchar(max) null
  , [History Metric Affiliate Team Owner]        varchar(max) null
  , [History Metric All Data Sources]            varchar(max) null
  , [History Metric Calculation]                 varchar(max) null
  , [History Metric All Configuration Variables] varchar(max) null
  , [History Metric All Enterprise Fields]       varchar(max) null
  , [History Metric All Source Table Fields]     varchar(max) null
  , [History Metric Source Value]                varchar(max) null
  , [History Metric SQL Logic]                   varchar(max) null
  , [History Metric Timing]                      varchar(max) null
  , [History Metric Definition Complete]         varchar(max) null
  , [History Metric Usage Count]                 varchar(max) null
  , [History Metric Duplicate Name]              varchar(max) null
  , [History Metric Save Date]                   date
  );

/************************** Metric Configuration Variable ***********************************/
     --drop table amMM.MetricConfigurationVariable;
  if OBJECT_ID('amMM.MetricConfigurationVariable') is null 
  create table amMM.MetricConfigurationVariable (
    Metric                          varchar(max) null
  , [Metric Configuration Variable] varchar(max) null
  , [Metric Save Date]              date
  );

/************************** History Metric Configuration Variable ***********************************/
     --drop table amMM.HistoryMetricConfigurationVariable;
  if OBJECT_ID('amMM.HistoryMetricConfigurationVariable') is null 
  create table amMM.HistoryMetricConfigurationVariable (
    [History Metric Date]                   varchar(max) null
  , [History Metric Configuration Variable] varchar(max) null
  , [History Metric Save Date]              date
  );

/************************** MetricDataSource ***********************************/
     --drop table amMM.MetricDataSource;
  if OBJECT_ID('amMM.MetricDataSource') is null 
  create table amMM.MetricDataSource (
    Metric                          varchar(max) null
  , [Metric Data Source]            varchar(max) null
  , [Metric Save Date]              date
  );

/************************** HistoryMetricDataSource ***********************************/
     --drop table amMM.HistoryMetricDataSource;
  if OBJECT_ID('amMM.HistoryMetricDataSource') is null 
  create table amMM.HistoryMetricDataSource (
    [History Metric Date]                   varchar(max) null
  , [History Metric Data Source]            varchar(max) null
  , [History Metric Save Date]              date
  );

/************************** MetricSourceField ***********************************/
     --drop table amMM.MetricSourceField;
  if OBJECT_ID('amMM.MetricSourceField') is null 
  create table amMM.MetricSourceField (
    Metric                          varchar(max) null
  , [Metric Source Table]           varchar(max) null
  , [Metric Source Field]           varchar(max) null
  , [Metric Save Date]              date
  );

/************************** HistoryMetricDataSource ***********************************/
     --drop table amMM.HistoryMetricSourceField;
  if OBJECT_ID('amMM.HistoryMetricSourceField') is null 
  create table amMM.HistoryMetricSourceField (
    [History Metric Date]                   varchar(max) null
  , [History Metric Source Table]           varchar(max) null
  , [History Metric Source Field]           varchar(max) null
  , [History Metric Save Date]              date
  );

/************************** MetricEnterpriseField ***********************************/
     --drop table amMM.MetricEnterpriseField;
  if OBJECT_ID('amMM.MetricEnterpriseField') is null 
  create table amMM.MetricEnterpriseField (
    Metric                          varchar(max) null
  , [Metric Enterprise Level]       varchar(max) null
  , [Metric Enterprise Table]       varchar(max) null
  , [Metric Enterprise Field]       varchar(max) null
  , [Metric Save Date]              date
  );

/************************** HistoryMetricEnterpriseField ***********************************/
     --drop table amMM.HistoryMetricEnterpriseField;
  if OBJECT_ID('amMM.HistoryMetricEnterpriseField') is null 
  create table amMM.HistoryMetricEnterpriseField (
    [History Metric Date]                   varchar(max) null
  , [History Metric Enterprise Level]       varchar(max) null
  , [History Metric Enterprise Table]       varchar(max) null
  , [History Metric Enterprise Field]       varchar(max) null
  , [History Metric Save Date]              date
  );

/************************** ApplicationMetric ***********************************/
     --drop table amMM.ApplicationMetric;
  if OBJECT_ID('amMM.ApplicationMetric') is null 
  create table amMM.ApplicationMetric (
    [Application]                           varchar(max) null
  , [Metric]                                varchar(max) null
  , [Application Metric]                    varchar(max) null
  , [Metric Application Metric Type]        varchar(max) null
  , [Metric Application In Scope]           varchar(max) null
  , [Metric Application Definition Status]  varchar(max) null
  , [Metric Application Approver]           varchar(max) null
  , [Metric Application Tab]                varchar(max) null
  , [Metric Application Operational SME]    varchar(max) null
  , [Metric Application Technical SME]      varchar(max) null
  , [Metric Application Dimensions]         varchar(max) null
  , [Metric Application Security]           varchar(max) null
  , [Metric Application Display Name]       varchar(max) null
  , [Metric Application UI Variable]        varchar(max) null
  , [Metric Application Visualization]      varchar(max) null
  , [Metric Application Target]             varchar(max) null
  , [Metric Application Save Date]          date null
  );

/************************** HistoryApplicationMetric ***********************************/
     --drop table amMM.HistoryApplicationMetric;
  if OBJECT_ID('amMM.HistoryApplicationMetric') is null 
  create table amMM.HistoryApplicationMetric (
    [History Application Date]                      varchar(max) null
  , [History Metric Date]                           varchar(max) null
  , [History Application Metric Date]               varchar(max) null
  , [History Metric Application Metric Type]        varchar(max) null
  , [History Metric Application In Scope]           varchar(max) null
  , [History Metric Application Definition Status]  varchar(max) null
  , [History Metric Application Approver]           varchar(max) null
  , [History Metric Application Tab]                varchar(max) null
  , [History Metric Application Operational SME]    varchar(max) null
  , [History Metric Application Technical SME]      varchar(max) null
  , [History Metric Application Dimensions]         varchar(max) null
  , [History Metric Application Security]           varchar(max) null
  , [History Metric Application Display Name]       varchar(max) null
  , [History Metric Application UI Variable]        varchar(max) null
  , [History Metric Application Visualization]      varchar(max) null
  , [History Metric Application Target]             varchar(max) null
  , [History Metric Application Save Date]          date null
  );

  /************************** ApplicationMetricUIVariable ***********************************/
     --drop table amMM.ApplicationMetricUIVariable;
  if OBJECT_ID('amMM.ApplicationMetricUIVariable') is null 
  create table amMM.ApplicationMetricUIVariable (
    [Application Metric]             varchar(max) null
  , [Metric Application UI Variable] varchar(max) null
  , [Metric Application Save Date]   date
  );

/************************** HistoryApplicationMetricUIVariablee ***********************************/
     --drop table amMM.HistoryApplicationMetricUIVariable;
  if OBJECT_ID('amMM.HistoryApplicationMetricUIVariable') is null 
  create table amMM.HistoryApplicationMetricUIVariable (
    [History Application Metric Date]        varchar(max) null
  , [History Metric Application UI Variable] varchar(max) null
  , [History Metric Application Save Date]   date
  );
  
/************************** Definition ***********************************/
     --drop table amMM.Definition;
  if OBJECT_ID('amMM.Definition') is null 
  create table amMM.Definition (
    [Definition ID]                 varchar(max) null
  , [Definition Tab]                varchar(max) null
  , [Definition Grouping]           varchar(max) null
  , [Definition Column Name]        varchar(max) null
  , [Definition Column Description] varchar(max) null
  , [Definition Level]              varchar(max) null
  , [Definition Required]           varchar(max) null
  );


/************************ HistoryMetricChanges ***************************/
     --drop table amMM.HistoryMetricChanges;
  if OBJECT_ID('amMM.HistoryMetricChanges') is null 
  create table amMM.HistoryMetricChanges (
    [History Metric Date]                varchar(max) null
  , [History Metric Changed Table]       varchar(max) null
  , [History Metric Changed From Value]  varchar(max) null
  , [Hisotry Metric Changed To Value]    varchar(max) null
  , [History Metric Changed Field]       varchar(max) null
  , [History Metric Save Date]           date
  );


    --clear the definition table
  if OBJECT_ID('amMM.MetricDefinition') is not null 
  truncate table amMM.MetricDefinition;

  --clear the history definition table
  if OBJECT_ID('amMM.HistoryMetricDefinition') is not null 
  truncate table amMM.HistoryMetricDefinition;

    --clear the MetricConfigurationVariable table
  if OBJECT_ID('amMM.MetricConfigurationVariable') is not null 
  truncate table amMM.MetricConfigurationVariable;

      --clear the HistoryMetricConfigurationVariable table
  if OBJECT_ID('amMM.HistoryMetricConfigurationVariable') is not null 
  truncate table amMM.HistoryMetricConfigurationVariable;

    --clear the MetricDataSource table
  if OBJECT_ID('amMM.MetricDataSource') is not null 
  truncate table amMM.MetricDataSource;

      --clear the HistoryMetricDataSource table
  if OBJECT_ID('amMM.HistoryMetricDataSource') is not null 
  truncate table amMM.HistoryMetricDataSource;

    --clear the MetricSourceField table
  if OBJECT_ID('amMM.MetricSourceField') is not null 
  truncate table amMM.MetricSourceField;

      --clear the HistoryMetricSourceField table
  if OBJECT_ID('amMM.HistoryMetricSourceField') is not null 
  truncate table amMM.HistoryMetricSourceField;

    --clear the MetricEnterpriseField table
  if OBJECT_ID('amMM.MetricEnterpriseField') is not null 
  truncate table amMM.MetricEnterpriseField;

      --clear the HistoryMetricEnterpriseField table
  if OBJECT_ID('amMM.HistoryMetricEnterpriseField') is not null 
  truncate table amMM.HistoryMetricEnterpriseField;

    --clear the ApplicationMetric table
  if OBJECT_ID('amMM.ApplicationMetric') is not null 
  truncate table amMM.ApplicationMetric;

      --clear the HistoryApplicationMetric table
  if OBJECT_ID('amMM.HistoryApplicationMetric') is not null 
  truncate table amMM.HistoryApplicationMetric;

    --clear the ApplicationMetricUIVariable table
  if OBJECT_ID('amMM.ApplicationMetricUIVariable') is not null 
  truncate table amMM.ApplicationMetricUIVariable;

      --clear the HistoryApplicationMetricUIVariable table
  if OBJECT_ID('amMM.HistoryApplicationMetricUIVariable') is not null 
  truncate table amMM.HistoryApplicationMetricUIVariable;

      --clear the Definition table
  if OBJECT_ID('amMM.Definition') is not null 
  truncate table amMM.Definition;

      --clear the HistoryMetricChanges table
  if OBJECT_ID('amMM.HistoryMetricChanges') is not null 
  truncate table amMM.HistoryMetricChanges;

  /***************************** Populate MetricDefinition ******************************/
 with tDates as (
  select distinct top(1) SaveDate from atMM.MetricDefinition order by SaveDate desc
  )
  --select * from tDates;

insert into amMM.MetricDefinition 
  select
    MetricID              as Metric
  , EnterpriseID          as [Metric Enterprise ID]
  , EnterpriseMetric      as [Metric Is Enterprise]
  , EnterpriseDuplicateID as [Metric Duplicate Enterprise ID]
  , OwningApplication     as [Metric Owning Application]
  , OwningApplicationPath as [Metric Owning Application Path]
  , Metric                as [Metric Name]
  , AffiliateTeamApprover as [Metric Affiliate Team Owner]
  , DataSource            as [Metric All Data Sources]
  , Calculation           as [Metric Calculation]
  , ConfigurationVariable as [Metric All Configuration Variables]
  , EnterpriseField       as [Metric All Enterprise Fields]
  , SourceTableField      as [Metric All Source Table Fields]
  , SourceValue           as [Metric Source Value]
  , [SQLLogic]            as [Metric SQL Logic]
  , Timing                as [Metric Timing]
  , DefinitionComplete    as [Metric Definition Complete]
  , UsageCount            as [Metric Usage Count]
  , DuplicateName         as [Metric Duplicate Name]
  , MetricDefinition.SaveDate              as [Metric Save Date]
  from atMM.MetricDefinition
  inner join tDates on MetricDefinition.SaveDate = tDates.SaveDate
  ;
  
  --select * from amMM.MetricDefinition;
  
  
/************************ Populate HistoryMetricDefinition *************************/
 with tDates as (
  select distinct top(1) SaveDate from atMM.MetricDefinition order by SaveDate desc
  )
  --select * from tDates;

insert into amMM.HistoryMetricDefinition 
  select
    MetricID + '|' + convert(varchar,MetricDefinition.SaveDate)            as [History Metric Date]
  , EnterpriseID          as [History Metric Enterprise ID]
  , EnterpriseMetric      as [History Metric Is Enterprise]
  , EnterpriseDuplicateID as [History Metric Duplicate Enterprise ID]
  , OwningApplication     as [History Metric Owning Application]
  , OwningApplicationPath as [History Metric Owning Application Path]
  , Metric                as [History Metric Name]
  , AffiliateTeamApprover as [History Metric Affiliate Team Owner]
  , DataSource            as [History Metric All Data Sources]
  , Calculation           as [History Metric Calculation]
  , ConfigurationVariable as [History Metric All Configuration Variables]
  , EnterpriseField       as [History Metric All Enterprise Fields]
  , SourceTableField      as [History Metric All Source Table Fields]
  , SourceValue           as [History Metric Source Value]
  , [SQLLogic]            as [History Metric SQL Logic]
  , Timing                as [History Metric Timing]
  , DefinitionComplete    as [History Metric Definition Complete]
  , UsageCount            as [History Metric Usage Count]
  , DuplicateName         as [History Metric Duplicate Name]
  , MetricDefinition.SaveDate as [History Metric Save Date]
  from atMM.MetricDefinition
  inner join tDates on MetricDefinition.SaveDate <> tDates.SaveDate
  ;
  
  --select * from amMM.HistoryMetricDefinition;


  /***************************** Populate MetricConfigurationVariable ******************************/
 with tDates as (
  select distinct top(1) SaveDate from atMM.MetricConfigurationVariable order by SaveDate desc
  )
  --select * from tDates;

insert into amMM.MetricConfigurationVariable 
  select
    MetricID                              as Metric
  , ConfigurationVariable                 as [Metric Configuration Variable]
  , MetricConfigurationVariable.SaveDate  as [Metric Save Date]
  from atMM.MetricConfigurationVariable
  inner join tDates on MetricConfigurationVariable.SaveDate = tDates.SaveDate
  ;
  
  --select * from amMM.MetricConfigurationVariable;

  /***************************** Populate HistoryMetricConfigurationVariable ******************************/
 with tDates as (
  select distinct top(1) SaveDate from atMM.MetricConfigurationVariable order by SaveDate desc
  )
  --select * from tDates;

insert into amMM.HistoryMetricConfigurationVariable 
  select
    MetricID + '|' + convert(varchar,MetricConfigurationVariable.SaveDate)  as [History Metric Date]
  , ConfigurationVariable                                                   as [History Metric Configuration Variable]
  , MetricConfigurationVariable.SaveDate                                    as [History Metric Save Date]
  from atMM.MetricConfigurationVariable
  inner join tDates on MetricConfigurationVariable.SaveDate <> tDates.SaveDate
  ;
  
  --select * from amMM.HistoryMetricConfigurationVariable;

  /***************************** Populate MetricDataSource ******************************/
 with tDates as (
  select distinct top(1) SaveDate from atMM.MetricDataSource order by SaveDate desc
  )
  --select * from tDates;

insert into amMM.MetricDataSource 
  select
    MetricID                   as Metric
  , DataSource                 as [Metric Data Source]
  , MetricDataSource.SaveDate  as [Metric Save Date]
  from atMM.MetricDataSource
  inner join tDates on MetricDataSource.SaveDate = tDates.SaveDate
  ;
  
  --select * from amMM.MetricDataSource;

  /***************************** Populate HistoryMetricDataSource ******************************/
 with tDates as (
  select distinct top(1) SaveDate from atMM.MetricDataSource order by SaveDate desc
  )
  --select * from tDates;

insert into amMM.HistoryMetricDataSource 
  select
    MetricID + '|' + convert(varchar,MetricDataSource.SaveDate)  as [History Metric Date]
  , DataSource                                                   as [History Metric Data Source]
  , MetricDataSource.SaveDate                                    as [History Metric Save Date]
  from atMM.MetricDataSource
  inner join tDates on MetricDataSource.SaveDate <> tDates.SaveDate
  ;
  
  --select * from amMM.HistoryMetricDataSource;

  /***************************** Populate MetricSourceField ******************************/
 with tDates as (
  select distinct top(1) SaveDate from atMM.MetricSourceTableField order by SaveDate desc
  )
  --select * from tDates;

insert into amMM.MetricSourceField 
  select
    MetricID                          as Metric
  , SourceTable                       as [Metric Source Table]
  , SourceField                       as [Metric Source Field]
  , MetricSourceTableField.SaveDate   as [Metric Save Date]
  from atMM.MetricSourceTableField
  inner join tDates on MetricSourceTableField.SaveDate = tDates.SaveDate
  ;
  
  --select * from amMM.MetricDataSource;

  /***************************** Populate HistoryMetricSourceField ******************************/
 with tDates as (
  select distinct top(1) SaveDate from atMM.MetricSourceTableField order by SaveDate desc
  )
  --select * from tDates;

insert into amMM.HistoryMetricSourceField
  select
    MetricID + '|' + convert(varchar,MetricSourceTableField.SaveDate)   as [History Metric Date]
  , SourceTable                                                         as [History Metric Source Table]
  , SourceField                                                         as [History Metric Source Field]
  , MetricSourceTableField.SaveDate                                     as [History Metric Save Date]
  from atMM.MetricSourceTableField
  inner join tDates on MetricSourceTableField.SaveDate <> tDates.SaveDate
  ;
  
  --select * from amMM.HistoryMetricSourceField;

    /***************************** Populate MetricEnterpriseField ******************************/
 with tDates as (
  select distinct top(1) SaveDate from atMM.MetricEnterpriseField order by SaveDate desc
  )
  --select * from tDates;

insert into amMM.MetricEnterpriseField 
  select
    MetricID                         as Metric
  , EnterpriseLevel                  as [Metric Enterprise Level]
  , EnterpriseTable                  as [Metric Enterprise Table]
  , EnterpriseField                  as [Metric Enterprise Field]
  , MetricEnterpriseField.SaveDate   as [Metric Save Date]
  from atMM.MetricEnterpriseField
  inner join tDates on MetricEnterpriseField.SaveDate = tDates.SaveDate
  ;
  
  --select * from amMM.MetricEnterpriseField;

  /***************************** Populate HistoryMetricEnterpriseField ******************************/
 with tDates as (
  select distinct top(1) SaveDate from atMM.MetricEnterpriseField order by SaveDate desc
  )
  --select * from tDates;

insert into amMM.HistoryMetricEnterpriseField
  select
    MetricID + '|' + convert(varchar,MetricEnterpriseField.SaveDate)    as [History Metric Date]
  , EnterpriseLevel                                                     as [History Metric Enterprise Level]
  , EnterpriseTable                                                     as [History Metric Enterprise Table]
  , EnterpriseField                                                     as [History Metric Enterprise Field]
  , MetricEnterpriseField.SaveDate                                      as [History Metric Save Date]
  from atMM.MetricEnterpriseField
  inner join tDates on MetricEnterpriseField.SaveDate <> tDates.SaveDate
  ;
  
  --select * from amMM.HistoryMetricEnterpriseField;



    /***************************** Populate ApplicationMetric ******************************/
 with tDates as (
  select distinct top(1) SaveDate from atMM.ApplicationMetric order by SaveDate desc
  )
  --select * from tDates;

insert into amMM.ApplicationMetric 
  select
    Application                   as [Application]
  , MetricID                      as [Metric]
  , Application + '|' + MetricID  as [Application Metric]
  , MetricType                    as [Metric Application Metric Type]
  , InScope                       as [Metric Application In Scope]
  , DefinitionStatus              as [Metric Application Definition Status]
  , Approver                      as [Metric Application Approver]
  , Tab                           as [Metric Application Tab]
  , OperationalSME                as [Metric Application Operational SME]
  , TechnicalSME                  as [Metric Application Technical SME]
  , Dimensions                    as [Metric Application Dimensions]
  , Security                      as [Metric Application Security]
  , DisplayName                   as [Metric Application Display Name]
  , UIVariable                    as [Metric Application UI Variable]
  , Visualization                 as [Metric Application Visualization]
  , Target                        as [Metric Application Target]
  , ApplicationMetric.SaveDate    as [Metric Application Save Date]
  from atMM.ApplicationMetric
  inner join tDates on ApplicationMetric.SaveDate = tDates.SaveDate
  ;
  
  --select * from amMM.ApplicationMetric;

  /***************************** Populate HistoryApplicationMetric ******************************/
 with tDates as (
  select distinct top(1) SaveDate from atMM.MetricEnterpriseField order by SaveDate desc
  )
  --select * from tDates;

insert into amMM.HistoryApplicationMetric
  select
    Application + '|' + convert(varchar,ApplicationMetric.SaveDate)                    as [History Application Date]
  , MetricID + '|' + convert(varchar,ApplicationMetric.SaveDate)                       as [History Metric Date]
  , Application + '|' + MetricID + '|' + convert(varchar,ApplicationMetric.SaveDate)   as [History Application Metric Date]
  , MetricType                    as [History Metric Application Metric Type]
  , InScope                       as [History Metric Application In Scope]
  , DefinitionStatus              as [History Metric Application Definition Status]
  , Approver                      as [History Metric Application Approver]
  , Tab                           as [History Metric Application Tab]
  , OperationalSME                as [History Metric Application Operational SME]
  , TechnicalSME                  as [History Metric Application Technical SME]
  , Dimensions                    as [History Metric Application Dimensions]
  , Security                      as [History Metric Application Security]
  , DisplayName                   as [History Metric Application Display Name]
  , UIVariable                    as [History Metric Application UI Variable]
  , Visualization                 as [History Metric Application Visualization]
  , Target                        as [History Metric Application Target]
  , ApplicationMetric.SaveDate    as [History Metric Application Save Date]
  from atMM.ApplicationMetric
  inner join tDates on ApplicationMetric.SaveDate <> tDates.SaveDate
  ;

  --select * from amMM.HistoryApplicationMetric;

    /***************************** Populate ApplicationMetricUIVariable ******************************/
 with tDates as (
  select distinct top(1) SaveDate from atMM.ApplicationUIVariable order by SaveDate desc
  )
  --select * from tDates;

insert into amMM.ApplicationMetricUIVariable 
  select
    Application + '|' + MetricID     as [Application Metric]
  , UIVariable                       as [Metric Application UI Variable]
  , ApplicationUIVariable.SaveDate   as [Metric Application Save Date]
  from atMM.ApplicationUIVariable
  inner join tDates on ApplicationUIVariable.SaveDate = tDates.SaveDate
  ;
  
  --select * from amMM.ApplicationMetricUIVariable;

  /***************************** Populate HistoryApplicationMetricUIVariable ******************************/
 with tDates as (
  select distinct top(1) SaveDate from atMM.ApplicationUIVariable order by SaveDate desc
  )
  --select * from tDates;

insert into amMM.HistoryApplicationMetricUIVariable
  select
    Application + '|' + MetricID + '|' + convert(varchar,ApplicationUIVariable.SaveDate)  as [History Application Metric Date]
  , UIVariable                                                                            as [History Metric Application UI Variable]
  , ApplicationUIVariable.SaveDate                                                        as [History Metric Application Save Date]
  from atMM.ApplicationUIVariable
  inner join tDates on ApplicationUIVariable.SaveDate <> tDates.SaveDate
  ;
  
  --select * from amMM.HistoryApplicationMetricUIVariable;


/**************************** Populate Definition ***************************************/
insert into amMM.Definition
select 
    [ID]          as [Definition ID]
  , [Tab]         as [Definition Tab]
  , [Grouping]    as [Definition Grouping]
  , [Name]        as [Definition Column Name]
  , [Definition]  as [Definition Column Description]
  , [Level]       as [Definition Level]
  , [Required]    as [Definition Required]
from aeMM.Definition
;
--select * from amMM.Definition;


/****************************** Populate HistoryMetricChanges ***************************/
insert into amMM.HistoryMetricChanges
select 
    MetricID + '|' + convert(varchar,Changes.SaveDate)  as [History Metric Date]
  --, [Application]
  --, [DimensionID]
  --, [MetricID]
  , [ChangedTable]      as [History Metric Changed Table]
  , [ChangedFromValue]  as [History Metric Changed From Value]
  , [ChangedToValue]    as [Hisotry Metric Changed To Value]
  , [ChangedField]      as [History Metric Changed Field]
  , SaveDate            as [History Metric Save Date]
from atMM.Changes
where len(MetricID) > 0
;
--select * from amMM.HistoryMetricChanges;

END
GO
