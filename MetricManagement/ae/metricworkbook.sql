-- =============================================
-- Author:		Prominence Advisors
-- Create date: 9/1/2017
-- Description:	Populate: aeMM.Metric aeMM.Definition aeMM.Workbook
-- =============================================

USE padev
--SET ANSI_NULLS ON
--GO
--SET QUOTED_IDENTIFIER ON
GO



  
ALTER PROCEDURE aeMM.MetricWorkbook --SET TO CREATE ON FIRST RUN INSTEAD OF ALTER
	-- Add the parameters for the stored procedure here
AS
BEGIN
  -- SET NOCOUNT ON added to prevent extra result sets from
  -- interfering with SELECT statements.
  SET NOCOUNT ON;

/*  Configuration Variables  */
declare @appPath varchar(1000) = 'D:\Prominence\Metric Management\0-External Data Files\';
declare @vaVariableFilename varchar(254) = 'Metric Management Variables.xlsm';
declare @vaMetricWorkbookFileName varchar(254) = 'D:\Prominence\Metric Management\0-External Data Files\Metric Management Starter Kit Metric Workbook.xlsx';


/*  Working Variables  */
declare @vcgVersion varchar(10);
declare @sql varchar(max);


  /***********************Create ae.Metric*************************/
  --in case we need to recreate or change the table definition during testing
  --NOTE: using ge schema...will need to change to ae or ae1

   -- drop table aeMM.Metric;
    if OBJECT_ID('aeMM.Metric') is not null 
    truncate table aeMM.Metric;
    
    
    if OBJECT_ID('aeMM.Metric') is null 
    create table aeMM.Metric (
      Application                     varchar(max) null
		, Version						              varchar(max) null
    , ID							                varchar(max) null
    , Metric                          varchar(max) null
    , MetricType                      varchar(max) null
    , InScope                         varchar(max) null
    , DefinitionStatus                varchar(max) null
    , AffiliateTeamApprover           varchar(max) null
    , Approver                        varchar(max) null
    , Tab                             varchar(max) null
    , OperationalSME                  varchar(max) null
		, TechnicalSME					          varchar(max) null
    , Calculation                     varchar(max) null
    , DataSource                      varchar(max) null
    , SourceTableField                varchar(max) null
    , SourceValue                     varchar(max) null
    , SQLLogic                        varchar(max) null
		, Dimensions                      varchar(max) null
		, Security                        varchar(max) null
		, Timing                          varchar(max) null
		, DisplayName                     varchar(max) null
		, ConfigurationVariable           varchar(max) null
		, EnterpriseField                 varchar(max) null
		, UIVariable                      varchar(max) null
		, Visualization                   varchar(max) null
		, Target                          varchar(max) null
    );

  /***********************Create ae.Definition*************************/
  --in case we need to recreate or change the table definition during testing
  --NOTE: using ge schema...will need to change to ae or ae1
 
    --drop table aeMM.Definition;
    if OBJECT_ID('aeMM.Definition') is not null 
    truncate table aeMM.Definition;

    if OBJECT_ID('aeMM.Definition') is null 
    create table aeMM.Definition (
          ID							                varchar(max) null
        , Tab                             varchar(max) null
        , Grouping                        varchar(max) null
		    , Name      					            varchar(max) null
        , Definition                      varchar(max) null
        , Level                           varchar(max) null
        , Required                        varchar(max) null
    );

  /***********************Create ae.Workbook*************************/
  --in case we need to recreate or change the table definition during testing
  --drop table aeMM.Workbook;
    if OBJECT_ID('aeMM.Workbook') is not null 
    truncate table aeMM.Workbook;

  if OBJECT_ID('aeMM.Workbook') is null 
    create table aeMM.Workbook (
          RowNum  				            int null
        , Application                 varchar(max) null
        , Path                        varchar(max) null
    );
  /******************************************************************/

/* create temp table for workbook list */
if OBJECT_ID('tempdb..#mmWorkbook') is not null drop table #mmWorkbook;
create table #mmWorkbook(
RowNum int
, Application varchar(255)
, Path varchar(1000)
)

if OBJECT_ID('tempdb..#mmWorkbook2') is not null drop table #mmWorkbook2;
create table #mmWorkbook2(
RowNum int
, Application varchar(254)
, Path varchar(1000)
, Version varchar(25)
)




Set @sql='insert into #mmWorkbook select
	row_number() over(order by Application asc) as RowNum
  , Application
  , Path
	from openrowset
	(''Microsoft.ACE.OLEDB.12.0'',
			''Excel 12.0; HDR=YES; IMEX=1; Database=' + @appPath + @vaVariableFilename + ''',
			''SELECT * FROM [Workbooks$A1:B5000]''
	)
	where Application is not NULL'

--Print @sql
Exec(@sql)

	

--select * from #mmWorkbook;
set @sql = '';
  
/* cycle through the workbooks and pull out details */
declare @cnt int = 1;
declare @max int = 0;
declare @tWorkbook varchar(1000);
declare @tApplication varchar(254);
set @max  = (
	select max(RowNum) as Workbooks
	from #mmWorkbook
);
--insert into #mmWorkbook 
while @cnt <= @max
begin
   --print @cnt;
   set @tWorkbook  = (select Path from #mmWorkbook where RowNum = @cnt);
   set @tApplication = (select Application from #mmWorkbook where RowNum = @cnt);
	/*  Get the version */
   set @sql='insert into #mmWorkbook2 select 
		 cast(' + cast(@cnt as varchar(10)) + ' as int)  as RowNum
		, '''+ @tApplication + ''' as Application
		, '''+ @tWorkbook + ''' as Path
		, (select [Metric Workbook]
		from openrowset
		(''Microsoft.ACE.OLEDB.12.0'',
				''Excel 12.0; HDR=YES; IMEX=1; Database=' + @tWorkbook + ''',
				''SELECT * FROM [Overview$A1:A2]''
		)) as Version'
	
	print @sql;
	exec(@sql);
	set @cnt = @cnt + 1;
end;

--select * from #mmWorkbook2;
drop table #mmWorkbook;




/* now cycle through and pull details for workbooks that have their versions set */
set @cnt = 1;
while @cnt <= @max
begin
	set @sql = '';
	--print @vcgVersion;
	set @vcgVersion = (select Version from #mmWorkbook2 where RowNum = @cnt);
	set @tWorkbook  = (select Path from #mmWorkbook2 where RowNum = @cnt);
	set @tApplication = (select Application from #mmWorkbook2 where RowNum = @cnt);

	if @vcgVersion = 'v2.0'
		set @sql='insert into aeMM.Metric select
		''' + @tApplication + ''' as Application
		  , ''' + @vcgVersion + ''' as Version
		  , MetricWorkbook#ID as ID
		  , MetricWorkbook#Metrics as Metric
		  , MetricWorkbook#Type as MetricType
		  , MetricWorkbook#InScope as InScope
		  , MetricWorkbook#DefinitionStatus as DefinitionStatus
		  , MetricWorkbook#AffiliateTeamApprover as AffiliateTeamApprover
		  , MetricWorkbook#Approver as Approver
		  , MetricWorkbook#Tab as Tab
		  , MetricWorkbook#OperationalSME as OperationalSME
		  , MetricWorkbook#TechnicalSME as TechnicalSME
		  , MetricWorkbook#Calculation as Calculation
		  , MetricWorkbook#DataSource as DataSource
		  , MetricWorkbook#SourceTableField as SourceTableField
		  , MetricWorkbook#SourceValue as SourceValue
		  , MetricWorkbook#SQLLogic as SQLLogic
		  , MetricWorkbook#Dimensions as Dimensions
		  , MetricWorkbook#Security as Security
		  , MetricWorkbook#Timing as Timing
		  , MetricWorkbook#DisplayName as DisplayName
		  , MetricWorkbook#ConfigurationVariable as ConfigurationVariable
		  , MetricWorkbook#EnterpriseField as EnterpriseField
		  , MetricWorkbook#UIVariable as UIVariable
		  , MetricWorkbook#Visualization as Visualization
		  , MetricWorkbook#Target as Target
		from openrowset
		(''Microsoft.ACE.OLEDB.12.0'',
				''Excel 12.0; HDR=YES; IMEX=1; Database=' + @tWorkbook + ''',
				''SELECT * FROM [Metric Definition$A5:AO]''
		)'
	--else --to accomodate another version add an else statement here
	;

	if @vcgVersion = 'v2.0'
		print @sql;
		exec(@sql);
	
	

	set @cnt = @cnt + 1;
end;

	
	
drop table #mmWorkbook2;


/* Populate the Definitions table from the Metric Management Metric Workbook*/

set @sql = '';

Set @sql='insert into aeMM.Definition select
	  ID
	, Tab
	, Grouping
	, [Column Name] as Name
	, Definition 
	, [Enterprise or Application Level] as Level
	, [Required/ Optional] as Required
	from openrowset
	(''Microsoft.ACE.OLEDB.12.0'',
			''Excel 12.0; HDR=YES; IMEX=1; Database=' + @vaMetricWorkbookFileName + ''',
			''SELECT * FROM [Definitions$A3:AG]''
	)';

--Print @sql;
Exec(@sql);

/*  create the Workbook table  */
Set @sql='insert into aeMM.Workbook select
	row_number() over(order by Application asc) as RowNum
  , Application
  , Path
	from openrowset
	(''Microsoft.ACE.OLEDB.12.0'',
			''Excel 12.0; HDR=YES; IMEX=1; Database=' + @appPath + @vaVariableFilename + ''',
			''SELECT * FROM [Workbooks$A1:B5000]''
	)
	where Application is not NULL'

--Print @sql
Exec(@sql);



END
GO
