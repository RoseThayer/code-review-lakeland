alter procedure gt1.tx_Patient as

if object_id('gt1.Patient', 'U') is not null drop table gt1.Patient;

create table [gt1].[Patient]
(
    PatientID varchar(254)       NOT NULL
  , MRN       varchar(254)           NULL
  , Name      varchar(254)            NULL
  , BirthDate date                    NULL
  , Gender    varchar(254)            NULL
  , Ethnicity varchar(254)            NULL
  , Race		varchar(254)            NULL
  , Zipcode   varchar(254)	          NULL
  , Status varchar(254)	          NULL
)

insert into [gt1].[Patient]
(
  PatientID
, MRN
, Name
, BirthDate
, Gender
, Ethnicity
, Race
, Zipcode
, Status
)

  select distinct
    pt.PAT_ID as PatientID
  , pt.PAT_MRN_ID as MRN
  , pt.PAT_NAME as Name
  , convert(date,pt.BIRTH_DATE) as BirthDate
  , s.NAME as Gender
	, eg.NAME as Ethnicity
  , first_Value(zpr.NAME) OVER (partition by pt.PAT_ID order by pt.PAT_ID asc) as Race
  , pt.ZIP as ZipCode
  , zps.NAME as Status
  --, pt.PAT_STATUS_C as PatientStatusID
  from
    ge.PATIENT pt
    left join ge.ZC_ETHNIC_GROUP eg
      on eg.ETHNIC_GROUP_C = pt.ETHNIC_GROUP_C
    left join ge.ZC_SEX s
      on s.RCPT_MEM_SEX_C = pt.SEX_C
   left join ge.PATIENT_RACE pr
      on pt.PAT_ID = pr.PAT_ID
    left join ge.ZC_PATIENT_RACE zpr
      on pr.PATIENT_RACE_C = zpr.PATIENT_RACE_C
    left join ge.ZC_PATIENT_STATUS zps
      on pt.PAT_STATUS_C = zps.PATIENT_STATUS_C
;

