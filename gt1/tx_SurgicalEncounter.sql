alter procedure gt2.tx_SurgicalEncounter as
/*
  drop procedures:
  - gt2.ORLogAllSurgeon
  - gt2.ORCasePrimarySurgeon
*/

if object_id('gt2.SurgicalEncounter') is not null
  drop table gt2.SurgicalEncounter
;

if object_id('gt2.tSurgicalEncounter') is not null
  drop table gt2.tSurgicalEncounter
;

create table gt2.tSurgicalEncounter(
	SurgeryID                     varchar(18)
	, SurgeryDate                 date
  , ScheduledStartTime          datetime
  , CancelDate                  datetime 
	, PatientID                   varchar(18)
	, ORRoomID                    varchar(18)
  , ORRoom                      varchar(200)
	, ORLocationID                numeric(18, 0)
	, PrimaryORServiceID          varchar(66)
	, PrimaryORService            varchar(254)
	, PrimarySurgeonID            varchar(18)
  , PrimarySurgeonStartTime     datetime
  , PrimarySurgeonEndTime       datetime
  , PrimarySurgeonPrimaryServiceID   varchar(66)
  , LogID                       varchar(18)
  , CaseID                      varchar(18)
  , SetupOffset                 int
  , CleanupOffset               int
  , Source                      varchar(254)
  , CaseClassID                 int -- todo: MW uses ORL/ORC for Add-On vs. Elective, but only ORC for Case Class name? Inconsistent.
  , HospitalEncounterID         numeric(18, 0)
  , SurgicalEncounterID         numeric(18, 0)
  , PatientClassID              varchar(66)
  , VoidReasonID                int
  , CancelReasonID              int
)


declare @UnspecifiedID            int = -1;
declare @UnknownID                int = -2;
declare @NotApplicableID          int = -3;
declare @UnspecifiedLabel         varchar(254) = '*Unspecified';
declare @UnknownLabel             varchar(254) = '*Unknown';
declare @NotApplicableLabel       varchar(254) = '*NotApplicable';
declare @AnesthesiaLabel          varchar(254) = 'Anesthesia';
declare @ORCaseSrcLabel           varchar(254) = 'OR_CASE';
declare @ORLogSrcLabel            varchar(254) = 'OR_LOG';
declare @FANRecordSummSrcLabel    varchar(254) = 'F_AN_RECORD_SUMMARY';

declare @PatientAgeAdultMin int = variables.GetSingleValue('@vcgPatientAgeAdultMin');

declare @ApplicationStartDate date = datefromparts(iif(datepart(mm,getdate())<10,datepart(yyyy,getdate())-2,datepart(yyyy,getdate())-1),10,1);

declare @ApplicationEndDate date = getdate()-1;

declare @ORFirstCaseStartRangeStart time = variables.GetSingleValue('@vcgORFirstCaseStartRangeStart');

declare @ORFirstCaseStartRangeStop time = variables.GetSingleValue('@vcgORFirstCaseStartRangeStop');

declare @VoidCaseStatus int = variables.GetSingleValue('@vcgORCaseStatusVoid');

declare @AddOnCase table (
  CaseClassID int
)
;
insert into @AddOnCase
select
	VariableValue
from variables.GetTableValues('@vcgCaseClassAddOn')
;

declare @AvoidableCancellation table (
  CancelReasonID int
)
;
insert into @AvoidableCancellation
select
	VariableValue
from variables.GetTableValues('@vcgORCancellationAvoidable')
;

with ORLogAllSurgeon as (
  select
    olas.LOG_ID                     as LogID
    --, olas.LINE
    , iif(min(olas.LINE) = first_value (min(olas.LINE)) over (
        partition by 
          olas.LOG_ID 
        order by 
          min(olas.LINE) asc
      )
      , 1
      , 0
      )                             as FirstPrimaryFlag
    , olas.SURG_ID
    , min(olas.START_TIME)          as START_TIME
    , max(olas.END_TIME)            as END_TIME
    , olas.SERVICE_C
  from ge.OR_LOG_ALL_SURG olas
  where
    olas.ROLE_C = 1 -- primary surgeon, todo: variable?
    and olas.PANEL = 1 -- first panel
    --and olas.LOG_ID = '121960'
  group by
    olas.LOG_ID
    , olas.SURG_ID
    , olas.SERVICE_C
)
, ORCaseAllSurgeon as (
  select
    ocas.OR_CASE_ID                 as CaseID
    , ocas.LINE
    , iif(ocas.LINE = first_value (ocas.LINE) over (
        partition by 
          ocas.OR_CASE_ID 
        order by 
          ocas.LINE asc
      )
      , 1
      , 0
      )                             as FirstPrimaryFlag
    , ocas.SURG_ID
    , ocas.SERVICE_C
  from ge.OR_CASE_ALL_SURG ocas
  where
    ocas.ROLE_C = 1 -- primary surgeon, todo: variable?
    and ocas.PANEL = 1 -- first panel
)
insert into gt2.tSurgicalEncounter (
  SurgeryID           
	, SurgeryDate    
  , ScheduledStartTime
  , CancelDate   
	, PatientID         
	, ORRoomID          
	, ORLocationID      
	, PrimaryORServiceID
	, PrimarySurgeonID  
  , PrimarySurgeonStartTime
  , PrimarySurgeonEndTime
  , PrimarySurgeonPrimaryServiceID
  , LogID
  , CaseID
  , SetupOffset
  , CleanupOffset
  , Source
  , CaseClassID
  , HospitalEncounterID 
  , SurgicalEncounterID 
  , PatientClassID
  , VoidReasonID
  , CancelReasonID
)
select 
  orl.LOG_ID                        as SurgeryID
  , cast(
      coalesce(orl.SURGERY_DATE
        , orc.SURGERY_DATE
      ) 
      as date
    )                               as SurgeryDate
  , coalesce(orl.SCHED_START_TIME
      , orc.TIME_SCHEDULED
    )                               as ScheduledStartTime
  , orc.CANCEL_DATE                 as CancelDate
  , orl.PAT_ID                      as PatientID 
  , orl.ROOM_ID                     as ORRoomID
  , orl.LOC_ID                      as ORLocationID
  , coalesce(orl.SERVICE_C 
    , orc.SERVICE_C
    )                               as PrimaryORServiceID
  , coalesce(olas.SURG_ID
    , ocas.SURG_ID
    )                               as PrimarySurgeonID
  , olas.START_TIME                 as PrimarySurgeonStartTime
  , olas.END_TIME                   as PrimarySurgeonEndTime
  , coalesce(olas.SERVICE_C
    , ocas.SERVICE_C
    )                               as PrimarySurgeonPrimaryServiceID
  , orl.LOG_ID                      as LogID
  , orl.CASE_ID                     as CaseID
  , orc.SETUP_OFFSET                as SetupOffset
  , orc.CLEANUP_OFFSET              as CleanupOffset
  , @ORLogSrcLabel                  as Source
  , coalesce(orl.CASE_CLASS_C
    , orc.CASE_CLASS_C
    )                               as CaseClassID
  , poal.OR_LINK_CSN                as HospitalEncounterID
  , poal.PAT_ENC_CSN_ID             as SurgicalEncounterID
  , orc.PAT_CLASS_C                 as PatientClassID
  , coalesce(orl.VOID_REASON_C
    , orc.VOID_REASON_C
    )                               as VoidReasonID
  , orc.CANCEL_REASON_C             as CancelReasonID
from ge.OR_LOG orl
left join ORLogAllSurgeon olas
  on olas.LogID = orl.LOG_ID
    and olas.FirstPrimaryFlag = 1
left join ORCaseAllSurgeon ocas
  on ocas.CaseID = orl.CASE_ID
    and ocas.FirstPrimaryFlag = 1
left join ge.PAT_OR_ADM_LINK poal
  on poal.LOG_ID = orl.LOG_ID
left join ge.OR_CASE orc
  on orc.OR_CASE_ID = orl.CASE_ID
where
  orl.SURGERY_DATE >= @ApplicationStartDate
    and orl.SURGERY_DATE < @ApplicationEndDate
;

with ORCaseAllSurgeon as (
  select
    ocas.OR_CASE_ID                 as CaseID
    , ocas.LINE
    , iif(ocas.LINE = first_value (ocas.LINE) over (
        partition by 
          ocas.OR_CASE_ID 
        order by 
          ocas.LINE asc
      )
      , 1
      , 0
      )                             as FirstPrimaryFlag
    , ocas.SURG_ID
    , ocas.SERVICE_C
  from ge.OR_CASE_ALL_SURG ocas
  where
    ocas.ROLE_C = 1 -- primary surgeon, todo: variable?
    and ocas.PANEL = 1 -- first panel
)
insert into gt2.tSurgicalEncounter (
  SurgeryID           
	, SurgeryDate       
  , ScheduledStartTime
  , CancelDate
	, PatientID         
	, ORRoomID          
	, ORLocationID      
	, PrimaryORServiceID	
	, PrimarySurgeonID 
  , PrimarySurgeonStartTime
  , PrimarySurgeonEndTime 
  , PrimarySurgeonPrimaryServiceID
  , LogID
  , CaseID
  , SetupOffset
  , CleanupOffset
  , Source
  , CaseClassID
  , HospitalEncounterID
  , SurgicalEncounterID
  , PatientClassID
  , VoidReasonID
  , CancelReasonID
)
select 
  orc.OR_CASE_ID                    as SurgeryID
  , cast(orc.SURGERY_DATE as date)  as SurgeryDate
  , orc.TIME_SCHEDULED              as ScheduledStartTime
  , orc.CANCEL_DATE                 as CancelDate
  , orc.PAT_ID                      as PatientID
  , orc.OR_ID                       as ORRoomID
  , orc.LOC_ID                      as ORLocationID
  , orc.SERVICE_C                   as PrimaryORServiceID  
  , ocas.SURG_ID                    as PrimarySurgeonID
  , null                            as PrimarySurgeonStartTime
  , null                            as PrimarySurgeonEndTime
  , ocas.SERVICE_C                  as PrimarySurgeonPrimaryServiceID
  , @UnspecifiedID                  as LogID
  , orc.OR_CASE_ID                  as CaseID
  , orc.SETUP_OFFSET                as SetupOffset
  , orc.CLEANUP_OFFSET              as CleanupOffset
  , @ORCaseSrcLabel                 as Source
  , orc.CASE_CLASS_C                as CaseClassID
  , poal.OR_LINK_CSN                as HospitalEncounterID
  , poal.PAT_ENC_CSN_ID             as SurgicalEncounterID
  , orc.PAT_CLASS_C                 as PatientClassID
  , orc.VOID_REASON_C               as VoidReasonID
  , orc.CANCEL_REASON_C             as CancelReasonID
from ge.OR_CASE orc
left join ORCaseAllSurgeon ocas
  on ocas.CaseID = orc.OR_CASE_ID
    and ocas.FirstPrimaryFlag = 1
left join ge.PAT_OR_ADM_LINK poal
  on poal.CASE_ID = orc.OR_CASE_ID
where 
  not exists (
    select
      null
    from gt2.tSurgicalEncounter tse
    where
      tse.CaseID = orc.OR_CASE_ID
  )
  and orc.SURGERY_DATE >= @ApplicationStartDate
  and orc.SURGERY_DATE < @ApplicationEndDate
;

-- todo: investigate this more, only one F_AN_RECORD_SUMMARY row coming through and it is mostly null.
with LatestEpisode as (
  select
    anrs.AN_LOG_ID
		, max(anrs.AN_EPISODE_ID)       as MaxEpisodeID
	from ge.F_AN_RECORD_SUMMARY anrs
  group by
    anrs.AN_LOG_ID
)
insert into gt2.tSurgicalEncounter (
  SurgeryID           
	, SurgeryDate     
  , ScheduledStartTime  
  , CancelDate
	, PatientID         
	, ORRoomID          
	, ORLocationID      
	, PrimaryORServiceID
	, PrimarySurgeonID  
  , PrimarySurgeonStartTime
  , PrimarySurgeonEndTime
  , PrimarySurgeonPrimaryServiceID
  , LogID
  , CaseID
  , SetupOffset
  , CleanupOffset
  , Source
  , CaseClassID
  , HospitalEncounterID
  , SurgicalEncounterID
  , PatientClassID
  , VoidReasonID
  , CancelReasonID
)
select
  anrs.AN_LOG_ID                          as SurgeryID  -- todo: ask Rose how we can have an ORC/ORL record here with no corresponding entry in OR_LOG or OR_CASE
  , cast(anrs.AN_DATE as date)            as SurgeryDate
  , null                                  as ScheduledStartTime
  , null                                  as CancelDate
  , anrs.AN_PAT_ID                        as PatientID
  , cast(@NotApplicableID as varchar)     as ORRoomID
  , @NotApplicableID                      as ORLocationID
  , @NotApplicableID                      as PrimaryORServiceID  
  , cast(@NotApplicableID as varchar)     as PrimarySurgeonID
  , null                                  as PrimarySurgeonStartTime
  , null                                  as PrimarySurgeonEndTime
  , cast(@NotApplicableID as varchar)     as PrimarySurgeonPrimaryServiceID
  , anrs.LOG_ID                           as LogID
  , anrs.CASE_ID                          as CaseID -- todo: ask Rose how we can have an ORC record here with no corresponding entry in OR_CASE
  , null                                  as SetupOffset
  , null                                  as CleanupOffset
  , @FANRecordSummSrcLabel                as Source
  , @NotApplicableID                      as CaseClassID
  , poal.OR_LINK_CSN                      as HospitalEncounterID -- todo: ask Rose how to get Hospital Encounter CSN for records not present in OR_LOG/OR_CASE (no join to PAT_OR_ADM_LINK found)
  , poal.PAT_ENC_CSN_ID                   as SurgicalEncounterID
  , @NotApplicableID                      as PatientClassID
  , @NotApplicableID                      as VoidReasonID
  , @NotApplicableID                      as CancelReasonID
from ge.F_AN_RECORD_SUMMARY anrs
inner join LatestEpisode ep
	on ep.MaxEpisodeID = anrs.AN_EPISODE_ID -- keep only the most recent episode 
left join ge.PAT_OR_ADM_LINK poal
  on poal.LOG_ID = anrs.AN_LOG_ID
where
  -- only F_AN_RECORD_SUMMARY records where the AN_LOG_ID is not present in OR_LOG.LOG_ID
  not exists (
    select
      null
    from gt2.tSurgicalEncounter tse
    where
      tse.SurgeryID = anrs.LOG_ID
  )
  -- and only F_AN_RECORD_SUMMARY records where the CASE_ID is not present in OR_LOG.CASE_ID or OR_CASE.OR_CASE_ID
  and not exists (
    select 
      null
    from gt2.tSurgicalEncounter tse
    where
      tse.CaseID = anrs.CASE_ID
  )
  and anrs.AN_DATE >= @ApplicationStartDate
  and anrs.AN_DATE < @ApplicationEndDate
;

-- sanity check
select count(*) from gt2.tSurgicalEncounter;

with ORANStaff as (
  select
    log_staff.LOG_ID
    , anes_staff.RECORD_ID            as StaffRecordID
    , anes_staff.ANESTH_STAFF_ID      as StaffID
  from ge.OR_LNLG_ANES_STAFF anes_staff
  inner join ge.OR_LOG_LN_ANESSTAF log_staff
    on log_staff.ANESTHESIA_STAFF_I = anes_staff.RECORD_ID
  union all
  select
    log_staff.LOG_ID
    , anes_staff.RECORD_ID            as StaffRecordID
    , anes_staff.ANES_STAFF_ID        as StaffID
  from ge.OR_LNLG_ANES_RESP anes_staff
  inner join ge.OR_LOG_LN_ANESSTAF log_staff
    on log_staff.ANESTHESIA_STAFF_I = anes_staff.RECORD_ID
  where
    anes_staff.ANES_STAFF_ID is not null
)
, LatestEpisode as (
  select
    anrs.AN_LOG_ID
		, max(anrs.AN_EPISODE_ID)       as MaxEpisodeID
	from ge.F_AN_RECORD_SUMMARY anrs
  group by
    anrs.AN_LOG_ID
)
, Anesthesiologist as (
  select distinct
    anrs.AN_LOG_ID                    as LogID
    , anrs.AN_RESP_PROV_ID            as AnesthesiologistID
  from ge.F_AN_RECORD_SUMMARY anrs
  inner join LatestEpisode
    on LatestEpisode.MaxEpisodeID = anrs.AN_EPISODE_ID
  union all
  select distinct
    ORANStaff.LOG_ID                  as LogID
    , first_value(ORANStaff.StaffID) over (
      partition by 
        ORANStaff.LOG_ID 
      order by 
        staff_times.ANES_STAFF_ST_TIME
        , ORANStaff.StaffRecordID
    )                                 as AnesthesiologistID
  from ORANStaff
  left join ge.OR_LNLG_ANESTF_TMS staff_times
    on staff_times.RECORD_ID = ORANStaff.StaffRecordID
  where 
    not exists (
      select 
        null
      from ge.F_AN_RECORD_SUMMARY anrs
      where
        anrs.AN_LOG_ID = ORANStaff.LOG_ID
    )
)
, ORAuditCase as (
  select
    audit_case.CaseID
    , max(audit_case.SameDayCancelledFlag)  as SameDayCancelledFlag -- todo: comment out eventually, just comparing OR_CASE.CANCEL_DATE to SURGERY_DATE now
    , max(audit_case.WasEverCancelledFlag)  as WasEverCancelledFlag
    , max(audit_case.LastScheduledDate)     as LastScheduledDate
  from gt1.ORAuditCase audit_case
  group by
    audit_case.CaseID
)
, FirstScheduled as (
  select  
    sched_hist.OR_CASE_ID               as CaseID
    , min(sched_hist.HIST_SCHED_DATE)   as FirstScheduledDate
    , max(sched_hist.HIST_SCHED_DATE)   as LastScheduledDate
  from ge.OR_CASE_SCHED_HIST sched_hist
  group by
    sched_hist.OR_CASE_ID
)
, PACUDelay as (
  select
    LogID
    , PACUDelayFlag
  from gt1.ORDelay ord
  where
    ord.PACUDelayFlag = 1
)
select 
  tse.SurgeryID
  , tse.SurgeryDate
  , tse.ScheduledStartTime
  , iif(ct.ProcedureStartTime is null,tse.CancelDate,NULL) as CancelDate --add restriction that it's only counted towards cancel if it doesn't have a cut time
  , datediff(mi
    , tse.ScheduledStartTime
    , ct.PatientInRoomTime
    )                                       as DelayedStartDuration
  , case
      when datediff(mi, tse.ScheduledStartTime, ct.PatientInRoomTime) > 1
        then 1
      else
        0
    end                                     as DelayFlag
  , case  
      when convert(time, tse.ScheduledStartTime) between @ORFirstCaseStartRangeStart and @ORFirstCaseStartRangeStop
        then 1
      else  
        0
    end                                     as FirstCaseFlag
  , coalesce(PACUDelay.PACUDelayFlag, 0)    as PACUDelayFlag
  , tse.Source
  , tse.CaseID
  , cast(tse.LogID as numeric)				as LogID
  , tse.ORRoomID                            as ORRoomID
  , case
      when tse.ORRoomID is null
        then @UnspecifiedLabel
      when orroom.PROV_ID is null
        then @UnknownLabel
      else
        orroom.PROV_NAME
    end                                     as ORRoom
  , tse.PrimarySurgeonID
  , ser.PROV_NAME								as PrimarySurgeon
  , tse.PrimarySurgeonStartTime
  , tse.PrimarySurgeonEndTime
  , tse.PrimarySurgeonPrimaryServiceID
  , zorsvc2.NAME as PrimarySurgeonPrimaryService
  , tse.PrimaryORServiceID
  , case
      when tse.Source = @FANRecordSummSrcLabel
        then @AnesthesiaLabel
      when tse.PrimaryORServiceID is null
        then @UnspecifiedLabel
      when zorsvc.SERVICE_C is null
        then @UnknownLabel
      else
        zorsvc.NAME
     end                                as PrimaryORService
  , tse.CaseClassID
  , case      
      when tse.CaseClassID is null
        then @UnspecifiedLabel
      when zcaseclass.CASE_CLASS_C is null
        then @UnknownLabel
      else
        zcaseclass.NAME
     end                                as CaseClass
  , iif(addOnCase.CaseClassID is not null
    , 1
    , 0
  )                                     as AddOnCaseFlag
  , iif(addOnCase.CaseClassID is null
    , 1
    , 0
  )                                     as ElectiveCaseFlag
  , tse.HospitalEncounterID             as HospitalEncounterID
  , tse.SurgicalEncounterID             as SurgicalEncounterID
  , datediff(year,pt.BirthDate,tse.SurgeryDate) as PatientAge
  ,iif(datediff(year,pt.BirthDate,tse.SurgeryDate) >=@PatientAgeAdultMin,1,0) as PatientAdultFlag
  , tse.PatientClassID                  as SchedPatientClassID
  , case
      when tse.PatientClassID is null
        then @UnspecifiedLabel
      when zpatclass.ADT_PAT_CLASS_C is null
        then @UnknownLabel
      else
        zpatclass.NAME
     end                                as SchedPatientClass
  , tse.VoidReasonID
  , case
      when tse.VoidReasonID is null
        then @UnspecifiedLabel
      when zvoidrsn.VOID_REASON_C is null
        then @UnknownLabel
      else
        zvoidrsn.NAME
     end                                as VoidReason
  , tse.CancelReasonID
  , case
      when tse.CancelReasonID is null
        then @UnspecifiedLabel
      when zcancelrsn.CANCEL_REASON_C is null
        then @UnknownLabel
      else
        zcancelrsn.NAME
     end                                as CancelReason
  , iif(avoidableCancellation.CancelReasonID is not null
	and ct.ProcedureStartTime is null --add restriction that it's only counted towards cancel if it doesn't have a cut time
    , 1
    , 0
    )                                   as AvoidableCancellationFlag
  , Anesthesiologist.AnesthesiologistID -- todo: compare against Beverly's report per MW
  , ser2.PROV_NAME						as ResponsibleAnesthesiologist
  , orl.ASA_RATING_C                    as ASARatingID
  , case
      when orl.ASA_RATING_C is null
        then @UnspecifiedLabel
      when zorasa.ASA_RATING_C is null
        then @UnknownLabel
      else
        zorasa.NAME
     end                                as ASARating
  , primary_proc.OR_PROC_ID             as PrimaryProcedureID
  , case
      when primary_proc.OR_PROC_ID is null
        then @UnspecifiedLabel
      when orproc.OR_PROC_ID is null
        then @UnknownLabel
      else
        orproc.PROC_NAME
    end                                 as PrimaryProcedure
  , orl.PROC_LEVEL_C                    as ProcedureLevelID
  , case
      when orl.PROC_LEVEL_C is null
        then @UnspecifiedLabel
      when zprclvl.PROC_LEVEL_C is null
        then @UnknownLabel
      else
        zprclvl.NAME
    end                                 as ProcedureLevel
  , orc.SCHED_STATUS_C                  as CaseScheduleStatusID
  , case
      when orc.SCHED_STATUS_C  is null
        then @UnspecifiedLabel
      when zorschedsts.SCHED_STATUS_C is null
        then @UnknownLabel
      else
        zorschedsts.NAME
    end                                 as CaseScheduleStatus
  , iif(orc.SCHED_STATUS_C <> @VoidCaseStatus
    , 1
    , 0
    )                                   as IncludeCaseInTotalFlag

	--Events
	,FirstScheduled.FirstScheduledDate   as FirstScheduledDate
	,ORAuditCase.LastScheduledDate       as LastScheduledDate
	,dateadd(minute,tse.SetupOffset,tse.ScheduledStartTime)	as ScheduledInRoomTime
	,tse.SetupOffset					as SetUpOffset
	,tse.CleanupOffset					as CleanUpOffset


	--Duration
  , datediff(d
    , ORAuditCase.LastScheduledDate
    , tse.SurgeryDate
    )                                   as ScheduledToSurgeryDuration
  , datediff(mi
      , first_value (tse.PrimarySurgeonEndTime) over (
          partition by 
            tse.SurgeryID
          order by
            tse.PrimarySurgeonEndTime desc
        )
      , ct.ProcedureFinishTime
    )                                   as SurgeonOutProcedureFinishDuration
	,datediff(minute, ct.PatientInPreopTime, ct.PatientInRoomTime) as PreOpDuration
	,datediff(minute, ct.PatientInRoomTime, ct.ProcedureStartTime) as IntraopWaitDuration
	,datediff(minute, dateadd(minute,tse.SetupOffset,tse.ScheduledStartTime), ct.ProcedureStartTime) as LateInciscionDuration 
	,datediff(minute, ct.PatientInRoomTime, ct.PatientOutRoomTime) as ORDuration
	,datediff(minute, ane.AnesthesiaStartTime, ane.AnesthesiaStopTime) as AnesthesiaDuration
	,datediff(minute, first_value (tse.PrimarySurgeonEndTime) over (
          partition by 
            tse.SurgeryID
          order by
            tse.PrimarySurgeonEndTime desc
        ), ct.ProcedureFinishTime)  as PrimarySurgeonEndtoProcedureEndDuration
	,datediff(minute, ct.ProcedureFinishTime, ct.PatientOutRoomTime) as PostOpFlowDuration
	,datediff(minute, ct.PatientOutRoomTime, ane.AnesthesiaStopTime) as PatientOutRoomtoAnesthesiaStopDuration
	,datediff(minute, ct.PatientInRecoveryTime, ct.PatientOutRecoveryTime) as RecoveryPhaseIDuration
	,datediff(minute, ct.PhaseIStartTime, ct.PhaseIFinishTime) as RecoveryPhaseIIDuration
	,datediff(minute, ct.PhaseIIStartTime, ct.PhaseIIFinishTime) as RecoveryDuration
	,datediff(minute, ane.AnesthesiaStartTime, ane.AnesthesiaStopTime) as SurgeonCalledToArrivalDuration
	,datediff(minute, ane.PatientReadyTime, ct.PatientInRoomTime) as PatientReadyToInRoomDuration

  , iif(ane.AnesthesiaStartTime>0 and ane.AnesthesiaStopTime>0
    , 1
    , 0
    )                                   as AnesthesiaFlag

	--Flags
  , iif(ct.ProcedureStartTime is not null
    , 1
    , 0
    )                                   as LogPerformedFlag
  --, ORAuditCase.WasEverCancelledFlag    as CancelledFlagOld -- todo: check to see if new/old flags match
  , iif(tse.CancelDate is not null
	and ct.ProcedureStartTime is null--add restriction that it's only counted towards cancel if it doesn't have a cut time
    , 1
    , 0
    )                                   as CancelledFlag
  --, ORAuditCase.SameDayCancelledFlag    as SameDayCancelledFlagOld
  , iif(tse.CancelDate = tse.SurgeryDate
	and ct.ProcedureStartTime is null--add restriction that it's only counted towards cancel if it doesn't have a cut time
	,1
	,0
	)									as SameDayCancelledFlag
  , case when datediff(s, ct.PatientInRecoveryTime, ct.PatientOutRecoveryTime) > 0 then 1 else 0 end as RecoveryCaseFlag

into gt2.SurgicalEncounter
from gt2.tSurgicalEncounter tse
left join ge.OR_LOG orl
  on orl.LOG_ID = tse.LogID
left join gt1.ORCaseTracking ct
	on tse.LogID=ct.LogID
left join ge.OR_CASE orc
  on orc.OR_CASE_ID = tse.CaseID
left join ge.OR_LOG_ALL_PROC primary_proc
  on primary_proc.LOG_ID = tse.LogID
    and primary_proc.LINE = 1
left join ge.OR_PROC orproc
  on orproc.OR_PROC_ID = primary_proc.OR_PROC_ID
left join ge.CLARITY_SER orroom
  on orroom.PROV_ID = tse.ORRoomID
left join Anesthesiologist
  on Anesthesiologist.LogID = tse.SurgeryID
left join ORAuditCase
  on ORAuditCase.CaseID = tse.CaseID
left join FirstScheduled 
  on FirstScheduled.CaseID = tse.CaseID
left join PACUDelay
  on PACUDelay.LogID = tse.LogID
left join @AddOnCase addOnCase
  on addOnCase.CaseClassID = tse.CaseClassID  
left join @AvoidableCancellation avoidableCancellation
  on avoidableCancellation.CancelReasonID = tse.CancelReasonID
left join ge.ZC_PAT_CLASS zpatclass
  on zpatclass.ADT_PAT_CLASS_C = tse.PatientClassID
left join ge.ZC_OR_VOID_REASON zvoidrsn
  on zvoidrsn.VOID_REASON_C = tse.VoidReasonID
left join ge.ZC_OR_CANCEL_RSN zcancelrsn
  on zcancelrsn.CANCEL_REASON_C = tse.CancelReasonID
left join ge.ZC_OR_CASE_CLASS zcaseclass
  on zcaseclass.CASE_CLASS_C = tse.CaseClassID
left join ge.ZC_OR_SERVICE zorsvc
  on zorsvc.SERVICE_C = tse.PrimaryORServiceID
left join ge.ZC_OR_SERVICE zorsvc2
  on zorsvc2.SERVICE_C = tse.PrimarySurgeonPrimaryServiceID
left join ge.ZC_OR_ASA_RATING zorasa
  on zorasa.ASA_RATING_C = orl.ASA_RATING_C
left join ge.ZC_PROC_LEVEL zprclvl
  on zprclvl.PROC_LEVEL_C = orl.PROC_LEVEL_C
left join ge.ZC_OR_SCHED_STATUS zorschedsts
  on zorschedsts.SCHED_STATUS_C = orc.SCHED_STATUS_C
left join gt1.AnesthesiaEvent ane on				--an events
	ane.SurgeryID=tse.LogID
left join gt1.HospitalEncounter hsp on
	hsp.EncounterID=tse.HospitalEncounterID
left join gt1.Patient pt on							--patient
	hsp.PatientID=pt.PatientID
left join ge.clarity_ser ser on						--primary surgeon
	ser.PROV_ID=tse.PrimarySurgeonID
left join ge.clarity_ser ser2 on						--anesthesiologist
	ser2.PROV_ID=Anesthesiologist.AnesthesiologistID
--where tse.HospitalEncounterID is null
--where
--  tse.LogID = '121960'
;

-- clean up

if object_id('gt2.tSurgicalEncounter') is not null
  drop table gt2.tSurgicalEncounter
;

/*
-- query to make sure rows aern't blowing up

select count(*) 
from gt2.SurgicalEncounter se
;

with LatestEpisode as (
  select
    anrs.AN_LOG_ID
		, max(anrs.AN_EPISODE_ID)       as MaxEpisodeID
	from ge.F_AN_RECORD_SUMMARY anrs
  group by
    anrs.AN_LOG_ID
)
select orl.Log_ID as ID
from ge.or_log orl
where 
  orl.SURGERY_DATE >= '2016-10-01' 
  and orl.SURGERY_DATE < getdate()
union all
select orc.OR_CASE_ID as ID
from ge.or_case orc
where 
  orc.SURGERY_DATE >= '2016-10-01'
  and orc.SURGERY_DATE < convert(date, getdate())
  and not exists (
    select null
    from ge.OR_LOG orl
    where
      orl.CASE_ID = orc.OR_CASE_ID
      and orl.SURGERY_DATE >= '2016-10-01' 
      and orl.SURGERY_DATE < convert(date, getdate())
  )
union all
select 
  anrs.AN_LOG_ID
from ge.F_AN_RECORD_SUMMARY anrs
inner join LatestEpisode
  on LatestEpisode.MaxEpisodeID = anrs.AN_EPISODE_ID
where
  anrs.AN_DATE >= '2016-10-01'
  and anrs.AN_DATE < convert(date, getdate())
  and not exists (
    select null
    from ge.OR_LOG orl
    where
      orl.LOG_ID = anrs.LOG_ID
      and orl.SURGERY_DATE >= '2016-10-01' 
      and orl.SURGERY_DATE < convert(date, getdate())
  )
  and not exists (
    select null
    from ge.OR_CASE orc
    where
      orc.OR_CASE_ID = anrs.CASE_ID
      and orc.SURGERY_DATE >= '2016-10-01' 
      and orc.SURGERY_DATE < convert(date, getdate())
  )
*/

go

exec gt2.tx_SurgicalEncounter;

/*
select surgeryid, count(*)
from gt2.SurgicalEncounter
group by surgeryid
having count(*) > 1

select *
from gt2.SurgicalEncounter se
where
  HospitalEncounterID = 22583848

select *
from gt2.SurgicalEncounter se
where 
  se.FirstCaseFlag = 1 and se.DelayFlag = 1

select distinct ScheduledStartTime
from gt2.SurgicalEncounter se
where FirstCaseFlag = 1

/*
--RT 11/18 this was throwing errors; not sure what the goal is
where (firstscheduleddate is not null and lastscheduleddate is null)
  or (firstscheduleddate is null and lastscheduleddate is not null)
  or (firstscheduleddate is null and lastscheduleddate is null)
  */

  -- try to link surg enc to anes

select *
from gt1.AnesthesiaEvent
where
  HospitalEncounterID = 22583848

select distinct
  ct.LOG_ID
  , orc.OR_CASE_ID
  , coalesce(orl.SURGERY_DATE, orc.SURGERY_DATE) as SURGERY_DATE
  , orl.PAT_ID
  , orl.ROOM_ID
  , orl.STATUS_C
  , coalesce(orl.SERVICE_C, orc.SERVICE_C)  as SERVICE_C
  , orc.SCHED_STATUS_C    
  , orc.CANCEL_REASON_C
  , orl.VOID_REASON_C
from ge.OR_LOG_CASE_TIMES ct
left join ge.OR_LOG orl
  on orl.LOG_ID = ct.LOG_ID
left join ge.OR_CASE orc
  on orc.OR_CASE_ID = orl.CASE_ID
left join ge.ZC_OR_SERVICE zorsvc
  on zorsvc.SERVICE_C = coalesce(orl.SERVICE_C, orc.SERVICE_C) 
where 
  not exists (
    select null
    from ge.OR_LOG_CASE_TIMES ct2
    where
      ct2.LOG_ID = ct.LOG_ID
        and ct2.TRACKING_EVENT_C = '90'
  )
  and not exists (
    select null
    from ge.OR_LOG_CASE_TIMES ct2
    where
      ct2.LOG_ID = ct.LOG_ID
        and ct2.TRACKING_EVENT_C = '390'
  )
  and not exists (
    select null
    from ge.OR_LOG_CASE_TIMES ct2
    where
      ct2.LOG_ID = ct.LOG_ID
        and ct2.TRACKING_EVENT_C = '80'
  )

select *
from ge.OR_CASE_AUDIT_TRL trl
where
  OR_CASE_ID = '130416'
order by
  line

select *
from ge.OR_CASE_SCHED_HIST
where
  OR_CASE_ID ='130416'

  select 
    ct.log_id
    , ct.line
    , ct.TRACKING_EVENT_C
    , zpe.name
    , ct.tracking_time_in
  from ge.OR_LOG_CASE_TIMES ct 
  left join ge.ZC_OR_PAT_EVENTS zpe
    on ct.TRACKING_EVENT_C = zpe.TRACKING_EVENT_C
  where 
    ct.log_id = '138423' -- swap this with LOG_ID of interest
  order by 
    ct.line
*/