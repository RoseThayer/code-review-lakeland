alter procedure gt1.tx_ORANAllStaff as

if object_id('gt1.ORANAllStaff', 'U') is not null drop table gt1.ORANAllStaff;

create table gt1.ORANAllStaff (
	LogID [numeric] (18,0) NOT NULL
	,StaffRecordID [numeric] (18,0) NULL
	,StaffTypeID [numeric] (18,0) NULL
	,StaffType [varchar](254) NULL
	,StaffID [numeric] (18,0) NULL
	,StaffName [varchar](254) NULL
	,StaffIsAnesthesia [numeric] (18,0) NULL
	,StaffStartTime [datetime] NULL
	,StaffEndTime [datetime] NULL
	,StaffTotalTime [numeric] (18,0) NULL
	,StaffMinutesWorked [numeric] (18,0) NULL
	,Source [varchar] (254) NULL
);
with ORStaff as (
	select
		cast(staff.LOG_ID as numeric) as LogID
		,staff.STAFF_ID as StaffRecordID
		,lnlg.STAFF_TYPE_C as StaffTypeID
		,ztype.NAME as StaffType
		,lnlg.STAFF_MEMBER_ID as StaffID
		,ser.PROV_NAME as StaffName
		,0 as StaffIsAnesthesia
		,times.STAFF_START_TIME as StaffStartTime
		,times.STAFF_END_TIME as StaffEndTime
		,times.STAFF_TOTAL_TIME as StaffTotalTime
		,'OR Staff' as Source
	from ge.OR_LOG_LN_STAFF staff
	left join ge.OR_LNLG_SURG_STAFF lnlg on
		lnlg.RECORD_ID=staff.STAFF_ID
	left join ge.ZC_OR_STAFF_TYPE ztype on
		ztype.SURG_STAFF_REQ_C=lnlg.STAFF_TYPE_C
	left join ge.CLARITY_SER ser on
		ser.PROV_ID=lnlg.STAFF_MEMBER_ID
	left join ge.OR_LNLG_SRGSTF_TMS times on
		times.RECORD_ID=staff.STAFF_ID
),
ANStaff as (
	select
		cast(log.LOG_ID as numeric) as LogID
		,anstaff.RECORD_ID as StaffRecordID
		,anstaff.ANESTH_STAFF_C as StaffTypeID
		,ztype.NAME as StaffType
		,anstaff.ANESTH_STAFF_ID as StaffID
		,ser.PROV_NAME as StaffName
		,1 as StaffIsAnesthesia
		,times.ANES_STAFF_ST_TIME as StaffStartTime
		,times.ANES_STAFF_END_TIM as StaffEndTime
		,times.ANES_STAFF_TOT_TIM as StaffTotalTime
		,'AN Staff' as Source
	from ge.OR_LNLG_ANES_STAFF anstaff
	inner join ge.OR_LOG_LN_ANESSTAF log on
		log.ANESTHESIA_STAFF_I=anstaff.RECORD_ID --only keeping AN records used on logs
	left join ge.ZC_OR_ANSTAFF_TYPE ztype on
		ztype.ANEST_STAFF_REQ_C=ANESTH_STAFF_C
	left join ge.CLARITY_SER ser on
		ser.PROV_ID=anstaff.ANESTH_STAFF_ID
	left join ge.OR_LNLG_ANESTF_TMS times on
		times.RECORD_ID=anstaff.ANESTH_STAFF_ID
),
ANResponsible as (
	select
		cast(log.LOG_ID as numeric) as LogID
		,anresp.RECORD_ID as StaffRecordID
		,anresp.ANES_STAFF_TYPE_C as StaffTypeID
		,ztype.NAME as StaffType
		,anresp.ANES_STAFF_ID as StaffID
		,ser.PROV_NAME as StaffName
		,1 as StaffIsAnesthesia
		,times.ANES_STAFF_ST_TIME as StaffStartTime
		,times.ANES_STAFF_END_TIM as StaffEndTime
		,times.ANES_STAFF_TOT_TIM as StaffTotalTime
		,'AN Responsible' as Source
	from ge.OR_LNLG_ANES_RESP anresp
	inner join ge.OR_LOG_LN_ANESSTAF log on
		log.ANESTHESIA_STAFF_I=anresp.RECORD_ID --only keeping AN records used on logs
	left join ge.ZC_OR_ANSTAFF_TYPE ztype on
		ztype.ANEST_STAFF_REQ_C=anresp.ANES_STAFF_TYPE_C
	left join ge.CLARITY_SER ser on
		ser.PROV_ID=anresp.ANES_STAFF_ID
	left join ge.OR_LNLG_ANESTF_TMS times on
		times.RECORD_ID=anresp.ANES_STAFF_ID
),
LogSurgeons as (
select
	cast(surg.LOG_ID as numeric) as LogID
	--,surg.PANEL as Panel
	--,ROLE_C as StaffRoleID
	,NULL as StaffRecordID
	,NULL as StaffTypeID
	,'Surgeon ' + role.NAME as StaffType
	,0 as StaffIsAnesthesia
	,surg.SURG_ID as StaffID
	,ser.PROV_NAME as StaffName
	,surg.START_TIME as StaffStartTime
	,surg.END_TIME as StaffEndTime
	,surg.TOTAL_LENGTH as StaffTotalTime
	,'OR Surgeon' as Source
from ge.OR_LOG_ALL_SURG surg
left join ge.ZC_OR_PANEL_ROLE role on
	role.ROLE_C = surg.ROLE_C
left join ge.CLARITY_SER ser on
	ser.PROV_ID = surg.SURG_ID
)
insert into gt1.ORANAllStaff
(
	LogID
	,StaffRecordID
	,StaffTypeID
	,StaffType
	,StaffID
	,StaffName
	,StaffIsAnesthesia
	,StaffStartTime
	,StaffEndTime
	,StaffTotalTime
	,StaffMinutesWorked
	,Source
)
select
	LogID
	,StaffRecordID
	,StaffTypeID
	,StaffType
	,StaffID
	,StaffName
	,StaffIsAnesthesia
	,StaffStartTime
	,StaffEndTime
	,StaffTotalTime
	,DateDiff( minute, StaffStartTime, StaffEndTime ) as StaffMinutesWorked
	,Source
from ORStaff
union all 
select
	LogID
	,StaffRecordID
	,StaffTypeID
	,StaffType
	,StaffID
	,StaffName
	,StaffIsAnesthesia
	,StaffStartTime
	,StaffEndTime
	,StaffTotalTime
	,DateDiff( minute, StaffStartTime, StaffEndTime ) as StaffMinutesWorked
	,Source
from ANStaff
union all 
select
	LogID
	,StaffRecordID
	,StaffTypeID
	,StaffType
	,StaffID
	,StaffName
	,StaffIsAnesthesia
	,StaffStartTime
	,StaffEndTime
	,StaffTotalTime
	,DateDiff( minute, StaffStartTime, StaffEndTime ) as StaffMinutesWorked
	,Source
from ANResponsible
union all 
select
	LogID
	,StaffRecordID
	,StaffTypeID
	,StaffType
	,StaffID
	,StaffName
	,StaffIsAnesthesia
	,StaffStartTime
	,StaffEndTime
	,StaffTotalTime
	,DateDiff( minute, StaffStartTime, StaffEndTime ) as StaffMinutesWorked
	,Source
from LogSurgeons
;