alter procedure gt1.tx_HospitalAccount as

if object_id('gt1.HospitalAccount', 'U') is not null drop table gt1.HospitalAccount;

create table [gt1].[HospitalAccount]
(
    HospitalAccountID 			numeric(18,0)    NOT NULL
  , PatientID					varchar(254)        NULL
  , BaseClassID 		    	varchar(254)        NULL
  , BaseClass 			    	varchar(254)         NULL
  , DischargeDepartmentID 		varchar(254)         NULL
  , DischargeDepartment 		varchar(254)         NULL
  , AccountClassID	 	varchar(254)         NULL
  , AccountClass 		varchar(254)         NULL
  , FinancialClassID 			varchar(254)	     NULL
  , FinancialClass 				varchar(254)         NULL
  , PrimaryPlanID				varchar(254)        NULL
  , PrimaryPlan 				varchar(254)         NULL
  , PrimaryPayorID				varchar(254)        NULL
  , PrimaryPayor				varchar(254)         NULL
  , AttendingProviderID			varchar(254)        NULL
  , AttendingProvider			varchar(254)         NULL
  , ReferringProviderID 		varchar(254)        NULL
  , ReferringProvider			varchar(254)         NULL
  , DischargeDate				date				 NULL
  , DischargeDateTime 			datetime       		 NULL
  , AdmitDate					date				 NULL
  , LOS							int 				 NULL
  , FinalDRGID					varchar(254)        NULL
  , FinalDRGCode				varchar(254)         NULL
  , FinalDRGDescription			varchar(254)         NULL
  --, FinalDRGVersion				varchar(254)        NULL --Not an available field
  , TotalCharges				numeric(18,0)        NULL
  , TotalPayments				numeric(18,0)        NULL
  , TotalAdjustments			numeric(18,0)        NULL
  , ExpectedPayment				numeric(18,0)        NULL
  , CurrentBalance				numeric(18,0)        NULL
  , AdmissionSourceID			varchar(254)	       NULL
  , AdmissionSource				varchar(254)         NULL
  , DischargeStatusID			varchar(254)        NULL
  , DischargeStatus				varchar(254)         NULL
  , AdmissionTypeID				varchar(254)        NULL
  , AdmissionType				varchar(254)         NULL
)

insert into [gt1].[HospitalAccount]
(
    HospitalAccountID
  , PatientID
  , BaseClassID
  , BaseClass
  , DischargeDepartmentID
  , DischargeDepartment
  , AccountClassID
  , AccountClass 
  , FinancialClassID
  , FinancialClass
  , PrimaryPlanID
  , PrimaryPlan 
  , PrimaryPayorID
  , PrimaryPayor
  , AttendingProviderID
  , AttendingProvider
  , ReferringProviderID 
  , ReferringProvider
  , DischargeDate
  , DischargeDateTime
  , AdmitDate
  , LOS
  , FinalDRGID
  , FinalDRGCode
  , FinalDRGDescription
  --, FinalDRGVersion --Not an available field
  , TotalCharges
  , TotalPayments
  , TotalAdjustments
  , ExpectedPayment
  , CurrentBalance
  , AdmissionSourceID
  , AdmissionSource
  , DischargeStatusID
  , DischargeStatus
  , AdmissionTypeID
  , AdmissionType
)


select distinct
    cast(ha.HSP_ACCOUNT_ID as numeric) as HospitalAccountID
  , ha.PAT_ID as PatientID
  , ha.ACCT_BASECLS_HA_C as BaseClassID
  , zabh.NAME as BaseClass
  , ha.DISCH_DEPT_ID as DischargeDepartmentID
  , cd.DEPARTMENT_NAME as DischargeDepartment
  , ha.ACCT_CLASS_HA_C as AccountClassID
  , zach.NAME as AccountClass    
  , ha.ACCT_FIN_CLASS_C as FinancialClassID
  , zfc.NAME as FinancialClass    
  , ha.PRIMARY_PLAN_ID as PrimaryPlanID 
  , ce.BENEFIT_PLAN_NAME as PrimaryPlan  
  , ha.PRIMARY_PAYOR_ID  as PrimaryPayorID
  , cp.PAYOR_NAME as PrimaryPayor
  , ha.ATTENDING_PROV_ID as AttendingProviderID
  , cs.PROV_NAME as AttendingProvider  
  , ha.REFERRING_PROV_ID as ReferringProviderID 
  , cs2.PROV_NAME as ReferringProvider
  , cast(ha.DISCH_DATE_TIME as date) as DischargeDate
  , ha.DISCH_DATE_TIME as DischargeDateTime
  , cast(ha.ADM_DATE_TIME as date) as AdmitDate
  , datediff(day,ha.ADM_DATE_TIME,ha.DISCH_DATE_TIME)+1 as LOS --Count admit and discharge date, to account for add 1 to datediff
  , ha.FINAL_DRG_ID as FinalDRGID
  , cdrg.DRG_NUMBER as FinalDRGCode
  , cdrg.DRG_NAME as FinalDRGDescription
  --, mdrg.DRG_TYPE as FinalDRGVersion --Not an available field
  , ha.TOT_CHGS as TotalCharges
  , ha.TOT_PMTS as TotalPayments
  , ha.TOT_ADJ as TotalAdjustments
  , cast(ha.TOT_CHGS + ha.TOT_ADJ as numeric) as ExpectedPayment
  , cast(ha.TOT_CHGS + ha.TOT_ADJ + TOT_PMTS as numeric) as CurrentBalance
  --, ha.TOT_ACCT_BAL as TotalAccountBalance
  , ha.ADMISSION_SOURCE_C as AdmissionSourceID
  , zmas.NAME as AdmissionSource
  , ha.PATIENT_STATUS_C as  DischargeStatusID
  , zmps.NAME as DischargeStatus
  , ha.ADMISSION_TYPE_C as AdmissionTypeID
  , zmat.NAME as AdmissionType 
from ge.HSP_ACCOUNT ha
  left join ge.CLARITY_DEP cd
    on cd.DEPARTMENT_ID = ha.DISCH_DEPT_ID
  left join ge.CLARITY_SER cs
	on cs.PROV_ID = ha.ATTENDING_PROV_ID
  left join ge.CLARITY_SER cs2
	on cs2.PROV_ID = ha.REFERRING_PROV_ID
  left join ge.ZC_ACCT_BASECLS_HA zabh
	on zabh.ACCT_BASECLS_HA_C = ha.ACCT_BASECLS_HA_C
  left join ge.ZC_FIN_CLASS zfc 
	on zfc.FIN_CLASS_C = ha.ACCT_FIN_CLASS_C
  left join ge.CLARITY_EPP ce
	on ce.BENEFIT_PLAN_ID = ha.PRIMARY_PLAN_ID
  left join ge.CLARITY_EPM cp
	on cp.PAYOR_ID = ha.PRIMARY_PAYOR_ID
  left join ge.ZC_MC_ADM_SOURCE zmas
	on zmas.ADMISSION_SOURCE_C = ha.ADMISSION_SOURCE_C
  left join ge.ZC_ACCT_CLASS_HA zach
	on zach.ACCT_CLASS_HA_C = ha.ACCT_CLASS_HA_C
  left join ge.ZC_MC_PAT_STATUS zmps
	on zmps.PAT_STATUS_C = ha.PATIENT_STATUS_C
  left join ge.ZC_MC_ADM_TYPE zmat
	on zmat.ADMISSION_TYPE_C = ha.ADMISSION_TYPE_C
  left join ge.CLARITY_DRG cdrg 
		on cdrg.DRG_ID = ha.FINAL_DRG_ID
  left join ge.HSP_ACCT_MULT_DRGS mdrg
		on mdrg.DRG_ID=ha.FINAL_DRG_ID
		and mdrg.HSP_ACCOUNT_ID=ha.HSP_ACCOUNT_ID
;