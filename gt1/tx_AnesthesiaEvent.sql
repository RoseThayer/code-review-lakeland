alter procedure gt1.tx_AnesthesiaEvent as

/*
create nonclustered index ncix__ED_IEV_PAT_INFO__EVENT_ID
  on ge.ED_IEV_PAT_INFO ([EVENT_ID])
  include ([PAT_ENC_CSN_ID])
;


create nonclustered index ncix__PAT_OR_ADM_LINK__OR_LINK_CSN
  on ge.PAT_OR_ADM_LINK ([OR_LINK_CSN])
  include ([CASE_ID], [LOG_ID])
;

create nonclustered index ncix__OR_CASE__OR_CASE_ID
  on ge.OR_CASE ([OR_CASE_ID])
  include ([SURGERY_DATE])
;

alter table ge.OR_CASE
  add constraint PK_OR_CASE unique ([OR_CASE_ID])
;

create nonclustered index ncix__PAT_ENC_HSP__PAT_ENC_CSN_ID
  on ge.PAT_ENC_HSP ([PAT_ENC_CSN_ID])
;

alter table ge.PAT_ENC_HSP
  add constraint PK__PAT_ENC_HSP unique ([PAT_ENC_CSN_ID])
;

create nonclustered index ncix__ED_IEV_EVENT_INFO__EVENT_TYPE_STATUS
  on ge.ED_IEV_EVENT_INFO ([EVENT_TYPE], [EVENT_STATUS_C])
  include ([EVENT_ID], [EVENT_TIME])
;

create nonclustered index ncix__OR_LOG_CASE_TIMES__TRACKING_EVENT_C
  on ge.OR_LOG_CASE_TIMES ([TRACKING_EVENT_C])
  include ([LOG_ID], [TRACKING_TIME_IN])
;

*/

if object_id('gt1.AnesthesiaEvent') is not null
  drop table gt1.AnesthesiaEvent
;

create table gt1.AnesthesiaEvent (
	SummaryBlockID                                  numeric(18, 0)
	, SurgeryID										numeric(18, 0)
	, HospitalEncounterID                           numeric(18, 0)
	, AnesthesiaStartTime                           datetime
	, AnesthesiaStopTime                            datetime
	, AirwayPlacedTime                              datetime
	, AirwayRemovedTime                             datetime
	, SurgeonCalledTime                             datetime
	, SurgeonArrivedTime                            datetime
	, PatientReadyTime                              datetime 
)

-- set this to NULL to allow NULL start times 
-- RT: Do we really want low and high times?  It may be safer to just have null durations (and flag null durations in the model)
declare @DefaultLowDate date = '1900-01-01';

-- set this to NULL to allow NULL end times
declare @DefaultHighDate date = '2099-12-31';

-- set this to NULL to allow NULL durations when the start and/or end times are NULL
declare @DefaultBadDuration int = -1000000;


declare @ApplicationStartDate date = variables.GetSingleValue('@vcgORStartDate');
if @ApplicationStartDate is null
  set @ApplicationStartDate = '2016-10-01' 
;

declare @ApplicationEndDate date = variables.GetSingleValue('@vcgOREndDate');
if @ApplicationEndDate is null
  set @ApplicationEndDate = getdate()
;

--RT need to update variables to not set if null and just pull from spreadsheet
declare @AnesthesiaStartEvent varchar(18) = variables.GetSingleValue('@vcgOREventAnesthesiaStart');
if @AnesthesiaStartEvent is null
  set @AnesthesiaStartEvent = '1120000001'
;

declare @AnesthesiaStopEvent varchar(18) = variables.GetSingleValue('@vcgOREventAnesthesiaFinish');
if @AnesthesiaStopEvent is null
  set @AnesthesiaStopEvent = '1120000002'
;

declare @AirwayPlacedEvent varchar(18) = variables.GetSingleValue('@vcgOREventAirwayPlaced');
if @AirwayPlacedEvent is null
  set @AirwayPlacedEvent = '1120000009'
;

declare @AirwayRemovedEvent varchar(18) = variables.GetSingleValue('@vcgOREventAirwayRemoved');
if @AirwayRemovedEvent is null
  set @AirwayRemovedEvent = '1120000061'
;

declare @SurgeonCalledEvent varchar(18) = variables.GetSingleValue('@vcgOREventSurgeonCalled');
if @SurgeonCalledEvent is null
  set @SurgeonCalledEvent = '112100267'
;

declare @SurgeonArrivedEvent varchar(18) = variables.GetSingleValue('@vcgOREventSurgeonArrived');
if @SurgeonArrivedEvent is null
  set @SurgeonArrivedEvent = '112100268'
;

declare @OutOfRoomEvent varchar(66) = variables.GetSingleValue('@vcgOREventPatOutRoom');
if @OutOfRoomEvent is null
  set @OutOfRoomEvent = '110'
;

declare @ANSignOffReadyForProcedure int = variables.GetSingleValue('@vcgANSignOffReadyForProcedure');
if @ANSignOffReadyForProcedure is null
  set @ANSignOffReadyForProcedure = 2
;

with anes_event as (
  -- per customer, always grab first event time if there are multiple
  select
      ievp.PAT_ENC_CSN_ID
    , ieve.EVENT_TYPE
    , min(ieve.EVENT_TIME)  as EVENT_TIME
  from ge.ED_IEV_PAT_INFO ievp
  left join ge.ED_IEV_EVENT_INFO ieve
    on ieve.EVENT_ID = ievp.EVENT_ID
  where
    ieve.EVENT_STATUS_C is null 
      and ieve.EVENT_TYPE in (
        @AnesthesiaStartEvent
        , @AnesthesiaStopEvent
        , @AirwayPlacedEvent
        , @AirwayRemovedEvent
        , @SurgeonCalledEvent
        , @SurgeonArrivedEvent)
  group by
    ievp.PAT_ENC_CSN_ID, ieve.EVENT_TYPE --RT adding log ID to the grouping
)
, anes_start as (  
  select
      ae.PAT_ENC_CSN_ID
    , ae.EVENT_TIME
  from anes_event ae
  where
    ae.EVENT_TYPE = @AnesthesiaStartEvent
)
, anes_stop as (
  select
      ae.PAT_ENC_CSN_ID
    , ae.EVENT_TIME
  from anes_event ae
  where
    ae.EVENT_TYPE = @AnesthesiaStopEvent
)
, airway_placed as (
  select
      ae.PAT_ENC_CSN_ID
    , ae.EVENT_TIME
  from anes_event ae
  where
    ae.EVENT_TYPE = @AirwayPlacedEvent
)
, airway_removed as (
  select
      ae.PAT_ENC_CSN_ID
    , ae.EVENT_TIME
  from anes_event ae
  where
    ae.EVENT_TYPE = @AirwayRemovedEvent
)
, surgeon_called as (
  select
      ae.PAT_ENC_CSN_ID
    , ae.EVENT_TIME
  from anes_event ae
  where
    ae.EVENT_TYPE = @SurgeonCalledEvent
)
, surgeon_arrived as (
  select
      ae.PAT_ENC_CSN_ID
    , ae.EVENT_TIME
  from anes_event ae
  where
    ae.EVENT_TYPE = @SurgeonArrivedEvent
)
, pat_ready as (
  select
    ahso.SUMMARY_BLOCK_ID
    , min(ahso.ANES_SIGNOFF_INST) as EVENT_TIME
  from ge.AN_HSB_SIGNOFF ahso
  where
    ahso.ANES_SIGNOFF_TYPE_C = @ANSignOffReadyForProcedure
  group by
    ahso.SUMMARY_BLOCK_ID
)
insert into gt1.AnesthesiaEvent (
	SummaryBlockID   
	, SurgeryID               
	, HospitalEncounterID                       
	, AnesthesiaStartTime                  
	, AnesthesiaStopTime                   
	, AirwayPlacedTime                     
	, AirwayRemovedTime                    
	, SurgeonCalledTime                    
	, SurgeonArrivedTime                   
	, PatientReadyTime 
)   
select distinct -- RT: duplicating because blocks can be associated with multiple surgeries. Changed linking structure to ensure block matched with the appropriate log.
  ahli.SUMMARY_BLOCK_ID                                                 as SummaryBlockID
  , adm.LOG_ID															as SurgeryID
  , adm.OR_LINK_CSN														 as HospitalEncounterID
  , coalesce(anes_start.EVENT_TIME, @DefaultLowDate)                    as AnesthesiaStartTime
  , coalesce(anes_stop.EVENT_TIME, @DefaultHighDate)                    as AnesthesiaStopTime
  , coalesce(airway_placed.EVENT_TIME, @DefaultLowDate)                 as AirwayPlacedTime
  , coalesce(airway_removed.EVENT_TIME, @DefaultHighDate)               as AirwayRemovedTime
  , coalesce(surgeon_called.EVENT_TIME, @DefaultLowDate)                as SurgeonCalledTime
  , coalesce(surgeon_arrived.EVENT_TIME, @DefaultHighDate)              as SurgeonArrivedTime
  , coalesce(pat_ready.EVENT_TIME, @DefaultLowDate)                     as PatientReadyTime	
	--RT 11 18 Moved durations to Surgical Encounters so they can more easily use base AN and base OR events
	--AnesthesiaDuration
  --DocErrFlag_AnesthesiaDuration
  --SurgeonCalledToArrival
  --DocErrFlag_SurgeonCalledToArrival
  --AnesthesiaChartingDuration  -- todo: let Natalie know that 25% of time anesthesia stop happens before out of room time
	--DocErrFlag_AnesthesiaChartingDuration  
from ge.an_hsb_link_info ahli
left join ge.AN_HSB_LINK_PROCS	procs	on			--Link from HSB > BLOCK ID > Surgical CSN > LogID
	procs.SUMMARY_BLOCK_ID=ahli.SUMMARY_BLOCK_ID
left join ge.PAT_OR_ADM_LINK adm on
	adm.PAT_ENC_CSN_ID=procs.EPT_PROC_CSN
left join ge.pat_enc_hsp peh
  on peh.PAT_ENC_CSN_ID = ahli.AN_BILLING_CSN_ID
left join ge.OR_LOG orl
  on adm.LOG_ID = orl.LOG_ID
left join ge.OR_CASE orc
  on adm.CASE_ID = orc.OR_CASE_ID
left join anes_start 
  on anes_start.PAT_ENC_CSN_ID = ahli.AN_52_ENC_CSN_ID
left join anes_stop 
  on anes_stop.PAT_ENC_CSN_ID = ahli.AN_52_ENC_CSN_ID
left join airway_placed 
  on airway_placed.PAT_ENC_CSN_ID = ahli.AN_52_ENC_CSN_ID
left join airway_removed 
  on airway_removed.PAT_ENC_CSN_ID = ahli.AN_52_ENC_CSN_ID
left join surgeon_called 
  on surgeon_called.PAT_ENC_CSN_ID = ahli.AN_52_ENC_CSN_ID
left join surgeon_arrived 
  on surgeon_arrived.PAT_ENC_CSN_ID = ahli.AN_52_ENC_CSN_ID
left join pat_ready 
  on pat_ready.SUMMARY_BLOCK_ID = ahli.SUMMARY_BLOCK_ID
where
  coalesce(orl.SURGERY_DATE, orc.SURGERY_DATE) >= @ApplicationStartDate
    and coalesce(orl.SURGERY_DATE, orc.SURGERY_DATE) < @ApplicationEndDate
;
go

exec gt1.tx_AnesthesiaEvent;

