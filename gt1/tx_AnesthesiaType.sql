alter procedure gt1.tx_AnesthesiaType as

if object_id('gt1.AnesthesiaType', 'U') is not null drop table gt1.AnesthesiaType;

create table gt1.AnesthesiaType (
	LogID [numeric] (18,0) NOT NULL
	,TypeID [varchar] (254) NULL
	,Type [varchar] (254) NULL
)
insert into gt1.AnesthesiaType
(
	LogID
	,TypeID
	,Type
)
select
	cast(LOG_ID as numeric) as LogID
	,LOG_ANESTH_TYPE_C as TypeID
	,t.NAME as Type
from ge.OR_LOG_ANES_TYPES an
left join ge.ZC_OR_ANESTH_TYPE t on 
	an.LOG_ANESTH_TYPE_C=t.ANESTHESIA_TYPE_C
;