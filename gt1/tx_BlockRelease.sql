--alter procedure gt1.tx_BlockRelease as

if object_id('gt1.BlockRelease', 'U') is not null drop table gt1.BlockRelease;

declare @SurgeonBlockID numeric (18,0) = variables.GetSingleValue('@vcgProviderORBlockTypeID'); 
declare @GroupBlockID numeric (18,0) = variables.GetSingleValue('@vcgGroupORBlockTypeID'); 
declare @ServiceBlockID numeric (18,0) = variables.GetSingleValue('@vcgServiceORBlockTypeID'); 

create table gt1.BlockRelease (
	ReleasedFromBlockEndDateTime [datetime] NULL
	,ReleasedFromBlockStartDateTime [datetime] NULL
	,ReleasedMinutes [numeric] (18,0) NULL
	,ReleasedFromBlockID [varchar](254) NULL
	,ReleasedFromBlockName [varchar](254) NULL
	,ReleasedFromBlockTypeID [int] NULL
	,ReleasedFromBlockType [varchar](254) NULL
	,ReleasedAtDateTime [datetime] NULL
	,ReleasedFromDate [date] NULL
    ,ReleasedOnDate [date] NULL
	,ReleaseTypeID [varchar](254) NULL
	,ReleaseType [varchar](254) NULL
	,ReleasedByUserID [varchar](254) NULL
	,ReleasedByUser [varchar](254) NULL
	,ReleaseRecordID [numeric] NULL
	,ReleaseDaysInAdvance [numeric] (18,0) NULL
	,ReleaseComments [varchar](254) NULL
	,ReleaseReasonID [int] NULL
	,ReleaseReason [varchar](254) NULL
	,ReleasedFromRecordID [varchar](254) NULL
	,ReleasedFromRecordName [varchar](254) NULL
	,ReleasedToBlockID [varchar](254) NULL
	,ReleasedToBlockName [varchar](254) NULL
	,ReleasedToBlockTypeID [int] NULL
	,ReleasedToBlockType [varchar](254) NULL
)
insert into gt1.BlockRelease
(
	ReleasedFromBlockEndDateTime
	,ReleasedFromBlockStartDateTime
	,ReleasedMinutes
	,ReleasedFromBlockID
	,ReleasedFromBlockName
	,ReleasedFromBlockTypeID
	,ReleasedFromBlockType
	,ReleasedAtDateTime
	,ReleasedFromDate
    ,ReleasedOnDate
	,ReleaseTypeID
	,ReleaseType
	,ReleasedByUserID
	,ReleasedByUser
	,ReleaseRecordID
	,ReleaseDaysInAdvance
	,ReleaseComments
	,ReleaseReasonID
	,ReleaseReason
	,ReleasedFromRecordID 
	,ReleasedFromRecordName
	,ReleasedToBlockID
	,ReleasedToBlockName
	,ReleasedToBlockTypeID
	,ReleasedToBlockType
)
select
	ota.BLOCK_END_INST as ReleasedFromBlockEndDateTime
	,ota.BLOCK_START_INST as ReleasedFromBlockStartDateTime
	,DateDiff( minute, BLOCK_START_INST, BLOCK_END_INST ) as ReleasedMinutes
	,ota.FROM_BLOCK_ID as ReleasedFromBlockID
	,name.TITLE as ReleasedFromBlockName
	,ota.FROM_BLOCK_TYPE_C as ReleasedFromBlockTypeID
	,case 	when ota.FROM_BLOCK_TYPE_C =@ServiceBlockID THEN 'Service'
			when ota.FROM_BLOCK_TYPE_C =@SurgeonBlockID THEN 'Provider'
			when ota.FROM_BLOCK_TYPE_C =@GroupBlockID THEN 'Group'
			else 'Unknown' end as ReleasedFromBlockType
	,ota.MOD_INST as ReleasedAtDateTime
	,cast(BLOCK_START_INST as date) as ReleasedFromDate
    ,cast(MOD_INST as date) as ReleasedOnDate
	,ota.MOD_TYPE_C as ReleaseTypeID
	,mt.NAME as ReleaseType
	,ota.MOD_USER_ID as ReleasedByUserID
	,emp.NAME as ReleasedByUser
	,ota.RECORD_ID as ReleaseRecordID
	,ota.REL_DAYS_IN_ADVANC as ReleaseDaysInAdvance
	,ota.RELEASE_COMMENTS as ReleaseComments
	,ota.RELEASE_REASON_C as ReleaseReasonID
	,zrr.NAME as ReleaseReason
	,ota.SER_RECORD_ID as ReleasedFromRecordID --either provider or room
	,ser.PROV_NAME as ReleasedFromRecordName
	,ota.TO_BLOCK_ID as ReleasedToBlockID
	,name2.TITLE as ReleasedToBlockName
	,ota.TO_BLOCK_TYPE_C as ReleasedToBlockTypeID
	,case 	when ota.TO_BLOCK_TYPE_C =@ServiceBlockID  THEN 'Service'
			when ota.TO_BLOCK_TYPE_C =@SurgeonBlockID THEN 'Provider'
			when ota.TO_BLOCK_TYPE_C =@GroupBlockID THEN 'Group'
			else 'Unknown' end as ReleasedToBlockType
from ge.OR_OTA ota
left join ge.OR_BLOCKNAMES name on 
	name.ID=ota.FROM_BLOCK_ID and
	name.TYPE=ota.FROM_BLOCK_TYPE_C
left join ge.OR_BLOCKNAMES name2 on 
	name2.ID=ota.TO_BLOCK_ID
left join ge.ZC_MOD_TYPE mt on 
	mt.MOD_TYPE_C=ota.MOD_TYPE_C
left join ge.ZC_RELEASE_REASON zrr on 
	zrr.RELEASE_REASON_C=ota.RELEASE_REASON_C
left join ge.CLARITY_SER ser on 
	ser.PROV_ID=ota.SER_RECORD_ID
left join ge.CLARITY_EMP emp on 
	emp.USER_ID=ota.MOD_USER_ID
;