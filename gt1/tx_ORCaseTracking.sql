alter procedure gt1.tx_ORCaseTracking as

if object_id('gt1.ORCaseTracking', 'U') is not null drop table gt1.ORCaseTracking;

----------------------------------
--VARIABLES
declare @OREventPatInPreop table ( TRACKING_EVENT_C smallint );
	insert into @OREventPatInPreop select * from variables.GetTableValues('@vcgOREventPatInPreop');
declare @OREventPatPreProcStart table ( TRACKING_EVENT_C smallint );
	insert into @OREventPatPreProcStart select * from variables.GetTableValues('@vcgOREventPatPreProcStart');
declare @OREventPatPreProcFinish table ( TRACKING_EVENT_C smallint );
	insert into @OREventPatPreProcFinish select * from variables.GetTableValues('@vcgOREventPatPreProcFinish');

declare @OREventPatInRoom table ( TRACKING_EVENT_C smallint );
	insert into @OREventPatInRoom select * from variables.GetTableValues('@vcgOREventPatInRoom');
declare @OREventPatOutRoom table ( TRACKING_EVENT_C smallint );
	insert into @OREventPatOutRoom select * from variables.GetTableValues('@vcgOREventPatOutRoom');

declare @OREventPatInRecovery table ( TRACKING_EVENT_C smallint );
	insert into @OREventPatInRecovery select * from variables.GetTableValues('@vcgOREventPatInRecovery');
declare @OREventPatOutRecovery table ( TRACKING_EVENT_C smallint );
	insert into @OREventPatOutRecovery select * from variables.GetTableValues('@vcgOREventPatOutRecovery');

declare @OREventPatInPACU table ( TRACKING_EVENT_C smallint );
	insert into @OREventPatInPACU select * from variables.GetTableValues('@vcgOREventPatInPACU');
declare @OREventPatOutPACU table ( TRACKING_EVENT_C smallint );
	insert into @OREventPatOutPACU select * from variables.GetTableValues('@vcgOREventPatOutPACU');

declare @OREventPhaseIIStart table ( TRACKING_EVENT_C smallint );
	insert into @OREventPhaseIIStart select * from variables.GetTableValues('@vcgOREventPhaseIIStart');
declare @OREventPhaseIIFinish table ( TRACKING_EVENT_C smallint );
	insert into @OREventPhaseIIFinish select * from variables.GetTableValues('@vcgOREventPhaseIIFinish');

declare @OREventProcedureStart table ( TRACKING_EVENT_C smallint );
	insert into @OREventProcedureStart select * from variables.GetTableValues('@vcgOREventProcedureStart');
declare @OREventProcedureFinish table ( TRACKING_EVENT_C smallint );
	insert into @OREventProcedureFinish select * from variables.GetTableValues('@vcgOREventProcedureFinish'); 

declare @OREventSetUpStart table ( TRACKING_EVENT_C smallint );
	insert into @OREventSetUpStart select * from variables.GetTableValues('@vcgOREventSetUpStart');
declare @OREventSetUpFinish table ( TRACKING_EVENT_C smallint );
	insert into @OREventSetUpFinish select * from variables.GetTableValues('@vcgOREventSetUpFinish'); 

declare @OREventInFacility table ( TRACKING_EVENT_C smallint );
	insert into @OREventInFacility select * from variables.GetTableValues('@vcgOREventInFacility');
	
declare @OREventAnesthesiaStart table ( TRACKING_EVENT_C smallint );
	insert into @OREventAnesthesiaStart select * from variables.GetTableValues('@vcgOREventAnesthesiaStart');
declare @OREventAnesthesiaFinish table ( TRACKING_EVENT_C smallint );
	insert into @OREventAnesthesiaFinish select * from variables.GetTableValues('@vcgOREventAnesthesiaFinish');

declare @OREventProceduralCareComplete table ( TRACKING_EVENT_C smallint );
	insert into @OREventProceduralCareComplete select * from variables.GetTableValues('@vcgOREventProceduralCareComplete');
--ADD NEW EVENT VARIABLE TABLE HERE
----------------------------------

create table gt1.ORCaseTracking (
	LogID [numeric] (18,0) NOT NULL
	,PatientInPreopTime [datetime] NULL
	,PreProcedureStartTime [datetime] NULL
	,PreProcedureCompleteTime [datetime] NULL
	,PatientInRoomTime [datetime] NULL
	,PatientOutRoomTime [datetime] NULL
	,PatientInRecoveryTime [datetime] NULL
	,PatientOutRecoveryTime [datetime] NULL
	,PhaseIStartTime [datetime] NULL
	,PhaseIFinishTime [datetime] NULL
	,PhaseIIStartTime [datetime] NULL
	,PhaseIIFinishTime [datetime] NULL
	,ProcedureStartTime [datetime] NULL
	,ProcedureFinishTime [datetime] NULL
	,SetUpStartTime [datetime] NULL
	,SetUpFinishTime [datetime] NULL
	,PatientInFacilityTime [datetime] NULL
	,ProceduralCareCompleteTime [datetime] NULL
	--ADD NEW EVENT FIELD HERE
)
insert into gt1.ORCaseTracking
(
	LogID
	,PatientInPreopTime
	,PreProcedureStartTime
	,PreProcedureCompleteTime
	,PatientInRoomTime
	,PatientOutRoomTime
	,PatientInRecoveryTime
	,PatientOutRecoveryTime
	,PhaseIStartTime
	,PhaseIFinishTime
	,PhaseIIStartTime
	,PhaseIIFinishTime
	,ProcedureStartTime
	,ProcedureFinishTime
	,SetUpStartTime
	,SetUpFinishTime
	,PatientInFacilityTime
	,ProceduralCareCompleteTime
	--ADD NEW EVENT FIELD HERE
)

select
	base.LOG_ID as LogID
	,min(iif(len(PatInPreop.TRACKING_EVENT_C)>0, base.TRACKING_TIME_IN,NULL)) as PatientInPreopTime

	,min(iif(len(PatPreProcStart.TRACKING_EVENT_C)>0, base.TRACKING_TIME_IN,NULL)) as PreProcedureStartTime
	,min(iif(len(PatPreProcFinish.TRACKING_EVENT_C)>0, base.TRACKING_TIME_IN,NULL)) as PreProcedureCompleteTime

	,min(iif(len(PatInRoom.TRACKING_EVENT_C)>0, base.TRACKING_TIME_IN,NULL)) as PatientInRoomTime
	,min(iif(len(PatOutRoom.TRACKING_EVENT_C)>0, base.TRACKING_TIME_IN,NULL)) as PatientOutRoomTime

	,min(iif(len(PatInRecovery.TRACKING_EVENT_C)>0, base.TRACKING_TIME_IN,NULL)) as PatientInRecoveryTime
	,min(iif(len(PatOutRecovery.TRACKING_EVENT_C)>0, base.TRACKING_TIME_IN,NULL)) as PatientOutRecoveryTime

	,min(iif(len(PatInPACU.TRACKING_EVENT_C)>0, base.TRACKING_TIME_IN,NULL)) as PhaseIStartTime
	,min(iif(len(PatOutPACU.TRACKING_EVENT_C)>0, base.TRACKING_TIME_IN,NULL)) as PhaseIFinishTime

	,min(iif(len(PhaseIIStart.TRACKING_EVENT_C)>0, base.TRACKING_TIME_IN,NULL)) as PhaseIIStartTime
	,min(iif(len(PhaseIIFinish.TRACKING_EVENT_C)>0, base.TRACKING_TIME_IN,NULL)) as PhaseIIFinishTime

	,min(iif(len(ProcedureStart.TRACKING_EVENT_C)>0, base.TRACKING_TIME_IN,NULL)) as ProcedureStartTime
	,min(iif(len(ProcedureFinish.TRACKING_EVENT_C)>0, base.TRACKING_TIME_IN,NULL)) as ProcedureFinishTime

	,min(iif(len(SetUpStart.TRACKING_EVENT_C)>0, base.TRACKING_TIME_IN,NULL)) as SetUpStartTime
	,min(iif(len(SetUpFinish.TRACKING_EVENT_C)>0, base.TRACKING_TIME_IN,NULL)) as SetUpFinishTime

	,min(iif(len(InFacility.TRACKING_EVENT_C)>0, base.TRACKING_TIME_IN,NULL)) as PatientInFacilityTime

	,min(iif(len(ProceduralCareComplete.TRACKING_EVENT_C)>0, base.TRACKING_TIME_IN,NULL)) as ProceduralCareCompleteTime
	--ADD NEW EVENT FIELD HERE
from ge.OR_LOG_CASE_TIMES base

left join @OREventPatInPreop PatInPreop
  on PatInPreop.TRACKING_EVENT_C = base.TRACKING_EVENT_C

left join @OREventPatPreProcStart PatPreProcStart
  on PatPreProcStart.TRACKING_EVENT_C = base.TRACKING_EVENT_C
left join @OREventPatPreProcFinish PatPreProcFinish
  on PatPreProcFinish.TRACKING_EVENT_C = base.TRACKING_EVENT_C

left join @OREventPatInRoom PatInRoom
  on PatInRoom.TRACKING_EVENT_C = base.TRACKING_EVENT_C
left join @OREventPatOutRoom PatOutRoom
  on PatOutRoom.TRACKING_EVENT_C = base.TRACKING_EVENT_C

left join @OREventPatInRecovery PatInRecovery
  on PatInRecovery.TRACKING_EVENT_C = base.TRACKING_EVENT_C
left join @OREventPatOutRecovery PatOutRecovery
  on PatOutRecovery.TRACKING_EVENT_C = base.TRACKING_EVENT_C

left join @OREventPatInPACU PatInPACU
  on PatInPACU.TRACKING_EVENT_C = base.TRACKING_EVENT_C
left join @OREventPatOutPACU PatOutPACU
  on PatOutPACU.TRACKING_EVENT_C = base.TRACKING_EVENT_C

  left join @OREventPhaseIIStart PhaseIIStart
  on PhaseIIStart.TRACKING_EVENT_C = base.TRACKING_EVENT_C
left join @OREventPhaseIIFinish PhaseIIFinish
  on PhaseIIFinish.TRACKING_EVENT_C = base.TRACKING_EVENT_C

left join @OREventProcedureStart ProcedureStart
  on ProcedureStart.TRACKING_EVENT_C = base.TRACKING_EVENT_C
left join @OREventProcedureFinish ProcedureFinish
  on ProcedureFinish.TRACKING_EVENT_C = base.TRACKING_EVENT_C

left join @OREventSetUpStart SetUpStart
  on SetUpStart.TRACKING_EVENT_C = base.TRACKING_EVENT_C
left join @OREventSetUpFinish SetUpFinish
  on SetUpFinish.TRACKING_EVENT_C = base.TRACKING_EVENT_C

left join @OREventInFacility InFacility
  on InFacility.TRACKING_EVENT_C = base.TRACKING_EVENT_C

left join @OREventAnesthesiaStart AnesthesiaStart
  on AnesthesiaStart.TRACKING_EVENT_C = base.TRACKING_EVENT_C
left join @OREventAnesthesiaFinish AnesthesiaFinish
  on AnesthesiaFinish.TRACKING_EVENT_C = base.TRACKING_EVENT_C

left join @OREventProceduralCareComplete ProceduralCareComplete
  on ProceduralCareComplete.TRACKING_EVENT_C = base.TRACKING_EVENT_C
--ADD NEW EVENT TABLE HERE
group by base.LOG_ID
;
