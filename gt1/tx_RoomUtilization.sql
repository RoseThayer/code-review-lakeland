alter procedure gt1.tx_RoomUtilization as

if object_id('gt1.RoomUtilization', 'U') is not null drop table gt1.RoomUtilization;

declare @ORUtilSnapshotRoom int = variables.GetSingleValue('@vcgORUTLSnapshotRoom');

create table gt1.RoomUtilization (
	LogID [numeric] (18,0) NULL
	,SnapshotDate [date] NULL
	,RoomID varchar(254) NOT NULL
	,Room  varchar(254)  NULL
   ,LocationID varchar(254) NULL
	,HourID [numeric] NULL
	,StartTime [time] NULL
	,EndTime [time] NULL
	,AvailableTime [numeric] NULL
	,CorrectTime [numeric] NULL
	,OutsideTime [numeric] NULL
)
insert into gt1.RoomUtilization
(
	LogID
	,SnapshotDate
	,RoomID
	,Room
  ,LocationID
	,HourID
	,StartTime
	,EndTime
	,AvailableTime
	,CorrectTime
	,OutsideTime
)

select
	cast(util.CASE_ID as numeric) as LogID
	,cast(util.SNAPSHOT_DATE as date) as SnapshotDate
	,util.ROOM_ID as RoomID
	,ser.PROV_NAME as Room
   ,facility.LOC_ID as LocationID
	,cast(datepart(hour,util.START_TIME) as numeric) as HourID
	,cast(util.START_TIME-2 as time) as StartTime
	,cast(util.END_TIME-2 as time) as EndTime
	,cast(iif(util.SLOT_TYPE='BLOCK' and util.SNAPSHOT_NUMBER = @ORUtilSnapshotRoom, util.SLOT_LENGTH,NULL) as numeric) as AvailableTime
	,cast(iif(util.SLOT_TYPE='CORRECT' and util.SNAPSHOT_NUMBER = @ORUtilSnapshotRoom,util.SLOT_LENGTH,NULL) as numeric) as CorrectTime
	,cast(iif(util.SLOT_TYPE='OUTSIDE' and util.SNAPSHOT_NUMBER = @ORUtilSnapshotRoom,util.SLOT_LENGTH,NULL) as numeric) as OutsideTime
from ge.OR_UTIL_ROOM util
left join ge.OR_SER_SURG_SRVC facility on --facility structure
  facility.PROV_ID=util.ROOM_ID
left join ge.CLARITY_SER ser on
	ser.PROV_ID=util.ROOM_ID
where
	--(util.SNAPSHOT_NUMBER=@ORUtilSnapshotRoom and util.SLOT_TYPE='BLOCK') -- available time
	(util.SNAPSHOT_NUMBER=@ORUtilSnapshotRoom and util.SLOT_TYPE='BLOCK')
	--or (util.SNAPSHOT_NUMBER=@ORUtilSnapshotRoom and util.SLOT_TYPE='CORRECT') --used time 
	or (util.SNAPSHOT_NUMBER=@ORUtilSnapshotRoom and util.SLOT_TYPE='CORRECT')
	or (util.SNAPSHOT_NUMBER=@ORUtilSnapshotRoom and util.SLOT_TYPE='OUTSIDE')
	and len(util.ROOM_ID)>0
;