alter procedure gt1.tx_ORAuditCase as

-- todo: clean up

if object_id('gt1.tORAuditCase', 'U') is not null 
  drop table gt1.tORAuditCase
;

if object_id('gt1.ORAuditCase', 'U') is not null 
  drop table gt1.ORAuditCase
;

create table gt1.tORAuditCase (
	CaseID numeric
  , SurgeryDate date
	, Line int
	, Action varchar(254)
	, ActionID numeric (18,0)
	, AuditDate date
	, AuditDateTime datetime
	, UserName varchar(254)
	, Comments varchar(max)
	, SchedulingAddOnFlag varchar(254)
	, UnscheduledFromDate date
	, UnscheduledFromTime datetime
	, UnscheduledFromRoomID varchar(254)
	, ScheduledToTime datetime
	, ScheduledToDate date
	, ScheduledToRoomID varchar(254)
  , ProcedureStartedFlag tinyint
  , LastProcedureStartTime datetime
  , CancelledActionFlag tinyint
  , SameDayActionFlag tinyint
  , ScheduledActionFlag tinyint
)
;

declare @UnspecifiedID            int = -1;
declare @UnknownID                int = -2;
declare @NotApplicableID          int = -3;
declare @UnspecifiedLabel         varchar(254) = '*Unspecified';
declare @UnknownLabel             varchar(254) = '*Unknown';
declare @NotApplicableLabel       varchar(254) = '*NotApplicable';
declare @AnesthesiaLabel          varchar(254) = 'Anesthesia';

declare @DefaultLowDate date = '1900-01-01';

declare @DefaultHighDate date = '2099-12-31';

declare @ApplicationStartDate date = variables.GetSingleValue('@vcgORStartDate');
if @ApplicationStartDate is null
  set @ApplicationStartDate = '2016-10-01' 
;

declare @ApplicationEndDate date = variables.GetSingleValue('@vcgOREndDate');
if @ApplicationEndDate is null
  set @ApplicationEndDate = getdate()
;

declare @ScheduleActionID int = variables.GetSingleValue('@vcgORScheduleActionID');
if @ScheduleActionID is null
  set @ScheduleActionID = 3
;

declare @CancelActionID int = variables.GetSingleValue('@vcgORCancelActionID');
if @CancelActionID is null
  set @CancelActionID = 4
;

declare @ProcedureStartEvent varchar(66) = variables.GetSingleValue('@vcgOREventProcedureStart');
if @ProcedureStartEvent is null
  set @ProcedureStartEvent = '80'
;

with ORCaseTimes as (
  select 
    case_time.LOG_ID                    as LogID
    , max(case_time.TRACKING_TIME_IN)   as LastEventTime
  from ge.OR_LOG_CASE_TIMES case_time
  where
    case_time.TRACKING_EVENT_C = @ProcedureStartEvent
  group by
    case_time.LOG_ID
)
insert into gt1.tORAuditCase
(
  CaseID
  , SurgeryDate
	, Line
	, Action
	, ActionID
	, AuditDate
	, AuditDateTime
	, UserName
	, Comments
	, SchedulingAddOnFlag
	, UnscheduledFromDate
	, UnscheduledFromTime
	, UnscheduledFromRoomID
	, ScheduledToTime
	, ScheduledToDate
	, ScheduledToRoomID
  , ProcedureStartedFlag
  , LastProcedureStartTime
  , CancelledActionFlag
  , SameDayActionFlag
  , ScheduledActionFlag
)
select
	cast(audit_trl.OR_CASE_ID as numeric)           as CaseID
  , coalesce(orl.SURGERY_DATE
    , orc.SURGERY_DATE
  )                                               as SurgeryDate
	, audit_trl.LINE                                as Line
	, case  
      when audit_trl.AUDIT_ACTION_C is null
        then @UnspecifiedLabel
      when zact.AUDIT_ACTION_C is null
        then @UnknownLabel
      else
        zact.NAME  
    end                                           as Action
	, audit_trl.AUDIT_ACTION_C                      as ActionID
	, convert(date, audit_trl.AUDIT_DATE)           as AuditDate
	, audit_trl.AUDIT_DATE                          as AuditDateTime
	, case  
      when audit_trl.AUDIT_USER_ID is null
        then @UnspecifiedLabel
      when emp.USER_ID is null
        then @UnknownLabel
      else
        emp.NAME  
    end                                           as UserName
	, audit_trl.AUDIT_COMMENTS                      as Comments
	, audit_trl.AUDIT_ADD_ON_SCH_YN                 as SchedulingAddOnFlag
	, convert(date, audit_trl.AUDIT_UNSCHED_DATE)   as UnscheduledFromDate
	, audit_trl.AUDIT_UNSCHED_TIME                  as UnscheduledFromTime
	, audit_trl.AUDIT_UNSCHED_OR                    as UnscheduledFromRoomID
	, audit_trl.AUDIT_SCHED_TO_TIM                  as ScheduledToTime
	, convert(date, audit_trl.AUDIT_SCHED_TO_DAT)   as ScheduledToDate
	, audit_trl.AUDIT_SCHED_TO_OR                   as ScheduledToRoomID
  , case  
      when case_time.LogID is not null
        then 1
      else
        0
    end                                           as ProcedureStartedFlag
  , case_time.LastEventTime                       as LastProcedureStartTime
  , case  
      when audit_trl.AUDIT_ACTION_C = @CancelActionID
        then 1
      else 
        0
    end                                           as CancelledActionFlag
  , case
      when convert(date, audit_trl.AUDIT_DATE) = coalesce(orl.SURGERY_DATE, orc.SURGERY_DATE)
        then 1
      else
        0
    end                                           as SameDayActionFlag
  , case
      when audit_trl.AUDIT_ACTION_C = @ScheduleActionID
        then 1
      else
        0
    end                                           as ScheduledActionFlag
from ge.OR_CASE_AUDIT_TRL audit_trl
left join ge.CLARITY_EMP emp 
	on emp.USER_ID = audit_trl.AUDIT_USER_ID
left join ge.ZC_OR_AUDIT_ACTION zact
	on zact.AUDIT_ACTION_C = audit_trl.AUDIT_ACTION_C
left join ge.OR_LOG orl
  on orl.CASE_ID = audit_trl.OR_CASE_ID
left join ge.OR_CASE orc
  on orc.OR_CASE_ID = audit_trl.OR_CASE_ID
left join ORCaseTimes case_time
  on case_time.LogID = orl.LOG_ID
--where
--  audit_trl.AUDIT_DATE >= @ApplicationStartDate
--    and audit_trl.AUDIT_DATE < @ApplicationEndDate
;

select count(*) from gt1.tORAuditCase;

with tAuditTrail as (
select 
  audit_trl.CaseID
  , audit_trl.SurgeryDate
	, audit_trl.Line
	, audit_trl.Action
	, audit_trl.ActionID
	, audit_trl.AuditDate
	, audit_trl.AuditDateTime
	, audit_trl.UserName
	, audit_trl.Comments
	, audit_trl.SchedulingAddOnFlag
	, audit_trl.UnscheduledFromDate
	, audit_trl.UnscheduledFromTime
	, audit_trl.UnscheduledFromRoomID
	, audit_trl.ScheduledToTime
	, audit_trl.ScheduledToDate
	, audit_trl.ScheduledToRoomID
  , audit_trl.ProcedureStartedFlag
  , audit_trl.LastProcedureStartTime
  , audit_trl.CancelledActionFlag
  , audit_trl.SameDayActionFlag
  , audit_trl.ScheduledActionFlag
  , first_value (case when audit_trl.ScheduledActionFlag = 1 then audit_trl.Line else 0 end) over (
      partition by
        audit_trl.CaseID
      order by
        case when audit_trl.ScheduledActionFlag = 1 then audit_trl.Line else 0 end desc
    )                                             as tLastScheduledLine
  , case
      when audit_trl.SameDayActionFlag = 1
        and audit_trl.CancelledActionFlag = 1
        and audit_trl.ProcedureStartedFlag = 0
        then 1
      else
        0
    end                                           as tSameDayCancelledFlag
  , case
      when audit_trl.CancelledActionFlag = 1
        and audit_trl.ProcedureStartedFlag = 0
        then 1
      else
        0
    end                                           as CancelledFlag
from gt1.tORAuditCase audit_trl
)
select 
  tAuditTrail.CaseID
  , tAuditTrail.SurgeryDate
	, tAuditTrail.Line
	, tAuditTrail.Action
	, tAuditTrail.ActionID
	, tAuditTrail.AuditDate
	, tAuditTrail.AuditDateTime
	, tAuditTrail.UserName
	, tAuditTrail.Comments
	, tAuditTrail.SchedulingAddOnFlag
	, tAuditTrail.UnscheduledFromDate
	, tAuditTrail.UnscheduledFromTime
	, tAuditTrail.UnscheduledFromRoomID
	, tAuditTrail.ScheduledToTime
	, tAuditTrail.ScheduledToDate
	, tAuditTrail.ScheduledToRoomID
  , tAuditTrail.ProcedureStartedFlag
  , tAuditTrail.LastProcedureStartTime
  , tAuditTrail.CancelledActionFlag
  , tAuditTrail.SameDayActionFlag  
  , tAuditTrail.ScheduledActionFlag
  , case
      when tAuditTrail.Line = tAuditTrail.tLastScheduledLine
        then 1
      else
        0
    end                                           as LastScheduledLineFlag
  , first_value (case when tAuditTrail.Line = tAuditTrail.tLastScheduledLine then AuditDate else null end) over (
      partition by
        tAuditTrail.CaseID
      order by
        case when tAuditTrail.Line = tAuditTrail.tLastScheduledLine then AuditDate else @DefaultLowDate end desc
    )                                             as LastScheduledDate
  , tAuditTrail.CancelledFlag
  , first_value (tAuditTrail.tSameDayCancelledFlag) over (
      partition by 
        tAuditTrail.CaseID 
      order by 
        tAuditTrail.tSameDayCancelledFlag desc
    )                                             as SameDayCancelledFlag
  , first_value (tAuditTrail.CancelledFlag) over (
      partition by 
        tAuditTrail.CaseID 
      order by 
        tAuditTrail.CancelledFlag desc
    )                                             as WasEverCancelledFlag
into gt1.ORAuditCase
from tAuditTrail
;

select *
from gt1.ORAuditCase
where CaseID = '10828'
--where
--  SameDayCancelledFlag = 1
order by 
  CaseID
  , AuditDateTime
  , Line
;

if object_id('gt1.tORAuditCase', 'U') is not null 
  drop table gt1.tORAuditCase
;

GO