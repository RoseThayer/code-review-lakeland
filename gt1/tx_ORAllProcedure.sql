alter procedure gt1.tx_ORAllProcedure as

if object_id('gt1.ORAllProcedure', 'U') is not null drop table gt1.ORAllProcedure;

create table gt1.ORAllProcedure (
	SurgeryID [numeric] (18,0) NULL
	,PanelID [varchar](254) NULL
	,ProcedureID [varchar](254) NULL
	,ProcedureType [varchar] (254) NULL
	,Ordinal [numeric] (18,0) NULL
	,Line [numeric] (18,0) NULL
	,ProcedureName [varchar](254) NULL
	,PrimaryProcedureFlag [numeric] (18,0) NULL
	,PrimaryProcedure2ndPanelFlag [numeric] (18,0) NULL
	,PrimaryProcedure3rdPanelFlag [numeric] (18,0) NULL
	,PrimaryProcedure4thPanelFlag [numeric] (18,0) NULL
	,PrimaryProcedure5thPanelFlag [numeric] (18,0) NULL
	,WoundClassID [numeric] (18,0) NULL
	,WoundClass [varchar](254) NULL
);
with ProcedureBase as (
	select
		prc.LOG_ID as SurgeryID
		,prc.ALL_PROCS_PANEL as PanelID
		,prc.ORDINAL as Ordinal
		,prc.LINE as Line
		,prc.OR_PROC_ID as ProcedureID
		,name.PROC_NAME as ProcedureName
		,prc.WND_CLS_C as WoundClassID
		,wound.NAME as WoundClass 
	from ge.OR_LOG_ALL_PROC prc
	left join ge.OR_PROC name on
		name.OR_PROC_ID=prc.OR_PROC_ID
	left join ge.ZC_OR_WOUND_CLASS wound on 
		wound.WND_CLS_C=prc.WND_CLS_C

	union
	
	select
		prc.OR_CASE_ID as SurgeryID
		,prc.PANEL as PanelID
		,prc.LINE as Ordinal
		,prc.LINE as Line
		,prc.OR_PROC_ID as ProcedureID
		,name.PROC_NAME as ProcedureName
		,NULL as WoundClassID 
		,NULL as WoundClass
	from ge.OR_CASE_ALL_PROC prc
	left join ge.OR_PROC name on
		name.OR_PROC_ID=prc.OR_PROC_ID
	where OR_CASE_ID not in (select LOG_ID from ge.OR_LOG_ALL_PROC) -- keeping only cases that aren't yet logs

	union --keep only records that don't yet exist

	select
		AN_LOG_ID as SurgeryID
		,1 as PanelID
		--,row_number() over (partition by AN_LOG_ID order by AN_PROC_NAME asc) as Ordinal
		--,row_number() over (partition by AN_LOG_ID order by AN_PROC_NAME asc) as Line
		,1 as Ordinal
		,1 as Line
		,NULL as ProcedureID
		,AN_PROC_NAME as ProcedureName
		,NULL as WoundClassID 
		,NULL as WoundClass
	from ge.F_AN_RECORD_SUMMARY
	where AN_LOG_ID not in (select LOG_ID from ge.OR_LOG_ALL_PROC) --keeping only AN records with no case or log, ie thin logs
	and AN_LOG_ID not in (select OR_CASE_ID from ge.OR_CASE_ALL_PROC)
	and AN_LOG_ID is not null
)
insert into gt1.ORAllProcedure
(
	SurgeryID
	,PanelID
	,ProcedureID
	,ProcedureType
	,Ordinal
	,Line
	,ProcedureName
	,PrimaryProcedureFlag
	,PrimaryProcedure2ndPanelFlag
	,PrimaryProcedure3rdPanelFlag
	,PrimaryProcedure4thPanelFlag
	,PrimaryProcedure5thPanelFlag
	,WoundClassID 
	,WoundClass
)
select
	cast(pb.SurgeryID as numeric) as SurgeryID
	,pb.PanelID
	,pb.ProcedureID
	,rob.MEAS_NAME as ProcedureType
	,cast(pb.Ordinal as numeric) as Ordinal
	,cast(pb.Line as numeric) as Ordinal
	,pb.ProcedureName
	,iif(pb.Ordinal=1 and pb.PanelID=1,1,0) as PrimaryProcedureFlag
	,iif(pb.Ordinal=1 and pb.PanelID=2,1,0) as PrimaryProcedure2ndPanelFlag
	,iif(pb.Ordinal=1 and pb.PanelID=3,1,0) as PrimaryProcedure3rdPanelFlag
	,iif(pb.Ordinal=1 and pb.PanelID=4,1,0) as PrimaryProcedure4thPanelFlag
	,iif(pb.Ordinal=1 and pb.PanelID=5,1,0) as PrimaryProcedure5thPanelFlag
	,WoundClassID
	,WoundClass
from ProcedureBase pb
left join ge.LRHS_ROBOTIC_ASSISTED_PROC rob
	on rob.OR_PROC_ID=pb.ProcedureID
;
