alter procedure gt1.tx_SurgeonGroup as

if object_id('gt1.SurgeonGroup', 'U') is not null drop table gt1.SurgeonGroup;

create table gt1.SurgeonGroup (
	GroupID [varchar] (254) NOT NULL
	,GroupName [varchar] (254) NULL
	,SurgeonID [varchar] (254) NULL
	,Surgeon [varchar] (254) NULL 
);
insert into gt1.SurgeonGroup
(
	GroupID 
	,GroupName 
	,SurgeonID 
	,Surgeon 
)
select
	gs.GROUP_ID as GroupID
	,gp.GROUP_NAME as GroupName
	,gs.SURGEONS_ID as SurgeonID
	,ser.PROV_NAME as Surgeon
from ge.OR_GRP_SURGEON gs
left join ge.OR_GRP gp on 
	gp.GROUP_ID=gs.GROUP_ID
left join ge.CLARITY_SER ser on
	ser.PROV_ID=gs.SURGEONS_ID
;