alter procedure gt1.tx_HospitalEncounter as

if object_id('gt1.HospitalEncounter', 'U') is not null drop table gt1.HospitalEncounter;

create table gt1.HospitalEncounter
(
   EncounterID			numeric(18,0)    NOT NULL
  , AdmitDate					date       			 NULL
  , AdmitDateTime 		 	    datetime        	 NULL
  , DischargeDate 			    date       			 NULL
  , DischargeDateTime 			datetime       		 NULL
  , LOS							int 		         NULL
  , PatientClassID				varchar(254)        NULL
 , PatientClass 				varchar(254)         NULL
  , HospitalAccountID			numeric(18,0)     NULL
   , PatientID 					varchar(254)    NOT NULL
)
;

insert into gt1.HospitalEncounter
(						
   EncounterID				
  , AdmitDate					
  , AdmitDateTime 		    
  , DischargeDate 			    
  , DischargeDateTime 	
  , LOS												
  , PatientClassID 			
  , PatientClass 				
  , HospitalAccountID	
  , PatientID	
)

select
   cast(peh.PAT_ENC_CSN_ID as numeric) as EncounterID
  , cast(peh.HOSP_ADMSN_TIME as date) as AdmitDate
  , peh.HOSP_ADMSN_TIME as AdmitDateTime
  , cast(peh.HOSP_DISCH_TIME as date) as DischargeDate
  , peh.HOSP_DISCH_TIME as DischargeDateTime
  , datediff(day,peh.HOSP_ADMSN_TIME,peh.HOSP_DISCH_TIME)+1 as LOS -- Count start and end date, account for with +1
	, peh.ADT_PAT_CLASS_C as PatientClassID
  , zpc.NAME as PatientClass
  , cast(peh.HSP_ACCOUNT_ID as numeric) as HospitalAccountID
  , peh.PAT_ID as PatientID
from ge.PAT_ENC_HSP peh
left join ge.ZC_PAT_CLASS zpc
    on zpc.ADT_PAT_CLASS_C = peh.ADT_PAT_CLASS_C

;