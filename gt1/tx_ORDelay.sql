alter procedure gt1.tx_ORDelay as

if object_id('gt1.ORDelay', 'U') is not null 
  drop table gt1.ORDelay;

create table gt1.ORDelay (
  LogID                           numeric(18,0)
  , DelayID                       numeric(18,0)
  , DelayTypeID                   numeric(18,0)
  , DelayType                     varchar(254)
  , DelayMinutes                  numeric(18,0)
  , DelayReasonID                 numeric(18,0)
  , DelayReason                   varchar(254)
  , PACUDelayFlag                 tinyint
  , DelayComments                 varchar(254)
)
;

declare @UnspecifiedID            int = -1;
declare @UnknownID                int = -2;
declare @NotApplicableID          int = -3;
declare @UnspecifiedLabel         varchar(254) = '*Unspecified';
declare @UnknownLabel             varchar(254) = '*Unknown';
declare @NotApplicableLabel       varchar(254) = '*NotApplicable';

-- todo: confirm with Rose/Laura that this is correct, 160 = Anesthesia-Extended Time to PACU/ICU, not listed in MW, didn't find PACU or WAIT in any other titles (other than waiting for family)
--       Note there are currently no logs with a delay reason of 160 so this probably isn't the right value
-- select * from ge.ZC_OR_DELAY_REASON where title like '%PACU%'
declare @ORDelayReasonWaitForPACU int = variables.GetSingleValue('@vcgORDelayReasonWaitForPACU');
if @ORDelayReasonWaitForPACU is null
  set @ORDelayReasonWaitForPACU = 160 
;

insert into gt1.ORDelay
(
  LogID
  , DelayID
  , DelayTypeID
  , DelayType
  , DelayMinutes
  , DelayReasonID
  , DelayReason
  , PACUDelayFlag
  , DelayComments
)
select
  delay.LOG_ID                              as LogID
  , delay.DELAY_ID                          as DelayID
  , lnlg.DELAY_TYPE_C                       as DelayTypeID
  , case
      when lnlg.DELAY_TYPE_C  is null
        then @UnspecifiedLabel
      when zctype.DELAY_TYPE_C is null
        then @UnknownLabel
      else
        zctype.NAME
    end                                     as DelayType
  , coalesce(lnlg.DELAY_LENGTH, 0)          as DelayMinutes
  , rsn.DELAY_REASON_C                      as DelayReasonID
  , case
      when rsn.DELAY_REASON_C  is null
        then @UnspecifiedLabel
      when zcreason.DELAY_REASON_C is null
        then @UnknownLabel
      else
        zcreason.NAME
    end                                     as DelayReason
  , case  
      when rsn.DELAY_REASON_C = @ORDelayReasonWaitForPACU
        then 1
      else
        0
    end                                     as PACUDelayFlag
  , cmt.DELAY_COMMENTS                      as DelayComments
from ge.OR_LOG_LN_DELAY delay
left join ge.OR_LNLG_DELAY lnlg 
  on lnlg.RECORD_ID = delay.DELAY_ID
left join ge.OR_LNLG_DELAY_RSNS rsn 
  on rsn.RECORD_ID = delay.DELAY_ID
left join ge.OR_LNLG_DELAY_CMTS cmt 
  on cmt.RECORD_ID = delay.DELAY_ID
left join ge.ZC_OR_DELAY_TYPE zctype 
  on zctype.DELAY_TYPE_C = lnlg.DELAY_TYPE_C
left join ge.ZC_OR_DELAY_REASON zcreason 
  on zcreason.DELAY_REASON_C = rsn.DELAY_REASON_C
;

GO

/*
exec gt1.tx_ORDelay;

select *
from gt1.ORDelay;

*/