alter procedure gt1.tx_BlockUtilization as

if object_id('gt1.BlockUtilization', 'U') is not null drop table gt1.BlockUtilization;

declare @ORUTLSnapshotBlock numeric (18,0) = variables.GetSingleValue('@vcgORUTLSnapshotBlock'); 
declare @ORUTLSnapshotTemplate numeric (18,0) = variables.GetSingleValue('@vcgORUTLSnapshotTemplate'); 

create table gt1.BlockUtilization (
	LogID [numeric] (18,0) NULL 
  ,SnapshotDate [date] NOT NULL
	,RoomID [varchar] (254) NOT NULL
	,LocationID [varchar](254) NULL
	,SurgeonID [varchar](254) NULL
	,ServiceID [varchar](254) NULL
  ,SurgeonPrimaryService [varchar](254) NULL
  ,GroupPrimaryService [varchar](254) NULL
	,Service [varchar](254)
	,GroupID [varchar](254) NULL
	,[Group]  [varchar](254) NULL
	,HourID [numeric] (18,0) NULL
	,TurnoverTimeFlag int NULL
	,CorrectTime [numeric] (18,0) NULL
	,OverbookTime [numeric] (18,0) NULL
	,OutsideTime [numeric] (18,0) NULL
	,AvailableTime [numeric] (18,0) NULL
	,StartTime  [time] NULL
	,EndTime  [time] NULL
	,BlockName [varchar](254)
	,BlockType [varchar](254)
	,BlockID [varchar](254) NULL
	,BlockTypeID [varchar](254) NULL
);
with
ProviderService as (
  select
    PROV_ID
	  ,LOC_ID
	  ,first_value(SERVICE_C) over (partition by PROV_ID,LOC_ID order by LINE asc) as SurgeonPrimaryServiceID
  from ge.OR_SER_SURG_SRVC
),
GroupProvider as (
  select distinct 
    gp.GROUP_ID
    ,first_value(gp.SURGEONS_ID) over (partition by gp.GROUP_ID order by LINE asc) as GroupPrimarySurgeonID
  from ge.OR_GRP_SURGEON gp
)
insert into gt1.BlockUtilization
(
	LogID
  ,SnapshotDate
	,RoomID
	,LocationID
	,SurgeonID
	,ServiceID
  ,SurgeonPrimaryService
  ,GroupPrimaryService
	,Service
	,GroupID
	,[Group]
	,HourID
	,TurnoverTimeFlag
	,CorrectTime
	,OverbookTime
	,OutsideTime
	,AvailableTime
	,StartTime
	,EndTime
	,BlockName
	,BlockType
	,BlockID
	,BlockTypeID
)
select distinct
	cast(u.LOG_ID as numeric) as LogID
	,cast(u.SNAPSHOT_DATE as date) as SnapshotDate
	,u.ROOM_ID as RoomID
	,f.LOC_ID as LocationID
	,u.SURGEON_ID as SurgeonID
	,u.SERVICE_C as ServiceID
	,zsp.NAME as SurgeonPrimaryService
	--,grpp.GroupPrimarySurgeonID as GroupPrimaryProviderID
	,zsg.NAME as GroupPrimaryService
	,iif(len(u.SERVICE_C)>0, zs.NAME , iif(len(u.SURGEON_ID)>0, zsp.NAME , iif(len(u.GROUP_ID)>0, zsg.NAME, NULL))) as Service
	,u.GROUP_ID as GroupID
	,zsg.NAME as [Group]
	,datepart(hour,u.START_TIME) as BlockHourID
	,iif(u.PROC_TIME<>'Y',1,0) as TurnoverTimeFlag
	,iif(u.SLOT_TYPE='CORRECT' and u.SNAPSHOT_NUMBER=@ORUTLSnapshotBlock, u.SLOT_LENGTH,NULL) as CorrectTime -- variable
	,iif(u.SLOT_TYPE='OVERBOOK' and u.SNAPSHOT_NUMBER=@ORUTLSnapshotBlock, u.SLOT_LENGTH,NULL) as OverbookTime  -- variable
	,iif(u.SLOT_TYPE='OUTSIDE' and u.SNAPSHOT_NUMBER=@ORUTLSnapshotBlock, u.SLOT_LENGTH,NULL) as OutsideTime -- variable
	,iif(u.SLOT_TYPE='BLOCK' and u.SNAPSHOT_NUMBER=@ORUTLSnapshotTemplate, u.SLOT_LENGTH,NULL) as AvailableTime -- variable
	,cast(u.START_TIME-2 as time) as StartTime
	,cast(u.END_TIME-2 as time) as EndTime
	,iif(len(u.SERVICE_C)>0, zs.NAME , iif(len(u.SURGEON_ID)>0, zp.PROV_NAME , iif(len(u.GROUP_ID)>0, grp.GROUP_NAME, NULL))) as BlockName
	,iif(len(u.SERVICE_C)>0, 'Service' , iif(len(u.SURGEON_ID)>0, 'Surgeon' , iif(len(u.GROUP_ID)>0, 'Group', NULL))) as BlockType
	,iif(len(u.SERVICE_C)>0, u.SERVICE_C , iif(len(u.SURGEON_ID)>0, u.SURGEON_ID , iif(len(u.GROUP_ID)>0, u.GROUP_ID, NULL)))  as BlockID
	,iif(len(u.SERVICE_C)>0, 1 , iif(len(u.SURGEON_ID)>0, 2 , iif(len(u.GROUP_ID)>0, 3, NULL))) as BlockTypeID
from ge.OR_UTIL_BLOCK u
left join ge.ZC_OR_SERVICE zs on
  zs.SERVICE_C=u.SERVICE_C
left join ge.CLARITY_SER zp on
  zp.PROV_ID=u.SURGEON_ID
left join ge.OR_GRP grp on
  grp.GROUP_ID=u.GROUP_ID
left join ge.OR_SER_SURG_SRVC f on
  f.PROV_ID=u.ROOM_ID
left join GroupProvider grpp on
  grpp.GROUP_ID=u.GROUP_ID
left join ProviderService ps on
  ps.PROV_ID=u.SURGEON_ID and
  ps.LOC_ID=f.LOC_ID
left join ge.ZC_OR_SERVICE zsp on
  zsp.SERVICE_C=ps.SurgeonPrimaryServiceID
left join ProviderService psg on
  psg.PROV_ID=grpp.GroupPrimarySurgeonID and
  psg.LOC_ID=f.LOC_ID
left join ge.ZC_OR_SERVICE zsg on
  zsg.SERVICE_C=psg.SurgeonPrimaryServiceID
where
u.SNAPSHOT_NUMBER = @ORUTLSnapshotBlock
or u.SNAPSHOT_NUMBER = @ORUTLSnapshotTemplate
;
