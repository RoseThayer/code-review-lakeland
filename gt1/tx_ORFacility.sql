alter procedure gt1.tx_ORFacility as

if object_id('gt1.ORFacility', 'U') is not null drop table gt1.ORFacility;


--todo get variables to work
declare @StJoeIDs table ( ORLocationID bigint );
	insert into @StJoeIDs select * from variables.GetTableValues('@vcgStJoeORLocationID');

declare @NilesIDs table ( ORLocationID bigint );
	insert into @NilesIDs select * from variables.GetTableValues('@vcgNilesORLocationID');

declare @CopsIDs table ( ORLocationID bigint );
	insert into @CopsIDs select * from variables.GetTableValues('@vcgCopsORLocationID');

declare @WatervlietIDs table ( ORLocationID bigint );
	insert into @WatervlietIDs select * from variables.GetTableValues('@vcgWatervlietORLocationID');

create table [gt1].[ORFacility]
(
    ORLocationID   varchar(254)     NOT NULL
	, Facility varchar (254) NULL
  , ORLocation varchar(254)      NOT NULL
  , DepartmentID varchar(254)          NULL
  , RoomID       varchar(254)         NULL
  , Room     varchar(254)          NULL
)

insert into [gt1].[ORFacility]
(
    ORLocationID
	,Facility 
  , ORLocation
  , DepartmentID
  , RoomID
  , Room
)

select
    ol.LOC_ID as ORLocationID
	,iif( stjoe.ORLocationID > 0, 'St Joe', iif( niles.ORLocationID > 0, 'Niles', iif( cops.ORLocationID > 0, 'Cops', iif( wv.ORLocationID > 0, 'WatervlietID','Unknown')))) as Facility
	--  ,'St Joe' as Facility	--todo, update variables to work.  Right now they are too large
  , loc.LOC_NAME as ORLocation
  , ol.OR_DEPARTMENT_ID as DepartmentID
  , os.PROV_ID as RoomID
  , cs.PROV_NAME as Room
  from
    ge.OR_LOC ol
      left join ge.CLARITY_LOC loc
        on ol.LOC_ID = loc.LOC_ID
      left join ge.OR_SER_SURG_SRVC os
        on ol.LOC_ID = os.LOC_ID
      left join ge.CLARITY_SER cs
        on os.PROV_ID = cs.PROV_ID
	left join @StJoeIDs stjoe
		on stjoe.ORLocationID = ol.LOC_ID
	left join @NilesIDs niles
		on niles.ORLocationID = ol.LOC_ID
	left join @CopsIDs cops
		on cops.ORLocationID = ol.LOC_ID
	left join @WatervlietIDs wv
		on wv.ORLocationID = ol.LOC_ID 
;
