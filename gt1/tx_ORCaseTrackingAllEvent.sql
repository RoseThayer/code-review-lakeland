alter procedure gt1.tx_ORCaseTrackingAllEvent as

if object_id('gt1.ORCaseTrackingAllEvent', 'U') is not null drop table gt1.ORCaseTrackingAllEvent;

create table gt1.ORCaseTrackingAllEvent (
	LogID [numeric] (18,0) NULL
	,CaseTrackingEventID [numeric] (18,0) NULL
	,CaseTrackingEventName [varchar](254) NULL
	,CaseTrackingEventTime [datetime] NULL
)
insert into gt1.ORCaseTrackingAllEvent
(
	LogID
	,CaseTrackingEventID
	,CaseTrackingEventName
	,CaseTrackingEventTime
)
select
	times.LOG_ID as LogID
	,times.TRACKING_EVENT_C as CaseTrackingEventID
	,zc.NAME as CaseTrackingEventName
	,min(times.TRACKING_TIME_IN) as CaseTrackingEventTime
from ge.OR_LOG_CASE_TIMES times
left join ge.ZC_OR_PAT_EVENTS zc on
	zc.TRACKING_EVENT_C=times.TRACKING_EVENT_C
group by times.LOG_ID, times.TRACKING_EVENT_C, zc.NAME
;
