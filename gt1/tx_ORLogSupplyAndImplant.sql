alter procedure gt1.tx_ORLogSupplyAndImplant as

if object_id('gt1.ORLogSupplyAndImplant') is not null
  drop table gt1.ORLogSupplyAndImplant
;

create table gt1.ORLogSupplyAndImplant (
	LogID                       varchar(18)
	, LogStatusID               int
	, LogStatus                 varchar(254)
	, CaseID                    varchar(18)
	, Source                    varchar(16)
	, SupplyID                  varchar(18)
	, Supply                    varchar(254)
	, SupplyFlag                int
	, ImplantID                 int
	, Implant                   varchar(254)
	, ImplantFlag               int
	, ImplantTypeID             int
	, ImplantType               varchar(254)
	, ChargeableFlag            int
	, ManufacturerID            varchar(66)
	, Manufacturer              varchar(254)
	, QtyUsed                   int
	, QtyWasted                 int
	, ImplantUnitCost           numeric(12, 2)
	, SupplyUnitCost            numeric(12, 2)
	, SupplyOTUnitCost          numeric(18, 2)
	, PickListSupplyUnitCost    numeric(12, 2)
	, BalanceOTSupplyUnitCost   numeric(18, 2)
	, DerivedUnitCost           numeric(18, 2)
  , DerivedUnitCostForAvg     numeric(18, 2)
)
;

declare @UnspecifiedID            int = -1;
declare @UnknownID                int = -2;
declare @NotApplicableID          int = -3;
declare @UnspecifiedLabel         varchar(254) = '*Unspecified';
declare @UnknownLabel             varchar(254) = '*Unknown';
declare @NotApplicableLabel       varchar(254) = '*NotApplicable';

with SupplyCount as (
  select 
    pklst.LOG_ID
    , sply.SUPPLY_NAME                        as SupplyName
    , sply.IMPLANT_TYPE_C                     as ImplantTypeID
    , case
        when sply.IMPLANT_TYPE_C is null
          then @UnspecifiedLabel
        when zorimptyp.IMPLANT_TYPE_C is null
          then @UnknownLabel
        else
          zorimptyp.NAME
      end                                       as ImplantType
    , pklst.PICK_LIST_NAME                      as PickListName
		, pklst_sup.SUPPLY_ID
		, pklst_sup.SUPPLY_INV_LOC_ID
    , sply.COST_PER_UNIT                        as SupplyUnitCost
		, pklst_sup.SUP_UNIT_COST                   as PickListSupplyUnitCost
    , ormfg.MANUFACTURER_C                      as ManufacturerID
    , case
        when ormfg.MANUFACTURER_C is null
          then @UnspecifiedLabel
        when zormfg.MANUFACTURER_C is null 
          then @UnknownLabel
        else
          zormfg.NAME
      end                                       as Manufacturer
		, iif(pklst_sup.CHARGEABLE_YN = 'Y', 1, 0)  as ChargeableFlag
		, sum(pklst_sup.SUPPLIES_USED)              as SuppliesUsed
		, sum(pklst_sup.SUPPLIES_WASTED)            as SuppliesWasted
	from ge.OR_PKLST pklst 
  inner join ge.OR_PKLST_SUP_LIST pklst_sup 
    on pklst.PICK_LIST_ID = pklst_sup.PICK_LIST_ID
  inner join ge.OR_SPLY sply 
    on pklst_sup.SUPPLY_ID = sply.SUPPLY_ID
  left join ge.OR_SPLY_MANFACTR ormfg
    on ormfg.ITEM_ID = pklst_sup.SUPPLY_ID
      and ormfg.LINE = 1 -- table is multi-response but all items to date only have one line. Adding this in for protection.
	left join ge.ZC_OR_MANUFACTURER zormfg
    on zormfg.MANUFACTURER_C = ormfg.MANUFACTURER_C
  left join ge.ZC_OR_IMPLANT_TYPE zorimptyp
    on zorimptyp.IMPLANT_TYPE_C = sply.IMPLANT_TYPE_C
  where pklst.LOG_ID is not null
		and (pklst.PICK_LIST_TYPE_C is null or pklst.PICK_LIST_TYPE_C <> 99)  --exclude depletion pick lists
		and (pklst.DELETED_YN is null or pklst.DELETED_YN = 'N')  --exclude deleted pick lists
		and (pklst_sup.IMPLANT_YN is null or pklst_sup.IMPLANT_YN = 'N')  --filter supply types considered implants from supply query
		and (pklst_sup.SUPPLIES_USED > 0 or pklst_sup.SUPPLIES_WASTED > 0)  --filter rows with no used or wasted supplies
	group by
    pklst.LOG_ID
    , sply.SUPPLY_NAME 
    , sply.IMPLANT_TYPE_C
    , pklst.PICK_LIST_NAME 
		, pklst_sup.SUPPLY_ID
		, pklst_sup.SUPPLY_INV_LOC_ID
    , sply.COST_PER_UNIT
		, pklst_sup.SUP_UNIT_COST
    , ormfg.MANUFACTURER_C
    , zormfg.MANUFACTURER_C
    , zormfg.NAME
    , sply.IMPLANT_TYPE_C
    , zorimptyp.IMPLANT_TYPE_C
    , zorimptyp.NAME
		, pklst_sup.CHARGEABLE_YN
)
, BalanceOverTime as (
  select 
    bal.BAL_ID
    , bal.SUPPLY_ID
    , bal.LOC_ID
    , bal_ovtm.TX_DATE
    , lead(bal_ovtm.TX_DATE) over (
      partition by 
        bal_ovtm.BAL_ID 
      order by 
        bal_ovtm.CONTACT_DATE_REAL
      ) as NEXT_TX_DATE  -- Use lead over to get the smallest next TX_DATE, review this logic if ever adding a where clause in this subquery, since lead over acts after the where clause		
    , bal_ovtm.COST_PER_UNIT_OT                 as BalanceOTSupplyUnitCost
    , row_number() over (
      partition by 
        bal_ovtm.BAL_ID 
      order by 
        bal_ovtm.CONTACT_DATE_REAL
      ) rowNum  -- Rows to order all contacts to get the first adminstration contact
  from ge.OR_BAL bal
  inner join ge.OR_BAL_OVTM bal_ovtm
    on bal.BAL_ID = bal_ovtm.BAL_ID 
      and bal_ovtm.CONTACT_TYPE_C = 1 -- admin contacts  
  where
    bal.RECORD_STATUS_C is null
      or bal.RECORD_STATUS_C < 4 -- exclude inactive BAL records
)
, SupplyOverTime as (
  select 
    sply_ovtm.ITEM_ID                           as SupplyID
		, sply_ovtm.COST_PER_UNIT_OT                as UnitCost
		, sply_ovtm.CONTACT_NUM                     as ContactNum  
		, sply_ovtm.EFFECTIVE_DATE                  as EffectiveDate
		, lead(EFFECTIVE_DATE) over (
      partition by 
        sply_ovtm.ITEM_ID 
      order by 
        sply_ovtm.EFFECTIVE_DATE_REAL
      )                                         as NextEffectiveDate
  from ge.OR_SPLY_OVTM sply_ovtm
)
insert into gt1.ORLogSupplyAndImplant (
	LogID                       
	, LogStatusID               
	, LogStatus                 
	, CaseID                    
	, Source                    
	, SupplyID                  
	, Supply                    
	, SupplyFlag                
	, ImplantID                 
	, Implant                   
	, ImplantFlag               
	, ImplantTypeID             
	, ImplantType               
	, ChargeableFlag            
	, ManufacturerID            
	, Manufacturer              
	, QtyUsed                      
	, QtyWasted                    
	, ImplantUnitCost           
	, SupplyUnitCost            
	, SupplyOTUnitCost          
	, PickListSupplyUnitCost    
	, BalanceOTSupplyUnitCost   
	, DerivedUnitCost         
  , DerivedUnitCostForAvg  
)
select --top 1000
  orl.LOG_ID                                    as LogID
  , orl.STATUS_C                                as LogStatusID
  , case
      when orl.STATUS_C is null
        then @UnspecifiedLabel
      when zorstat.STATUS_C is null
        then @UnknownLabel
      else
        zorstat.NAME
    end                                         as LogStatus
  , orl.CASE_ID                                 as CaseID
  , 'SUPPLY'                                    as Source                          
  , SupplyCount.SUPPLY_ID                       as SupplyID
  --, sply.SUPPLY_NAME
  --, SupplyCount.PickListName
  , coalesce(SupplyCount.SupplyName
    , SupplyCount.PickListName)                 as Supply -- todo: confirm with Rose that it makes sense to use the pick list name in the absence of a supply name (pick list has 1:many supplies)
  , case
      when SupplyCount.ImplantTypeID is null
        then 1
      else
        0
    end                                         as SupplyFlag  
  , null                                        as ImplantID
  , null                                        as Implant
  , case
      when SupplyCount.ImplantTypeID is not null
        then 1
      else
        0
    end                                         as ImplantFlag  
  , SupplyCount.ImplantTypeID                   as ImplantTypeID
  , SupplyCount.ImplantType                     as ImplantType
  , SupplyCount.ChargeableFlag                  as ChargeableFlag
  , SupplyCount.ManufacturerID                  as ManufacturerID
  , SupplyCount.Manufacturer                    as Manufacturer
  --, SupplyCount.CHARGEABLE_YN
  , SupplyCount.SuppliesUsed                    as QtyUsed
  , SupplyCount.SuppliesWasted                  as QtyWasted
  , null                                        as ImplantUnitCost
  , SupplyCount.SupplyUnitCost                  as SupplyUnitCost -- todo: confirm that cost hierarchy should use OR_SPLY.COST_PER_UNIT (always null). Should it be OR_SPLY_OVTM.COST_PER_UNIT_OT? Training companion says not to use costs from OR_SPLY
  , SupplyOverTime.UnitCost                     as SupplyOTUnitCost
  , SupplyCount.PickListSupplyUnitCost          as PickListSupplyUnitCost
  , BalanceOverTime.BalanceOTSupplyUnitCost     as BalanceOTSupplyUnitCost
  , coalesce(SupplyCount.SupplyUnitCost
    , SupplyCount.PickListSupplyUnitCost
    , BalanceOverTime.BalanceOTSupplyUnitCost
    , 0
    )                                           as DerivedUnitCost
  , coalesce(BalanceOverTime.BalanceOTSupplyUnitCost
    , SupplyOverTime.UnitCost
    )                                           as DerivedUnitCostForAvg
from ge.OR_LOG orl
inner join SupplyCount 
  on orl.LOG_ID = SupplyCount.LOG_ID
left join BalanceOverTime
  on BalanceOverTime.SUPPLY_ID = SupplyCount.SUPPLY_ID
    and BalanceOverTime.LOC_ID = SupplyCount.SUPPLY_INV_LOC_ID 
inner join SupplyOverTime
  on SupplyOverTime.SupplyID = SupplyCount.SUPPLY_ID
left join ge.ZC_OR_STATUS zorstat
  on zorstat.STATUS_C = orl.STATUS_C
where 
  (
    BalanceOverTime.BAL_ID is null
      or (
        orl.SURGERY_DATE >= BalanceOverTime.TX_DATE 
        and (
          BalanceOverTime.NEXT_TX_DATE is null 
          or orl.SURGERY_DATE < BalanceOverTime.NEXT_TX_DATE
        )
      ) -- The surgery date is after or on the balance contact date and the next balance contact date is after the surgery date or null
			or (
        orl.SURGERY_DATE < BalanceOverTime.TX_DATE 
          and BalanceOverTime.rowNum = 1
      ) -- If the first administration contact date is after the surgery date, always use the first administration contact 
  ) 
  and (
    (
      orl.SURGERY_DATE >= SupplyOverTime.EffectiveDate 
      and (
        SupplyOverTime.NextEffectiveDate is null 
        or orl.SURGERY_DATE < SupplyOverTime.NextEffectiveDate
      )
    )  -- The surgery date is after or on the supply contact date and the next supply contact date is after the surgery date or null
		or (
      orl.SURGERY_DATE < SupplyOverTime.EffectiveDate 
      and SupplyOverTime.ContactNum = 1
    )
  ) -- If the first contact date is after the surgery date, always use the first contact regardless
		
insert into gt1.ORLogSupplyAndImplant (
	LogID                       
	, LogStatusID               
	, LogStatus                 
	, CaseID                    
	, Source                    
	, SupplyID                  
	, Supply                    
	, SupplyFlag                
	, ImplantID                 
	, Implant                   
	, ImplantFlag               
	, ImplantTypeID             
	, ImplantType               
	, ChargeableFlag            
	, ManufacturerID            
	, Manufacturer              
	, QtyUsed                      
	, QtyWasted                    
	, ImplantUnitCost           
	, SupplyUnitCost            
	, SupplyOTUnitCost          
	, PickListSupplyUnitCost    
	, BalanceOTSupplyUnitCost   
	, DerivedUnitCost 
  , DerivedUnitCostForAvg          
)
select
  orl.LOG_ID                                as LogID
  , orl.STATUS_C                            as LogStatusID
  , case
      when orl.STATUS_C is null
        then @UnspecifiedLabel
      when zorstat.STATUS_C is null
        then @UnknownLabel
      else
        zorstat.NAME
    end                                     as LogStatus
  , orl.CASE_ID                             as CaseID
  , 'IMPLANT'                               as Source
  , lnlg.IMP_INV_TYPE_ID                    as SupplyID
  , sup.SUPPLY_NAME                         as Supply
  , 0                                       as SupplyFlag
  , lnlg.IMPLANT_ID                         as ImplantID
  , imp.IMPLANT_NAME                        as Implant
  , 1                                       as ImplantFlag
  , sup.IMPLANT_TYPE_C                      as ImplantTypeID
  , case
      when sup.IMPLANT_TYPE_C is null
        then @UnspecifiedLabel
      when zorimptyp.IMPLANT_TYPE_C is null
        then @UnknownLabel
      else
        zorimptyp.NAME
    end                                     as ImplantType
  , iif(imp.CHARGEABLE_YN = 'Y', 1, 0)      as ChargeableFlag
  , ormfg.MANUFACTURER_C                    as ManufacturerID
  , case
      when ormfg.MANUFACTURER_C is null
        then @UnspecifiedLabel
      when zormfg.MANUFACTURER_C is null 
        then @UnknownLabel
      else
        zormfg.NAME
    end                                     as Manufacturer  
  , case
      when lnlg.IMPLANT_ACTION_C = '1' -- todo: variable
        then coalesce(lnlg.IMPLANT_NUM_USED, 0)
      else
        0
    end                                     as QtyUsed -- todo: Lakeland doesn't use lnlg.IMPLANT_USAGE_C = 1 --> Implanted (Used)?
  , case
      when lnlg.IMPLANT_ACTION_C = '3' -- todo: variable
        then coalesce(lnlg.IMPLANT_NUM_USED, 0)
      else
        0
    end                                     as QtyWasted -- todo: Lakeland doesn't use lnlg.IMPLANT_USAGE_C = 2 --> Wasted?
  , imp.COST_PER_UNIT                       as ImplantUnitCost
  , 0                                       as SupplyUnitCost
  , 0                                       as SupplyOTUnitCost
  , 0                                       as PickListSupplyUnitCost
  , 0                                       as BalanceOTSupplyUnitCost
  , imp.COST_PER_UNIT                       as DerivedUnitCost
  , imp.COST_PER_UNIT                       as DerivedUnitCostForAvg
from ge.OR_LOG orl
inner join ge.OR_LOG_LN_IMPLANT orln 
  on orl.LOG_ID = orln.LOG_ID
inner join ge.OR_LNLG_IMPLANTS lnlg 
  on orln.IMPLANTS_ID = lnlg.RECORD_ID 
inner join ge.OR_IMP imp 
  on lnlg.IMPLANT_ID = imp.IMPLANT_ID
left join ge.OR_SPLY sup 
  on lnlg.IMP_INV_TYPE_ID = sup.SUPPLY_ID
left join ge.OR_SPLY_MANFACTR ormfg
  on ormfg.ITEM_ID = lnlg.IMP_INV_TYPE_ID
    and ormfg.LINE = 1 -- table is multi-response but all items to date only have one line. Adding this in for protection.
left join ge.ZC_OR_MANUFACTURER zormfg
  on zormfg.MANUFACTURER_C = ormfg.MANUFACTURER_C
left join ge.ZC_OR_IMPLANT_TYPE zorimptyp
  on zorimptyp.IMPLANT_TYPE_C = sup.IMPLANT_TYPE_C
left join ge.ZC_OR_STATUS zorstat
  on zorstat.STATUS_C = orl.STATUS_C
where
  lnlg.IMPLANT_ACTION_C in ('1', '3') -- todo: variables for implanted (1) or wasted (3)
  and lnlg.IMPLANT_NUM_USED > 0 -- filter rows in which no quantity is defined
  and orln.IMPLANTS_ID is not null  -- from ORY_LOG_COSTS
  --and lnlg.IMPLANT_USAGE_C is not null -- only implants used or wasted

-- 1914465 supplies

-- ORY_LOG_COSTS
-- v_log_supplies_implants

go

exec gt1.tx_ORLogSupplyAndImplant;

select top 10 * from gt1.ORLogSupplyAndImplant
-- join this to my table and see if costs are different/missing when compared to DerivedUnitCostForAvg
select top 10 * from rdsvclarity.clarity.dbo.ory_log_costs