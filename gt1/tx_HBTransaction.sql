alter procedure gt1.tx_HBTransaction as

if object_id('gt1.HBTransaction', 'U') is not null drop table gt1.HBTransaction;

create table gt1.HBTransaction (
  TransactionID [numeric] (18,0) NULL
 ,HospitalAccountID [numeric] (18,0) NULL
 ,SupplyID [varchar](254) NULL
 ,LogID [numeric] (18,0) NULL
 ,ImplantID [varchar](254) NULL
 ,AccountClassID [varchar](254)  NULL
 ,AccountClass [varchar](254) NULL
 ,BillingProviderID [varchar](254)  NULL
 ,BillingProvider [varchar](254) NULL
 ,BucketID [varchar](254) NULL
 ,CPTCode [varchar](254) NULL
 ,CostCenterID [varchar](254) NULL
 ,CostCenter [varchar](254) NULL
 ,CostCenterCode [varchar](254) NULL
 ,DepartmentID [varchar](254) NULL
 ,Department [varchar](254) NULL
 ,IsLateChargeFlag [varchar] (254) NULL
 ,EncounterCSN [numeric] (18,0) NULL
 ,ProcedureID [varchar](254)  NULL
  ,ProcedureCode [varchar] (254)  NULL
, ProcedureDescription [varchar] (254)  NULL
 ,Amount [numeric] (18,0) NULL
 ,Quantity [numeric] (18,0) NULL
 ,RevenueCodeID [varchar] (254)  NULL
 ,RevenueCode [varchar] (254) NULL
 ,ServiceDate [date] NULL
 ,ChargeAmount [numeric] (18,0) NULL
 ,PaymentAmount [numeric] (18,0) NULL
 ,ChargePerUnit  [numeric] (18,0) NULL
 ,PostDate [date] NULL
 ,PostLag [numeric] (18,0) NULL
)
insert into gt1.HBTransaction
(
  TransactionID
 ,HospitalAccountID
 ,SupplyID
 ,LogID
 ,ImplantID
 ,AccountClassID
 ,AccountClass
 ,BillingProviderID
 ,BillingProvider
 ,BucketID
 ,CPTCode
 ,CostCenterID
 ,CostCenter
 ,CostCenterCode
 ,DepartmentID
 ,Department
 ,IsLateChargeFlag
 ,EncounterCSN
 ,ProcedureID
 ,ProcedureCode
, ProcedureDescription
 ,Amount
 ,Quantity
 ,RevenueCodeID
 ,RevenueCode 
 ,ServiceDate
 ,ChargeAmount
 ,PaymentAmount
 ,ChargePerUnit
 ,PostDate
 ,PostLag
)
select
 cast(tx.TX_ID as numeric) as TransactionID
, cast(tx.HSP_ACCOUNT_ID as numeric) as HospitalAccountID
, tx.SUP_ID as SupplyID
, cast(tx.OPTIME_LOG_ID as numeric) as LogID
, tx.IMPLANT_ID as ImplantID
, tx.ACCT_CLASS_HA_C as AccountClassID
, zac.NAME as AccountClass
, tx.BILLING_PROV_ID as BillingProviderID
, ser.PROV_NAME as BillingProvider
, tx.BUCKET_ID as BucketID
, tx.CPT_CODE as CPTCode
, tx.COST_CNTR_ID as CostCenterID
, cc.COST_CENTER_NAME as CostCenter
, cc.COST_CENTER_CODE as CostCenterCode
, tx.DEPARTMENT as DepartmentID
, dep.DEPARTMENT_NAME as Department
, iif(tx.IS_LATE_CHARGE_YN='Y', 1, 0) as IsLateChargeFlag
, cast(tx.PAT_ENC_CSN_ID as numeric) as EncounterCSN
, tx.PROC_ID as ProcedureID
, eap.PROC_CODE as ProcedureCode
, eap.PROC_NAME as ProcedureDescription
, cast(tx.TX_AMOUNT as numeric) as Amount
, cast(tx.QUANTITY as numeric) as Quantity
, tx.UB_REV_CODE_ID as RevenueCodeID
, rev.REVENUE_CODE as RevenueCode
, cast(tx.SERVICE_DATE as date) as ServiceDate
, cast(iif(tx.TX_TYPE_HA_C='1',tx.TX_AMOUNT,NULL) as numeric) as ChargeAmount
, cast(iif(tx.TX_TYPE_HA_C='2',tx.TX_AMOUNT,NULL) as numeric) as PaymentAmount
, iif(tx.TX_TYPE_HA_C='1', cast(iif(tx.TX_TYPE_HA_C='1',tx.TX_AMOUNT,NULL) as numeric) /tx.QUANTITY, NULL) as ChargePerUnit
, cast(tx.TX_POST_DATE as date) as PostDate
, cast(datediff(day,tx.SERVICE_DATE,tx.TX_POST_DATE) as numeric) as PostLag
from ge.HSP_TRANSACTIONS tx
left join ge.ZC_ACCT_CLASS_HA zac on
  zac.ACCT_CLASS_HA_C = tx.ACCT_CLASS_HA_C
left join ge.CLARITY_SER ser on
  ser.PROV_ID = tx.BILLING_PROV_ID
left join ge.CL_COST_CNTR cc on
  cc.COST_CNTR_ID = tx.COST_CNTR_ID
left join ge.CLARITY_DEP dep on
  dep.DEPARTMENT_ID=tx.DEPARTMENT
left join ge.CL_UB_REV_CODE rev on 
	rev.UB_REV_CODE_ID=tx.UB_REV_CODE_ID
left join ge.CLARITY_EAP eap on 
	eap.PROC_ID=tx.PROC_ID
where tx.SERVICE_DATE>=dateadd(year,-1,getdate()) --date restriction added for performance
and tx.OPTIME_LOG_ID is not null --restriction on surgical logs for performance
;

/*
use [padev]
go
create nonclustered index idx__HspTransactions__ServiceDateLogID
ON [ge].[HSP_TRANSACTIONS] ([SERVICE_DATE],[OPTIME_LOG_ID])
include ([TX_ID],[HSP_ACCOUNT_ID],[ACCT_CLASS_HA_C],[BILLING_PROV_ID],[BUCKET_ID],[COST_CNTR_ID],[CPT_CODE],[DEPARTMENT],[IS_LATE_CHARGE_YN],[PAT_ENC_CSN_ID],[PROC_ID],[QUANTITY],[UB_REV_CODE_ID],[TX_AMOUNT],[TX_POST_DATE],[TX_TYPE_HA_C],[SUP_ID],[IMPLANT_ID])
go
*/