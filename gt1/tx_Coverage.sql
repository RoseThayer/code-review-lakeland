alter procedure gt1.tx_Coverage as

if object_id('gt1.Coverage', 'U') is not null drop table gt1.Coverage;

  create table [gt1].[Coverage]
  (
	HospitalAccountID 		[numeric] (18,0) NOT NULL
	,LineNumber				[numeric] (18,0) NULL
	,CoverageID  			varchar(254) NULL
	,PayorName				varchar(254) NULL
	,PlanName				varchar(254) NULL
  )

insert into [gt1].[Coverage]
(
	HospitalAccountID
	,LineNumber
	,CoverageID
	,PayorName
	,PlanName
) 
select
	cast(list.HSP_ACCOUNT_ID as numeric) as HospitalAccountID
	,list.LINE as LineNumber
	,list.COVERAGE_ID as CoverageID
	,cvg.PAYOR_NAME as PayorName
	,cvg.BENEFIT_PLAN_NAME as PlanName
from ge.HSP_ACCT_CVG_LIST list
left join ge.V_COVERAGE_PAYOR_PLAN cvg on
	cvg.COVERAGE_ID=list.COVERAGE_ID
where list.LINE<=4 --only pulling the first 4 lines of coverage