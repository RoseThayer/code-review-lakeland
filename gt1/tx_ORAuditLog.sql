alter procedure gt1.tx_ORAuditLog as

if object_id('gt1.ORAuditLog', 'U') is not null drop table gt1.ORAuditLog;

create table gt1.ORAuditLog (
	LogID [numeric] NOT NULL
  ,Line [numeric] (18,0) NULL
	,Action [varchar](254) NULL
	,ActionID [numeric] (18,0) NULL
	,Date [date] NULL
	,DateTime [datetime] NULL
	,[User] [varchar](254) NULL
	,UserID [varchar] (254) NULL
	,Comments [varchar](254) NULL
)
insert into gt1.ORAuditLog
(
	LogID
  ,Line
	,Action
	,ActionID
	,Date
	,DateTime
	,[User]
	,UserID
	,Comments
)
select
	cast(audit.LOG_ID as numeric) as LogID
  ,audit.LINE as Line
	,zact.NAME as Action
	,audit.AUDIT_ACTION_C as ActionID
	,convert(date,audit.AUDIT_DATE) as Date
	,audit.AUDIT_DATE as DateTime
	,emp.NAME as [User]
	,audit.AUDIT_USER_ID as UserID
	,audit.AUDIT_COMMENTS as Comments
from ge.OR_LOG_AUDIT_TRAIL audit
left join ge.CLARITY_EMP emp on
	emp.USER_ID = audit.AUDIT_USER_ID
left join ge.ZC_OR_AUDIT_ACTION zact on
	zact.AUDIT_ACTION_C=audit.AUDIT_ACTION_C
;
