if object_id('gt2.ORTurnover', 'U') is not null drop table gt2.ORTurnover;

declare @ORTurnoverTimeMaxLimit [numeric] (18,0) = variables.GetSingleValue('@vcgORTurnoverTimeMaxLimit');

create table gt2.ORTurnover (
	PreviousLogID [numeric] (18,0) NULL
	,PreviousLogEndTime [datetime] NULL
	,LogID [numeric] (18,0) NOT NULL
	,SurgeonID [varchar](254) NULL
	,Surgeon [varchar](254) NULL
	,SurgeonType [varchar](254) NULL
	,LogStartTime [datetime] NULL
	,TurnoverTimeMinutes [numeric] (18,0) NULL
	,TurnoverType [varchar](254) NULL
);
with Surgery as
(
	select
		log.LOG_ID as LogID
		,row_number() over (partition by surg.StaffID order by surg.StaffStartTime) as Iteration --this is surgeon across rooms
		,log.SURGERY_DATE as SurgeryDate
		--,CaseTrackingProcedureStartTime --could use this or patient in room time or surgeon start time for turnover
		--,CaseTrackingProcedureFinishTime
		--,CaseTrackingPatientInRoomTime
		--,CaseTrackingPatientOutRoomTime
		--,PrimarySurgeonID
		,surg.StaffID as SurgeonID
		,surg.StaffStartTime as StartTime
		,surg.StaffEndTime as EndTime
		,log.ROOM_ID as RoomID
	from ge.OR_LOG log
	left join gt1.ORANAllStaff surg 
		on surg.LogID=log.LOG_ID
	where surg.StaffType like 'Surgeon%'
	--any filters go here
)
insert into gt2.ORTurnover
(
	PreviousLogID
	,PreviousLogEndTime
	,LogID
	,SurgeonID
	,Surgeon
	,SurgeonType
	,LogStartTime
	,TurnoverTimeMinutes
	,TurnoverType
)
select distinct
	t1.LogID as PreviousLogID
	,t1.EndTime as PreviousLogEndTime
	,t2.LogID as LogID
	,t1.SurgeonID as SurgeonID
	,s.StaffName as Surgeon
	,s.StaffType as SurgeonType
	,t2.StartTime as LogStartTime
	,datediff(minute, t1.EndTime, t2.StartTime) as TurnoverTimeMinutes --this is surgeon turnover across rooms
	,'Across Room, Same Surgeon' as TurnoverType
from Surgery t1
inner join Surgery t2
	on t1.Iteration = t2.Iteration-1
	and t1.SurgeonID=t2.SurgeonID --turnover time for surgeons
left join gt1.ORANAllStaff s on 
	s.StaffID=t2.SurgeonID
	and s.LogID=t2.LogID
where t1.SurgeryDate = t2.SurgeryDate --only keep same day turnover
	and datediff(minute, t1.EndTime, t2.StartTime)<90--configurable turnover limit
	and t1.LogID<>t2.LogID
;

--potentially add other turnover times here with a union
